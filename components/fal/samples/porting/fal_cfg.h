/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-05-17     armink       the first version
 */

#ifndef _FAL_CFG_H_
#define _FAL_CFG_H_

#include <rtconfig.h>
#include <board.h>
#include "global.h"
//#define NOR_FLASH_DEV_NAME             "norfla0"

/* ===================== Flash device Configuration ========================= */
extern const struct fal_flash_dev at32_onchip_flash;
extern struct fal_flash_dev nor_flash0;

/* flash device table */
#define FAL_FLASH_DEV_TABLE                                          \
{                                                                    \
    &at32_onchip_flash,                                           \
    &nor_flash0,                                                     \
}
/* ====================== Partition Configuration ========================== */
#ifdef FAL_PART_HAS_TABLE_CFG
/* partition table */
#define FAL_PART_TABLE                                                               \
{                                                                                    \
		{FAL_PART_MAGIC_WORD, "boot",    "onchip_flash",         					0,   32*1024, 0}, \
		{FAL_PART_MAGIC_WORD, "app",     "onchip_flash",   								32*1024,  504*1024, 0}, \
		{FAL_PART_MAGIC_WORD, "up_flag",FAL_USING_NOR_FLASH_DEV_NAME, 		UPGRADE_FLAG_ADDR, 				UPGRADE_FLAG_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "download",FAL_USING_NOR_FLASH_DEV_NAME, 		DOWN_APP_ADDR, 							DOWN_APP_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "up_bk",FAL_USING_NOR_FLASH_DEV_NAME, 			BK_OLD_APP_ADDR, 		BK_OLD_APP_SIZE, 0}, \
		{FAL_PART_MAGIC_WROD, "kv1",     FAL_USING_NOR_FLASH_DEV_NAME,    FLASH_DB_KV1_ADDR, 						FLASH_DB_KV1_SIZE, 0}, \
		{FAL_PART_MAGIC_WROD, "ts1",     FAL_USING_NOR_FLASH_DEV_NAME,    FLASH_DB_TS1_ADDR,  FLASH_DB_TS1_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, FS_PARTITION_NAME, FAL_USING_NOR_FLASH_DEV_NAME, FS_ADDR, 	FS_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, FAL_EF_PART_NAME,FAL_USING_NOR_FLASH_DEV_NAME, 		EF_ADDR,EF_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "addrt",FAL_USING_NOR_FLASH_DEV_NAME, 		ADDR_TYPE_CFG_ADDR,ADDR_TYPE_CFG_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "sic",FAL_USING_NOR_FLASH_DEV_NAME, 		SCENE_INFO_CFG_ADDR,SCENE_INFO_CFG_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "sip",FAL_USING_NOR_FLASH_DEV_NAME, 		SCENE_INFO_POOL_ADDR,SCENE_INFO_POOL_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "dic",FAL_USING_NOR_FLASH_DEV_NAME, 		DEV_INFO_CFG_ADDR,DEV_INFO_CFG_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "dip",FAL_USING_NOR_FLASH_DEV_NAME, 		DEV_INFO_POOL_ADDR,DEV_INFO_POOL_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "stic",FAL_USING_NOR_FLASH_DEV_NAME, 		SCENE_TIMER_INFO_CFG_ADDR,SCENE_TIMER_INFO_CFG_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "stip",FAL_USING_NOR_FLASH_DEV_NAME, 		SCENE_TIMER_INFO_POOL_ADDR,SCENE_TIMER_INFO_POOL_SIZE, 0}, \
		{FAL_PART_MAGIC_WORD, "imp_p",FAL_USING_NOR_FLASH_DEV_NAME, 		IMPORTANT_PARA_ADDR,IMPORTANT_PARA_SIZE, 0}, \
}
#endif /* FAL_PART_HAS_TABLE_CFG */

#endif /* _FAL_CFG_H_ */
