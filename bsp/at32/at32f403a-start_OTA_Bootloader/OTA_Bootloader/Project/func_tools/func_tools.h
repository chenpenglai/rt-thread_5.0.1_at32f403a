#ifndef __func_tools_H
#define __func_tools_H
#include "stdint.h"

#include "stdbool.h"
#include <time.h>

typedef enum{
	EVENT_CLEAR=0,
	EVENT_NOT_CLEAR,
	
}EVENT_CTRL;

uint16_t u8_2_u16(uint8_t* dat);		//拼接2个byte为word
uint32_t u8_2_u32(uint8_t* dat);		//拼接4个byte为word
uint8_t half_2_u8(uint8_t* dat);		//拼接2个半字为byte
uint16_t get_crc(uint8_t* pack, uint16_t length);			//返回CRC校验
int str_search(const char* src,char* dst,int num);							//在 src 查找 dst 第n次 出现的 首位置
int mem_search(uint8_t* array,int array_len,char dst,int num);		//在 array 查找 dst 第n 次出现的 首位置
//int hex_to_str(uint8_t* sdata,uint8_t hdata);
//int hexarr_to_strarr(uint8_t * sdata,uint8_t * hdata,uint16_t hlength);
//void str_to_hex(uint8_t *str,uint8_t *hex);
//int strarr_to_hexarr(uint8_t *sdata, uint8_t *hdata, uint16_t str_len);
#define BCD_to_DEC(bcd) ((bcd&0xf0)>>4)*10+(bcd&0x0f)		//0x25 => 25
#define DEC_to_BCD(dec) ((dec/10)<<4)|((dec%10)&0x0f)		// 25 => 0x25
void hexdump(const char* note,uint8_t * buf,int len) ;
void hexdump_16(const char* note,uint16_t *buf, int len);
void chardump(const char* note,uint8_t *buf, int len);
void set_event(uint32_t * flag,uint32_t bit);
bool get_event(uint32_t * flag,uint32_t bit,uint32_t* e,EVENT_CTRL clear);			//是否清除事件位
void clr_event(uint32_t * flag,uint32_t bit);
void ef_set_env_int(char* key,int val);
int ef_get_env_int(char* key);
char *ef_get_env(const char *key);//lkk add 2020-06-10

uint32_t ef_get_env_u32(char* key);
void ef_set_env_u32(char* key,uint32_t val);
uint16_t hex_check_sum(uint8_t *data, uint16_t len);
uint16_t str_check_sum(uint8_t *data, uint16_t len)  ;
uint16_t cal_check_sum(uint8_t *data, int len);
unsigned short CRC16_1(char* cp, unsigned short leng);


typedef enum{
    CRC8_MAXIM,
    CRC16_MODBUS,
    CRC32,
    CRC_TYPE_MAX,
}CRC_TYPE;
//通用的CRC 耗时高 对时间要求高的时候不要用这个
uint32_t get_crc_8(const uint8_t* data, uint16_t len);
uint16_t get_random(uint16_t min,uint16_t max);			//返回一个区间的随机数
void hex_to_ascii(uint8_t *in, uint16_t in_len, uint8_t *out);
uint32_t float_to_hex(float in);
float hex_to_float(uint32_t in);
char *strrpl(char *s, const char *s1, const char *s2);		//字符串替换
void show_process(uint8_t process) ;




#endif /* __TOOL_H */
