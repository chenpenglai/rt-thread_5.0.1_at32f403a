#define FUNC_DIRTRAN_MAIN
#include "func_master_dirtran_slv.h"
#include "func_tools.h"
#include "func_en_id.h"
#define SET_BIT(x, y)   (x |= (1<<y))      // 特定位置1 
#define CLEAR_BIT(x, y) (x &= ~(1<<y))   // 特定位清0 



/************** 产品保护功能 从机设备 **************/	
#define PROTECT_READ_MCU_ID_HR_START_DATA_ADDR						264
#define PROTECT_READ_MCU_ID_HR_NUM 												6

#define PROTECT_WRITE_EN_ID_HR_START_DATA_ADDR						270
#define PROTECT_WRITE_EN_ID_HR_NUM 												4


void func_master_dirtran_slv_init(FUNC_DIRTRAN_T *p,unsigned char slv,USART_T *pu1)
{
	FUNC_DIRTRAN_PORT *pc;
	
//	p->ch0.pu=pu0;
	p->ch1.pu=pu1;
//	p->ch0.buf_size=FUNC_DIRTRAN_BUF_SIZE;
	p->ch1.buf_size=FUNC_DIRTRAN_BUF_SIZE;
	
//	pc=&p->ch0;
//#ifdef MODBUS_COIL_OFS_EN
//	pc->md.coil_ofs=MODBUS_SLV_COIL_OFS;
//#endif

//#ifdef MODBUS_INPUT_OFS_EN
//	pc->md.input_ofs=MODBUS_SLV_INPUT_OFS;
//#endif
//	pc->md.slv=slv;
//	pc->md.phr=HR;
//	pc->md.hr_n=countof(HR);
//-----------------------------------------
	pc=&p->ch1;
#ifdef MODBUS_COIL_OFS_EN
	pc->md.coil_ofs=MODBUS_SLV_COIL_OFS;
#endif

#ifdef MODBUS_INPUT_OFS_EN
	pc->md.input_ofs=MODBUS_SLV_INPUT_OFS;
#endif
	pc->md.slv=slv;
	pc->md.phr=HR1;
	pc->md.hr_n=countof(HR1);
	

	uint32_t mcu_id[3];
	read_mcu_id(mcu_id);
	
	int pos=PROTECT_READ_MCU_ID_HR_START_DATA_ADDR;
	
//	HR1[pos++]=(uint16_t)(mcu_id[0])>>8|(uint16_t)(mcu_id[0])<<8;
//	HR1[pos++]=(uint16_t)(mcu_id[0]>>16)>>8|(uint16_t)(mcu_id[0]>>16)<<8;
//	HR1[pos++]=(uint16_t)(mcu_id[1])>>8|(uint16_t)(mcu_id[1])<<8;
//	HR1[pos++]=(uint16_t)(mcu_id[1]>>16)>>8|(uint16_t)(mcu_id[1]>>16)<<8;
//	HR1[pos++]=(uint16_t)(mcu_id[2])>>8|(uint16_t)(mcu_id[2])<<8;
//	HR1[pos++]=(uint16_t)(mcu_id[2]>>16)>>8|(uint16_t)(mcu_id[2]>>16)<<8;
	
	
	memcpy(&HR1[PROTECT_READ_MCU_ID_HR_START_DATA_ADDR],mcu_id,PROTECT_READ_MCU_ID_HR_NUM*sizeof(uint16_t));
	
	
	
	
}





__weak void func_dirtran_after_rec(FUNC_DIRTRAN_T *p)
{
	return;
}

__weak void func_dirtran_before_send(FUNC_DIRTRAN_T *p)
{
	return;
}




enum{
	FUNC_DIRTRAN_CH0_STEP_START,
	FUNC_DIRTRAN_CH0_STEP_MODBUS_SEND,
	FUNC_DIRTRAN_CH0_STEP_WAIT_RESPONSE,
};

enum{
	FUNC_DIRTRAN_CH1_STEP_START,
	FUNC_DIRTRAN_CH1_STEP_MODBUS_SEND,
	FUNC_DIRTRAN_CH1_STEP_WAIT_RESPONSE,
};

__weak void func_dirtran_success(MODBUS_T *p)
{
	(void)p;
}
	unsigned short p_hr[50];
unsigned short *p_hr_bak;
//uint8_t g_slv_addr;
void func_master_dirtran_slv_exec(FUNC_DIRTRAN_T *p)
{
	FUNC_DIRTRAN_PORT *pc,*pc1;
	USART_T*pu;
	unsigned int i;
	unsigned char k;

//========================================================================================================
//	pc0=&p->ch0;			//和上位机（网关）通讯
	pc1=&p->ch1;			//和从机通讯
//--------------------------------------
//回环测试
//	if(usart_chk_frame(pc0->pu))
//	{
//		usart_copy_usart(pc0->pu,pc0->pu,1);
//	}
//	return;
	//通道1操作
	pc=&p->ch1;
	pu=pc->pu;
	switch(pc->step)
	{
		case FUNC_DIRTRAN_CH1_STEP_START:
			if(usart_chk_recbyte(pu))
			{
				i= MyRnd() % 10;		//取0-9随机数
				i += FUNC_DIRTRAN_CHKBUS_DELAY;
				left_ms_set(&pc->tm,i);		//如果期间收到其他接收，缓10mS再操作
			}
			if(usart_chk_frame(pu))
			{
				if(pu->rx_buf[0] == pc->md.slv)
				{
					if(modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e)>0)			//是本地从机地址，MODBUS解析
					{
						
						left_ms_set(&pc->tm,FUNC_DIRTRAN_SEND_DELAY);			//解析成功				
						printf("modbus_slv_rec, hr_addr %d, func: 0x%02X\n",pc->md.da_adr ,pc->md.func);	
						//g_slv_addr=HR1[pc->md.da_adr];
						
						if(pc->md.func==MD_FR_MHR){
							//printf("写指令\n");
							
							if(0x0088==HR1[pc->md.da_adr]){
								
								uint32_t en_id=(uint32_t)HR1[pc->md.da_adr+1]<<16|(uint32_t)HR1[pc->md.da_adr+2];
								uint32_t e_code=HR1[pc->md.da_adr+3];
								printf("写 en_id 0x%X e_code 0x%X\n",en_id,e_code);
								write_en_id_e_code(en_id,e_code);
			
							}
							
							
						}
						
						//hexdump("-- HR --",(uint8_t*)&HR1[pc->md.da_adr],pc->md.da_n*2);
											
						

						pc->step=FUNC_DIRTRAN_CH1_STEP_MODBUS_SEND;				//下一步
					}
				}
				usart_rx_rst(pc->pu);
			}
			else if(pc->buf_n && left_ms(&pc->tm)==0)
			{
						
				usart_send(pc->pu,pc->buf,pc->buf_n);			//透传给子设备
				//printf("send a packet %d\n",pc->buf_n);
				pc->buf_n=0;

			}
			break;
		case FUNC_DIRTRAN_CH1_STEP_MODBUS_SEND:
			if(left_ms(&pc->tm)==0)
			{
				i=modbus_slv_send(&pc->md,pu->tx_buf);
				if(i)
				{
					usart_send_start(pu,i);
				}
				pc->step=FUNC_DIRTRAN_CH1_STEP_START;
			}
			break;
	}
//================================================================
}



