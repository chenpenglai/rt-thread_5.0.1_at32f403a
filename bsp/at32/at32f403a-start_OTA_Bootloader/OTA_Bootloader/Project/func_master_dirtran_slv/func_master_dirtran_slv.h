#ifndef __func_master_dirtran_slv_H__
#define __func_master_dirtran_slv_H__
#include "includes.h"

//+++++User_Code_define Begin+++++//

//-----User_Code_define End-----//
//=============================================================================
//使用主模块需要对以下内容进行定义	
#ifndef __FUNC_DIRTRAN_DEFINE
	
	#define FUNC_DIRTRAN_CHKBUS_DELAY 200	//判断总线空闲延时10mS-20mS
	#define FUNC_DIRTRAN_SEND_DELAY 10	//发送前延时10mS
	#define FUNC_DIRTRAN_SEND_PERIOD 100	//发送周期100ms,0表示连续发送
	#define FUNC_DIRTRAN_SEND_TOUT 100	//发送超时100mS
	#define FUNC_DIRTRAN_SEND_RTY 1		//重发1次
	#define FUNC_DIRTRAN_CYC_NB 4			//循环命令队列长度
	#define FUNC_DIRTRAN_INJ_NB 2			//注入命令队列长度
#endif
//=============================================================================
	#ifdef FUNC_DIRTRAN_MAIN
		#define FUNC_DIRTRAN_EXT
	#else
		#define FUNC_DIRTRAN_EXT extern
	#endif
	
	#define FUNC_DIRTRAN_BUF_SIZE 1024
		
	typedef struct{
		unsigned char step;
		time_ms_T tm;					//定时器
		USART_T *pu;
		unsigned char buf[FUNC_DIRTRAN_BUF_SIZE];
		unsigned short buf_n;
		unsigned short buf_size;
		MODBUS_T md;
	}FUNC_DIRTRAN_PORT;
	
	typedef struct{
		FUNC_DIRTRAN_PORT ch1;
	}FUNC_DIRTRAN_T;
	
	FUNC_DIRTRAN_EXT FUNC_DIRTRAN_T g_func_dirtran;
		
	
	
	void func_master_dirtran_slv_init(FUNC_DIRTRAN_T *p,unsigned char slv,USART_T *pu1);
	void func_master_dirtran_slv_exec(FUNC_DIRTRAN_T *p);
	


#endif
