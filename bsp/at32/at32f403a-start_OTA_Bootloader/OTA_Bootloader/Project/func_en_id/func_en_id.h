#ifndef __func_en_id_H
#define __func_en_id_H
#include "stdint.h"
#include "stdbool.h"




/**************************			外部调用			********************************/
void en_id_init();
void write_en_id_e_code(uint32_t enid,uint16_t ecode);		//保存解码器发来的加密ID和企业代码 	en_id 和 e_code
//void write_en_id(uint32_t en_id);		//保存解码器发来的加密ID 	en_id
//void write_e_code(uint16_t e_code);	//保存解码器发来的企业代码 e_code
void read_mcu_id(uint32_t *p);			//读取MCU_ID，12位
uint32_t en(uint32_t *dat,uint16_t key);	//传入芯片id和企业代码key， return 加密结果
uint32_t cmp_en_id(void);		//比较计算的加密ID和读出的加密ID，返回：（1：不正确，0：正确）
/****************************************************************************/

void en_id(void);			//加密ID，并且写入
void clr_en_id_e_code(void);				//清除加密ID

#endif /* __TOOL_H */
