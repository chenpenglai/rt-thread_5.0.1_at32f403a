#include "func_en_id.h"
#include "func_tools.h"
#include "func_shell.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stdlib.h"
#include "flash.h"
//const static uint8_t ss[4] __attribute__((at(CODE_1_ADDR)))={0xff,0xff,0xff,0xff}; 

#define FLASH_EN_ID_START_ADDR 	0x8006000						//地址
#define FLASH_E_CODE_START_ADDR 	(FLASH_EN_ID_START_ADDR+4)						//地址
uint16_t g_e_code=0;
uint32_t g_en_id=0;


//volatile u32 ID_addr[3]={0x1ffff7e8 ,0x1ffff7ec ,0x1ffff7f0 };

//返回加密结果
uint32_t en(uint32_t *dat,uint16_t key){
	uint32_t result;
	result= dat[0] + dat[1] - (dat[2]/key);
	return result;
}


//从 flash读出en_id
uint32_t read_en_id(){
	return *(vu32*)FLASH_EN_ID_START_ADDR; 

}

//从 flash读出en_id
uint16_t read_e_code(){
	return *(vu16*)FLASH_E_CODE_START_ADDR; 

}


//将 e_code写入 flash
void write_en_id_e_code(uint32_t enid,uint16_t ecode){

		FLASH_Unlock();
		FLASH_ErasePage(FLASH_EN_ID_START_ADDR);     //擦除页
		FLASH_ProgramWord(FLASH_EN_ID_START_ADDR,enid); // 保存这个数，写进32位
		FLASH_ProgramHalfWord(FLASH_E_CODE_START_ADDR,ecode); // 保存这个数，写进16位
		FLASH_Lock();
		g_e_code=ecode;
		g_en_id=enid;
}


/********************************************************************
函数功能：读出芯片 ID
入口参数：p
返    回：
备    注：
********************************************************************/
void read_mcu_id(uint32_t *p)
{
	#define DEVICE_UID_ADDR1 0x1FFFF7E8		
	#define DEVICE_UID_ADDR2 0x1FFFF7EC
	#define DEVICE_UID_ADDR3 0x1FFFF7F0

	uint32_t ID[3];

	ID[0] = *(int*)DEVICE_UID_ADDR1;
	ID[1] = *(int*)DEVICE_UID_ADDR2;
	ID[2] = *(int*)DEVICE_UID_ADDR3;
	memcpy(p,ID,3*4);
	
//   volatile u32 Addr;
//// 因为不想让程序在反汇编后直接找到这个地址，所以这个地址是运算出来的，
//// 跟ID_addr反运算，当然了也可以用高级的算法，注意不能让编译器优化这个地址
//   Addr = ID_addr[0] ;
//   p[0] = *(vu32*)(Addr);
//   Addr = ID_addr[1] ;
//   p[1] = *(vu32*)(Addr);
//   Addr = ID_addr[2] ;
//   p[2] = *(vu32*)(Addr);
}
/********************************************************************
函数功能：加密ID并保存
入口参数：
返    回：
备    注：
********************************************************************/

void en_id(void)
{
   u32 stm32ID[3],dat;   
   read_mcu_id(stm32ID);
   // 这里可以用其它一些高级的算法，但解和加要一样
   //stm32ID[3] = E_CODE;        
  dat = en(stm32ID,g_e_code);// dat = stm32ID[0] + stm32ID[1] - (stm32ID[2]/stm32ID[3]);
   //Flash_Write(FLASH_EN_ID_START_ADDR,&dat,1);
	write_en_id_e_code(dat,g_e_code);
		//write_en_id(dat);

}

/********************************************************************
函数功能：比较加密ID，正确返回0
入口参数：
返    回：1：不正确，0：正确
备    注：
********************************************************************/

uint32_t cmp_en_id(void)
{
		u32 stm32ID[3],dat;   
		read_mcu_id(stm32ID);
		// 这里可以用其它一些高级的算法，但解和加要一样   
		//stm32ID[3] = E_CODE;      
		dat = en(stm32ID,g_e_code);//stm32ID[0] + stm32ID[1] - (stm32ID[2]/stm32ID[3]);

		//enid = read_en_id();
		
		if(dat == g_en_id)
		{
			printf("check ok 0x%X\n",g_en_id);
			return 0;		// 相同
		} 
		else          
		{
			return 1;		// 不同
		} 
}

/********************************************************************
函数功能：清除en_id和e_code
入口参数：
返    回：
备    注：
********************************************************************/
void clr_en_id_e_code(void)
{
   u32 dat;   
   dat = 0xffffffff;	
	 write_en_id_e_code(dat,dat);
}



void en_id_init(){
	g_e_code=read_e_code();
	g_en_id=read_en_id();
	printf(">>> en_id: 0x%X, e_code: 0x%X\n",g_en_id,g_e_code);
	
} 

//串口 shell 调试

int enc_id(uint8_t argc, char **argv) {
		en_id();
 
		return 0;
}
CMD_EXPORT(enc_id, skip);
int cmp_id(uint8_t argc, char **argv) {
		cmp_en_id();
 
		return 0;
}
CMD_EXPORT(cmp_id, skip);
int clr_id(uint8_t argc, char **argv) {
		clr_en_id_e_code();
		return 0;
}
CMD_EXPORT(clr_id, skip);


//int test1(uint8_t argc, char **argv) {
//		e_code=atoi(argv[1]);
//		return 0;
//}
//CMD_EXPORT(test1, skip);

//int test2(uint8_t argc, char **argv) {
//		printf("e_code %d\n",e_code);
//		return 0;
//}
//CMD_EXPORT(test2, skip);





