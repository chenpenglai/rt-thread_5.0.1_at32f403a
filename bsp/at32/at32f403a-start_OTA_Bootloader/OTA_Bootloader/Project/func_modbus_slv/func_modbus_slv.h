#ifndef __FUNC_MODBUS_SLV_H__
#define __FUNC_MODBUS_SLV_H__
#include "includes.h"
#include "global.h"

	
//+++++User_Code_define Begin+++++//
//-----User_Code_define End-----//

//==============================================================================
//使用这个模块需要对以下参数进行定义
#ifndef __FUNC_MODBUS_SLV_DEFINE
	#ifdef USE_MASTER_GATEWAY
		#define MODBUS_SLV_HR_NB (253)			//MODBUS HR个数
	#else
		#define MODBUS_SLV_HR_NB (100)
	#endif
	#define MODBUS_SLV_DFT 10				//默认地址
	#define MODBUS_SLV_SEND_DELAY 10		//发送前延时10mS
	#define MODBUS_SLV_COIL_OFS 0			//线圈偏移量
	#define MODBUS_SLV_INPUT_OFS 0			//输入偏移量
#endif
//==============================================================================	
	
	
	#ifdef FUNC_MODBUS_SLV_MAIN
		#define FUNC_MODBUS_SLV_EXT
	#else
		#define FUNC_MODBUS_SLV_EXT extern
	#endif
	
	typedef struct{
		unsigned char step;
		unsigned char send_delay;
		USART_T *pu;
		time_ms_T tm;
		MODBUS_T md;
	}FUNC_MODBUS_SLV_T;
	
	void func_modbus_slv_init(FUNC_MODBUS_SLV_T *p,USART_T *pu);
	void func_modbus_slv_exec(FUNC_MODBUS_SLV_T *p);

//+++++User_Code_define1 Begin+++++//
//	#ifndef USE_MASTER_GATEWAY
		FUNC_MODBUS_SLV_EXT unsigned short HR1[273];		//V2.5修改
//	#endif
//-----User_Code_define1 End-----//
	FUNC_MODBUS_SLV_EXT FUNC_MODBUS_SLV_T g_func_modbus_slv;
	
	FUNC_MODBUS_SLV_EXT unsigned short HR[MODBUS_SLV_HR_NB];
//+++++User_Code_define2 Begin+++++//
//-----User_Code_define2 End-----//

#endif
