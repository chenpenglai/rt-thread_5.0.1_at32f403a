#define FUNC_MODBUS_SLV_MAIN
#include "func_modbus_slv.h"

enum {
	FUNC_MODBUS_SLV_STEP_CHECK_FRAME,
	FUNC_MODBUS_SLV_STEP_SEND
};

void func_modbus_set_slv(FUNC_MODBUS_SLV_T *p,unsigned char slv)
{
	p->md.slv=slv;
}


void func_modbus_slv_init(FUNC_MODBUS_SLV_T *p,USART_T *pu)
{
	p->pu=pu;
	p->send_delay=MODBUS_SLV_SEND_DELAY;
#ifdef MODBUS_COIL_OFS_EN
	p->md.coil_ofs=MODBUS_SLV_COIL_OFS;
#endif

#ifdef MODBUS_INPUT_OFS_EN
	p->md.input_ofs=MODBUS_SLV_INPUT_OFS;
#endif
	p->md.slv=MODBUS_SLV_DFT;
	p->md.phr=HR;
	p->md.hr_n=countof(HR);
}


//从机发送前函数
 void func_modbus_slv_before_send(MODBUS_T *p)
{
	
}

//从机接收到一包合法的数据后函数
 void func_modbus_slv_success(MODBUS_T *p)
{
	//printf("succeed\n");
}


void func_modbus_slv_exec(FUNC_MODBUS_SLV_T *p)
{
	USART_T *pu;
	int i;
	
	pu=p->pu;
	switch(p->step)
	{
		case FUNC_MODBUS_SLV_STEP_CHECK_FRAME:
			if(usart_chk_frame(pu))
			{
				if(modbus_slv_rec(&p->md,pu->rx_buf,pu->r_e)>0)
				{
					left_ms_set(&p->tm,p->send_delay);
					//func_modbus_slv_after_rec(p);
					func_modbus_slv_success(&p->md);
					p->step=FUNC_MODBUS_SLV_STEP_SEND;	
				}
//				else{
//					printf("err %d\n",modbus_slv_rec(&p->md,pu->rx_buf,pu->r_e));
//					
//				}
				usart_rx_rst(pu);
			}
			break;
		case FUNC_MODBUS_SLV_STEP_SEND:
			if(left_ms(&p->tm)==0)
			{
				func_modbus_slv_before_send(&p->md);
				i=modbus_slv_send(&p->md,pu->tx_buf);
				if(i)
				{
					usart_send_start(pu,i);
				}
				p->step=FUNC_MODBUS_SLV_STEP_CHECK_FRAME;
			}
			break;
	}
}
