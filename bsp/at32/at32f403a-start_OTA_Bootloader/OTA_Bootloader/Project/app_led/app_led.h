#ifndef __LED_DEV_H_
#define __LED_DEV_H_

#include "stdbool.h"
#include "stdint.h"
#include "global.h"
#include "app_led_msp.h"


#define LED_ON  0
#define LED_OFF 1


//0=绿 1=蓝 2=红
#define LED_G 0
#define LED_B 1 
#define LED_R 2
	


#define LED_LOOP_NUM_FOREVER -1

#define LED0_CTRL(x) 			GPIO_WriteBit(LED0_PORT,LED0_PIN, x)			//灯亮
#define LED1_CTRL(x) 			GPIO_WriteBit(LED1_PORT,LED1_PIN, x)			//灯亮
#define LED2_CTRL(x) 			GPIO_WriteBit(LED2_PORT,LED2_PIN, x)			//灯亮

typedef enum
{
	LED_END=0,			//停止闪烁
	LED_START,		//开始闪烁
	LED_ALWAYS_ON,	//常亮
}LED_MSG_EVENT;

//speed：设置LED闪烁速度，LED_FAST：快速闪烁，LED_NORMAL：正常速度，LED_SLOW：缓慢闪烁
typedef enum
{
	LED_FAST=0,			//快速闪烁
	LED_NORMAL,		//正常速度
	LED_SLOW,	//缓慢闪烁
}LED_SPEED;


typedef struct __led_msg
{
    LED_MSG_EVENT msg_event;
	  uint8_t mode;
		int16_t num;
		uint8_t led_idx;
}_led_msg;
		

typedef struct{
	unsigned char step;
	time_ms_T tm;
	uint8_t count;
	_led_msg led_msg;
} APP_LED_T;

//定义应用实例
extern APP_LED_T g_app_led;


typedef struct _led_effect{
	uint8_t status;
	uint16_t interval_time;
}led_effect;

typedef struct _led_loop{
	uint8_t count;
	led_effect effect[5];
}led_loop;


enum{
	APP_LED_STEP_IDLE ,	//初始化
	APP_LED_STEP_RUN ,	//
};

int app_led_init(APP_LED_T* p);
void app_led_task(APP_LED_T* p);

/********   外部调用   ***********/
void led_start(uint8_t mode, uint16_t loop_num,uint8_t led_idx);
void led_end(void);

void set_led(uint8_t speed,uint8_t led_idx);
void set_led_always_on(uint8_t led_idx);
#endif
