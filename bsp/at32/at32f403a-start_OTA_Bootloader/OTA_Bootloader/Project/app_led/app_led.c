#include "app_led.h"
#include "global.h"
#include "func_tools.h"
#include "func_shell.h"

APP_LED_T g_app_led;


#define led_printf //printf


void all_led_off(void)
{
  LED0_CTRL(LED_OFF);
	LED1_CTRL(LED_OFF);
	LED2_CTRL(LED_OFF);
}

void led_idx_ctrl(uint8_t idx,uint8_t on_off)
{
	switch (idx)
  {
  	case 0:LED0_CTRL(on_off);
  		break;
  	case 1:LED1_CTRL(on_off);
  		break;
		case 2:	LED2_CTRL(on_off);
  		break;
  	default:
  		break;
  }
}

led_loop led_loop_array[] =
{
 		{
			5, 
			{ 
				{LED_ON, 50}, 
				{LED_OFF, 50}, 
				{LED_ON, 50}, 
				{LED_OFF, 50},
				{LED_ON, 50}
			}
		},
		{
			5, 
			{ 
				{LED_ON, 	400}, 
				{LED_OFF, 400}, 
				{LED_ON, 	400}, 
				{LED_OFF, 400}, 
				{LED_ON, 	400}
			}
		},    //fast_flash  used for upgrade
   
		{
			5,
			{
				{LED_ON, 1000}, 
				{LED_OFF, 1000}, 
				{LED_ON, 1000}, 
				{LED_OFF, 1000},
				{LED_ON, 1000}
			}
		},    //slow_flash	used for button
};
 
void led_start(uint8_t mode, uint16_t loop_num,uint8_t led_idx)
{
		all_led_off();
		//g_app_led.led_msg msg;
		g_app_led.led_msg.msg_event = LED_START;//NET_SEND_DATA;
		g_app_led.led_msg.mode=mode;
		g_app_led.led_msg.num = loop_num;
		g_app_led.led_msg.led_idx=led_idx;
		//rt_mq_send(&led_mq, &msg, sizeof( g_app_led.led_msg));
}
void led_end(void)
{
		//g_app_led.led_msg msg;
		g_app_led.led_msg.msg_event = LED_END;//NET_SEND_DATA;
		//rt_mq_send(&led_mq, &msg, sizeof( g_app_led.led_msg));
		all_led_off();
}
void set_led_always_on(uint8_t led_idx){ 
		all_led_off();
		//g_app_led.led_msg msg;
		g_app_led.led_msg.msg_event = LED_ALWAYS_ON;//NET_SEND_DATA;
		g_app_led.led_msg.led_idx=led_idx;

}

void app_led_step_idle(APP_LED_T* p){
		if(g_app_led.led_msg.msg_event==LED_START)
		{
			if (g_app_led.led_msg.mode > sizeof(led_loop_array) / sizeof(led_loop) - 1)
			{
					led_printf("led mode error\n");
					g_app_led.led_msg.msg_event=LED_END;
					return;
			}
			p->count=led_loop_array[g_app_led.led_msg.mode].count;

			p->step=APP_LED_STEP_RUN;
		}

}
void app_led_step_run(APP_LED_T* p){
		static int i = 0;
    if(left_ms(&g_app_led.tm) == 0)
    {
			if (0 != p->count&&g_app_led.led_msg.msg_event==LED_START)
			{
					
					//for(;;){
							all_led_off();
							if (i >= p->count)
							{
									i = 0;
									if(g_app_led.led_msg.num!=LED_LOOP_NUM_FOREVER)
									{
										--g_app_led.led_msg.num;
									}
									led_printf("g_app_led.led_msg.num %d\n",g_app_led.led_msg.num);
									if (g_app_led.led_msg.num <= 0&&g_app_led.led_msg.num!=LED_LOOP_NUM_FOREVER)
									{
											led_idx_ctrl(g_app_led.led_msg.led_idx,LED_OFF);
											g_app_led.led_msg.msg_event=LED_END;
											p->step=APP_LED_STEP_IDLE;
											return;
											//break;
									}
							}
							
							led_idx_ctrl(g_app_led.led_msg.led_idx,led_loop_array[g_app_led.led_msg.mode].effect[i].status);
							led_printf("led state %d mode %d led_idx %d\n",led_loop_array[g_app_led.led_msg.mode].effect[i].status,g_app_led.led_msg.mode,g_app_led.led_msg.led_idx);
							if (++i < p->count || g_app_led.led_msg.num > 0)
							{
								 left_ms_set(&g_app_led.tm, led_loop_array[g_app_led.led_msg.mode].effect[i - 1].interval_time);	//设定下一次延时时间
									//rt_thread_delay(rt_tick_from_millisecond());
							}
					//}
			}
			else if(g_app_led.led_msg.msg_event==LED_ALWAYS_ON){
				//all_led_off();
				led_idx_ctrl(g_app_led.led_msg.led_idx,LED_ON);
			}
			else
			{
				//g_app_led.led_msg.msg_event=LED_END;
				p->step=APP_LED_STEP_IDLE;
				i=0;
				g_app_led.led_msg.num=0;
				led_printf("led end\n");
			}
		}
}



void app_led_task(APP_LED_T* p)
{
		switch (p->step)
		{
				case 
					APP_LED_STEP_IDLE:
						app_led_step_idle(p);
				break;
				case 
					APP_LED_STEP_RUN:
						app_led_step_run(p);
				break;
		}				
}

int app_led_init(APP_LED_T* p)
{
	led_gpio_init();
	all_led_off();
}

int cmd_led_start(uint8_t argc, char **argv)
{
	int16_t a,b,c;
	sscanf(argv[1],"%d,%d,%d",&a,&b,&c);
	led_start(a,b,c);
	return 0;
}
//CMD_EXPORT(cmd_led_start,hhh);

int cmd_led_end(uint8_t argc, char **argv)
{
	led_end();
	return 0;
}
//CMD_EXPORT(cmd_led_end,hhh);


//speed：设置LED闪烁速度，LED_FAST：快速闪烁，LED_NORMAL：正常速度，LED_SLOW：缓慢闪烁
void set_led(uint8_t speed,uint8_t led_idx){
	led_start(speed, LED_LOOP_NUM_FOREVER , led_idx);

}

int cmd_set_led(uint8_t argc, char **argv)
{
	set_led(atoi(argv[1]),atoi(argv[2]));
	return 0;
}
//CMD_EXPORT(cmd_set_led,hhh);

int cmd_set_led_always_on(uint8_t argc, char **argv)
{
	set_led_always_on(atoi(argv[1]));
	return 0;
}
//CMD_EXPORT(cmd_set_led_always_on,hhh);

