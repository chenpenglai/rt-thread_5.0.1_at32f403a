#include "includes.h"

#include "app_led_msp.h"

void led_gpio_init(void){
	  GPIO_InitType GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(LED0_RCC, ENABLE);
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_MaxSpeed=GPIO_MaxSpeed_2MHz;
		
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;			
    GPIO_InitStructure.GPIO_Pins = LED0_PIN;
    GPIO_Init(LED0_PORT,&GPIO_InitStructure);
	
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;			
    GPIO_InitStructure.GPIO_Pins = LED1_PIN;
    GPIO_Init(LED1_PORT,&GPIO_InitStructure);
	    
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;			
    GPIO_InitStructure.GPIO_Pins = LED2_PIN;
    GPIO_Init(LED2_PORT,&GPIO_InitStructure);
	
	
}



