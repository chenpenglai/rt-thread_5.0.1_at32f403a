/**
  ******************************************************************************
  * @file    IAP demo
  * @author  ArteryTek SW 
  * @version V1.0
  * @date    19-Dec-2017  
  * @brief   Description of the SPIM operate demo.
  ******************************************************************************
  */
/*


  @brief
    This demo is based on the AT-START-F403 board,in this demo,it can receive user code and write to user address like bootloader.	
	LED2 toggle when run in IAP address
*/
/****************** (C) COPYRIGHT 2017 ArteryTek *********END OF FILE*********/