#include "led.h"

#define LED2_GPIO_PIN			GPIO_Pins_13
#define LED3_GPIO_PIN			GPIO_Pins_14
#define LED4_GPIO_PIN			GPIO_Pins_15
#define LED_GPIO_PORT			GPIOD

void LED_Init(void)
{
  GPIO_InitType GPIO_InitStructure;
	TMR_TimerBaseInitType TMR_TimeBaseStructure;
	TMR_OCInitType TMR_OCInitStructure;

	RCC_APB1PeriphClockCmd(	RCC_APB1PERIPH_TMR4, 
													ENABLE);
  RCC_APB2PeriphClockCmd(	RCC_APB2PERIPH_GPIOD	|
													RCC_APB2PERIPH_AFIO,
													ENABLE);
	GPIO_PinsRemapConfig(		GPIO_Remap_TMR4,
													ENABLE);
	
	GPIO_StructInit(&GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pins = LED2_GPIO_PIN | LED3_GPIO_PIN | LED4_GPIO_PIN;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);
	
	TMR_TimeBaseStructInit(&TMR_TimeBaseStructure);
  TMR_TimeBaseStructure.TMR_Period = 10000-1;
  TMR_TimeBaseStructure.TMR_DIV = (SystemCoreClock/40000)-1;
  TMR_TimeBaseStructure.TMR_ClockDivision = 0;
  TMR_TimeBaseStructure.TMR_CounterMode = TMR_CounterDIR_Up;
  TMR_TimeBaseInit(TMR4, &TMR_TimeBaseStructure);
	
	TMR_OCStructInit(&TMR_OCInitStructure);
	TMR_OCInitStructure.TMR_OCMode = TMR_OCMode_PWM2;
	TMR_OCInitStructure.TMR_Pulse = 5000;
	TMR_OCInitStructure.TMR_OutputState = TMR_OutputState_Enable;
	TMR_OCInitStructure.TMR_OCPolarity = TMR_OCPolarity_High;
	TMR_OC2Init(TMR4, &TMR_OCInitStructure);
	
	TMR_OC2PreloadConfig(TMR4, TMR_OCPreload_Enable);
  TMR_ARPreloadConfig(TMR4, ENABLE);
	TMR_Cmd(TMR4, ENABLE);
}
