#ifndef __USART_H
#define __USART_H
#include "at32f4xx.h"

#define USART_REC_LEN  		1024 
typedef struct{
uint16_t buf[USART_REC_LEN];
uint16_t head;
uint16_t tail;
uint16_t count;	
}UARTSTRUCT;

extern UARTSTRUCT UART1_struct;	  	  	
extern u8  USART_RX_BUF[USART_REC_LEN]; //接收緩衝,最大USART_REC_LEN個字節.末字節為換行符 
extern u16 USART_RX_STA;         		//接收狀態標記	
extern u16 USART_RX_CNT;				//接收的字節數	  
//如果想串口中斷接收，請不要註釋以下宏定義
void UART_Init(u32 bound);

#endif


