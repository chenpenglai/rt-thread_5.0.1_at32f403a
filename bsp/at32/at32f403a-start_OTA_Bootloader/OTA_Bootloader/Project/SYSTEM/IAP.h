#ifndef __IAP_H__
#define __IAP_H__

#include "at32f4xx.h"
#include "global.h"
#include "prj_config.h"

typedef  void (*iapfun)(void);              //定义一个函数类型的参数.
#define FLASH_APP1_ADDR                 0x08008000      //第一个应用程序起始地址(存放在FLASH)   保留0X08000000~0X0800FFFF的空间为IAP使用

/*******				iap升级参数（注意：和 APP 程序内必须保持一致）					*********/

#ifdef NEW_BOOT_PFS
	#define BK_APP_ADDR                     (8*1024)       //外部flash存储新APP起始地址 (最前面4K为app参数配置的easyflash区域)
	#define BK_APP_SIZE                     (200*1024)

	#define OLD_APP_ADDR                    (BK_APP_ADDR+BK_APP_SIZE)
	#define OLD_APP_SIZE                    (200*1024)    

#else
	#define BK_APP_ADDR                     (8*1024)       //外部flash存储新APP起始地址 (最前面4K为app参数配置的easyflash区域)
	#define BK_APP_SIZE                     (100*1024)

	#define OLD_APP_ADDR                    (BK_APP_ADDR+BK_APP_SIZE)
	#define OLD_APP_SIZE                    (100*1024)    
#endif 

#define UPGRADE_FLAG_ADDR               (OLD_APP_ADDR+OLD_APP_SIZE)         //0K 放置标志位（不能放在0和216K以后，貌似flash驱动有问题）
#define UPGRADE_FLAG_SIZE               (4*1024)                    //空间不能小于4K（因为还要写入）

//#define SUBSET_APP_ADDR                 (UPGRADE_FLAG_ADDR+UPGRADE_FLAG_SIZE)
//#define SUBSET_APP_SIZE                  (100*1024)

//#define EF_START_ADDR 	(SUBSET_APP_ADDR+SUBSET_APP_SIZE)

#define NEED_UPGRADE_FLAG               1                           
#define SUCCESSED_UPGRADE_FLAG          2                                    
#define ERR_UPGRADE_FLAG                3      


#define HAVE_OLD_APP    1
#define NO_OLD_APP      0

uint32_t set_bak_app_start_addr(uint32_t app_start_addr) ; 
typedef struct
{
    uint8_t is_upgrade;
    uint32_t app_size;
    uint16_t need_check;
    uint8_t have_old_app;
		
		
		
} _upgrade_info;

typedef enum
{
    NO_ERR,
    ERASE_ERR,
    WRITE_ERR,
    ENV_NAME_ERR,
    ENV_NAME_EXIST,
    ENV_FULL,
    ENV_INIT_FAILED,
} IapErrCode;

void set_upgrade_flag_info(_upgrade_info *upgrade_info);
uint8_t get_upgrade_flag_info(_upgrade_info *upgrade_info);
void iap_load_app(uint32_t appxaddr);
void check_upgrade(_upgrade_info *upgrade_info);
void clear_upgrade_flag();
void goto_iap(void) ;
IapErrCode app_read(uint32_t addr, uint32_t *buf, size_t size);
IapErrCode copy_spec_app_from_bak(uint32_t user_app_addr, size_t app_size) ;
IapErrCode erase_spec_user_app(uint32_t user_app_addr, size_t app_size) ;
IapErrCode bak_Firmware_check(uint32_t user_app_addr, size_t app_size, uint16_t get_check);
IapErrCode copy_old_app_to_bak(uint32_t user_app_addr, size_t old_app_size) ;
void print_intern_flash_app_hex(uint32_t user_app_addr, size_t app_size);
void print_extern_flash_app_hex(uint32_t user_app_addr, size_t app_size);
#endif

