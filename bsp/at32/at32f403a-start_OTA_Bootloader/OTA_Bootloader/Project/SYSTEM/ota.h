#ifndef __OTA_H
#define __OTA_H
#include "at32f4xx.h"

#define APP_UPGRADE_FLAG_ADDR		(u32)(FLASH_APP_ADDR-PAGE_SIZE)
#define APP_UPGRADE_FLAG 				0x41544B38

typedef enum{
	UPDATE_IDLE,
	UPDATE_WRITE,
	UPDATE_DONE,
	UPDATE_ERR,
	UPDATE_END,
}UPDATE_STATUS;

typedef void (*otafun)(void);
void APP_Upgrade_Handle(void);
void Run_APP(void);
u8 FLASH_Read_Upgrade_Flag(void);

#endif
