#include "ota.h"
#include "iap.h"
#include "flash.h"

u8 bUpdateStatus=UPDATE_IDLE;
u8 g_bFlashErrCnt=0;
otafun Jump2App; 

//u8 FLASH_Read_Upgrade_Flag(void)
//{
//	if((*(u32*)APP_UPGRADE_FLAG_ADDR)==APP_UPGRADE_FLAG)
//		return SET;
//	else
//		return RESET;
//}

void Reset_Periph_Clock(void)
{
	RCC->APB1RST = 0xFFFFFFFF;
	RCC->APB2RST = 0xFFFFFFFF;
	RCC->APB1RST = 0x00000000;
	RCC->APB2RST = 0x00000000;
}
#include "w25qxx_spi.h"
#include "func_shell.h"
void OTA_Load_APP(u32 wAppAddr)
{

 	__disable_irq();		//加了这个 之前没有 
	//Reset_Periph_Clock();
	

	NVIC->ICER[0]=0xffffffff;	
	NVIC->ICER[1]=0xffffffff;	
	NVIC->ICER[2]=0xffffffff;	
	

	USART_Reset(USART1);
	USART_Reset(USART2);
	USART_Reset(USART3);
	USART_Reset(UART4);
	USART_Reset(UART5);
	
	
	if(((*(vu32*)wAppAddr)&0x2FFE0000)==0x20000000)
	{
		Jump2App=(otafun)*(vu32*)(wAppAddr+4);		
		MSR_MSP(*(vu32*)wAppAddr);
		Jump2App();
	}
}

void Run_APP(void)
{
	if(((*(vu32*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)
	{
		OTA_Load_APP(FLASH_APP1_ADDR);
	}
	else{
		//printf("err\n");
		
	
	}
}

//void APP_Upgrade_Handle(void)
//{
//	u8 bStatusTemp=bUpdateStatus;
//	
//	switch(bStatusTemp)
//	{
//		case UPDATE_IDLE:
//		  if(	(FLASH_Read_Upgrade_Flag() == SET)	&&
//					(g_bFlashErrCnt<3)									)
//			{
//				RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_FLASH, ENABLE);
//				FLASH_ClearFlag(FLASH_FLAG_PRGMFLR);
//				bStatusTemp=UPDATE_WRITE;
//			}
//			break;
//		case UPDATE_WRITE:
//			FLASH_Update();
//			if(g_bFlashStatus==ERROR)
//			{
//				g_bFlashErrCnt++;
//				bStatusTemp=UPDATE_ERR;
//			}
//			else
//			{
//				g_bFlashErrCnt=0;
//				bStatusTemp=UPDATE_DONE;
//			}
//			break;
//		case UPDATE_DONE:
//			Run_APP();
//			bStatusTemp=UPDATE_END;
//			break;
//		case UPDATE_ERR:
//			bStatusTemp=UPDATE_IDLE;
//			break;
//		case UPDATE_END:
//			bStatusTemp=UPDATE_IDLE;
//			break;
//	}
//	bUpdateStatus=bStatusTemp;
//}
