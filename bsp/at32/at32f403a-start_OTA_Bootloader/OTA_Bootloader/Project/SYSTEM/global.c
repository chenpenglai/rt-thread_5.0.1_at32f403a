#include "global.h"
#include "stdint.h"


//uint8_t half_2_u8(uint8_t* dat){				//ƴ��2������Ϊbyte
//	return (uint8_t)dat[0]<<4|(uint8_t)dat[1];

//}

#include "func_iwdg.h"


int reboot() {
    NVIC_SystemReset();
    return 0;

}
void delay_ms(uint32_t t)
	{
		while(t--){
			iwdg_free();
			delay_us(1000);
		}
	}
	
	

void extend_SRAM(void)
{
/* Target set_SRAM_16K is selected */
#ifdef EXTEND_SRAM_16K
  // check if RAM has been set to 16K, if not, change EOPB0
  if(((UOPTB->EOPB0)&0x03)!=0x01)
  {
    /* Unlock Option Bytes Program Erase controller */
    FLASH_Unlock();
    /* Erase Option Bytes */
    FLASH_EraseUserOptionBytes();
    /* Change SRAM size to 16KB */
    FLASH_ProgramUserOptionByteData((uint32_t)&UOPTB->EOPB0,0xFD);
    NVIC_SystemReset();
  }
#endif

/* Target set_SRAM_32K is selected */
#ifdef EXTEND_SRAM_32K
  // check if RAM has been set to 32K, if not, change EOPB0
  if(((UOPTB->EOPB0)&0x03)!=0x03)
  {
    /* Unlock Option Bytes Program Erase controller */
    FLASH_Unlock();
    /* Erase Option Bytes */
    FLASH_EraseUserOptionBytes();
    /* Change SRAM size to 32KB */
    FLASH_ProgramUserOptionByteData((uint32_t)&UOPTB->EOPB0,0xFF);
    NVIC_SystemReset();
  }
 #endif

/* Target set_SRAM_64K is selected */
#ifdef EXTEND_SRAM_64K
  // check if RAM has been set to 64K, if not, change EOPB0
  if(((UOPTB->EOPB0)&0x01))  
  {
    /* Unlock Option Bytes Program Erase controller */
    FLASH_Unlock();
    /* Erase Option Bytes */
    FLASH_EraseUserOptionBytes();
    /* Change SRAM size to 64KB */
    FLASH_ProgramUserOptionByteData((uint32_t)&UOPTB->EOPB0,0xFC);
    NVIC_SystemReset();
  }
#endif

/* Target set_SRAM_224K is selected */
#ifdef EXTEND_SRAM_224K
/* check if RAM has been set to 224K, if not, change EOPB0 */
  if(((UOPTB->EOPB0)&0xFF)!=0xFE)
  {
    /* Unlock Option Bytes Program Erase controller */
    FLASH_Unlock();
    /* Erase Option Bytes */
    FLASH_EraseUserOptionBytes();
    /* Change SRAM size to 224KB */
    FLASH_ProgramUserOptionByteData((uint32_t)&UOPTB->EOPB0,0xFE);
    NVIC_SystemReset();
  }
#endif
}