#include "IAP.h"
#include "global.h"
#include "w25qxx.h"


#include "usart.h"
#include "stdio.h"
#include "W25QXX.h"

#include "flash.h"			//片内 flash


#define W25QXX_Read flash_r
#define W25QXX_Write flash_w
//void hexdump(char* note,uint8_t* dat,int len){
//	printf("\n\n%s\n",note);
//	for(int i=0;i<len;i++){
//		printf("%X",dat[i]);
//		
//	}
//	printf("\n%s\n",note);
//	
//}
//iapfun jump2app;
////设置栈顶地址
////addr:栈顶地址
//__asm void MSR_MSP(uint32_t addr)
//{
//    MSR MSP, r0             //set Main Stack value
//    BX r14
//}

////跳转到应用程序段
////appxaddr:用户代码起始地址.
//void iap_load_app(uint32_t appxaddr)
//{
//    if (((*(volatile uint32_t *)appxaddr) & 0x2FFE0000) == 0x20000000) //检查栈顶地址是否合法.
//    {
//        jump2app = (iapfun) * (volatile uint32_t *)(appxaddr + 4); //用户代码区第二个字为程序开始地址(复位地址)
//        __set_MSP(*(volatile uint32_t *)appxaddr);                  //初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
////      for(int i = 0; i < 8; i++){
////          NVIC->ICER[i] = 0xFFFFFFFF; /* 关闭中断*/
////          NVIC->ICPR[i] = 0xFFFFFFFF; /* 清除中断标志位 */
////      }
//        jump2app();                                 //跳转到APP.
//    }
//    else
//        printf("iap_load_app failed\n");
//}


/*************      copy app to innernal flash from external flash      ****************/

uint8_t get_upgrade_flag_info(_upgrade_info *upgrade_info)
{

    uint8_t buf[8] = {0};
    W25QXX_Read(UPGRADE_FLAG_ADDR, buf, 8);
		//W25QXX_Read(UPGRADE_FLAG_ADDR, (uint8_t*)upgrade_info, 8);
    for (int i = 0; i < 8; i++)
        printf("%02X", buf[i]);
    printf("\n");
    upgrade_info->is_upgrade = buf[0];
    upgrade_info->app_size = u8_2_u32(&buf[1]);
    upgrade_info->need_check = u8_2_u16(&buf[5]);
    upgrade_info->have_old_app = buf[7];
		 printf("<-------- BOOT EX FLASH ------->\n");
		
		printf("%-18s - 0x%02X\n","FLASH_APP1_ADDR",FLASH_APP1_ADDR);
		printf("%-18s -   %d K, size %d K\n","BK_APP_ADDR",BK_APP_ADDR/1024,BK_APP_SIZE/1024);
		printf("%-18s - %d K, size %d K\n","OLD_APP_ADDR",OLD_APP_ADDR/1024,OLD_APP_SIZE/1024);
		printf("%-18s - %d K, size %d K\n","UPGRADE_FLAG_ADDR",UPGRADE_FLAG_ADDR/1024,UPGRADE_FLAG_SIZE/1024);
		//printf("%-18s - %d K, size %d K\n","SUBSET_APP_ADDR",SUBSET_APP_ADDR/1024,SUBSET_APP_SIZE/1024);
		
    printf("is_upgrade: %d\n", upgrade_info->is_upgrade);
    printf("app_size  : %d\n", upgrade_info->app_size);
    printf("need_check: %d\n", upgrade_info->need_check);
    if ((upgrade_info->have_old_app) == 1)
    {
        printf("have old app\n");
    }
    else
    {
        printf("no old app\n");
    }
    printf("<------------------------------->\n");

    return (upgrade_info->is_upgrade);
}
void set_upgrade_flag_info(_upgrade_info *upgrade_info)
{

    

    //W25QXX_EraseSector(UPGRADE_FLAG_ADDR,UPGRADE_FLAG_ADDR+10);           //必须擦除再写入
    uint8_t buf[8] = {0};
    int pos = 0;
    buf[pos++] = upgrade_info->is_upgrade;
    buf[pos++] = (uint8_t)(upgrade_info->app_size >> 24);
    buf[pos++] = (uint8_t)(upgrade_info->app_size >> 16);
    buf[pos++] = (uint8_t)(upgrade_info->app_size >> 8);
    buf[pos++] = (uint8_t)(upgrade_info->app_size);

    buf[pos++] = (uint8_t)(upgrade_info->need_check >> 8);
    buf[pos++] = (uint8_t)(upgrade_info->need_check);
    buf[pos++] = upgrade_info->have_old_app;
    W25QXX_Write(UPGRADE_FLAG_ADDR, buf, pos);

    

}


//uint32_t check_buff[1024*3];
IapErrCode app_write(uint32_t addr, uint32_t *buf, size_t size)
{
    IapErrCode result = NO_ERR;
		Flash_Write(addr,buf,size);
    return result;
}

uint32_t bak_app_start_addr = BK_APP_ADDR;
uint32_t get_bak_app_start_addr(void)
{
		//可能是地址不对
    return bak_app_start_addr;
}
uint32_t set_bak_app_start_addr(uint32_t app_start_addr)
{
    bak_app_start_addr = app_start_addr;
}

/* 32 words size buffer */
uint32_t buff[1 * 1024]; //一次性拷贝 4*1*1024 = 4K 加快速度
uint32_t check_buff[1 * 1024]; //校验缓存
//uint32_t check_buff[1024*3];
IapErrCode copy_spec_app_from_bak(uint32_t user_app_addr, size_t app_size)
{
    size_t cur_size;
    uint32_t app_cur_addr, bak_cur_addr;
    IapErrCode result = NO_ERR;

    for (cur_size = 0; cur_size < app_size; cur_size += sizeof(buff))
    {
//            led_gpio_togle();
        printf("updata progress %f %%.\n", ((float)cur_size / app_size) * 100);
        app_cur_addr = user_app_addr + cur_size;
        bak_cur_addr = get_bak_app_start_addr() + cur_size;
        if (app_size - cur_size > sizeof(buff))
        {
            W25QXX_Read(bak_cur_addr, (uint8_t *) buff, sizeof(buff));
            result = app_write(app_cur_addr, buff, sizeof(buff));
					
					/*******		校验		******/
						Flash_Read(app_cur_addr, check_buff, sizeof(check_buff));
						//W25QXX_Read(bak_cur_addr,(uint8_t *)check_buff,sizeof(buff));
#ifdef OUTPUT_BURN_NEW_APP_CHECK_BUFF				
					hexdump("----buff-----",(uint8_t*)buff, sizeof(buff));
					hexdump("----check_buff-----",(uint8_t*)check_buff, sizeof(check_buff));
#endif					
						for(int i=0;i<sizeof(buff)/4;i++){
							if(buff[i]!=check_buff[i]){
								printf("check err 0x%X 0x%X\n",buff[i],check_buff[i]);
								
								return WRITE_ERR;
							}
						}
				/*******		校验		******/				
					
        }
        else
        {
            W25QXX_Read(bak_cur_addr, (uint8_t *)buff, app_size - cur_size);
            result = app_write(app_cur_addr, buff, app_size - cur_size);

        }

        // printf("app_cur_addr %d bak_cur_addr%d \n",app_cur_addr,bak_cur_addr);
        if (result != NO_ERR)
        {
            break;
        }
    }

    

    switch (result)
    {
    case NO_ERR:
    {
        printf("Write data to application entry OK.\n");
        break;
    }
    case WRITE_ERR:
    {
        printf("Warning: Write data to application entry fault!\n");
        break;
    }
    }
    return result;
}



void print_intern_flash_app_hex(uint32_t user_app_addr, size_t app_size)
{
	//Stop_Dog();
		//app_size/=4;			//是否要除4？
    size_t cur_size;
    uint32_t app_cur_addr, bak_cur_addr;
		printf("输出内部 app hex\n");
    for (cur_size = 0; cur_size < app_size; cur_size += sizeof(buff))
    {
        app_cur_addr = user_app_addr + cur_size;
        bak_cur_addr = get_bak_app_start_addr() + cur_size;
        if (app_size - cur_size > sizeof(buff))
        {
            app_read(bak_cur_addr, (uint32_t *) buff, sizeof(buff));				
						//输出内部flash  FLASH_APP1_ADDR的app hex
						for(uint32_t i=0;i<sizeof(buff);i++){
							 //
							printf("%02X",buff[i]);
						}						
        }
        else
        {
            app_read(bak_cur_addr, (uint32_t *)buff, app_size - cur_size);
							for(uint32_t i=0;i<app_size - cur_size;i++){
								 //
							printf("%02X",buff[i]);
						}
        }
    }
		//Start_Dog();
    //
}


void print_extern_flash_app_hex(uint32_t user_app_addr, size_t app_size)
{
//	Stop_Dog();
    size_t cur_size;
    uint32_t app_cur_addr, bak_cur_addr;
		printf("输出外部 app hex\n");
    for (cur_size = 0; cur_size < app_size; cur_size += sizeof(buff))
    {
        app_cur_addr = user_app_addr + cur_size;
        bak_cur_addr = get_bak_app_start_addr() + cur_size;
        if (app_size - cur_size > sizeof(buff))
        {
            W25QXX_Read(bak_cur_addr, (uint8_t *) buff, sizeof(buff));
						//输出外部flash  FLASH_APP1_ADDR的app hex
						for(uint32_t i=0;i<sizeof(buff);i++){
							//
							printf("%02X",buff[i]);
						}				
        }
        else
        {
            W25QXX_Read(bak_cur_addr, (uint8_t *)buff, app_size - cur_size);
						for(uint32_t i=0;i<app_size - cur_size;i++){
							//
							printf("%02X",buff[i]);
						}
        }
    }
//		Start_Dog();
    
}



	#if defined(MCU_AT32F415x)||defined(MCU_AT32F413x)
		#define FLASH_SIZE_ADR 0x1ffff7e0
							  
		#define IAP_RDP_Key			((unsigned short)0x00A5)	 	
		#define IAP_FLASH_KEY1		((unsigned int)0x45670123)
		#define IAP_FLASH_KEY2		((unsigned int)0xCDEF89AB)
		#define IAP_PAGE_ZISE		0x800				// 扇区大小

		//读取本芯片的Flash大小
		unsigned int __func_flash_iap_get_size(void)
		{
			return(*((unsigned short*)FLASH_SIZE_ADR));
		}

		//读取本芯片的扇区大小
		unsigned int __func_flash_iap_get_pagesize(void)
		{
			unsigned int i;
			
			i=__func_flash_iap_get_size();
			if(i>128)
			{
				i=0x800;
			}
			else
			{
				i=0x400;
			}
			return(i);
		}
		
		//读取本芯片的FLASH起始地址
		unsigned int __func_flash_iap_get_baseadr(void)
		{
			return(FLASH_BASE);	//起始地址
		}


		void __func_flash_iap_unlock(void)
		{
			__disable_irq();	//关中断
			FLASH_Unlock();
			__enable_irq();		//开中断
		}

		void __func_flash_iap_lock(void)
		{
			;
		}

		unsigned char __func_flash_iap_chk_busy(void)
		{
			return(0);
		}


		//向flash写一个半字
		unsigned char __func_flash_iap_write_hword(unsigned int adr,unsigned short us)
		{
			return FLASH_ProgramHalfWord(adr,us);
		}




		/***************************************************************************************/
		//V101加入页擦除
		void __func_flash_iap_erase_page(unsigned int adr)
		{
			FLASH_ErasePage(adr);
		}
	#endif


#include "func_iwdg.h"

#define PAGE_SIZE     1024
IapErrCode app_erase(uint32_t addr, size_t size)
{
    IapErrCode result = NO_ERR;
 //   HAL_StatusTypeDef status = HAL_OK;
    size_t erase_pages, i;
	FLASH_Status status;
    /* make sure the start address is a multiple of FLASH_ERASE_MIN_SIZE */
    //ASSERT(addr % EF_ERASE_MIN_SIZE == 0);

    /* calculate pages */
    erase_pages = size / __func_flash_iap_get_pagesize();
    if (size % PAGE_SIZE != 0)
    {
        erase_pages++;
    }
//	RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_FLASH, ENABLE);
//	FLASH_ClearFlag(FLASH_FLAG_PRGMFLR);
		//printf("扇区 %d\n",__func_flash_iap_get_pagesize());
		FLASH_Unlock();
		for(int i=0;i<erase_pages;i++){
			iwdg_free();
			FLASH_ErasePage(addr+(__func_flash_iap_get_pagesize()*i));
		}
		FLASH_Lock();
		
//    /* start erase */
//    HAL_FLASH_Unlock();
//    FLASH_EraseInitTypeDef f;
//    uint32_t PageError = 0;

//    f.TypeErase = FLASH_TYPEERASE_PAGES;
//    f.PageAddress = addr;
//    f.NbPages = erase_pages;
//    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR);
//    status = HAL_FLASHEx_Erase(&f, &PageError);
//    if (status != HAL_OK)
//    {
//        result = WRITE_ERR;
//    }
//    HAL_FLASH_Lock();
		//printf("status %d\n",status);
    return result;
}

IapErrCode erase_spec_user_app(uint32_t user_app_addr, size_t app_size)
{
    IapErrCode result = NO_ERR;

    result = app_erase(user_app_addr, app_size);
    switch (result)
    {
    case NO_ERR:
    {
        printf("Erased user application OK.\n");
        break;
    }
    case WRITE_ERR:
    {
        printf("Warning: Erase user application fault!\n");
        /* will return when erase fault */
        return result;
    }
    }

    return result;
}

static uint16_t check_bin(uint16_t pre_check, uint8_t *data, uint16_t len)
{
    static int test_count = 0;
    test_count += len;
    uint16_t check = pre_check;
    for (int i = 0; i < len; i++)
    {
        check += *(data + i); //mmp
    }
    return check;
}

IapErrCode bak_Firmware_check(uint32_t user_app_addr, size_t app_size, uint16_t get_check)
{
    size_t cur_size ;
    uint32_t  bak_cur_addr;
    IapErrCode result = NO_ERR;
    /* 32 words size buffer */
    uint16_t check_value = 0;
    /*check bin */
    for (cur_size = 0; cur_size < app_size; cur_size += sizeof(buff))
    {
//          led_gpio_togle();
        printf("Firmware check is in progress %f %%.\n", ((float)cur_size / app_size) * 100);
        bak_cur_addr = get_bak_app_start_addr() + cur_size;
        if (app_size - cur_size > sizeof(buff))
        {
            memset(buff, 0, sizeof(buff));
            W25QXX_Read(bak_cur_addr, (uint8_t *)buff, sizeof(buff));
            check_value = check_bin(check_value, (uint8_t *)buff, sizeof(buff));
						//hexdump("----buff-----",(uint8_t*)buff, sizeof(buff));
						
        }
        else
        {
            W25QXX_Read(bak_cur_addr, (uint8_t *)buff, app_size - cur_size);

            check_value = check_bin(check_value, (uint8_t *)buff, app_size - cur_size);

            printf("------> %ld\n", app_size - cur_size);
            printf("-- cal check value %d ---->\n", check_value);

        }

    }

    if (get_check != check_value)
    {
        printf("Firware addr is 0X%x ,Firmware get check is %d,but cal check is %d .\n", get_bak_app_start_addr(), get_check, check_value);
        result =  WRITE_ERR;
    }
    else
    {
        result =  NO_ERR;
    }
    return result;
}

/*********  backup old app          *************/

IapErrCode app_read(uint32_t addr, uint32_t *buf, size_t size)
{
    IapErrCode result = NO_ERR;
		
    ASSERT(size % 4 == 0);
    /*copy from flash to ram */
		//printf("缓存区地址 %p\n",buf);
    for (; size > 0; size -= 4, addr += 4, buf++)
    {
        *buf = *(uint32_t *) addr;

        

    }

    return result;
}

uint32_t old_app_start_addr = OLD_APP_ADDR;
uint32_t get_old_app_start_addr(void)
{
    return old_app_start_addr;
}

//uint8_t bk_buff[256];
IapErrCode copy_old_app_to_bak(uint32_t user_app_addr, size_t old_app_size)
{
    size_t cur_size;
    uint32_t app_cur_addr, bak_cur_addr;
    IapErrCode result = NO_ERR ;
    /* 32 words size buffer */
    /* cycle copy data */
		printf("start copy old app size: %d K\n",old_app_size/1024);
    for (cur_size = 0; cur_size < old_app_size; cur_size += sizeof(buff))
    {


        printf("old app backup progress %f %%.\n", ((float)cur_size / old_app_size) * 100);
        app_cur_addr = user_app_addr + cur_size;
        bak_cur_addr = get_old_app_start_addr() + cur_size;
        if (old_app_size - cur_size > sizeof(buff))
        {

            result = app_read(app_cur_addr, (uint32_t *) buff, sizeof(buff));       //读取 app
            //软件W25QXX_Write函数 写不了256以上
            W25QXX_Write(bak_cur_addr, (uint8_t *) buff, sizeof(buff));
						
				/*******		校验		******/
						W25QXX_Read(bak_cur_addr,(uint8_t *)check_buff,sizeof(buff));
#ifdef OUTPUT_BACKUP_OLD_APP_CHECK_BUFF
					hexdump("----buff-----",(uint8_t*)buff, sizeof(buff));
					hexdump("----check_buff-----",(uint8_t*)check_buff, sizeof(check_buff));
#endif
						for(int i=0;i<sizeof(buff)/4;i++){
							if(buff[i]!=check_buff[i]){
								printf("check err 0x%X 0x%X\n",buff[i],check_buff[i]);
								
								return WRITE_ERR;
							}
						}
				/*******		校验		******/
					
//                    W25QXX_Read(bak_cur_addr,(uint8_t *) buff, sizeof(buff));
//            result = app_write(app_cur_addr, buff, sizeof(buff));
        }
        else
        {
            result = app_read(app_cur_addr, (uint32_t *) buff, old_app_size - cur_size);
            W25QXX_Write(bak_cur_addr, (uint8_t *) buff, old_app_size - cur_size);
//                  W25QXX_Read(bak_cur_addr, (uint8_t *)buff, old_app_size-cur_size);
//          result = app_write(app_cur_addr, buff, old_app_size-cur_size);
        }

        if (result != NO_ERR)
        {
            break;
        }
    }

    

    switch (result)
    {
    case NO_ERR:
    {
        printf("Write old app to bak entry OK.\n");
        break;
    }
    case WRITE_ERR:
    {
        printf("Warning: Write data to bak entry fault!\n");
        break;
    }
    }
    return result;
}




void test_burn(){			//测试外部flash里app烧录到片内app的功能
		if (NO_ERR == copy_old_app_to_bak(FLASH_APP1_ADDR, OLD_APP_SIZE))
		{
			 printf("backup old app ok !!\n");
		}

		set_bak_app_start_addr(OLD_APP_ADDR);      
		if (erase_spec_user_app(FLASH_APP1_ADDR, OLD_APP_SIZE) || copy_spec_app_from_bak(FLASH_APP1_ADDR, BK_APP_SIZE))            //注意：擦除必须大小>=OLD_APP_SIZE
		{
				printf("copy old app failed !!\n");
		}
		else
		{
				printf("copy old app successed\n");
		}


}



void check_upgrade(_upgrade_info *upgrade_info)
{
#ifdef BACKUP_OLD_APP
		//if (1)
    if (SUCCESSED_UPGRADE_FLAG == upgrade_info->is_upgrade)
    {

        printf("-----> app is not working, recovering old app...\n");           //未跳到APP 就重启了（可能是看门狗导致）
        //return;
        if (upgrade_info->have_old_app == HAVE_OLD_APP)       
        {
          
            set_bak_app_start_addr(OLD_APP_ADDR);      
						
            if (erase_spec_user_app(FLASH_APP1_ADDR, OLD_APP_SIZE) || copy_spec_app_from_bak(FLASH_APP1_ADDR, BK_APP_SIZE))            //注意：擦除必须大小>=OLD_APP_SIZE
            {
                printf("copy old app failed !!\n");
            }
            else
            {
                printf("copy old app successed\n");

                upgrade_info->is_upgrade = ERR_UPGRADE_FLAG;
                set_upgrade_flag_info(upgrade_info);
            }
        }
        else
        {
//            Stop_Dog();
            
            while(1){
				
							printf("-----> no old app for recovery !\n");
							delay_ms(500);
						}

        }
    }
    else
#endif
#ifdef TEST_UPGRADE
        if (NEED_UPGRADE_FLAG == upgrade_info->is_upgrade)
        {
#else
        if ((NEED_UPGRADE_FLAG == upgrade_info->is_upgrade) && (upgrade_info->app_size <= OLD_APP_SIZE))
        {
#endif
#ifdef TEST_UPGRADE
            if (0)
            {
#else
            if (bak_Firmware_check(FLASH_APP1_ADDR, upgrade_info->app_size, upgrade_info->need_check))
            {
#endif
                upgrade_info->is_upgrade = ERR_UPGRADE_FLAG;
                set_upgrade_flag_info(upgrade_info);
                printf("firmaware check was fail\n");
            }
            else
            {
                printf("firmaware check was successed\n");
#ifdef BACKUP_OLD_APP
                if (NO_ERR == copy_old_app_to_bak(FLASH_APP1_ADDR, OLD_APP_SIZE))
                {
                    upgrade_info->have_old_app = HAVE_OLD_APP;
                    set_upgrade_flag_info(upgrade_info);
                }

#endif
								//set_bak_app_start_addr(BK_APP_ADDR);
                if (erase_spec_user_app(FLASH_APP1_ADDR, OLD_APP_SIZE) || copy_spec_app_from_bak(FLASH_APP1_ADDR, upgrade_info->app_size+1024))
                {
                    printf("copy new app failed !!\n");
                }
                else
                {
                    
                    printf("copy new app successed\n");
									#ifdef CYCLE_UPGRADE
										printf("<---- test mode will cycle upgrade ---->\n");
									#else
                    upgrade_info->is_upgrade = SUCCESSED_UPGRADE_FLAG;
                    set_upgrade_flag_info(upgrade_info);
									#endif
                }
            }
        }

}






