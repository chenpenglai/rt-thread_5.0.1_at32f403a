/******************************************************************************
* Copyright (c) 2018,汇邦电子
* All rights reserved.
*
* 文件名称：main.c
* 文件摘要：spi驱动程序
* 当前版本：1.0
* 作    者: 张建
* 完成日期：2018年5月25日
******************************************************************************/
#ifndef _w25qxx_spi_H_
#define _w25qxx_spi_H_
//--------------------------------------------------------------------------------------
#include "stdint.h"
#include "at32f4xx.h"
#if defined(AT_START_F413_V1_0)
#define FLASH_SPI               SPI1
	#define W25QXX_CS_Port 	GPIOD
	#define W25QXX_CS_Pin 	GPIO_Pins_2
#elif defined(AT_START_F413_V1_0_gateway_4g)
	#define FLASH_SPI               SPI1
	#define SPIx_RCC_CLK            RCC_APB2PERIPH_SPI1
	#define SPIx_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOA 
	#define SPIx_PIN_NSS            GPIO_Pins_4
	#define SPIx_PORT_NSS           GPIOA	

	#define W25QXX_CS_Pin 	SPIx_PIN_NSS
	#define W25QXX_CS_Port 	SPIx_PORT_NSS
		
	#define SPIx_PIN_SCK            GPIO_Pins_5
	#define SPIx_PORT_SCK           GPIOA
	#define SPIx_PIN_MISO           GPIO_Pins_6
	#define SPIx_PORT_MISO          GPIOA
	#define SPIx_PIN_MOSI           GPIO_Pins_7
	#define SPIx_PORT_MOSI          GPIOA

	#define SPIx_DMA                DMA1
	#define SPIx_DMA_RCC_CLK        RCC_AHBPERIPH_DMA1
	#define SPIx_Rx_DMA_Channel     DMA1_Channel2
	#define SPIx_Rx_DMA_FLAG        DMA1_FLAG_TC2
	#define SPIx_Tx_DMA_Channel     DMA1_Channel3
	#define SPIx_Tx_DMA_FLAG        DMA1_FLAG_TC3

	#define FLASH_CS_HIGH()         GPIO_SetBits(SPIx_PORT_NSS, SPIx_PIN_NSS)
	#define FLASH_CS_LOW()          GPIO_ResetBits(SPIx_PORT_NSS, SPIx_PIN_NSS)
#elif defined(AT_START_F413_V1_0_gateway_new)
	#define FLASH_SPI               SPI2
	#define SPIx_RCC_CLK            RCC_APB1PERIPH_SPI2
	
	#define SPIx_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOB
	#define SPIx_PIN_NSS            GPIO_Pins_12
	#define SPIx_PORT_NSS           GPIOB

	#define W25QXX_CS_Pin 	SPIx_PIN_NSS
	#define W25QXX_CS_Port 	SPIx_PORT_NSS
			
	#define W25QXX_WP_Pin 		GPIO_Pins_6
	#define W25QXX_WP_Port 		GPIOC
	
	
	#define SPIx_PIN_SCK            GPIO_Pins_13
	#define SPIx_PORT_SCK           GPIOB
	#define SPIx_PIN_MISO           GPIO_Pins_14
	#define SPIx_PORT_MISO          GPIOB
	#define SPIx_PIN_MOSI           GPIO_Pins_15
	#define SPIx_PORT_MOSI          GPIOB

	#define SPIx_DMA                DMA1
	#define SPIx_DMA_RCC_CLK        RCC_AHBPERIPH_DMA1
	#define SPIx_Rx_DMA_Channel     DMA1_Channel2
	#define SPIx_Rx_DMA_FLAG        DMA1_FLAG_TC2
	#define SPIx_Tx_DMA_Channel     DMA1_Channel3
	#define SPIx_Tx_DMA_FLAG        DMA1_FLAG_TC3

	#define FLASH_CS_HIGH()         GPIO_SetBits(W25QXX_CS_Port, W25QXX_CS_Pin)
	#define FLASH_CS_LOW()          GPIO_ResetBits(W25QXX_CS_Port, W25QXX_CS_Pin)

#elif defined(AT_START_F415_V1_0)

	#define FLASH_SPI               SPI1
	#define SPIx_RCC_CLK            RCC_APB2PERIPH_SPI1
	#define SPIx_GPIO_RCC_CLK       RCC_APB2PERIPH_GPIOA 
	#define SPIx_PIN_NSS            GPIO_Pins_4
	#define SPIx_PORT_NSS           GPIOA	

	#define W25QXX_CS_Pin 	SPIx_PIN_NSS
	#define W25QXX_CS_Port 	SPIx_PORT_NSS
		
	#define SPIx_PIN_SCK            GPIO_Pins_5
	#define SPIx_PORT_SCK           GPIOA
	#define SPIx_PIN_MISO           GPIO_Pins_6
	#define SPIx_PORT_MISO          GPIOA
	#define SPIx_PIN_MOSI           GPIO_Pins_7
	#define SPIx_PORT_MOSI          GPIOA

	#define SPIx_DMA                DMA1
	#define SPIx_DMA_RCC_CLK        RCC_AHBPERIPH_DMA1
	#define SPIx_Rx_DMA_Channel     DMA1_Channel2
	#define SPIx_Rx_DMA_FLAG        DMA1_FLAG_TC2
	#define SPIx_Tx_DMA_Channel     DMA1_Channel3
	#define SPIx_Tx_DMA_FLAG        DMA1_FLAG_TC3

	#define FLASH_CS_HIGH()         GPIO_SetBits(SPIx_PORT_NSS, SPIx_PIN_NSS)
	#define FLASH_CS_LOW()          GPIO_ResetBits(SPIx_PORT_NSS, SPIx_PIN_NSS)

#endif
	
typedef enum
{
	SPI_FREE = 0,									// 空闲状态
	SPI_READ,										// 读数据状态
	SPI_WRITE,										// 写数据状态
}SPI_State_InitTypeDef;

//--------------------------------------------------------------------------------------
// SPI时钟分频
#define SPI_PREC_2			(uint8_t)0x01
#define SPI_PREC_4			(uint8_t)0x02
#define SPI_PREC_8			(uint8_t)0x03
#define SPI_PREC_16			(uint8_t)0x04
#define SPI_PREC_32			(uint8_t)0x05
#define SPI_PREC_64 		(uint8_t)0x06
#define SPI_PREC_128		(uint8_t)0x07
#define SPI_PREC_256		(uint8_t)0x08

//--------------------------------------------------------------------------------------
// 若是想一次读更多的数据，下面这两个值都要修该
#define RX_LEN 			256							// 发送数据缓存区长度 
#define TX_LEN          256							// 接收数据缓存区长度
#define SPI2_DR_ADDR    (uint32_t)0x4000380C 		// SPI DR寄存器的地址，可通过查看SPI2一步一步得到
//#define USE_DMA_TRANS								// 使用DMA读写SPI

//--------------------------------------------------------------------------------------
extern SPI_State_InitTypeDef SPI_STATE;             // spi读写状态
extern uint8_t SPI_RX_BUFFER[RX_LEN];				// spi接收命令缓存区
extern uint8_t SPI_TX_BUFFER[TX_LEN];				// spi发送命令缓存区

//--------------------------------------------------------------------------------------
// 普通模式读写spi函数，用于发送命令
extern uint8_t SPI_ReadWriteByte(uint8_t data);
// spi速度设置
extern void set_spi_speed(uint8_t speed_mode);
// spi初始化
extern void spi_init(void);

// 使用dma
#ifdef USE_DMA_TRANS
// DMA初始化
static void spi_dma_init(void);
#endif 
uint8_t SPI2_ReadWriteByte(uint8_t TxData);
// 读写数据
extern void spi_trans(uint8_t *rx_buf, uint8_t *tx_buf, uint16_t length);

#endif
/***************************************************************************************
**  End Of File
***************************************************************************************/
