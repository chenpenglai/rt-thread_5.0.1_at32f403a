#include "usart.h"	 
#include "stdio.h"

///************* 支持printf *************/
//#pragma import(__use_no_semihosting) 
////标准库需要的支持函数 
//struct __FILE 
//{
//int handle; 
//}; 

//FILE __stdout; 
////定义_sys_exit()以避免使用半主机模式 
//void _sys_exit(int x) 
//{
//x = x; 
//}
////重定义fputc函数 
//int fputc(int ch, FILE *f)
//{
//	#if  defined(AT_START_F413_V1_0)
// 		while(SET!=USART_GetFlagStatus(USART3,USART_FLAG_TRAC));
//		USART_SendData(USART3,ch);
//		
//	#elif  defined(AT_START_F413_V1_0_gateway_4g)
// 		while(SET!=USART_GetFlagStatus(UART4,USART_FLAG_TRAC));
//		USART_SendData(UART4,ch);
//	
//	#elif defined(AT_START_F415_V1_0)
//	 	while(SET!=USART_GetFlagStatus(USART2,USART_FLAG_TRAC));
//		USART_SendData(USART2,ch);
//	#endif
//		return ch;
//} 

//u8 USART_RX_BUF[USART_REC_LEN] __attribute__ ((at(0X20001000)));//钡Μ絯侥,程USART_REC_LEN竊,癬﹍0X20001000.    
u16 USART_RX_STA=0;			//钡Μ篈夹癘	  
u16 USART_RX_CNT=0;			//钡Μ竊计	   
  

void UART3_Init(u32 bound)
{
	GPIO_InitType GPIO_InitStructure;
	USART_InitType USART_InitStructure;
	NVIC_InitType NVIC_InitStructure;
		RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOB  , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1PERIPH_USART3 , ENABLE);//ㄏUSART1GPIOA牧
	
	//USART3_TX   GPIOA.9 
	GPIO_InitStructure.GPIO_Pins = GPIO_Pins_10; //PA.9
	GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//確ノ崩块
	GPIO_Init(GPIOB, &GPIO_InitStructure);//﹍てGPIOA.9

	//USART3_RX	  GPIOA.10
	GPIO_InitStructure.GPIO_Pins = GPIO_Pins_11;//PA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//疊块
	GPIO_Init(GPIOB, &GPIO_InitStructure);//﹍てGPIOA.10  

	//Usart1 NVIC 皌竚
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;//穖纔3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//纔3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ硄笵ㄏ
	NVIC_Init(&NVIC_InitStructure);	//沮﹚把计﹍てVIC盚竟

	//USART ﹍て砞竚
	USART_InitStructure.USART_BaudRate = bound;//﹃猧疭瞯
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//8计沮Α
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//氨ゎ
	USART_InitStructure.USART_Parity = USART_Parity_No;//礚案喷
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//礚祑ン计沮瑈北
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//Μ祇家Α

	USART_Init(USART3, &USART_InitStructure); //﹍て﹃1
	USART_INTConfig(USART3, USART_INT_RDNE, ENABLE);//秨币﹃钡い耞
	USART_Cmd(USART3, ENABLE);                    //ㄏ﹃1 
}
void UART2_Init(u32 bound)
{
	GPIO_InitType GPIO_InitStructure;
	USART_InitType USART_InitStructure;
	NVIC_InitType NVIC_InitStructure;
		RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOA  , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1PERIPH_USART2 , ENABLE);//ㄏUSART1GPIOA牧
	
		//配置RX2口
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_PU;
		GPIO_InitStructure.GPIO_Pins = GPIO_Pins_3;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
		
		//配置TX2口
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_InitStructure.GPIO_MaxSpeed=GPIO_MaxSpeed_2MHz;
		GPIO_InitStructure.GPIO_Pins = GPIO_Pins_2;
		GPIO_Init(GPIOA, &GPIO_InitStructure);


	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;//穖纔3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//纔3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ硄笵ㄏ
	NVIC_Init(&NVIC_InitStructure);	//沮﹚把计﹍てVIC盚竟


	USART_InitStructure.USART_BaudRate = bound;//﹃猧疭瞯
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//8计沮Α
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//氨ゎ
	USART_InitStructure.USART_Parity = USART_Parity_No;//礚案喷
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//礚祑ン计沮瑈北
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//Μ祇家Α

	USART_Init(USART2, &USART_InitStructure); //﹍て﹃1
	USART_INTConfig(USART2, USART_INT_RDNE, ENABLE);//秨币﹃钡い耞
	USART_Cmd(USART2, ENABLE);                    //ㄏ﹃1 
}
void UART4_Init(u32 bound)
{
	GPIO_InitType GPIO_InitStructure;
	USART_InitType USART_InitStructure;
	NVIC_InitType NVIC_InitStructure;
		RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOC  , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1PERIPH_UART4 , ENABLE);//ㄏUSART1GPIOA牧
	
		//配置RX2口
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_PU;
		GPIO_InitStructure.GPIO_Pins = GPIO_Pins_11;
		GPIO_Init(GPIOC, &GPIO_InitStructure);
		
		//配置TX2口
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_InitStructure.GPIO_MaxSpeed=GPIO_MaxSpeed_2MHz;
		GPIO_InitStructure.GPIO_Pins = GPIO_Pins_10;
		GPIO_Init(GPIOC, &GPIO_InitStructure);


	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;//穖纔3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//纔3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ硄笵ㄏ
	NVIC_Init(&NVIC_InitStructure);	//沮﹚把计﹍てVIC盚竟


	USART_InitStructure.USART_BaudRate = bound;//﹃猧疭瞯
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//8计沮Α
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//氨ゎ
	USART_InitStructure.USART_Parity = USART_Parity_No;//礚案喷
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//礚祑ン计沮瑈北
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//Μ祇家Α

	USART_Init(UART4, &USART_InitStructure); //﹍て﹃1
	USART_INTConfig(UART4, USART_INT_RDNE, ENABLE);//秨币﹃钡い耞
	USART_Cmd(UART4, ENABLE);                    //ㄏ﹃1 
}
void UART_Init(u32 bound)
{
	#if defined(AT_START_F413_V1_0)
		UART3_Init(bound);
	#elif defined(AT_START_F413_V1_0_gateway_4g)
		UART4_Init(bound);
	#elif defined(AT_START_F415_V1_0)
		UART2_Init(bound);
	#endif
}
