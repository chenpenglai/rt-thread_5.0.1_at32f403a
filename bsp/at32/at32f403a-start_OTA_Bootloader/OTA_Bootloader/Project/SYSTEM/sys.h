#ifndef __SYS_H
#define __SYS_H	
#include "at32f4xx.h"

//以下為彙編函數
void WFI_SET(void);		//執行WFI指令
void INTX_DISABLE(void);//關閉所有中斷
void INTX_ENABLE(void);	//開啟所有中斷
void MSR_MSP(u32 addr);	//設置堆棧地址

#endif
