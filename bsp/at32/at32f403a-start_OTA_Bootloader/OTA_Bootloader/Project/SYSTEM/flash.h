#ifndef __FLASH_H
#define __FLASH_H
#include "sys.h"  
#include "at32f4xx.h"

//#define FLASH_SIZE	(256*1024)
//#define APP_SIZE		(200*1024)
//#define OTA_SIZE		(16*1024)
//#define PAGE_SIZE		(2*1024)

//#define FLASH_APP_ADDR			(u32)(FLASH_BASE+OTA_SIZE)  						// APP Area: 0x08004000~0x0807FFFF


/*		改成从外部flash拷贝		*/
//#define FLASH_BKP_ADDR			(u32)(FLASH_BASE+(FLASH_SIZE/2))  			// BKP Area: 0x08080000~0x080FFFFF
//#define FLASH_BKP_END_ADDR	(u32)(FLASH_BKP_ADDR+APP_SIZE)

void Flash_App_Test_Write(void);
void FLASH_Update(void);
extern u8 g_bFlashStatus;
void Flash_Write(u32 wAddr, u32 *pBuffer, u16 hwNum2Write);
void Flash_Read(u32 wAddr, u32 *pBuffer, u16 hwNum2Read);
#endif

