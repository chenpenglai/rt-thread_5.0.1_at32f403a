#ifndef __GLOBAL_H
#define __GLOBAL_H
#include "stdio.h"
#include "string.h"
#include "stdint.h"
#include "stdbool.h"
#include "w25qxx.h"
int reboot() ;
#define flash_w(addr,buf,size) EN25QXX_Write(buf,addr,size)
#define flash_r(addr,buf,size)  EN25QXX_Read(buf,addr,size)

//#define CYCLE_UPGRADE
//#define TEST_UPGRADE				//（调试功能 生产后需关闭）测试烧录功能（将当前APP区拷贝到备份区，然后擦除APP区，再将备份区烧录到APP区。由此可验证烧录功能）
#define BACKUP_OLD_APP				//备份旧的APP（用于升级失败回滚）
#define USE_IWDG						//开启独立看门狗
//#define OUTPUT_BURN_NEW_APP_CHECK_BUFF				//（调试功能 生产后需关闭）将APP从外部flash拷贝到芯片内的 源数据和读出的数据 通过串口输出 用于检查
//#define OUTPUT_BACKUP_OLD_APP_CHECK_BUFF			//（调试功能 生产后需关闭）将旧的APP从芯片内拷贝到外部flash的 源数据和读出的数据 通过串口输出 用于检查
//#define USE_EN_ID				//使用加密id 企业代码 校验功能，

#define USE_PRINTF						//开启printf
#ifndef USE_PRINTF
	#define printf //
#endif



#define SHELL_USART_TX_BUF_SIZE 	200
#define SHELL_USART_RX_BUF_SIZE 	200

#if defined(AT_START_F413_V1_0)


#define SHELL_USART_CFG 					"18n1fpu1abp+"//"48n1fpu1b9p+"		//调试口 主机串口
#define SHELL_USART_BUAD					115200							//初始化波特率，运行后会自动修改成 ef_get_env_int("usart_baud")

#define SLV_USART_CFG 						"28n1fpu1a1p+"//"18n1fpu1abp+"		//从机 设备串口		e偶校验 n无校验 
#define SLV_USART_BUAD						19200
#define SLV_ADDR 									250			//modbus从机地址
#elif defined(AT_START_F413_V1_0_gateway_4g)


#define SHELL_USART_CFG 					"18n1fpu1abp+"//"48n1fpu1b9p+"		//调试口 主机串口
#define SHELL_USART_BUAD					115200							//初始化波特率，运行后会自动修改成 ef_get_env_int("usart_baud")

#define SLV_USART_CFG 						"28n1fpu1a1p+"//"18n1fpu1abp+"		//从机 设备串口		e偶校验 n无校验 
#define SLV_USART_BUAD						19200
#define SLV_ADDR 									250			//modbus从机地址

#elif defined(AT_START_F413_V1_0_gateway_new)

#define SHELL_USART_CFG 					"38n1fpu1afp+"
#define SHELL_USART_BUAD					115200							//初始化波特率，运行后会自动修改成 ef_get_env_int("usart_baud")

#define SLV_USART_CFG 						"48n1fpu1afp+"//"18n1fpu1a8p+"		//从机 设备串口		e偶校验 n无校验 
#define SLV_USART_BUAD						19200
#define SLV_ADDR 									250			//modbus从机地址


#endif
//#define	SHELL_RUN_IN_IRQ		//shell在中断运行
	
#define ASSERT(EXPR)                                                       \
if (!(EXPR))                                                                  \
{                                                                             \
    printf("(%s) has assert failed at %s.\n", #EXPR, __FUNCTION__);         \
    while (1);                                                                \
}
void get_mcu_uid(uint8_t* uid);
#ifdef NEW_BOOT_PFS
	#define VERSION    	"V1.0.7_P"
#else
	#define VERSION    	"V1.0.7"
#endif
/*
	更新说明：
	
	V1.0.1
		flash.c内读写片内flash函数for循环内长度/4（因为传入的缓存区是u32，解决烧录失败的问题）
	
	V1.0.2
		完善测试烧录功能
		
	V1.0.3
		去掉所有printf打印
		
	V1.0.4
		搬入框架内的串口功能和 modbus功能		
		完成加密id的功能
		添加MD5程序 未调用

	V1.0.5
		添加看门狗 防止升级死机
	
	V1.0.6
		w25qxx_init内添加300ms延时，拉高WP引脚

	V1.0.7
		由于app内扩展内存224K 修改了启动文件，这里也被修改  MOV32   R0, #0x20001000



*/



void delay_ms(uint32_t t);

#endif

