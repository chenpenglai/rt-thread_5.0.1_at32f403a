/***************************************************************************************
* Copyright (c) 2018,汇邦电子
* All rights reserved.
*
* 文件名称：main.c
* 文件摘要：spi驱动程序
* 当前版本：1.0
* 作    者: 张建
* 完成日期：2018年5月25日
***************************************************************************************/
#include "w25qxx_spi.h"
#include "func_iwdg.h"

uint8_t SPI_RX_BUFFER[RX_LEN];							// 发送命令缓存区
uint8_t SPI_TX_BUFFER[TX_LEN];							// 接收命令缓存区
SPI_State_InitTypeDef SPI_STATE = SPI_FREE;				// spi状态
	void w25qxx_WR_Byte(unsigned char dat)
	{
		while (SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_TE) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
		{
			;
		}	
		SPI_I2S_TxData(FLASH_SPI, dat); //通过外设SPIx发送一个数据

		while (SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_BUSY) != RESET) //检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
			;
		}
	}

	void w25qxx_WR_Buf(unsigned char *buf,unsigned int n)
	{
		//HAL_SPI_Transmit(&hspi1, buf, n, 100);
		int i;
		for(i=0;i<n;i++)
		{
			while (SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_TE) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
			{
				;
			}	
			SPI_I2S_TxData(FLASH_SPI, *buf++); //通过外设SPIx发送一个数据
		}
		
		while (SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_BUSY) != RESET) //检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
			;
		}
	}
	#define SPI_TX_Err 0x01
	#define SPI_RX_Err 0x02
	uint8_t SPI2_ReadWriteByte(uint8_t TxData)
	{
		
		uint8_t perr;
		u32 retry=0;				 	
		while (SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_TE) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
		{
			retry++;
			if(retry>200){
				perr |= SPI_TX_Err;
				return 0;
			}
		}
		iwdg_free();
		SPI_I2S_TxData(FLASH_SPI, TxData); //通过外设SPIx发送一个数据
		retry=0;

		while (SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_RNE) == RESET) //检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
			retry++;
			if(retry>65536){
				perr |= SPI_RX_Err;
				return 0;
			}
		}	  	
		iwdg_free();	
		return SPI_I2S_RxData(FLASH_SPI); //返回通过SPIx最近接收的数据					    
	}
	
	
	
	
	
/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:		2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    spi一般模式读写一个数据
** 入口参数:		data：要写入的数据
** 返 回 值:    读到的数据
***************************************************************************************/
//uint8_t SPI_ReadWriteByte(uint8_t data)
//{
//	
////	 SPI_I2S_TxData(SPI1, data); //通过外设SPIx发送一个数据						    
////		return SPI_I2S_RxData(SPI1); //返回通过SPIx最近接收的数据					   
//	
//	return SPI1_ReadWriteByte(data,0);
//	
////	while ((SPI2->SR & 2) == 0);						// 等待发送区空
////	SPI2->DR = data;									// 通过外设SPIx发送一个byte  数据
////	while ((SPI2->SR & 1) == 0);						// 等待接收完一个byte
////	return SPI2->DR;									// 返回通过SPIx最近接收的数据
//}          

#ifdef USE_DMA_TRANS // 如果定义为DMA传输方式，进行DMA初始化 
/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:		2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    DMA初始化
** 入口参数:    无
** 返 回 值:    无
***************************************************************************************/
static void spi_dma_init(void)
{
	RCC->AHB1ENR |= 1 << 21;// DMA1时钟使能
	__HAL_RCC_DMA1_CLK_ENABLE();

	while (DMA1_Stream3->CR & 0X01);					// 等待DMA可配置 
	while (DMA1_Stream4->CR & 0X01);					// 等待DMA可配置 

	HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 2, 0);		// dma接收中断优先级
	HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);				// 使能DMA接收中断

// 	// RX
	DMA1_Stream3->PAR = SPI2_DR_ADDR;					// DMA外设地址
	DMA1_Stream3->M0AR = (uint32_t)SPI_RX_BUFFER;		// DMA 存储器0地址
	DMA1_Stream3->NDTR = 20;							// DMA 传输数据长度
	DMA1_Stream3->CR  = 0;								// 先全部复位CR寄存器值 
	DMA1_Stream3->CR &= ~(1 << 6);						// 外设到存储器模式
	DMA1_Stream3->CR |= 0 << 8;							// 非循环模式(即使用普通模式)
	DMA1_Stream3->CR |= 0 << 9;							// 外设非增量模式
	DMA1_Stream3->CR |= 1 << 10;						// 存储器增量模式
	DMA1_Stream3->CR |= 0 << 11;						// 外设数据长度:8位
	DMA1_Stream3->CR |= 0 << 13;						// 存储器数据长度:8位
	DMA1_Stream3->CR |= 1 << 16;						// 中等优先级
	DMA1_Stream3->CR |= 0 << 21;						// 外设突发单次传输
	DMA1_Stream3->CR |= 0 << 23;						// 存储器突发单次传输
	DMA1_Stream3->CR |= (uint32_t)0 << 25;				// 通道选择
	DMA1_Stream3->CR |= 1 << 4;							// 开接收完成中断
	// TX
	DMA1_Stream4->PAR = SPI2_DR_ADDR;					// DMA外设地址
	DMA1_Stream4->M0AR = (uint32_t)SPI_TX_BUFFER;		// DMA 存储器0地址
	DMA1_Stream4->NDTR = 20;							// DMA 传输数据长度
	DMA1_Stream4->CR  = 0;								// 先全部复位CR寄存器值 
	DMA1_Stream4->CR |= 1 << 6;							// 存储器到外设模式
	DMA1_Stream4->CR |= 0 << 8;							// 非循环模式(即使用普通模式)
	DMA1_Stream4->CR |= 0 << 9;							// 外设非增量模式
	DMA1_Stream4->CR |= 1 << 10;						// 存储器增量模式
	DMA1_Stream4->CR |= 0 << 11;						// 外设数据长度:8位
	DMA1_Stream4->CR |= 0 << 13;						// 存储器数据长度:8位
	DMA1_Stream4->CR |= 1 << 16;						// 中等优先级
	DMA1_Stream4->CR |= 0 << 21;						// 外设突发单次传输
	DMA1_Stream4->CR |= 0 << 23;						// 存储器突发单次传输
	DMA1_Stream4->CR |= (uint32_t)0 << 25;				// 通道选择
//	DMA1_Stream4->CR |= 1 << 4;							// 开发送完成中断

	DMA1->LIFCR |= (1 << 27);							// 清除传输完成标志
	DMA1->HIFCR |= (1 << 5);
}
#endif /* USE_DMA_TRANS */

/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:		2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    spi速度设置函数
** 入口参数:    SPI_PREC_2      // 2分频
				SPI_PREC_4	    // 4分频
				SPI_PREC_8	    // 8分频
				SPI_PREC_16	    // 16分频
				SPI_PREC_32     // 32分频
				SPI_PREC_64     // 64分频
				SPI_PREC_128    // 128分频
				SPI_PREC_256    // 256分频
** 返 回 值:    无
***************************************************************************************/
void set_spi_speed(uint8_t speed_mode)
{
//	SPI2->CR1 &= 0xFF87;								// 关闭SPI,清零3-5位
//	switch (speed_mode)
//	{
//	case SPI_PREC_2:	SPI2->CR1 |= 0 << 3; break;
//	case SPI_PREC_4:	SPI2->CR1 |= 1 << 3; break;
//	case SPI_PREC_8:	SPI2->CR1 |= 2 << 3; break;
//	case SPI_PREC_16:	SPI2->CR1 |= 3 << 3; break;
//	case SPI_PREC_32:	SPI2->CR1 |= 4 << 3; break;
//	case SPI_PREC_64:	SPI2->CR1 |= 5 << 3; break;
//	case SPI_PREC_128:	SPI2->CR1 |= 6 << 3; break;
//	case SPI_PREC_256:	SPI2->CR1 |= 7 << 3; break;
//	default: break;
//	}
//	SPI2->CR1 |= 1 << 6; 								// 打开SPI
}

#if defined(AT_START_F413_V1_0)
void func_w25qxx_msp_init(void)
{
		GPIO_InitType GPIO_InitStructure;
		SPI_InitType   SPI_InitStructure;
		
		RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_GPIOA|RCC_APB2PERIPH_GPIOD | RCC_APB2PERIPH_GPIOB | RCC_APB2PERIPH_GPIOC | RCC_APB2PERIPH_SPI1, ENABLE);
		
		GPIO_InitStructure.GPIO_Pins = W25QXX_CS_Pin;
		GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
		GPIO_Init(W25QXX_CS_Port, &GPIO_InitStructure);
		
		
		GPIO_PinsRemapConfig(GPIO_Remap01_SPI1,ENABLE);
		
		GPIO_InitStructure.GPIO_Pins = GPIO_Pins_3 | GPIO_Pins_5;
		GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		
		
		SPI_DefaultInitParaConfig(&SPI_InitStructure);
		SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;
		SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;
		SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;
		SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
		SPI_InitStructure.SPI_CPHA = SPI_CPHA_2EDGE;
		SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;
		SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_16;	//SPI_MCLKP_8;
		SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
		SPI_InitStructure.SPI_CPOLY = 7;
		SPI_Init(SPI1, &SPI_InitStructure);
		
		SPI_Enable(SPI1, ENABLE);
}
#elif defined(AT_START_F413_V1_0_gateway_4g)

void func_w25qxx_msp_init(void)
{
	  GPIO_InitType GPIO_InitStructure;
  SPI_InitType  SPI_InitStructure;

  RCC_APB2PeriphClockCmd(SPIx_RCC_CLK | SPIx_GPIO_RCC_CLK, ENABLE);
  RCC_AHBPeriphClockCmd(SPIx_DMA_RCC_CLK, ENABLE);
		
  /* Configure SPI_FLASH pins*/
  GPIO_InitStructure.GPIO_Pins =  SPIx_PIN_MOSI;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPIx_PORT_MOSI, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pins = SPIx_PIN_MISO;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(SPIx_PORT_MISO, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pins = SPIx_PIN_NSS;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
  GPIO_Init(SPIx_PORT_NSS, &GPIO_InitStructure);  

  GPIO_InitStructure.GPIO_Pins =  SPIx_PIN_SCK;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPIx_PORT_SCK, &GPIO_InitStructure);

  FLASH_CS_HIGH();
  /* SPI_FLASH configuration ------------------------------------------------------*/
  SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2EDGE;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
  SPI_InitStructure.SPI_CPOLY = 0;
  SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
  SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;
  SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_8;
  SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;
  SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;
  SPI_Init(FLASH_SPI, &SPI_InitStructure);

  /* Enable SPI module */
  SPI_Enable(FLASH_SPI, ENABLE);
}
#elif  defined(AT_START_F413_V1_0_gateway_new)
void func_w25qxx_msp_init(void)
{
	 GPIO_InitType GPIO_InitStructure;
  SPI_InitType  SPI_InitStructure;
 
	
	RCC_APB1PeriphClockCmd( SPIx_RCC_CLK, ENABLE);
  RCC_APB2PeriphClockCmd( SPIx_GPIO_RCC_CLK, ENABLE);
  RCC_AHBPeriphClockCmd(SPIx_DMA_RCC_CLK, ENABLE);
		
  /* Configure SPI_FLASH pins*/
  GPIO_InitStructure.GPIO_Pins =  SPIx_PIN_MOSI;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPIx_PORT_MOSI, &GPIO_InitStructure);
	
  GPIO_InitStructure.GPIO_Pins = SPIx_PIN_MISO;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(SPIx_PORT_MISO, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pins = SPIx_PIN_NSS;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
  GPIO_Init(SPIx_PORT_NSS, &GPIO_InitStructure);  

	GPIO_InitStructure.GPIO_Pins = W25QXX_WP_Pin;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
  GPIO_Init(W25QXX_WP_Port, &GPIO_InitStructure); 
	GPIO_SetBits(W25QXX_WP_Port, W25QXX_WP_Pin);
	
	
  GPIO_InitStructure.GPIO_Pins =  SPIx_PIN_SCK;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPIx_PORT_SCK, &GPIO_InitStructure);

  FLASH_CS_HIGH();
  /* SPI_FLASH configuration ------------------------------------------------------*/
  SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2EDGE;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
  SPI_InitStructure.SPI_CPOLY = 0;
  SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
  SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;
  SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_8;
  SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;
  SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;
  SPI_Init(FLASH_SPI, &SPI_InitStructure);

  /* Enable SPI module */
  SPI_Enable(FLASH_SPI, ENABLE);
}
#elif defined(AT_START_F415_V1_0)

void func_w25qxx_msp_init(void)
{
	  GPIO_InitType GPIO_InitStructure;
  SPI_InitType  SPI_InitStructure;

  RCC_APB2PeriphClockCmd(SPIx_RCC_CLK | SPIx_GPIO_RCC_CLK, ENABLE);
  RCC_AHBPeriphClockCmd(SPIx_DMA_RCC_CLK, ENABLE);
		
  /* Configure SPI_FLASH pins*/
  GPIO_InitStructure.GPIO_Pins =  SPIx_PIN_MOSI;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPIx_PORT_MOSI, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pins = SPIx_PIN_MISO;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(SPIx_PORT_MISO, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pins = SPIx_PIN_NSS;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz;  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
  GPIO_Init(SPIx_PORT_NSS, &GPIO_InitStructure);  

  GPIO_InitStructure.GPIO_Pins =  SPIx_PIN_SCK;
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_10MHz; 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPIx_PORT_SCK, &GPIO_InitStructure);

  FLASH_CS_HIGH();
  /* SPI_FLASH configuration ------------------------------------------------------*/
  SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2EDGE;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
  SPI_InitStructure.SPI_CPOLY = 0;
  SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
  SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;
  SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_8;
  SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;
  SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;
  SPI_Init(FLASH_SPI, &SPI_InitStructure);

  /* Enable SPI module */
  SPI_Enable(FLASH_SPI, ENABLE);
}
#endif

/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:		2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    spi初始化
** 入口参数:    无
** 返 回 值:    无
***************************************************************************************/
void spi_init(void)
{
	func_w25qxx_msp_init();
}

#ifdef USE_DMA_TRANS // 如果定义为DMA传输 
/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:		2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    spi_dma传输函数
** 入口参数:		rx_buf：接收缓存区
				tx_buf：发送缓存区
				length: 数据长度
** 返 回 值:    无
***************************************************************************************/
void spi_trans(uint8_t *rx_buf, uint8_t *tx_buf, uint16_t length)
{
	// RX
	DMA1_Stream3->CR &= ~(1 << 0);						// 关闭DMA传输 
	DMA1_Stream3->NDTR = length;						// DMA 传输数量
	DMA1_Stream3->CR |= (1 << 10);						// 内存地址自增
	DMA1_Stream3->M0AR = (uint32_t)rx_buf;				// 接收缓存区
	// TX			
	DMA1_Stream4->CR &= ~(1 << 0);						// 关闭发送通道
	DMA1_Stream4->NDTR = length;						// 数据长度
	DMA1_Stream4->CR |= (1 << 10);						// 内存地址自增
	DMA1_Stream4->M0AR = (uint32_t)tx_buf;				// 发送缓存区
			
	SPI2->DR;											// 读取一次DR，使其清空
	while ((SPI2->SR & 1 << 1) == 0);					// 等待发送区空
			
	DMA1_Stream3->CR |= 1 << 0;							// 打开通道
	DMA1_Stream4->CR |= 1 << 0;
}

#else  // 使用一般的传输模式进行读写 
/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:		2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    spi一般模式传输函数
** 入口参数:	rx_buf：接收缓存区
				tx_buf：发送缓存区
				length: 数据长度
** 返 回 值:    无
***************************************************************************************/
//void spi_trans(uint8_t *rxbuf, uint8_t *txbuf, uint16_t length)
//{
//	while (length--) *rxbuf++ = SPI_ReadWriteByte(*txbuf++);
//}

#endif /* USE_DMA_TRANS */    
/***************************************************************************************
** 创    建:	ZhangJian
** 创建日期:	2018-5-10 8:50:17
** 版    本:	1.0
** 描    述:    dma接收完成中断
** 入口参数:
** 返 回 值:
***************************************************************************************/
//void DMA1_Stream3_IRQHandler(void)
//{
//	if((DMA1->LISR>>27) & 0x0001)
//	{
//		DMA1_Stream3->CR&=~(1<<0);						// 关接收通道
//		DMA1->LIFCR |= (1 << 27); 						// 清除传输完成标志
//		DMA1_Stream4->CR&=~(1<<0);						// 关发送通道
//		DMA1->HIFCR |= (1 << 5); 						// 清除传输完成标志
//		W25QXX_CS_H;									// 片选拉高
//		SPI_STATE = SPI_FREE;
//	}
//}

/***************************************************************************************
**  End Of File
***************************************************************************************/
