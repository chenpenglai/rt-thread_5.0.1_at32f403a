#define FUNC_DIRTRAN_MAIN
#include "func_dirtran.h"
#ifdef USE_SLV_GATEWAY
#define SET_BIT(x, y)   (x |= (1<<y))      // 特定位置1 
#define CLEAR_BIT(x, y) (x &= ~(1<<y))   // 特定位清0 

void func_dirtran_init(FUNC_DIRTRAN_T *p,unsigned char slv,USART_T *pu0,USART_T *pu1)
{
	FUNC_DIRTRAN_PORT *pc;
	
	p->ch0.pu=pu0;
	p->ch1.pu=pu1;
	p->ch0.buf_size=FUNC_DIRTRAN_BUF_SIZE;
	p->ch1.buf_size=FUNC_DIRTRAN_BUF_SIZE;
	
	pc=&p->ch0;
#ifdef MODBUS_COIL_OFS_EN
	pc->md.coil_ofs=MODBUS_SLV_COIL_OFS;
#endif

#ifdef MODBUS_INPUT_OFS_EN
	pc->md.input_ofs=MODBUS_SLV_INPUT_OFS;
#endif
	pc->md.slv=slv;
	pc->md.phr=HR;
	pc->md.hr_n=countof(HR);
//-----------------------------------------
	pc=&p->ch1;
#ifdef MODBUS_COIL_OFS_EN
	pc->md.coil_ofs=MODBUS_SLV_COIL_OFS;
#endif

#ifdef MODBUS_INPUT_OFS_EN
	pc->md.input_ofs=MODBUS_SLV_INPUT_OFS;
#endif
	pc->md.slv=slv;
	pc->md.phr=HR1;
	pc->md.hr_n=countof(HR1);
}





__weak void func_dirtran_after_rec(FUNC_DIRTRAN_T *p)
{
	return;
}

__weak void func_dirtran_before_send(FUNC_DIRTRAN_T *p)
{
	return;
}




enum{
	FUNC_DIRTRAN_CH0_STEP_START,
	FUNC_DIRTRAN_CH0_STEP_MODBUS_SEND,
	FUNC_DIRTRAN_CH0_STEP_WAIT_RESPONSE,
};

enum{
	FUNC_DIRTRAN_CH1_STEP_START,
	FUNC_DIRTRAN_CH1_STEP_MODBUS_SEND,
	FUNC_DIRTRAN_CH1_STEP_WAIT_RESPONSE,
};

__weak void func_dirtran_success(MODBUS_T *p)
{
	(void)p;
}
	unsigned short p_hr[100];
unsigned short *p_hr_bak;
unsigned short t_hr[100];

void func_dirtran_exec(FUNC_DIRTRAN_T *p)
{
	FUNC_DIRTRAN_PORT *pc,*pc0,*pc1;
	USART_T*pu;
	unsigned int i;
	unsigned char k;

//========================================================================================================
	pc0=&p->ch0;			//和上位机（网关）通讯
	pc1=&p->ch1;			//和下位机（灯）通讯
//--------------------------------------
//回环测试
//	if(usart_chk_frame(pc0->pu))
//	{
//		usart_copy_usart(pc0->pu,pc0->pu,1);
//	}
//	return;
//========================================================================================================
	//通道0操作
	pc=&p->ch0;
	pu=pc->pu;
	
	switch(pc->step)
	{
		case FUNC_DIRTRAN_CH0_STEP_START:
			if(usart_chk_frame(pu))
			{
				if(pu->rx_buf[0] != pc->md.slv)
				{
//					#define STR "hello\n"
//					strcpy(pc1->buf,STR);
//					pc1->buf_n=strlen(STR);
					pc1->buf_n=usart_recv(pu,pc1->buf,pc1->buf_size);			//接收到上位机数据。不是给网关的，透传给下面设备
					left_ms_set(&pc->tm,1000);									//设透传超时
					pc->step=FUNC_DIRTRAN_CH0_STEP_WAIT_RESPONSE;
//					pu->r_s=0;
//					pu->r_e=0;
				}
				else
				{
					if(modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e)>0)			//是本地从机地址，MODBUS解析
					{
						left_ms_set(&pc->tm,FUNC_DIRTRAN_SEND_DELAY);								//解析成功
						if(0X88 == HR[16])
						{
							HR[16] = 0;
							for(k = 0;k<16;k++)
							{		
								p_hr[k] &= ~HR[k];
							}
							for(k = 0;k<16;k++)
							{
								HR1[k]=HR[k] = p_hr[k];

							}
						}
						pc->step=FUNC_DIRTRAN_CH0_STEP_MODBUS_SEND;											//下一步
					}
//					else{
//						printf("err %d\n",modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e));
//						
//					}
				}
				usart_rx_rst(pc->pu);
			}
			
			break;
		case FUNC_DIRTRAN_CH0_STEP_MODBUS_SEND:
			if(left_ms(&pc->tm)==0)
			{
				i=modbus_slv_send(&pc->md,pu->tx_buf);
				if(i)
				{
					usart_send_start(pu,i);
				}
				pc->step=FUNC_DIRTRAN_CH0_STEP_START;
			}
			break;
		case FUNC_DIRTRAN_CH0_STEP_WAIT_RESPONSE:
			if(pc->buf_n)
			{

//				if((pc->buf[0]==pu->rx_buf[0])&&(pc->buf[1]==pu->rx_buf[1]))
//				{
					usart_send(pc->pu,pc->buf,pc->buf_n);					//收到透传的回应
					pc->buf_n=0;
					pc->step=FUNC_DIRTRAN_CH0_STEP_START;
//				}
			}
			else if(left_ms(&pc->tm)==0)
			{
	
				pc->step=FUNC_DIRTRAN_CH0_STEP_START;					//透传超时
			}
			else				//没有收到子设备响应，也没到超时
			{
				if(usart_chk_frame(pc->pu))
				{
					if(pc->pu->rx_buf[0] != pc->md.slv)				//接收到最近的一条上位机数据。不是给网关的，透传给下面设备
					{
						//printf("clear\n");
						usart_rx_rst(pc->pu);		//直接清除上位机下发的数据，为了下次只下发最新数据到子设备
						//pc1->buf_n=
						//usart_recv(pu,pc1->buf,pc1->buf_size);			
						//printf("recv a packet %d\n",pc1->buf_n);
					}
				}
			
			}
			break;
	}
//========================================================================================================
	//通道1操作
	pc=&p->ch1;
	pu=pc->pu;
	switch(pc->step)
	{
		case FUNC_DIRTRAN_CH1_STEP_START:
			if(usart_chk_recbyte(pu))
			{
				i= MyRnd() % 10;		//取0-9随机数
				i += FUNC_DIRTRAN_CHKBUS_DELAY;
				left_ms_set(&pc->tm,i);		//如果期间收到其他接收，缓10mS再操作
			}
			if(usart_chk_frame(pu))
			{
				if(pu->rx_buf[0] == pc->md.slv)
				{
					if(modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e)>0)			//是本地从机地址，MODBUS解析
					{
						left_ms_set(&pc->tm,FUNC_DIRTRAN_SEND_DELAY);			//解析成功						
						
						HR1[(HR1[20]-1)/16] = SET_BIT(HR1[(HR1[20]-1)/16],(((HR1[20]-1)%16)));
						memcpy(HR,HR1,20);
						memcpy(p_hr,HR1,20);
						
						pc->step=FUNC_DIRTRAN_CH1_STEP_MODBUS_SEND;				//下一步
					}
//					else{
//						printf("err %d\n",modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e));
//						
//					}
				}
				else if(pc0->step==FUNC_DIRTRAN_CH0_STEP_WAIT_RESPONSE)
				{
					//printf("接收一包\n");
					pc0->buf_n=usart_recv(pu,pc0->buf,pc0->buf_size);			//不是本地从机地址，且主机在等待回复，透传
					
					
				}
				usart_rx_rst(pc->pu);
			}
			else if(pc->buf_n && left_ms(&pc->tm)==0)
			{
						
				usart_send(pc->pu,pc->buf,pc->buf_n);			//透传给子设备
				//printf("send a packet %d\n",pc->buf_n);
				pc->buf_n=0;

			}
			break;
		case FUNC_DIRTRAN_CH1_STEP_MODBUS_SEND:
			if(left_ms(&pc->tm)==0)
			{
				i=modbus_slv_send(&pc->md,pu->tx_buf);
				if(i)
				{
					usart_send_start(pu,i);
				}
				pc->step=FUNC_DIRTRAN_CH1_STEP_START;
			}
			break;
	}
//================================================================
}

#endif

