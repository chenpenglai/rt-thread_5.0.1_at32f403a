/************************************Copyright (c)**************************************

            单片机编程助手，帮您从繁杂的重复劳动中脱离出来，专心做您的创新与业务                  

                                    wwww.mcuabc.cn

----------------------------------------文件信息-----------------------------------------
文 件 名: 
创 建 人: 
email:	mcusys@qq.com
创建日期: 
描    述: 
----------------------------------------版本信息-----------------------------------------
 版    本: V1.00
 说    明: 
----------------------------------------------------------------------------------------
Copyright(C) xsy,ccmcu,zcw 2019/06/18
All rights reserved
****************************************************************************************/

#define APP_SHELL_MAIN
#include "app_shell.h"
#include "func_shell.h"
#include "includes.h"
#include "global.h"

/***** 应用描述 这里输入对应用描述 *****/

//+++++User_Code_define Begin+++++//

//#define FUNC_SHELL_TM_PERIOD 500
 
//-----User_Code_define End-----//
/******************************** 步骤函数定义 ******************************/
 
//步骤函数之前执行的函数
void app_shell_before_step(APP_SHELL_T *p)
{
//+++++User_Code_app_shell_before_step Begin+++++//
//-----User_Code_app_shell_before_step End-----//
}

//步骤 0:APP_SHELL_STEP_INIT    初始化
void app_shell_step_init(APP_SHELL_T *p)
{
//+++++User_Code_app_shell_step_init Begin+++++//
	p->step=APP_SHELL_STEP_RUN;	
		
//-----User_Code_app_shell_step_init End-----//
}
 

//步骤 1:APP_SHELL_STEP_SHELL_ON    
void app_shell_step_run(APP_SHELL_T *p)
{
//+++++User_Code_app_shell_step_shell_on Begin+++++//	
//		if(usart_chk_frame(shell_struct.pu))
//		{
			if(shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt - 1] == '\n' && shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt - 2] == '\r') {
					shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt - 2] = '\0';		//去掉 "\r\n" 方便匹配尾部字符串
					if(0==shell_search_handle(&shell_struct.shell_recv_buf[0], shell_struct.shell_recv_cnt - 2))
					{
						//delay_ms(20);			//输出响应需要一点时间
					}
					shell_struct.shell_recv_cnt = 0;
					
			}
//		}
//-----User_Code_app_shell_step_shell_on End-----//
}
		

//步骤函数之后执行的函数
void app_shell_after_step(APP_SHELL_T *p){
//+++++User_Code_app_shell_after_step Begin+++++//
//-----User_Code_app_shell_after_step End-----//
}

/******************************** 实例主体函数定义 ********************************/



void app_shell_init(APP_SHELL_T *p)
{
	USART_T *pu=usart_open(SHELL_USART_CFG,SHELL_USART_BUAD,SHELL_USART_TX_BUF_SIZE,SHELL_USART_RX_BUF_SIZE);	//全双工，无RS485控制，中断优先级1
	shell_init(pu);
}
void app_shell(APP_SHELL_T *p)
{
//+++++User_Code_app_shell_Entry Begin+++++//
//-----User_Code_app_shell_Entry End-----//
	//步骤函数之前执行的函数
	app_shell_before_step(p);

	switch(p->step)
	{
	//步骤 0 : 初始化
	case APP_SHELL_STEP_INIT:
		app_shell_step_init(p);
		break;

	//步骤 1 : 
	case APP_SHELL_STEP_RUN:
		app_shell_step_run(p);
		break;


	}
	//步骤函数之后执行的函数
	app_shell_after_step(p);
//+++++User_Code_app_shell_Exit Begin+++++//
//-----User_Code_app_shell_Exit End-----//
}

//+++++User_Code_end Begin+++++//
//-----User_Code_end End-----//
