#ifndef __FUNC_IWDG_H
#define __FUNC_IWDG_H	    
#include "at32f4xx.h"
#include "global.h"
void iwdg_init(void);

#ifdef USE_IWDG
	#define iwdg_free() IWDG_ReloadCounter()		//ι��
#else
	#define iwdg_free()
#endif

#endif
