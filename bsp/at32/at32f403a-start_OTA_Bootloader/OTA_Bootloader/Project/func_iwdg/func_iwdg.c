#include "func_iwdg.h"		  

void iwdg_init(void){
	#ifdef USE_IWDG
		/* IWDG timeout equal to 250 ms (the TMReout may varies due to LSI frequency dispersion) */
		/* Enable write access to IWDG_PSC and IWDG_RLD registers */
		IWDG_KeyRegWrite(IWDG_KeyRegWrite_Enable);
		
		/* IWDG counter clock: LSI/32 */
		IWDG_SetPrescaler(IWDG_Psc_32);

		/* Set counter reload value to obtain 250ms IWDG TMReOut.
			 Counter Reload Value = 250ms/IWDG counter clock period
														= 250ms / (32/LSI)
														= 0.25s / (32/LsiFreq)
														= LsiFreq/(32 * 4)
														= LsiFreq/128
		*/
		__IO uint32_t LsiFreq = 40000;
		IWDG_SetReload(LsiFreq / 128);

		/* Reload IWDG counter */
		IWDG_ReloadCounter();
		MCUDBG_PeriphDebugModeConfig(MCUDBG_IWDG_STOP,ENABLE);			//开启debug模式 停止看门狗
		/* Enable IWDG (the LSI oscillator will be enabled by hardware) */
		IWDG_Enable();
	#else
		printf("boot 未开启看门狗\n");
	#endif
}


