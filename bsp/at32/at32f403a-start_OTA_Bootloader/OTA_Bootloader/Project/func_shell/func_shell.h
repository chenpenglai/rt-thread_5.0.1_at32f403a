#ifndef __func_shell_H
#define __func_shell_H
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "includes.h"



#define SHELL_RECV_SIZE 150			//shell 接收缓存区大小

//extern USART_T* shell_usart;
//#define HANDLE_GROUP_SIZE_MAX 15			//最大shell命令条数，一条shell 3个指针，占用内存12字节,20*12=240
typedef int (*fun)(uint8_t argc,char **argv);			//argc：argv参数的个数，argv[0]=命令 argv[1]=Key argv[2]=Value

struct CMD_LIST
{
    char *cmd;		//命令字符串
    fun funs;						//函数地址
    const char  *desc;	//描述
};

#define CAT_TO_STR(x,y) (#x#y)			//拼接为字符串
#define TO_STR(x)		#x							//转字符串

#define SECTION(level)  __attribute__((used,__section__(".fn_cmd."level)))
#define CMD_START_EXPORT(func,func_s) const struct CMD_LIST cmd_fn_##func SECTION("0.end") = {TO_STR(func),func,func_s}
#define CMD_EXPORT(func,func_s) const struct CMD_LIST cmd_fn_##func SECTION("1") = {TO_STR(func),func,TO_STR(func_s)}
#define CMD_END_EXPORT(func,func_s) const struct CMD_LIST cmd_fn_##func SECTION("1.end") = {TO_STR(func),func,func_s}

typedef enum _SHELL_MODE {
    TRAN_MODE=0,	//透传模式
    SHELL_MODE			//命令模式
} _SHELL_MODE;

//记录数据来源，实时修改shell输出端
typedef enum _RECV_SOURCE {
    RECV_FROM_USART=0,	//数据接收来自串口
    RECV_FROM_NET,			//数据接收来自网络
} _RECV_SOURCE;

typedef struct _shell_struct {
//    _SHELL_MODE shell_mode;
//    _RECV_SOURCE recv_source;
		USART_T* pu;
		uint16_t shell_recv_cnt ;
		char shell_recv_buf[SHELL_RECV_SIZE];
//		uint8_t recv_cmd_flag;
	
} _shell_struct;
extern _shell_struct shell_struct;
typedef struct __cmd_handle {
    const char *cmd;
    int (*func)(uint8_t* buf,uint16_t len);
    const char *desc;			//命令使用说明
} _cmd_handle;
//extern _cmd_handle handle_group[];

//#define USART_GET_CHAR(read_char)	shell_get_char(read_char,RECV_FROM_USART);
//#define NET_GET_CHAR(read_char)	shell_get_char(read_char,RECV_FROM_NET);
extern uint8_t recv_cmd_flag;
int shell_search_handle(char *buf, uint16_t len)   ;
void shell_init(USART_T *pu);
void shell_get_char(char rd_char);			//放到命令字节接收处，用于shell接收数据

#define shell_printf printf


#endif
