#include "func_shell.h"
//#include "easyflash.h"
#include "includes.h"
#include "stdarg.h"

#include "func_tools.h"
#include "global.h"

_shell_struct shell_struct;
const static struct CMD_LIST *CmdList;

/*命令函数段起始位置*/
int cmd_start(uint8_t argc, char **argv)
{
    return 0;
}
CMD_START_EXPORT(cmd_start, "cmd_start");

/*命令函数段结束位置*/
int cmd_end(uint8_t argc, char **argv)
{
    return 0;
}
CMD_END_EXPORT(cmd_end, "cmd_end");

//uint16_t net_printf(const char *format, ...)      //通过网络调试打印
//{
//    char buf[strlen(format) + 50];
//    uint16_t len = 0;
//    va_list args;
//    va_start(args, format);

//    len = vsnprintf(buf, sizeof(buf), format, args);
//    va_end(args);
//    eth_net_send_data(buf, len);
//    va_end(args);
//    return len;
//}

////通过记录的数据源 自动切换打印输出端
//void shell_printf(const char *format, ...) {
//    /****************		直接切换调试输出 网络或串口		*******************/
//    //printf(format);
//    /****************		直接切换调试输出 网络或串口		*******************/

////    if(shell_struct.recv_source == RECV_FROM_USART)
//        printf(format);
////    else
////        net_printf(format);				//方便网口配置时，结果通过网口输出

//}

//int shell_mode(uint8_t argc, char **argv) {
//    uint8_t mode = atoi(argv[1]);

//    //sscanf(buf,"shell_mode %d",&mode);

//    if(mode == TRAN_MODE || mode == SHELL_MODE)
//    {
//        shell_struct.shell_mode = mode;
////        ef_set_env_int("shell_mode", shell_struct.shell_mode);
//    }
//    if(mode == SHELL_MODE)
//        return 0;			//串口将响应OK
//    return -1;
//    //ef_save_env();
//}
//CMD_EXPORT(shell_mode, 0: tran 1: cmd);		//必须在第一个位置

//int stop_tmr(uint8_t* buf,uint16_t len){
//	TMR_INTConfig(TMR1, TMR_INT_Overflow, DISABLE);
//	//TMR_Cmd(TMR1,DISABLE );
//}
//CMD_EXPORT(stop_tmr,stop_tmr);

//int start_tmr(uint8_t* buf,uint16_t len){
//	TMR_INTConfig(TMR1, TMR_INT_Overflow, ENABLE);
//	//TMR_Cmd(TMR1, ENABLE);
//}
//CMD_EXPORT(start_tmr,start_tmr);
//extern void time1_init( void ) ;
//int time_init(uint8_t* buf,uint16_t len){
//	time1_init();
//}
//CMD_EXPORT(time_init,start_tmr);


CMD_EXPORT(reboot, reboot);


int help(uint8_t argc, char **argv) {

    const struct CMD_LIST *cmd_ptr;
    CmdList = &cmd_fn_cmd_start;
    CmdList++;
    shell_printf("commands:\n");
    for (cmd_ptr = CmdList; cmd_ptr < &cmd_fn_cmd_end; cmd_ptr++)
    {
        shell_printf("%-10s - %s\n", cmd_ptr->cmd, cmd_ptr->desc);
    }
    shell_printf("\n");
		return -1;
}
CMD_EXPORT(help, all cmd);



//int test_iwdg(uint8_t argc, char **argv) {
//    while(1);
//}
//CMD_EXPORT(test_iwdg,test_iwdg);


//_cmd_handle handle_group[20];// =
//{
//		SHELL_CMD(shell_mode,"设置串口模式, 0:透传模式, 1:命令模式"),		//必须在第一个位置
//		SHELL_CMD(reboot,"重启"),
//		SHELL_CMD(printenv,"所有参数"),
//		SHELL_CMD(setenv,"设置参数"),
//		SHELL_CMD(saveenv,"保存参数"),
//		SHELL_CMD(resetenv,"恢复默认参数"),
//		SHELL_CMD(getvalue,"获取参数值"),
//		SHELL_CMD(delenv,"删除参数"),
//		SHELL_CMD(help,"所有命令"),

//		SHELL_CMD(start_update,"升级"),
//		SHELL_CMD(end_update,"结束升级"),
//添加命令
//};


int shell_search_handle(char *buf, uint16_t len)       //遍历回调
{
    const struct CMD_LIST *cmd_ptr;
    uint8_t argc = 0;			//参数个数
    char* argv[3] = {NULL};		//参数
    CmdList = &cmd_fn_cmd_start;
    CmdList++;
//    if(shell_struct.shell_mode == TRAN_MODE)
//    {
//        cmd_ptr = CmdList;
//        if (!strncmp(cmd_ptr->cmd, (char*)&buf[0], strlen(cmd_ptr->cmd)))
//        {
//            if (cmd_ptr->funs != NULL && (buf[strlen(cmd_ptr->cmd)] == ' ' || buf[strlen(cmd_ptr->cmd)] == '\0'))
//            {
//                shell_printf("\n%s\n", buf);
//                argv[argc++] = cmd_ptr->cmd;
//                int a = str_search((char*)buf, " ", 1);
//                if (a != -1) {
//                    argv[argc++] = &buf[a + 1];
//                }
//                if(0 == cmd_ptr->funs(argc, argv))
//                    shell_printf("OK\r\n");
//                return 0;
//            }
//        }
//    }
//    else
//    {
        for (cmd_ptr = CmdList; cmd_ptr < &cmd_fn_cmd_end; cmd_ptr++)
        {
            if (!strncmp(cmd_ptr->cmd, &buf[0], strlen(cmd_ptr->cmd)))
            {
                if (cmd_ptr->funs != NULL && (buf[strlen(cmd_ptr->cmd)] == ' ' || buf[strlen(cmd_ptr->cmd)] == '\0'))
                {
                    //shell_printf("\n%s\n", buf);
                    argv[argc++] = cmd_ptr->cmd;
                    /************		以空格为间隔符，解析参数		***********/
                    int a = str_search((char*)buf, " ", 1);
                    if (a != -1) {
                        argv[argc++] = &buf[a + 1];
                        int b = str_search((char*)buf, " ", 2);
                        if (b != -1) {
                            buf[b] = '\0';      //将空格置零
                            argv[argc++] = &buf[b + 1];
                        }
                    }
                    /*************************/
//										__disable_irq();
                    if(0 == cmd_ptr->funs(argc, argv))
                        shell_printf("OK\n");
//										__enable_irq();
                    return 0;
                }
            }
        }
        shell_printf("not found: %s\n", buf);
//    }
//		printf("handler\n");
    return -1;
}
void shell_get_char(char rd_char)
{
    //shell_struct.recv_source = recv_source;
    shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt++] = rd_char;
    shell_struct.shell_recv_cnt %= SHELL_RECV_SIZE;
		printf("%c",rd_char);
	#ifdef SHELL_RUN_IN_IRQ
		if(shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt - 1] == '\n' && shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt - 2] == '\r') {
        shell_struct.shell_recv_buf[shell_struct.shell_recv_cnt - 2] = '\0';		//去掉 "\r\n" 方便匹配尾部字符串
				shell_search_handle(&shell_struct.shell_recv_buf[0], shell_struct.shell_recv_cnt - 2);
        shell_struct.shell_recv_cnt = 0;
    }
	#endif
		

}


//static UINT8 CmdSize = 0;

/*命令函数初始化*/
//void SerialCmdInit(void)
//{
//	const struct CMD_LIST *cmd_ptr;
//	CmdList = &cmd_fn_cmd_start;
//	CmdList++;
//	for (cmd_ptr = CmdList; cmd_ptr < &cmd_fn_cmd_end;cmd_ptr++)
//	{
//        /*这里如果用于初始化的话可以使用下面这种方式来执行初始化函数，因为我的应用并不是用于初始
//         化，所以就没有进行函数调用。
//         （*cmd_ptr->fun）();
//         */
//		//(*cmd_ptr->funs)(0,0);
//		if(handle_group_size>=HANDLE_GROUP_SIZE_MAX){
//			printf("ERR shell add failed, max size %d\n",HANDLE_GROUP_SIZE_MAX);
//			return;
//		}
//		handle_group[handle_group_size].cmd=cmd_ptr->cmd;
//		handle_group[handle_group_size].func=cmd_ptr->funs;
//		handle_group[handle_group_size].desc=cmd_ptr->desc;
//
//
//		handle_group_size++;
//		//CmdSize++;
//	}
//}
void shell_init(USART_T *pu) {
    //SerialCmdInit();
		shell_struct.pu=pu;
    //shell_struct.recv_source = RECV_FROM_USART;

    //设备每次启动默认为透传模式，解决了：配置完成后reboot设备，还要再次发送 shell_mode 0命令才能正常使用
    //shell_struct.shell_mode = TRAN_MODE; //ef_get_env_int("shell_mode");


}

