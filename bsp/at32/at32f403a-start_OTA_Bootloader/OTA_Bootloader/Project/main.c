#include "at32f4xx.h"
#include "ota.h"
#include "func_usart.h"
#include "usart.h"
#include "func_tools.h"
#include "func_en_id.h"
#include "main_msp.h"
#include "IAP.h"
#include "global.h"
#include "w25qxx.h"
#include "stdio.h"
#include "func_master_dirtran_slv.h"
#include "app_shell.h"
#include "func_shell.h"
#include "func_iwdg.h"

_upgrade_info upgrade_info;

void test_upgrade(){
	uint8_t buf1[8];
	uint8_t buf2[20];
	uint32_t size=1024*100;
	buf1[0]=NEED_UPGRADE_FLAG;
	buf1[1]=0;
	buf1[2]=0;
	buf1[3]=(uint8_t)(size>>8);
	buf1[4]=(uint8_t)(size);
	flash_w(UPGRADE_FLAG_ADDR,buf1,20);
	flash_r(UPGRADE_FLAG_ADDR,buf2,20);
	for(int i=0;i<20;i++)
		printf("%02X",buf2[i]);
	
}


//#define CODE_1_ADDR 0X8006000
//const static uint8_t ss[4] __attribute__((at(CODE_1_ADDR)))={0xff,0xff,0xff,0xff}; 



int main(void)
{
	Target_ReaetInit();
	delay_us_set_k(1000);
	func_usart_init();
	app_shell_init(&g_app_shell);
	//UART_Init(115200);
	printf("BootLoader software version: %s\n",VERSION);
#ifdef USE_EN_ID
	USART_T *slv_pu=usart_open(SLV_USART_CFG,SLV_USART_BUAD,SHELL_USART_TX_BUF_SIZE,SHELL_USART_RX_BUF_SIZE);	//全双工，无RS485控制，中断优先级1
	func_master_dirtran_slv_init(&g_func_dirtran,SLV_ADDR,slv_pu);
#endif
	w25qxx_init();
//	en_id_init(); 
	iwdg_init();				//喂狗
	//hexdump("----",(uint8_t*)ss,sizeof(ss));
//	uint32_t mcu_id[3];
//	read_mcu_id(mcu_id);
//	hexdump("-- mcu_id --",(uint8_t*)mcu_id,sizeof(mcu_id));
	
  for(;;)
  {
		iwdg_free();
		get_upgrade_flag_info(&upgrade_info);
//		upgrade_info.is_upgrade=1;
#ifdef TEST_UPGRADE			//强制执行升级
		upgrade_info.is_upgrade=NEED_UPGRADE_FLAG;
		upgrade_info.app_size=100*1024;		
#endif
		check_upgrade(&upgrade_info);
//		print_intern_flash_app_hex(FLASH_APP1_ADDR,upgrade_info.app_size);
//		print_extern_flash_app_hex(BK_APP_SIZE,upgrade_info.app_size);	

#ifdef USE_EN_ID
			//clr_en_id_e_code();						//清除校验
			while(cmp_en_id()==1)		//en_id读出来验证
			{
					//验证不通过，一直在while
					func_master_dirtran_slv_exec(&g_func_dirtran);			//接收modbus指令
					app_shell(&g_app_shell);			//只有等待解码器的期间 运行shell
					
			}
#endif

#ifdef BACKUP_OLD_APP
		for(;;){
			//test_burn();
#endif
			iwdg_free();
			if(((*(volatile uint32_t*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
			{
				//delay_ms(1000);
				
				printf("jumping app...\n\n");				
				Run_APP();//iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码	
			}
			else
			{
				printf("-----> app is not working\n");
				if(upgrade_info.have_old_app==HAVE_OLD_APP){
					upgrade_info.is_upgrade=SUCCESSED_UPGRADE_FLAG;			
					check_upgrade(&upgrade_info);
				}else{
					while(1){
						
						printf("no app...\n");	
						delay_ms(500);
					}
					
				}
			}

#ifdef BACKUP_OLD_APP
		}
#endif
  }
//  if(FLASH_Read_Upgrade_Flag() == RESET)
//		Run_APP();
//  while(1)
//	{
//		APP_Upgrade_Handle();
//	}
}

