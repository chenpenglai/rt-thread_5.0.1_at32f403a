#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "device_env.h"

#include "byte_tools.h"

#include <arpa/inet.h>
#include <netdev.h> 
//#include "solar_app.h"


extern uint8_t g_mode ;

/*
  为了后期页面设置 参数权限 增删改查 做统一接口
	RW 代表此变量可读权限
	_S 变量保存位置以及获取
	
	
	! 代表获取此变量是有条件限制 v:50,min:30;max90 例如值是50 最小值30 最大值90
	
	统一转换存储类型为十进制字符串类型
*/
struct device_env
{
	unsigned  char        bAccessType;
	unsigned  short       bDataType;
	unsigned   int        size;    
  char  name[20];
	uint16_t tag;
	int (*opt)(uint8_t,uint8_t *,int );
	char *tempBuf;
};


//struct rtu_run_para
//{
//	 uint8_t net_type;
//	 char *buf;
//};
#define OPT_WRITE 1
#define OPT_READ  2 
void opt_4g_imei(uint8_t cmd ,uint8_t *data, int len)
{
	
}

extern struct netdev *netdev_default;
uint8_t get_net_type(void)
{
		rt_kprintf("opt net type(%s)\n",netdev_default->name);
	  uint8_t value = 0;

		if(rt_strcmp(netdev_default->name, "BC26") == 0)
		{
		   value = 1;
		}
		else if(rt_strcmp(netdev_default->name, "air720") == 0 || rt_strcmp(netdev_default->name, "e0") == 0)
		{
			 value = 2;
		
		}
		else if(rt_strcmp(netdev_default->name, "W5500") == 0)
		{
			 value = 3;

		}
		else
		{
			value = 4;
		}	
	  return value;
}
int opt_net_type(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
	  *data = get_net_type();
		return 1;
	}
}
//#include "temp_humi_app.h"
int opt_temp(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		int  temp = 0;//get_temp()*10;
		rt_kprintf("opt read temp %d\n",temp);
		DecToBcd(temp,(char *)data,len);
		return 2;
	}
}

int opt_humi(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		int  humi = 0;//get_humi()*10;
		rt_kprintf("opt read humi %d\n",humi);
		DecToBcd(humi,(char *)data,len);
		return 2;
	}
}

int opt_tiltSta(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		int  angle = 0;//read_max_angle();
		DecToBcd(angle,(char *)data,len);
		return 1;
	}
}
//#include "solar_app.h"
//extern struct solarData solar_data;
int opt_solar_v(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		if(g_mode == 1)
		{
			//solar_data.solar_v = 1250;
		}
		//DecToBcd(solar_data.solar_v,(char *)data,len);
		return 1;
	}
}
int opt_solar_c(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{	
		if(g_mode == 1)
		{
			//solar_data.solar_c = 1360;
		}
		//DecToBcd(solar_data.solar_c,(char *)data,len);
		return 1;
	}
}
int opt_battery_v(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		if(g_mode == 1)
		{
			//solar_data.battery_v = 1250;
		}
		//DecToBcd(solar_data.battery_v,(char *)data,len);
		return 1;
	}
}
int opt_battery_c(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		if(g_mode == 1)
		{
					//solar_data.battery_c = 1360;
		}

		//DecToBcd(solar_data.battery_c,(char *)data,len);
		return 1;
	}
}
//
int opt_Hour15waterLevel(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		//DecToBcd(solar_data.battery_c,(char *)data,len);
		//rt_memcpy(data,one_hour_waterLevel,len);
		return 1;
	}
}
int opt_Hour15rainfall(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		//rt_memcpy((char *)data,one_hour_rainfall,len);
		return 1;
	}
}
int opt_curWateLevel(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		//DecToBcd(get_waterLevel_data(),(char *)data,len);
		return 1;
	}
}
#include "at_device.h"
//extern struct at_device_info *nb_read_info(void);
int opt_NBCSQ(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
		
//		struct at_device_info *info = nb_read_info();
		
		if(g_mode == 1)
		{
			//info->csq = 22;
		}
		
		
		//DecToBcd(info->csq,(char *)data,len);
		return 1;
	}
}
//extern struct at_device_info *ec20_read_info(void);
int opt_4GCSQ(uint8_t cmd ,uint8_t *data, int len)
{
	uint8_t value = 0;
	if(cmd == OPT_READ)
	{
//		struct at_device_info *info = ec20_read_info();
		//DecToBcd(info->csq,(char *)data,len);
		return 1;
	}
}

#define BIT_MASK(n) 1<<n         
#define RAINFALL_STATUS_BIT    0 
#define WATERLEVEL_STATUS_BIT  1 
#define DOOR_STATUS_BIT        3
#define FLASH_STATUS_BIT       4
#define CAMERA_STATUS_BIT      5 

#define BATTERY_STATUS_LOW     8
#define BATTERY_STATUS_HIGH    9 
#define TEMP_HUMI_STATUS       10
#include "stdbool.h"
static void set_status_bit(int status,uint8_t bit,bool flag)
{
		if(flag == true)
		{
			 status |= BIT_MASK(bit);
		}
		else
		{
			 status  &= ~BIT_MASK(bit);
		}
}
		
int opt_selfStatus(uint8_t cmd ,uint8_t *data, int len)
{
	  int status = 0;
	  if(cmd == OPT_READ)
		{
//			set_status_bit(status,WATERLEVEL_STATUS_BIT,isWaterLevelNormal());
//			set_status_bit(status,TEMP_HUMI_STATUS,isTempHumiNormal());
		//	set_status_bit(status,BATTERY_STATUS_LOW,isTempHumiNormal());
			DecToBcd(status,(char *)data,len);
		}
		return 1;
}

struct device_env env[] = {
			{ RW_S, BCD_DIS,         1,  "net_type" ,0xFF10,opt_net_type} ,
			{ RW_S, visible_string, 16 , "4GIMEI"   ,0xFF13} ,
			{ RW_S, visible_string, 20 , "4GICCID"  ,0xFF14} ,
			{ RW_S, visible_string, 30 , "hard_ver" ,0xFF15} , 
			{ RW  , BCD_DIS, 1 ,         "4GCSQ"    ,0xFF21 ,opt_4GCSQ},
			{ RW_S, HEX_DIS, 1 ,         "camSta"   ,0xFF29 } ,
			{ RW_S, HEX_DIS, 1 ,         "tiltSta"  ,0xFF26 ,opt_tiltSta} ,
			{ RW_S, HEX_DIS, 1 ,         "crashSta" ,0xFF27  }  ,
			{ RW_S, HEX_DIS, 1 ,         "diagnoseD",0xFF28 } ,
			{ RW_S, HEX_DIS, 5 ,         "longitude",0xFF30 } ,
			{ RW_S, HEX_DIS, 5 ,         "latitudes",0xFF31 } ,
			{ RW_S, BCD_DIS, 5 ,         "observe_time" , 0xF0F0} ,
			{ RW_S, N(7,3), 4 ,          "curWateLevel"   ,0x393B,opt_curWateLevel}  ,
			{ RW_S, BCD_DIS, 5 ,         "telemetryAddr"  ,0x0228} ,
			{ RW_S, HEX_DIS, 4 ,         "centralAddr"    ,0x0120}   ,
			{ RW_S, BCD_DIS,   1,         "workMode"       ,0x0C08}      ,
			{ RW_S, BCD_DIS, 2,          "passWord"       ,0x0310}      ,
			{ RW_S, N(2,0), 1,           "TimerMsgInterval",0x2008} ,
			{ RW_S, N(2,0), 1,           "addMsgInterval",0x2108} ,
			{ RW_S, N(2,0), 1,           "dayStartInterval",0x2208} ,
			{ RW_S, N(2,0), 1,           "addMsgrainfall_T",0x2708} ,
			{ RW_S, N(3,0), 2,           "addMsgTilt_T",0x4212} ,
			{ RW_S, N(3,1), 2,           "addMsgCrash_T",0x4312} ,
			{ RW_S, BCD_DIS, 4,          "selfStatus",0xFF00,opt_selfStatus},
			{ RW_S, BCD_DIS, 4,          "!save_interval"} ,
};


#include <flashdb.h>
#include <string.h>
extern struct fdb_kvdb _global_kvdb ;

/* by name find env*/
int find_env_by_name(char *get_name)
{
	int i=0;
	for(i = 0;i<sizeof(env)/sizeof(struct device_env);i++ )
	{
		if(rt_strcmp(env[i].name,get_name) == 0)
		{
			return i;
		}
	}
	if(i == sizeof(env)/sizeof(struct device_env))
	{
		rt_kprintf("not find env name(%s)\n", get_name); 
		return -1;
	}
}
/*by tag find env*/
int find_env_by_tag(uint16_t tag)
{
	int i=0;
	for(i = 0;i<sizeof(env)/sizeof(struct device_env);i++ )
	{
		if( env[i].tag == tag )
		{
			return i;
		}
	}
	if(i == sizeof(env)/sizeof(struct device_env))
	{
		rt_kprintf("not find device tag(%X)\n", tag); 
		return -1;
	}
}
int checkIsBCD(uint8_t *buf,int len)
{
	
}
int get_env_buf_by_struct(int index ,uint8_t *buf)
{
	if(env[index].opt != RT_NULL)
	{
		if(env[index].opt != RT_NULL)
		{
			env[index].opt(OPT_READ,buf,env[index].size);
			rt_kprintf("find tag(%s):",env[index].name); 
			for(int i=0;i<env[index].size;i++)
			{
				rt_kprintf("%02X",buf[i]);
			}
			rt_kprintf("\n");
			return env[index].size;
		}
	}else
	{
		return -1;
	}
}

int get_device_env_Bcd(char *key, char *dataBuf, uint8_t totalLen)
{
	 char * get_env = fdb_kv_get(&_global_kvdb, key);
	 int num = rt_strlen(get_env); //"003070"
	 char *tempBuf = rt_calloc(1,totalLen*2+1);
	 if(tempBuf == RT_NULL)
	 {
		 return -1;
	 }
	 rt_memset(tempBuf,'0',totalLen*2); 
	 //不足补位
	 if(num <= totalLen *2 ) 
	 {
		 rt_memcpy(tempBuf+(totalLen*2 - num),get_env,num);
	 }else
	 {
		 rt_memcpy(tempBuf,get_env,num);
	 }
	// rt_kprintf("envLen %d len %d str %s \n",num,totalLen,tempBuf);
	 StrToBCD(dataBuf,tempBuf,totalLen*2);
	// fomat_printf((uint8_t *)dataBuf,totalLen);
	 rt_free(tempBuf);
	 return 0;
}

int get_device_env_buf(char *get_name,uint8_t *buf,int len)
{

	int i ;
	for(i = 0;i<sizeof(env)/sizeof(struct device_env);i++ )
	{
		if(rt_strcmp(env[i].name,get_name) == 0)
		{
			if(env[i].opt != RT_NULL)
			{
				int ret = env[i].opt(OPT_READ,buf,env[i].size);
				rt_kprintf("find tag(%s):%s\n",env[i].name,buf); 
				return env[i].size;
			}
			if(env[i].bDataType == BCD_DIS || env[i].bDataType == HEX_DIS)
			{
				char * get_env = fdb_kv_get(&_global_kvdb, env[i].name);
				rt_kprintf("find name(%s):%s\n",env[i].name,get_env); 
				if(get_env == NULL)
				{
					rt_kprintf("not find env (%s)\n",env[i].name);
					return env[i].size;
				}else
				{
					if(env[i].size > len)
					{
						StrToHex(buf,get_env,len);
						rt_kprintf("warning get env size out \n");
						return len;
					}else
					{
						StrToHex(buf,get_env,env[i].size);
						return env[i].size;
					}
				}
			}
		}
	}
	if(i == sizeof(env)/sizeof(struct device_env))
	{
		rt_kprintf("not find device env(%s)\n",get_name); 
	}
	return len;

}

int get_device_env_buf_by_tag(uint16_t tag,uint8_t *buf,int len)
{
	int index = find_env_by_tag(tag);
	if(index == -1)
	{
		return -1;
	}
	//从结构体中读取数据
	int ret = get_env_buf_by_struct(index,buf);
	if(ret != -1)
	{
		return  ret;
	}
	
	if( env[index].bDataType == HEX_DIS)
	{
		char * get_env = fdb_kv_get(&_global_kvdb, env[index].name);
		rt_kprintf("find tag(%x)(%s):%s\n",tag,env[index].name,get_env); 
		if(get_env == NULL)
		{
			rt_kprintf("not find env (%s)\n",env[index].name);
			return env[index].size;
		}
		else
		{
			if(env[index].size > len)
			{
				StrToHex((char *)buf,get_env,len);
				rt_kprintf("warning get env size out \n");
				
				return len;
			}
			else
			{
				StrToHex((char *)buf,get_env,env[index].size);
				return env[index].size;
			}
		} 
	}
	else if(env[index].bDataType >= 0xFF || env[index].bDataType == BCD_DIS )
	{
		get_device_env_Bcd((char *)env[index].name,(char *)buf,env[index].size);
		rt_kprintf("find tag(%x) len(%d) (%s):",tag,env[index].size,env[index].name,buf); 
		
		for(int i=0;i<env[index].size;i++)
		{
			rt_kprintf("%02X ",(uint8_t)buf[i]);
		}
		rt_kprintf("\n");
		return env[index].size;
	}
	else if(env[index].bDataType == visible_string)
	{
		char * get_env = fdb_kv_get(&_global_kvdb, env[index].name);
		rt_kprintf("find tag(%x)(%s):%s\n",tag,env[index].name,get_env); 
		if(get_env == NULL)
		{
			rt_kprintf("not find env (%s)\n",env[index].name);
			return env[index].size;
		}else
		{
			if(env[index].size > rt_strlen(get_env))
			{
				rt_memset(buf,0,env[index].size);
				rt_memcpy(buf,get_env,rt_strlen(get_env));
				rt_kprintf("warning get env size out \n");
				return env[index].size;
			}else
			{
				rt_memcpy(buf,get_env,env[index].size);
				return env[index].size;
			}
		}
		
	}
	else
	{
		rt_kprintf("not support type %x \n",env[index].bDataType);
	}

	return len;

}




int get_device_env_int(char *get_name)
{
		char * get_env = fdb_kv_get(&_global_kvdb, get_name);
	  
		if(get_env == NULL)
		{
			rt_kprintf("not find env (%s) ready find ram\n",get_name);
			return 0;
		}else
		{
			if(get_name[0] == '!' )
			{
				int v=0;
				int min = 0;
				int max = 0;
				int ret = 0;
				//v:5,min:300;max9000
				sscanf(get_env, "v:%d,min:%d;max%d", &v,&min,&max);
				if(v<=min)
				{
					ret = min;
				}else if(v >= max)
				{
					ret = max;
				}
				else
				{
					ret = v;
				}
				//rt_kprintf("find name(%s):%s v:%d\n",get_name,get_env,ret); 
				return ret;
			}
			else
			{
				return atoi(get_env);
			}
		}
	return 0;
}


int set_device_env_buf_by_tag(uint16_t tag,uint8_t *buf)
{
	int i=0;
	
	for(i = 0;i<sizeof(env)/sizeof(struct device_env);i++ )
	{
		if( env[i].tag == tag )
		{
			if(env[i].bDataType == BCD_DIS || env[i].bDataType == HEX_DIS || env[i].bDataType >= 0xFF)
			{
				char * strBuf = rt_calloc(1,env[i].size*2+1);
				if(strBuf == RT_NULL)
				{
					rt_kprintf("no mem\n");
					return -1;
				}
				rt_memset(strBuf,0,env[i].size*2);
				
				HexToStr(strBuf,(char *)buf,env[i].size);
				strBuf[env[i].size*2] = '\0';
				rt_kprintf("set env tag(%x)(%s) len(%d):%s\n",tag,env[i].name,env[i].size,strBuf); 
				
				fdb_kv_set(&_global_kvdb, env[i].name,strBuf);
				
				rt_free(strBuf);
				return env[i].size;
			}
			else if(env[i].bDataType >= 0xFF)
			{
				
			}
			else
			{
				rt_kprintf("not support type %x \n",env[i].bDataType);
			}
		}
	}
	
	
	if(i == sizeof(env)/sizeof(struct device_env))
	{
		rt_kprintf("not find device tag(%X)\n", tag); 
		return -1;
	}
	return 0;

}


int set_device_env_buf(char *get_name,uint8_t *buf)
{
	int i=0;
	
	for(i = 0;i<sizeof(env)/sizeof(struct device_env);i++ )
	{
		if(rt_strcmp(env[i].name,get_name) == 0)
		{
			if(env[i].bDataType == BCD_DIS || env[i].bDataType == HEX_DIS || env[i].bDataType >= 0xFF)
			{
				char * strBuf = rt_calloc(1,env[i].size*2+1);
				if(strBuf == RT_NULL)
				{
					rt_kprintf("no mem\n");
					return -1;
				}
				rt_memset(strBuf,0,env[i].size*2);
				
				HexToStr(strBuf,(char *)buf,env[i].size);
				strBuf[env[i].size*2] = '\0';
				rt_kprintf("set env (%s) len(%d):%s\n",env[i].name,env[i].size,strBuf); 
				
				fdb_kv_set(&_global_kvdb, env[i].name,strBuf);
				
				rt_free(strBuf);
				return env[i].size;
			}
			else if(env[i].bDataType >= 0xFF)
			{
				
			}
			else
			{
				rt_kprintf("not support type %x \n",env[i].bDataType);
			}
		}
	}
	
	
	if(i == sizeof(env)/sizeof(struct device_env))
	{
		rt_kprintf("not find device tag(%X)\n", get_name); 
		return -1;
	}
	return 0;
}

int set_device_env_string(char *get_name,uint8_t *buf)
{
	int i=0;
	
	for(i = 0;i<sizeof(env)/sizeof(struct device_env);i++ )
	{
		if(rt_strcmp(env[i].name,get_name) == 0)
		{
			if(env[i].bDataType == visible_string)//字符串 copy
			{
				char * strBuf = rt_calloc(1,env[i].size+1);
				if(strBuf == RT_NULL)
				{
					rt_kprintf("no mem\n");
					return -1;
				}
				rt_memcpy(strBuf,buf,env[i].size);
				strBuf[env[i].size] = '\0';
				rt_kprintf("set env (%s) len(%d):%s\n",env[i].name,env[i].size,strBuf); 
				
				fdb_kv_set(&_global_kvdb, env[i].name,strBuf);
				
				rt_free(strBuf);
				return env[i].size;
			}
			else if(env[i].bDataType >= 0xFF)
			{
				
			}
			else
			{
				rt_kprintf("not support type %x \n",env[i].bDataType);
			}
		}
	}
	
	
	if(i == sizeof(env)/sizeof(struct device_env))
	{
		rt_kprintf("not find tag(%X)\n", get_name); 
		return -1;
	}
	return 0;
}



int set_device_env_int(char *get_name,int value)
{
		char env_buff[20]={0};
		sprintf(env_buff,"%d",value);
		rt_kprintf("set env (%s) :%s\n",get_name,env_buff); 
		fdb_kv_set(&_global_kvdb, get_name,env_buff);
	  return 0;
}

int set_device_env(char *name,char *buf)
{
	return fdb_kv_set(&_global_kvdb,name,buf);
}

char * get_device_env(char *name)
{
	return fdb_kv_get(&_global_kvdb,name);
}

int set_env(char *name,char *buf)
{
	return fdb_kv_set(&_global_kvdb,name,buf);
}

char * get_env(char *name)
{
	return fdb_kv_get(&_global_kvdb,name);
}


int get_env_int(char *name)
{
	return atoi(fdb_kv_get(&_global_kvdb,name));
}

void test_device_env()
{
  char buf[5];
	get_device_env_buf("test_station_num1",buf,5);
  hexdump(">>>>",buf,5);
}

MSH_CMD_EXPORT(test_device_env, test_device_env);    