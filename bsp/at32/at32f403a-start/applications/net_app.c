#include <global.h>
#include "at_device.h"
#include <at.h>
#include "easyflash.h"
#include "net_app.h"
#include "string.h"
#include "func_tools.h"
#include "time.h"
#include "sal_socket.h"
//#include "socket.h"
#include "rtthread.h"
//#include "app_power.h"
#define LOG_TAG "net_app"
//#include <rtdbg.h>
// struct send_msg msg;

//#define USING_KEEP_LINK			//TCP 连接保活包（目前的问题：只能为一个连接做保活 需要每个连接都创建该线程）

int net_reset(void);
time_ms_T check_link_tm;
struct net_trans  *net1 = RT_NULL;
struct net_trans  *net2 = RT_NULL;
struct net_trans  *net3 = RT_NULL;
struct net_trans  *net4 = RT_NULL;

//#define NET_DEBUG_MODE

//char ip_buf[20]= {0};

void is_first_start(){
		static uint8_t is_startup=0;
		if(is_startup==0)
		{
			is_startup=1;	
			rt_kprintf("the device is first startup\n");
//			set_life_alarm();				//设置周期心跳闹钟
//			if(DEFENCE==get_defence()) {		//布防状态下，打开周期性检测
//					set_period_check_alarm();
//			}
//			stop_recover_old_app_upload_result();
		}
}
//int sock_fd=-1;
//struct at_socket * net_at_sock;

static struct netdev * cur_net_dev = RT_NULL;
static int netdev_find_high_prio(struct net_trans *thiz,uint8_t mode)		//
{
	static int reboot_count=0;
	static int reboot_net = 0;
	int ex_result=-1;//=exchange_net(cur_net_dev);

	cur_net_dev = netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP);
	
	if(RT_NULL == cur_net_dev)
	{
	//	netdev_close_all();
		rt_kprintf("打开所有网卡\n");
		log_w("open all netdev\n");
		
		netdev_open_all();
	}
	else
	{
		rt_kprintf("启动网卡 %s\n",cur_net_dev->name);
		log_i("set up netdev: %s\n",cur_net_dev->name);
		
		if(netdev_is_up(cur_net_dev) == RT_FALSE)
		{
			netdev_set_up(cur_net_dev);
		}
		
	}
	
	while(RT_NULL == netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP))
	{
		#define DELAY_S 5		//延时间隔 5秒
		rt_kprintf("没有可用网络 次数：%d\n",reboot_count);
		log_e("no LINK_UP netdev num %d\n",reboot_count);
		rt_thread_delay(DELAY_S*1000);			//延时5秒
		reboot_count++;
		reboot_net++;
		if(reboot_net > (60*1)/DELAY_S)				//1分钟连不上网 重启模组
		{
			reboot_net = 0;
			netdev_close_all();
			rt_thread_delay(3000);
			netdev_open_all();
		}
		if(reboot_count > (60*5)/DELAY_S)		//5分钟连不上网 重启网关
		{
			
				log_e("<<<<< reconnect timeout 5 min, abort >>>>>\n");
				log_e("no netdev\n");
				reboot_count=0;
				reboot_net = 0;
				return -1;
				//reboot();
		}

	}
	printf("有可用网卡\n");
		reboot_count=0;
	//	if(ex_result!=0){
		cur_net_dev = netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP);
//	}
	if(RT_NULL == cur_net_dev)
	{
	//	netdev_close_all();	
		rt_kprintf("打开所有网卡\n");
		log_w("open all netdev\n");
	
		netdev_open_all();
	}
	else
	{
		rt_kprintf("启动网卡 %s\n",cur_net_dev->name);
		log_i("set up netdev: %s\n",cur_net_dev->name);
		
		if(netdev_is_up(cur_net_dev) == RT_FALSE)
		{
			netdev_set_up(cur_net_dev);
		}
	} //196kcta
	
	
	rt_kprintf("%s 使用网卡：%s\n",thiz->name,cur_net_dev->name);
	log_i("%s used netdev: %s\n",thiz->name,cur_net_dev->name);
	
//	if(rt_strcmp(cur_net_dev->name,"bc35")==0)
//	{
//		send_flag=1;
//		rt_kprintf("\n选择nb模组 连接aep\n");
//		memset(thiz->ip,0,20);
//		rt_memcpy(thiz->ip,get_env("nb_ip"),rt_strlen(get_env("nb_ip")));
//		thiz->port = get_env_int("nb_port"); 

//	}
//	if(rt_strcmp(cur_net_dev->name,"W5500")==0)
//	{
//		
//		//估计是配置和4G一样的ip和port
//		send_flag=0;
//		rt_thread_mdelay(10000);
//	}
	
//	if(rt_strcmp(cur_net_dev->name,"air720")==0)
//	{
////		send_flag=0;
//		memset(thiz->ip,0,20);
//		if(0==rt_strcmp(thiz->name,NET1_NAME))
//		{
//			rt_memcpy(thiz->ip,NET1_IP,rt_strlen(NET1_IP));
//			thiz->port = NET1_PORT; 
//		}
////		else if(0==rt_strcmp(thiz->name,NET2_NAME))			//第二个连接的参数
////		{
////			rt_memcpy(thiz->ip,NET2_IP,rt_strlen(NET2_IP));
////			thiz->port = NET2_PORT; 
////		}
//		rt_kprintf("选择4G模组：%s，直连平台 ip: %s:%d\n",thiz->name,thiz->ip,thiz->port);


//		rt_thread_mdelay(4000);				//多平台连接 需要创建多个socket 模块连接 需要间隔时间
//	}
		
	reboot_count=0;
	
	netdev_set_default(cur_net_dev);
	//*thiz=*cur_net_dev;
	//memcpy(thiz,cur_net_dev,sizeof(struct netdev));
	
	return 0;
}
static int netdev_find_by_name(struct net_trans *thiz,char* name,uint8_t mode)
{
	static int reboot_count=0;
	static int reboot_net = 0;
	int ex_result=-1;//=exchange_net(cur_net_dev);
	cur_net_dev = netdev_get_by_name(name);
	
	
	if(RT_FALSE==netdev_is_up(cur_net_dev))
	{
	//	netdev_close_all();
		rt_kprintf("打开网卡 %s\n",cur_net_dev->name);
		log_w("open netdev %s\n",cur_net_dev->name);
		netdev_set_up(cur_net_dev);
	}
	else
	{
		rt_kprintf("网卡已启动 %s\n",cur_net_dev->name);
		log_i("netdev %s already up\n",cur_net_dev->name);
	}
	
	while(RT_FALSE==netdev_is_up(cur_net_dev))
	{
		#define DELAY_S 5		//延时间隔 5秒
		rt_kprintf("网卡 %s 未启动 次数：%d\n",cur_net_dev->name,reboot_count);
		log_e("netdev %s no LINK_UP num %d\n",cur_net_dev->name,reboot_count);
		rt_thread_delay(DELAY_S*1000);
		reboot_count++;
		reboot_net++;
		if(reboot_net > (60*1)/DELAY_S)				//1分钟连不上网 重启模组
		{
			reboot_net = 0;
			
			netdev_set_down(cur_net_dev);
			netdev_low_level_set_link_status(cur_net_dev,RT_FALSE);
			
			rt_thread_delay(3000);
			netdev_set_up(cur_net_dev);
		}
		if(reboot_count > (60*5)/DELAY_S)		//5分钟连不上网 重启网关
		{
				
				log_e("<<<<< reconnect timeout 5 min, abort >>>>>\n");
				log_e("no netdev\n");
				reboot_count=0;
				reboot_net = 0;
				return -1;
				//reboot();
		}
		
	

	}
		reboot_count=0;
	//	if(ex_result!=0){
//		cur_net_dev = netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP);
//	}
	if(RT_FALSE==netdev_is_up(cur_net_dev))
	{
			rt_kprintf("打开网卡 %s\n",cur_net_dev->name);
			log_w("open netdev %s\n",cur_net_dev->name);
			netdev_set_up(cur_net_dev);
	}
	else
	{
			rt_kprintf("网卡已启动 %s\n",cur_net_dev->name);
			log_i("netdev %s already up\n",cur_net_dev->name);
	} 
	
	rt_kprintf("%s 使用网卡：%s\n",thiz->name,cur_net_dev->name);
	log_i("%s used netdev: %s\n",thiz->name,cur_net_dev->name);
	
//	if(rt_strcmp(cur_net_dev->name,"bc35")==0)
//	{
//		send_flag=1;
//		rt_kprintf("\n选择nb模组 连接aep\n");
//		memset(thiz->ip,0,20);
//		rt_memcpy(thiz->ip,get_env("nb_ip"),rt_strlen(get_env("nb_ip")));
//		thiz->port = get_env_int("nb_port"); 

//	}
//	if(rt_strcmp(cur_net_dev->name,"W5500")==0)
//	{
//		
//		//估计是配置和4G一样的ip和port
//		send_flag=0;
//		rt_thread_mdelay(10000);
//	}
	
//	if(rt_strcmp(cur_net_dev->name,"air720")==0)
//	{
////		send_flag=0;
//		memset(thiz->ip,0,20);
//		if(0==rt_strcmp(thiz->name,NET1_NAME))
//		{
//			rt_memcpy(thiz->ip,NET1_IP,rt_strlen(NET1_IP));
//			thiz->port = NET1_PORT; 
//		}
////		else if(0==rt_strcmp(thiz->name,NET2_NAME))			//第二个连接的参数
////		{
////			rt_memcpy(thiz->ip,NET2_IP,rt_strlen(NET2_IP));
////			thiz->port = NET2_PORT; 
////		}
//		rt_kprintf("选择4G模组：%s，直连平台 ip: %s:%d\n",thiz->name,thiz->ip,thiz->port);


//		rt_thread_mdelay(4000);				//多平台连接 需要创建多个socket 模块连接 需要间隔时间
//	}
		
	reboot_count=0;
	
	netdev_set_default(cur_net_dev);
	//*thiz=*cur_net_dev;
	//memcpy(thiz,cur_net_dev,sizeof(struct netdev));
	
	return 0;
}
	
bool net_open_finish(struct net_trans * thiz){
	return !thiz->open_busy;
	
}

void close_cur_netdev(struct net_trans * thiz)
{
//	rt_kprintf("关闭当前网卡 跳出\n");
//	return;
	
	//--------------------------------------
	
	rt_kprintf("关闭当前网卡\n");
//	if(updating_flag)
//	{
//		rt_kprintf("升级模式中 不关闭网卡\n");
//		return;
//	}
	
	
//	if(net1->sock_fd!=-1)
//	{
//		rt_kprintf("释放net1 socket: %d\n",net1->sock_fd);
//		closesocket(net1->sock_fd);
//		net1->sock_fd=-1;
//	}
//	if(net2->sock_fd!=-1)
//	{
//		rt_kprintf("释放net2 socket: %d\n",net2->sock_fd);

//		closesocket(net2->sock_fd);
//		net2->sock_fd=-1;
//	}
		
	if(cur_net_dev != RT_NULL)
	{
			log_e("close cur net name: %s\n",cur_net_dev->name);
			if(thiz->recv_thread != RT_NULL)
			{
					//rt_kprintf("删除 recv 线程\n");
					rt_thread_delete(thiz->recv_thread);
					thiz->recv_thread=RT_NULL;
				
			}
			if(thiz->recv_data!=RT_NULL)
			{
					rt_free(thiz->recv_data);
					thiz->recv_data=RT_NULL;
			}
			rt_thread_mdelay(200);

			netdev_set_down(cur_net_dev);
			netdev_low_level_set_link_status(cur_net_dev,RT_FALSE);
	}
}
void free_cur_netdev(struct net_trans * thiz)
{
//	rt_kprintf("关闭当前网卡 跳出\n");
//	return;
	
	//--------------------------------------
	
	rt_kprintf("释放当前连接 不关闭网卡\n");
//	if(updating_flag)
//	{
//		rt_kprintf("升级模式中 不关闭网卡\n");
//		return;
//	}
	
	
//	if(net1->sock_fd!=-1)
//	{
//		rt_kprintf("释放net1 socket: %d\n",net1->sock_fd);
//		closesocket(net1->sock_fd);
//		net1->sock_fd=-1;
//	}
//	if(net2->sock_fd!=-1)
//	{
//		rt_kprintf("释放net2 socket: %d\n",net2->sock_fd);

//		closesocket(net2->sock_fd);
//		net2->sock_fd=-1;
//	}
		
	if(cur_net_dev != RT_NULL)
	{
			log_e("free cur sock name: %s\n",cur_net_dev->name);
			if(thiz->recv_thread != RT_NULL)
			{
					//rt_kprintf("删除 recv 线程\n");
					rt_thread_delete(thiz->recv_thread);
					thiz->recv_thread=RT_NULL;
				
			}
			if(thiz->recv_data!=RT_NULL)
			{
					rt_free(thiz->recv_data);
					thiz->recv_data=RT_NULL;
			}
			rt_thread_mdelay(200);

//			netdev_set_down(cur_net_dev);
//			netdev_low_level_set_link_status(cur_net_dev,RT_FALSE);
	}
}


#include "time.h"
//#include "func_rtc.h"				
int get_gps_num=0;
int get_time_gps_flag=0;
static void recv_thread_entry(void *param);
static int tcp_net_start(struct net_trans * thiz)
{
	 struct sockaddr_in dst_addr;
	 struct hostent *hostname;   
	 rt_int32_t res = 0;
	 
	 if (thiz == RT_NULL)
				return -1;
	 if(thiz->sock_fd != -1)
	 {
		 closesocket(thiz->sock_fd);
	 }
	 
		thiz->sock_fd = socket(AF_INET, SOCK_STREAM, 0);
		if (thiz->sock_fd == -2||thiz->sock_fd == -1)
		{
			LOG_E(">>>>> socket init,socket create failed: %d\n",thiz->sock_fd);
			return -2;
		}
		
		
		hostname = gethostbyname(thiz->ip);

		dst_addr.sin_family = AF_INET;
		dst_addr.sin_port = htons(thiz->port);
		dst_addr.sin_addr = *((struct in_addr *)hostname->h_addr);
		memset(&(dst_addr.sin_zero), 0, sizeof(dst_addr.sin_zero));
		
		//rt_kprintf("执行连接\n");
		log_i("%s socket(%d) connect %s:%d \n",thiz->name,thiz->sock_fd,thiz->ip,thiz->port);
		res = connect(thiz->sock_fd, (struct sockaddr *)&dst_addr, sizeof(struct sockaddr));
		if (res == -1)
		{
			
			closesocket(thiz->sock_fd);
			thiz->sock_fd = -1;
			LOG_E("socket init,socket connect failed\n");
			return -3;
		}
		log_i("%s socket(%d) connect OK\n",thiz->name,thiz->sock_fd);
		if(get_time_gps_flag==0){		//就启动后第一次联网 获取一次gps和时间
			get_time_gps_flag=1;
					/*****		设置时间		******/
//			if(0==strcmp(cur_net_dev->name,NET_4G_DEIVCE_NAME)){
//					struct at_device *at_dev=at_device_get_by_name(cur_net_dev->name);
//					rt_kprintf("设置网络时间\n");
//					struct tm t;
//					at_device_control(at_dev,CMD_GET_TIME,&t);
//					log_i("get net time: %d/%d/%d,%d:%d:%d\n",t.tm_year,t.tm_mon,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec);
//					t.tm_year+=2000;
//					if(t.tm_year>2022&&t.tm_year<2050)
//					rtc_set_time(&t);
//					
//					struct air_get_module_info m_info;
//					at_device_control(at_dev,GET_MODULE_INFO,&m_info);			//获取IMEI和iccid
//					ef_set_env("IMEI",m_info.imei);			//保存IMEI
//					ef_set_env("ICCID",m_info.iccid);		//保存ICCID
//				
//					struct air_get_net_info n_info;
//					at_device_control(at_dev,GET_NET_INFO,&n_info);			//获取CSQ
//					ef_set_env_int("CSQ",n_info.csq);		//保存CSQ

//				#if USE_GPS
//						//获取 经纬度
//						if(get_gps_num<3)
//						{
//								gps_data gps;
//								memset(&gps,0,sizeof(gps));
//								at_device_control(at_dev,GET_GPS,&gps);
//								if(gps.latitude==0&&gps.longitude==0)
//								{
//										get_gps_num++;
//										log_e("get GPS err num: %d, skip\n",get_gps_num);		//获取gps失败就重启模组
//		//								p->step=AT_APP_NETWORK_STEP_ERROR;
//		//								return;
//								}
//								else
//								{
//										ef_set_env_float("longitude",gps.longitude);
//										ef_set_env_float("latitude",gps.latitude);
//										printf("get longitude: %f latitude: %f\n",gps.longitude,gps.latitude);
//								}
//						}
//						else
//						{
//								log_w("skip get gps\n");
//						}
//					
//				#endif
//									
//			}else{
//				//rt_kprintf("不是AT设备 不设置网络参数\n");
//				log_e("not 4G dev, not set time csq...\n");
//				
//			}
		}
		/*************************/
		
		//create recv net thread	
		char recv_name[10] = "R_";
		rt_memcpy(recv_name+2,thiz->name,rt_strlen(thiz->name));
		if(thiz->recv_thread != RT_NULL)
		{
			rt_thread_delete(thiz->recv_thread);
		}
		thiz->recv_thread = rt_thread_create(recv_name, recv_thread_entry, thiz, 2048, 12, 10);
		if (thiz->recv_thread == RT_NULL)
		{
				LOG_E("%s thread,thread create failed\n",recv_name);
				return -4;
		}
		if(rt_thread_startup(thiz->recv_thread) == RT_EOK)
		{
			LOG_I("%s thread,thread startup succeed\n",recv_name);
		}
		return 0;
		
}
//static uint8_t * recv_data=NULL;
static void recv_thread_entry(void *param)
{
	  struct net_trans *thiz = param;
	  struct send_msg msg;
		int  bytes_received;
		if(thiz->recv_data==NULL)
			thiz->recv_data = rt_malloc(2048);
		if(thiz->recv_data==NULL){
			log_e("recv_data 无可用内存\n");
			return;
		}
		while(1)
		{
			bytes_received = recv(thiz->sock_fd, thiz->recv_data,2048 - 1, 0);			
	//#ifdef DOUBLE_PLATFORM		
//			if(updating_flag){
//				rt_kprintf("升级模式中 协议接收不再处理\n");
//				rt_thread_delay(10000);
//				continue;
//			
//			}
	//#endif
			
			rt_kprintf("%s recv data len: %d\n",thiz->name,bytes_received);
			
			if(bytes_received<=0){
						
				log_e("close socket recv ERR: %d\n",bytes_received);
				goto __exit;
			
			}
			left_ms_set(&check_link_tm,CHECK_LINK_TM);			//延迟检测连接状态
			thiz->recv_data[bytes_received] = '\0';

			if(bytes_received > 0)
			{
				//rt_kprintf("%s 接收到数据\n",thiz->name);
			#ifdef NET_DEBUG_MODE
				chardump("<<<<",thiz->recv_data,bytes_received);
			#endif
				if(thiz->ackCb != RT_NULL)
				{
					
					struct pro_msg ret = thiz->ackCb(thiz->recv_data,bytes_received);
					if(ret.type != ERR_MSG)
					{

						if(thiz->recv_event!=NULL){
				
							rt_event_send(thiz->recv_event, MSG_NET_PRO_ACK);
						}
						else
						{
							log_e("recv_event NULL\n");
						}
					}
					
					thiz->ackCb=RT_NULL;
					rt_kprintf("clr ackCb not exec urcCb\n");	
					continue;			//跳过是为了：有ackcb 就不执行urcCb
				}
				else
				{
						//rt_kprintf("应答回调 ackCb 为空\n");	
				}
				
				if(thiz->urcCb != RT_NULL)
				{
					struct pro_msg ret = thiz->urcCb(thiz->recv_data,bytes_received); 
					if(ret.type != ERR_MSG)
					{
						if(thiz->recv_event!=NULL){
							rt_event_send(thiz->recv_event, MSG_NET_PRO_ACK);
						}
						else
						{
							log_e("recv_event NULL\n");
						}
					}
				}
				
			}

		}
	__exit:
		log_e("exit recv_thread \n");
		msg.type = MSG_CLIENT_CLOSE;
		rt_mq_urgent(thiz->s_mq, &msg, sizeof(struct send_msg));
		thiz->recv_thread = RT_NULL;
}
void keep_link_timer_start(void);
void keep_link_timer_stop(void);
static int send_data_wait(struct net_trans * thiz,char * data,int len,bool isAck)
{
	
	  rt_event_control(thiz->recv_event,RT_IPC_CMD_RESET,RT_NULL);
		int res;
		for(int i=0;i<thiz->retryNums;i++)
		{
			
			if(thiz->sock_fd != -1)
			{
				rt_kprintf("%s, ip: %s, socket(%d) send data len %d\n",thiz->name,thiz->ip,thiz->sock_fd,len);
			#ifdef NET_DEBUG_MODE
				chardump(">>>>",data,len);
			#endif
#ifdef USING_KEEP_LINK
        keep_link_timer_stop();
#endif
				
				res = send(thiz->sock_fd, data,len, 0);	
				
#ifdef USING_KEEP_LINK
        keep_link_timer_start();
#endif
				
				if(res <= 0)
				{
					if(thiz->sock_fd != -1)
					{
						closesocket(thiz->sock_fd);
						thiz->sock_fd = -1;
						rt_kprintf("发送失败\n");
						log_e("send failed\n");
						//thiz->wait_resp_cnt=0;
						return -1;
					}
				}
			}
			else
			{
				
			}
			if(isAck == true)
			{
				rt_uint32_t e;
				
				if (rt_event_recv(thiz->recv_event, MSG_NET_PRO_ACK ,RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,thiz->retryTimeout, &e) == RT_EOK)
				{
					//thiz->wait_resp_cnt=0;
					rt_kprintf("%s ack received\n",thiz->name);
					return 0;
				}
				else
				{
//					printf("不等待应答\n");
//					return 0;
				#ifdef DOUBLE_PLATFORM
					rt_kprintf("无回复，由于双平台，不断开连接\n");
					return 0;		//就算没有回复 也不断开当前socket，因为双平台的原因，一直保持连接（不然无法下发升级命令）
				#endif
				}
			}
			else
			{
				return 0;
			}
		 
		}
    //thiz->wait_resp_cnt++;
		rt_kprintf("发送无应答\n");
		log_e("send no ack\n");
	#ifdef TEST_NET_MODE
		log_e("send no ack TEST_NET_MODE wait...\n");
		return 0;
	#else
		return -1;
	#endif
	
}

static void send_thread_entry(void *param)
{
	#define STEP_RECV_MSG           0
  #define STEP_CHANGE_NET         1
  #define STEP_CHANGE_NET_SEL     2
	#define STEP_CONNECT_NET        3
	#define STEP_SEND_DATA          4
	#define STEP_CLOSE_NET          5
	#define STEP_WIAT_ACK           6 
	#define STEP_RE_COUNT           7
	#define STEP_SAVE_PACK          8
	#define STEP_SEND_HISTORY_PACK  9
	#define STEP_CLOSE_NET_DEV  		10
	#define STEP_CHECK_LINK         11
	
	struct net_trans *thiz = param;
	RT_ASSERT(thiz->s_mq);
	char net_module_name[10];
	struct send_msg msg;
	rt_err_t result;
	uint8_t status = STEP_CONNECT_NET;
	//rt_kprintf("新建发送线程\n");
	thiz->recv_event = rt_event_create("recvevent", RT_IPC_FLAG_FIFO);
	if (thiz->recv_event == RT_NULL)
	{
			rt_kprintf("net event,event create failed\n");
	}
	thiz->net_step = STEP_CHANGE_NET;
	int connect_err_count =0;
	int ret;
	while(1)
	{
				//rt_kprintf("--- 这里 %d\n",net_status);
				result = rt_mq_recv(thiz->s_mq, &msg, sizeof(struct send_msg), RT_WAITING_FOREVER);
			  //printf("result %d\n",result);
				if(result == RT_EOK)
				{
					if(msg.type == MSG_CLIENT_CLOSE)
					{
							rt_kprintf("关闭步骤\n");
							thiz->connected=0;		//未连接 状态
							if(thiz->sock_fd != -1)
							{
								log_e("close socket: %d\n",thiz->sock_fd);
								closesocket(thiz->sock_fd);
								thiz->sock_fd = -1;
							}
							close_cur_netdev(thiz);
							thiz->net_step = STEP_CHANGE_NET;
							
							continue;
					}
					if(msg.type == MSG_CLIENT_OPEN)						//选择网卡
					{
						
							thiz->net_step = STEP_CHANGE_NET;
					}
					if(msg.type == MSG_CLIENT_OPEN_SEL)						//选择指定名字的网卡
					{
							
	//						if(strlen(thiz->net_module_name)>0)
							thiz->net_step = STEP_CHANGE_NET_SEL;
//						else{
//							log_e("%s net_module_name is NULL, STEP_CHANGE_NET\n",thiz->name);
//							thiz->net_step = STEP_CHANGE_NET;
//						}
					}
					else if(msg.type==MSG_CLIENT_CONNECT){
							
//						if(thiz->connected==0)
//						{
							thiz->net_step = STEP_CONNECT_NET;			 //执行连接
//						}else{
//							
//							rt_kprintf("已连接\n");
//							log_i("already connected\n");
//							continue;
//						}
						
					}
					else if(msg.type==MSG_CLIENT_FREE_SOCK){
							rt_kprintf("释放连接步骤\n");
							thiz->connected=0;		//未连接 状态
							if(thiz->sock_fd != -1)
							{
								log_e("close socket: %d\n",thiz->sock_fd);
								closesocket(thiz->sock_fd);
								thiz->sock_fd = -1;
							}
							free_cur_netdev(thiz);
							//close_cur_netdev(thiz);
							thiz->net_step = STEP_CHANGE_NET;
							
							continue;					
						
					}
					else if(msg.type==MSG_SEND_DATA){
						
						if(thiz->connected==1)
						{
							thiz->net_step=STEP_SEND_DATA;
						}
						else{
							log_e("no connection, execute connect...\n");
							thiz->net_step = STEP_CHANGE_NET;			//选择网卡 执行连接
							
						}
					}else if(msg.type==MSG_CHECK_LINK){
						
						if(thiz->connected==1)		//存在连接
						{
							thiz->net_step=STEP_CHECK_LINK;		//检测硬件连接状态
						}
						else{
							log_e("no need check link\n");
							
						}
					}
	
					switch(thiz->net_step)
					{
						
						case STEP_CHANGE_NET:{
							rt_kprintf("打开可用网卡步骤\n");
							thiz->open_busy=1;
							int netdev_res=netdev_find_high_prio(thiz,1);
//							if(netdev_find_high_prio(thiz,1) == -1)
//							//if(netdev_find_by_name(thiz,thiz->net_module_name,1) == -1)
//							{
//									if(msg.malloc_buf != RT_NULL)
//									{
//										rt_free(msg.malloc_buf);
//									}
//								//break;
//							}
							//rt_mq_control(thiz->s_mq,RT_IPC_CMD_RESET,RT_NULL);		//重置消息队列（问题 内存泄漏：发送消息申请的内存 无法被释放 ）
							while(RT_EOK==(result = rt_mq_recv(thiz->s_mq, &msg, sizeof(struct send_msg), RT_WAITING_NO))){
								if(msg.type==MSG_SEND_DATA||msg.type==MSG_CLIENT_OPEN_SEL){
										if(msg.malloc_buf != RT_NULL)
										{
											rt_kprintf("free expire msg.malloc_buf\n");
											rt_free(msg.malloc_buf);
										}
								}
							}
							rt_kprintf("clear %s msg queue\n",thiz->name);
							rt_kprintf("退出打开网卡步骤\n");
							thiz->open_busy=0;
						}
						break;		
							
						case STEP_CHANGE_NET_SEL:{
							memset(net_module_name,0,sizeof(net_module_name));
							strcpy(net_module_name,msg.malloc_buf);
							if(msg.malloc_buf != RT_NULL)
							{
								rt_free(msg.malloc_buf);
							}
							rt_kprintf("打开指定网卡 %s 步骤\n",net_module_name);
							thiz->open_busy=1;
							int netdev_res=netdev_find_by_name(thiz,net_module_name,1);
							//if(netdev_find_high_prio(thiz,1) == -1)
//							if(netdev_find_by_name(thiz,net_module_name,1) == -1)
//							{
//									if(msg.malloc_buf != RT_NULL)
//									{
//										rt_free(msg.malloc_buf);
//									}
//								//break;
//							}
							//rt_mq_control(thiz->s_mq,RT_IPC_CMD_RESET,RT_NULL);		//重置消息队列（问题 内存泄漏：发送消息申请的内存 无法被释放 ）
							while(RT_EOK==(result = rt_mq_recv(thiz->s_mq, &msg, sizeof(struct send_msg), RT_WAITING_NO))){
								if(msg.type==MSG_SEND_DATA||msg.type==MSG_CLIENT_OPEN_SEL){
										if(msg.malloc_buf != RT_NULL)
										{
											rt_kprintf("free expire msg.malloc_buf\n");
											//rt_kprintf("free msg.malloc_buf msg %.*s\n",msg.len,msg.malloc_buf);
											rt_free(msg.malloc_buf);
										}
								}
							}
							rt_kprintf("clear %s msg queue\n",thiz->name);
							rt_kprintf("退出打开网卡步骤\n");
							thiz->open_busy=0;
						}
						break;			
						case STEP_CONNECT_NET:
							
							rt_kprintf("执行连接步骤 connect ip: %s:%d\n",thiz->ip,thiz->port);
						  ret = tcp_net_start(thiz);			//里面会创建一个接收线程
							//rt_kprintf("成功\n");
							if(ret<0)
							{
								thiz->connected=0;		//未连接 状态
								if(msg.malloc_buf != RT_NULL)
								{
									rt_kprintf("释放内存1 \n");	
									rt_free(msg.malloc_buf);
									msg.malloc_buf=NULL;
								}
								log_e("start net fail (%d)",ret);
								if(ret == -2 || ret == -3)
								{
									close_cur_netdev(thiz);

								}
								thiz->net_step = STEP_CHANGE_NET;					//连接失败后 跳转CHANGE_NET （2.0.26）
								break;
							}
							else
							{
									rt_kprintf("连接成功\n");
									thiz->net_step = STEP_SEND_DATA;			
									thiz->connected=1;		//已连接 状态
									thiz->retry_check_link_cnt=0;
							}
							
							//rt_mq_control(thiz->s_mq,RT_IPC_CMD_RESET,RT_NULL);		////重置消息队列（问题 内存泄漏：发送消息申请的内存 无法被释放 ）
							while(RT_EOK==(result = rt_mq_recv(thiz->s_mq, &msg, sizeof(struct send_msg), RT_WAITING_NO))){
								if(msg.type==MSG_SEND_DATA||msg.type==MSG_CLIENT_OPEN_SEL){
										if(msg.malloc_buf != RT_NULL)
										{
											rt_kprintf("free expire msg.malloc_buf\n");
											//rt_kprintf("free msg.malloc_buf msg %.*s\n",msg.len,msg.malloc_buf);
											rt_free(msg.malloc_buf);
										}
								}
							}
							rt_kprintf("clear %s msg queue\n",thiz->name);
							break;			//RTU V2.0.13 无break导致发送死机问题	
						case STEP_SEND_DATA:  
								//rt_kprintf("发送数据步骤\n");
						
								//rt_kprintf("发送数据\n");
								//netdev_find_high_prio(thiz,1);
							  if(msg.isAck == true)
								{
									thiz->ackCb = msg.ackCb;//每次都更新
								}
								else
								{
									thiz->ackCb = RT_NULL;
								}
							  
								if(send_data_wait(thiz,msg.malloc_buf,msg.len,msg.isAck)<0)
								{
									
										log_e("send failed close socket %d\n",thiz->sock_fd);		
										thiz->connected=0;		//未连接 状态
										if(thiz->sock_fd != -1)
										{
											closesocket(thiz->sock_fd);
											thiz->sock_fd = -1;
										}
										close_cur_netdev(thiz);
						
										thiz->net_step = STEP_CHANGE_NET;
								}
								else
								{
										
										thiz->net_step = STEP_SEND_DATA;								
								}
								if(msg.malloc_buf != RT_NULL)
								{
									rt_free(msg.malloc_buf);
							
									//增加发送间隔 连续发送平台可能会接收不到
									printf("send interval 500 ms\n");
									rt_thread_mdelay(500);
									
								}
								
						break;
				#ifdef USE_NET_CHECK_LINK
					case STEP_CHECK_LINK:  {			//检查模组socket的连接状态
							int connected=0;
							connected=check_socket_state(thiz->sock_fd);		//读取模组socket的连接状态
							if(connected==-1){
									thiz->retry_check_link_cnt++;
									log_e("check link connect break %d\n",thiz->retry_check_link_cnt);
									if(thiz->retry_check_link_cnt>=RETRY_CHECK_LINK_CNT_MAX){
										thiz->retry_check_link_cnt=0;
										log_e("check link connect break max, reset net\n");
										thiz->connected=0;		//未连接 状态
										if(thiz->sock_fd != -1)
										{
											closesocket(thiz->sock_fd);
											thiz->sock_fd = -1;
										}
										close_cur_netdev(thiz);
						
										thiz->net_step = STEP_CHANGE_NET;
									
									}
							}
							else if(connected==1){
									thiz->retry_check_link_cnt=0;
								
							}	
								//rt_thread_mdelay(300);		//避免刚发送 AT+CIPSTATUS 又去发送 send_data 了
						}
						break;
					#endif
					}
				}
	}
	_exit:
		
		
		log_e("exit send_thread \n");
		thiz->send_thread = RT_NULL;
		if(thiz->sock_fd != -1 )
		{
			closesocket(thiz->sock_fd);thiz->sock_fd = -1;
		}
//		if(thiz->recv_thread != RT_NULL)
//		{
//			rt_thread_delete(thiz->recv_thread);
//			thiz->recv_thread =RT_NULL ;
//		}
//		LOG_I("%s net close!\n",thiz->name);
		return;
}
struct net_trans * net_new(char * name)
{
    struct net_trans *thiz = RT_NULL;
		
    thiz = rt_malloc(sizeof(struct net_trans));
		rt_memset(thiz,0,sizeof(struct net_trans));
    if (thiz == RT_NULL)
    {
        LOG_E("net object alloc malloc error\n");
        return RT_NULL;
    }
		thiz->sock_fd = -1;
		thiz->s_mq = RT_NULL;
		
    memcpy(thiz->name, name, rt_strlen(name));
    thiz->faildCb = RT_NULL;
    thiz->ackCb = RT_NULL; 
		thiz->successdCb = RT_NULL;
    return thiz;
}
int net_free( struct net_trans * thiz)
{

	log_i("net close\n");	
	if(thiz==NULL){
		return 0;
	}
	log_i("close %s\n",thiz->name);	
	thiz->connected=0;
	if(thiz->send_thread != RT_NULL)
	{
		rt_thread_delete(thiz->send_thread);//删除发送线程
		thiz->send_thread =RT_NULL ;
	}
	if(thiz->recv_thread != RT_NULL)	
	{
		rt_kprintf("删除接收线程\n");
		if(thiz->recv_data!=RT_NULL){
			rt_free(thiz->recv_data);
			thiz->recv_data=RT_NULL;
		}
		rt_thread_delete(thiz->recv_thread);	//删除接收线程
		thiz->recv_thread =RT_NULL ;
	}
	if(thiz->sock_fd != -1 )
	{
		closesocket(thiz->sock_fd );
	}
	if(thiz->malloc_buf != RT_NULL)  
	{
		rt_free(thiz->malloc_buf);
	}
	if(thiz->s_mq != RT_NULL)
	{
		rt_mq_delete(thiz->s_mq );
	}
	rt_free(thiz);
	thiz = RT_NULL;
	//关闭线程，关闭信号
	return 0;
}
static int net_start( struct net_trans * thiz)
{
	thiz->s_mq = rt_mq_create(thiz->name,sizeof(struct send_msg),16,RT_IPC_FLAG_FIFO);			//最多排队16个消息
	if(thiz->s_mq == RT_NULL)
	{
			LOG_E("%s msq error\n",thiz->name);
			goto _exit;
	}
	char send_name[10] = "S_";
	rt_memcpy(send_name+2,thiz->name,rt_strlen(thiz->name));
	thiz->send_thread = rt_thread_create(send_name, send_thread_entry, thiz, 2048, 12, 10);		//每个都创建一个发送线程
	if (thiz->send_thread == RT_NULL)
	{
			LOG_E("%s thread,thread create failed\n",send_name);
			goto _exit;
	}
	if(rt_thread_startup(thiz->send_thread) == RT_EOK)
	{
		LOG_I("%s thread,thread init succeed\n",send_name);
	}
  rt_kprintf("net: %s start ok\n",thiz->name);
	return RT_EOK;
	
	_exit:
		net_free(thiz);
	return RT_ERROR;
}
int net_send_by_cb(struct net_trans * thiz,uint8_t *data,uint16_t len,struct pro_msg (*ackCb)(uint8_t *,uint16_t))
{
	if(thiz->connected==0){
		log_i("%s no connect, unable send\n",thiz->name);
		return -1;
	}
	struct send_msg msg;
	msg.malloc_buf = rt_malloc(len);
	rt_memcpy(msg.malloc_buf,data,len);
	msg.len = len;
	msg.isAck = true;
	msg.ackCb = ackCb;
	msg.type=MSG_SEND_DATA;
	rt_mq_send_wait(thiz->s_mq, &msg, sizeof(struct send_msg),RT_WAITING_FOREVER);
	return 0;
}


int net_send_no_ack(struct net_trans * thiz,uint8_t *data,uint16_t len)
{
	if(thiz->connected==0){
		log_i("%s no connect, unable send\n",thiz->name);
		return -1;
	}
	struct send_msg msg;
	msg.malloc_buf = rt_malloc(len);
	rt_memcpy(msg.malloc_buf,data,len);
	msg.len = len;
	msg.isAck = false;
	msg.type=MSG_SEND_DATA;
	rt_mq_send_wait(thiz->s_mq, &msg, sizeof(struct send_msg),RT_WAITING_FOREVER);
	return 0;
}
struct pro_msg protocol_urccb(uint8_t *data_buf, uint16_t len)
{
	rt_kprintf("测试回调 urcCb\n");
	chardump("<<<<",data_buf,len);
	
}
int net_set_recv_cb(struct net_trans * thiz,struct pro_msg (*urcCb)(uint8_t *,uint16_t)) 		//设置接收回调函数
{
	if(thiz != RT_NULL)
	{
		thiz->urcCb = urcCb;
		rt_kprintf("net set recv cb %p\n",urcCb);
	}
	else
	{
		rt_kprintf("thiz is NULL\n");
		
	}

	return 0;
}

int net_connect(struct net_trans * thiz,char* ip,uint16_t port)
{
	if(thiz == RT_NULL)
	{
		thiz =  net_new(NET1_NAME);
		if(thiz == RT_NULL)
		{
			log_e("net1 new error!\n");
			return -1;
		}
 
//		rt_memcpy(net1->ip,"mondata.cn",sizeof("mondata.cn"));
//		net1->port = 28002;   		
		thiz->retryNums = 3;         
		thiz->retryTimeout = 3000;   
		//thiz->urcCb = urcCb;
		net_start(thiz);
	}
	else
	{
		rt_kprintf("%s is exist connecting...\n",thiz->name);
		
	}
	
	rt_strncpy(thiz->ip,ip,rt_strlen(ip));
	thiz->port = port;   
	struct send_msg msg;
	msg.type=MSG_CLIENT_CONNECT;
	rt_mq_urgent(thiz->s_mq, &msg, sizeof(struct send_msg));		//发送紧急消息。插入队列头
	//rt_mq_send_wait(thiz->s_mq, &msg, sizeof(struct send_msg),RT_WAITING_FOREVER);	
	return 0;
}
int net_free_sock(struct net_trans * thiz)
{
	if(thiz == RT_NULL)
	{
			log_e("net new error!\n");
			return -1;
	}
	struct send_msg msg;
	msg.type=MSG_CLIENT_FREE_SOCK;
	//rt_mq_urgent(thiz->s_mq, &msg, sizeof(struct send_msg));		//发送紧急消息。插入队列头
	rt_mq_send_wait(thiz->s_mq, &msg, sizeof(struct send_msg),RT_WAITING_FOREVER);	
	return 0;
}
int net_open(struct net_trans * thiz)
{
 
	struct send_msg msg;
	msg.type=MSG_CLIENT_OPEN;
	rt_mq_urgent(thiz->s_mq, &msg, sizeof(struct send_msg));	//发送紧急消息。插入队列头
	//rt_mq_send_wait(thiz->s_mq, &msg, sizeof(struct send_msg),RT_WAITING_FOREVER);		
	return 0;
}
int net_open_sel_module(struct net_trans * thiz,char* net_module_name)
{
 
	struct send_msg msg;
	msg.type=MSG_CLIENT_OPEN_SEL;

	int len=strlen(net_module_name)+1;
	msg.malloc_buf = rt_malloc(len);
	rt_memcpy(msg.malloc_buf,net_module_name,strlen(net_module_name));
	msg.malloc_buf[len]='\0';
	msg.len = len;
	rt_mq_urgent(thiz->s_mq, &msg, sizeof(struct send_msg));	//发送紧急消息。插入队列头
	//rt_mq_send_wait(thiz->s_mq, &msg, sizeof(struct send_msg),RT_WAITING_FOREVER);		
	return 0;
}
int net_close(struct net_trans * thiz)
{
 
	struct send_msg msg;
	msg.type=MSG_CLIENT_CLOSE;
	rt_mq_urgent(thiz->s_mq, &msg, sizeof(struct send_msg));		//发送紧急消息。插入队列头
	return 0;
}

int net_check_link(struct net_trans * thiz)
{
	 if(thiz->s_mq==RT_NULL){
		return 0;
	 }
		struct send_msg msg;
		msg.type=MSG_CHECK_LINK;
		rt_mq_send(thiz->s_mq, &msg, sizeof(struct send_msg));		//发送消息
		return 0;
}


//static struct net_trans  *net1 = RT_NULL;
int net_send_by_ackcb(struct net_trans * thiz,char*ip,uint16_t port,uint8_t *data,int len,struct pro_msg (*ackCb)(uint8_t *,uint16_t))
{
	
//	fomat_rt_kprintf_tag(">>>>",data,len);
///*create net */

	//static struct net_trans  *net1 = RT_NULL;
	if(thiz == RT_NULL)
	{
		thiz =  net_new(NET1_NAME);
		if(thiz == RT_NULL)
		{
			log_e("net new error!\n");
			return -1;
		}
		rt_memcpy(thiz->ip,ip,rt_strlen(ip));
		thiz->port = port;    
//		rt_memcpy(net1->ip,"mondata.cn",sizeof("mondata.cn"));
//		net1->port = 28002;   		
		thiz->retryNums = 3;         
		thiz->retryTimeout = 3000;   
//		thiz->urcCb = protocol_test_urccb;
		net_start(thiz);
	}
	else
	{
		rt_kprintf("%s is exist sending...\n",thiz->name);
		
	}
	if(thiz != RT_NULL)
	{

		if(ackCb != RT_NULL)
		{
			net_send_by_cb(thiz,data,len,ackCb);
		}
		else
		{
			net_send_no_ack(thiz,data,len);
		}
		
	}
	
#ifdef DOUBLE_PLATFORM
	if(send_flag==1)
	{
		rt_kprintf("NB上报，不直连采集\n");
		return 0;
	}
	rt_thread_mdelay(1000);
	static struct net_trans  *net2 = RT_NULL;
	if(net2 == RT_NULL)
	{
		rt_kprintf("net2 不存在\n");
		net2 =  v1_net_new(NET2_NAME);
		if(net2 == RT_NULL)
		{
			rt_kprintf("net2 new error!\n");
			return -1;
		}
		rt_memcpy(net2->ip,NET2_IP,rt_strlen(NET2_IP));
		net2->port = NET2_PORT;                                         
		net2->retryNums = 3;         
		net2->retryTimeout = 3000;
		net1->urcCb = protocol_sl651_urc;
		v1_net_start(net2);          
	}
	else
	{
		rt_kprintf("net2 is exist.\n");
	}
	
	if(net2 != RT_NULL)
	{
		if(ackCb != RT_NULL)
		{
			v1_net_send_by_cb(net2,data,len,ackCb);
		}
		else
		{
			v1_net_send_no_ack(net2,data,len);
		}
		
	}
#endif
	      
	return 0;
}
int net_connected_state(struct net_trans * thiz){
	
	
	return thiz->connected;
	
}
//#include "app_update.h"
static void check_link_thread(void *param)
{
	
	rt_err_t err;
	rt_uint32_t e=0;
	int idx=0;
	struct net_trans* tmp_net_trans[NET_TRANS_NUM];
	
	left_ms_set(&check_link_tm,CHECK_LINK_TM);
	while(1)
	{
			
			if(left_ms(&check_link_tm)==0)
			{
				left_ms_set(&check_link_tm,CHECK_LINK_TM);
//				delay_ms(45*1000);			//30秒 发送一次检测连接
				tmp_net_trans[0]=net1;
				tmp_net_trans[1]=net2;
				tmp_net_trans[2]=net3;
				tmp_net_trans[3]=net4;
				
	//			for (idx++; idx < NET_TRANS_NUM && (tmp_net_trans[idx]->connected!=1);idx++);
				if(tmp_net_trans[idx]!=NULL&&tmp_net_trans[idx]->connected==1){
					//printf("序号 %d\n",idx);
					net_check_link(tmp_net_trans[idx]);
					idx++;
					idx%=NET_TRANS_NUM;
//					if(g_app_update.start_update==1||g_app_update.pause_update==1)		//封装一个消息发送过来 告知延迟检测连接
//					{
//							printf("delay 240s check link, updating ...\n");
//							delay_ms(240*1000);			
//						
//					}
					
				}
				for (; idx < NET_TRANS_NUM && (tmp_net_trans[idx]->connected!=1);idx++,idx%=NET_TRANS_NUM);

			}
			delay_ms(10);
	}
}
int net_app_init(void)
{
	
/**************			网络连接1（业务连接）	***************/	
	net1 =  net_new(NET1_NAME);
	if(net1 == RT_NULL)
	{
		rt_kprintf("net1 new error!\n");
		return -1;
	}
//	rt_memcpy(net1->ip,NET1_IP,rt_strlen(NET1_IP));
//	net1->port = NET1_PORT;           
	net1->retryNums = 3;
	net1->retryTimeout = 3000;   
	net_start(net1);
	
///**************			网络连接2（用于下载升级包）		***************/	
	net2 =  net_new(NET2_NAME);
	if(net2 == RT_NULL)
	{
		rt_kprintf("net2 new error!\n");
		return -1;
	}
//	rt_memcpy(net2->ip,NET2_IP,rt_strlen(NET2_IP));
//	net2->port = NET2_PORT;                                        
	net2->retryNums = 3;         
	net2->retryTimeout = 3000;   
	net_start(net2); 
//	
	
/**************			网络连接3（未用）			***************/	
//	net3 =  net_new(NET3_NAME);
//	if(net3 == RT_NULL)
//	{
//		rt_kprintf("net3 new error!\n");
//		return -1;
//	}
//	rt_memcpy(net3->ip,NET3_IP,rt_strlen(NET3_IP));
//	net3->port = NET3_PORT;                                        
//	net3->retryNums = 3;         
//	net3->retryTimeout = 3000;   
//	net_start(net3); 
	
	
//	rt_kprintf("v1_net_init %s %s\n",net1->name,net2->name);
	#ifdef USE_NET_CHECK_LINK
    rt_thread_t tid;		//创建用于定时发送检测连接状态消息的任务
    tid = rt_thread_create("chk_lk",
                           check_link_thread, RT_NULL,
                           512+128,
                           18, 10);
    if (tid != RT_NULL)
        rt_thread_startup(tid);
    return RT_EOK;
	#endif
	return 0;
}
INIT_APP_EXPORT(net_app_init);



#ifdef USING_KEEP_LINK

/***************	保活包 （目前的问题：只能为一个连接做保活 需要每个连接都创建该线程）	************************/
rt_event_t keep_link_event=NULL;
static rt_timer_t keep_link_timer = RT_NULL;

void keep_link_timer_cb(void *parameter) {
    //if(!updating_flag) {				//非网络升级，就发心跳包
				//rt_kprintf("发送保活包事件\n");
        rt_event_send(keep_link_event,SEND_KEEP_LINK_PACK);

    //}
}
void keep_link_timer_start(void) {
    int keeplink=KEEP_LINK_TIME;
    if(keep_link_timer==NULL) {
        keep_link_timer = rt_timer_create("keep_link",
                                          keep_link_timer_cb,
                                          RT_NULL,
                                          rt_tick_from_millisecond(1000*keeplink),
                                          RT_TIMER_FLAG_ONE_SHOT);
    }
    //rt_timer_control(keep_link_timer,RT_TIMER_CTRL_SET_TIME,&keeplink);
    rt_timer_start(keep_link_timer);
}
void keep_link_timer_stop(void) {
    if(keep_link_timer!=RT_NULL) {
        rt_timer_stop(keep_link_timer);
    }
}

void send_heart_pack(){
	net1_send_by_ackCb("hi",strlen("hi"),RT_NULL);
}
static void keep_link_thread(void *param)
{
	rt_err_t err;
	rt_uint32_t e=0;
	for(;;)
	{
		err=rt_event_recv(keep_link_event,SEND_KEEP_LINK_PACK,RT_EVENT_FLAG_CLEAR|RT_EVENT_FLAG_AND,RT_WAITING_FOREVER,&e);
		if(err==RT_EOK)
		{
			rt_kprintf("发送保活包\n");
			send_heart_pack();
     
		
		}
	}
}

static void heart(uint8_t argc, char **argv)
{
	send_heart_pack();
}
MSH_CMD_EXPORT(heart, s);

int keep_link_thread_init(void)
{
    rt_thread_t tid;
		keep_link_event=rt_event_create("keep_link_event",RT_IPC_FLAG_FIFO);		
    tid = rt_thread_create("keep_link",
                           keep_link_thread, RT_NULL,
                           256+128,
                           18, 10);
    if (tid != RT_NULL)
        rt_thread_startup(tid);
    return RT_EOK;
}
INIT_APP_EXPORT(keep_link_thread_init);
#endif


int sock_close(uint8_t argc, char **argv)
{
	closesocket(atoi(argv[1]));
}
MSH_CMD_EXPORT(sock_close, n);


struct pro_msg protocol_test_ackcb(uint8_t *data_buf, uint16_t len)
{
	rt_kprintf("测试回调 ackCb\n");
	chardump("<<<<",data_buf,len);
	
}

//连接1
int net1_send_ack(uint8_t argc, char **argv)
{
		net_send_by_ackcb(net1,net1->ip,net1->port,argv[1], rt_strlen(argv[1]),protocol_test_ackcb);
	//net1_send_by_ackCb(argv[1], rt_strlen(argv[1]),protocol_test_ackcb);

}
MSH_CMD_EXPORT(net1_send_ack, s);

int cmd_net1_send_no_ack(uint8_t argc, char **argv)
{
	//net1_send_by_ackCb(argv[1], rt_strlen(argv[1]),RT_NULL);
		net_send_by_ackcb(net1,net1->ip,net1->port,argv[1], rt_strlen(argv[1]),RT_NULL);
}
//MSH_CMD_EXPORT(cmd_net1_send_no_ack, s);
