#include <stdint.h>
#include <rtthread.h>
//#include <tusb.h>
#include <ulog.h>
#include "usb_device.h"
//#include "at32f4xx_it.h"
//#include "iwdg.h"
//#include "global.h"
///*******		原工程搬来的		*********/
//#include "at32_board.h"
//#include "usb_lib.h"
//#include "hw_config.h"
//#include "usb_pwr.h"
//#include "mass_mal.h"

//void mass_usb_init(void){
//	AT32_USB_GPIO_init();
//	USB_Interrupts_Config();    
//	Set_USBClock(USBCLK_FROM_HSI);/* 必须使用192M时钟 Set USB Clock, USB Clock must 48MHz*/		
//	USB_Init();
//	rt_kprintf("USB using F413/F403 MASS\n");
//}

//extern rt_err_t rt_usb_device_init(void);


//#ifdef MCU_AT32F403x
//void ACC_IRQHandler(void)
//{
//  if(ACC_GetFlagStatus(ACC_FLAG_CALRDY) == SET)
//  {
//    /*Claer ACC Calibration ready flag*/
//    ACC_ClearFlag(ACC_FLAG_CALRDY);
//  }
//  if(ACC_GetFlagStatus(ACC_FLAG_RSLOST) == SET)
//  {
//    /*Claer ACC Reference Signal Lost flag*/
//    ACC_ClearFlag(ACC_FLAG_RSLOST);
//  }
//}
//#endif

//void USBWakeUp_IRQHandler(void)
//{
//    EXTI_ClearIntPendingBit(EXTI_Line18);
//}
//#ifdef USE_USB
//	#if defined(AT_START_F413_V1_0)||defined(MCU_AT32F403x)
//		#include "usb_istr.h"				//F413 MASS USB
//		void USB_LP_CAN1_RX0_IRQHandler(void)
//		{
//				iwdg_free();
//				USB_Istr();
//		}		
//	#elif defined(AT_START_F415_V1_0)
//		#include "usb_dcd_int.h"		//F415 OTG device
//		extern USB_OTG_CORE_HANDLE    USB_OTG_dev;
//		void USBOTG_IRQHandler(void)
//		{
//			//iwdg_free();			//USB读写死机 就打开喂狗
//			USB_DEVICE_OTG_INTSTS_Handler(&USB_OTG_dev);
//		}
//	#endif
//#endif


//int usbd_demo(void)
//{
////		//mass_usb_init();
////		struct udcd _at32_udc;
////    rt_memset((void *)&_at32_udc, 0, sizeof(struct udcd));
////    _at32_udc.parent.type = RT_Device_Class_USBDevice;
////	
////    _at32_udc.parent.init = _init;
////    _at32_udc.parent.user_data = &_at32_pcd;
////    _at32_udc.ops = &_udc_ops;
////    /* Register endpoint infomation */
////    _at32_udc.ep_pool = _ep_pool;
////    _at32_udc.ep0.id = &_ep_pool[0];
////    rt_device_register((rt_device_t)&_at32_udc, "usbd", 0);
////    rt_usb_device_init();
//    return 0;
//}
//INIT_DEVICE_EXPORT(usbd_demo);

int cdc_demo(void)
{
		
    rt_device_t dev = RT_NULL;
    char buf[] = "hello rt-thread!\r\n";
    /* user app entry */
    dev = rt_device_find("vcom");
    if (dev)
            rt_device_open(dev, RT_DEVICE_FLAG_RDWR);
    else{
			LOG_E("vcom open err");
            return -RT_ERROR;
		}

    rt_device_write(dev, 0, buf, rt_strlen(buf));
     
    return 0;
}


MSH_CMD_EXPORT(cdc_demo, TinyUSB cdc example)




