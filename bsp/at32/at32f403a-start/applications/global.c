#include "global.h"
#include "stdint.h"
#include "stdarg.h"
#include "stdio.h"
#include <rtthread.h>
#include "at32f403a_407.h"
#define LOG_TAG "global"

#define PRODUCT_DESC "4G/ETH gateway for rt-thread"
int show_version(void)
{
    rt_kprintf("\n \\ | /\n");
    rt_kprintf("- TIWY -     %s\n",PRODUCT_DESC);
    rt_kprintf(" / | \\       %s build %s\n",SOFTWARE_VER, __DATE__);
    rt_kprintf("2021 - 2023 Copyright by tiwy team\n");
	
		rt_kprintf("\n注意：正式产品需要将 BK_APP_SIZE 改大\n\n");
}
INIT_DEVICE_EXPORT(show_version);
int reset_info_chk(void)
{
    //显示 由何种原因导致的 重启
    if(crm_flag_get(CRM_WWDT_RESET_FLAG))
    {
        log_i("---> WWDG reset");
    }
    else if(crm_flag_get(CRM_LOWPOWER_RESET_FLAG))
    {
        log_i("---> LPW reset");
    }
    else if(crm_flag_get(CRM_POR_RESET_FLAG))
    {
        log_i("---> POW reset");
    }
    else if(crm_flag_get(CRM_SW_RESET_FLAG))
    {
        log_i("---> SOFT reset");
    }
    else if(crm_flag_get(CRM_WDT_RESET_FLAG))
    {
        log_i("---> IWDG reset");
    }
    else if(crm_flag_get(CRM_NRST_RESET_FLAG))
    {
        log_i("---> PIN reset");
    }
		crm_flag_clear(CRM_ALL_RESET_FLAG);
//    RCC_ClearFlag();
}
//INIT_APP_EXPORT(reset_info_chk);

//#define show_addr_info(x,y)  rt_kprintf("%-18s -   %d K, size %d K\n",#x,x/KB,y/KB);//printf("-----------------------------------------\n");

//int show_ex_flash_desc(void) {
//    rt_kprintf("%-18s - 0x%02X\n",						"FLASH_APP1_ADDR",			SCB->VTOR);

//    rt_kprintf("<-------- APP EX FLASH -------->\n");
//		show_addr_info(BK_APP_ADDR,BK_APP_SIZE);
//		show_addr_info(OLD_APP_ADDR,OLD_APP_SIZE);
//		show_addr_info(UPGRADE_FLAG_ADDR,UPGRADE_FLAG_SIZE);
//	
//		show_addr_info(ADDR_TYPE_CFG_ADDR,ADDR_TYPE_CFG_SIZE);
//		show_addr_info(SCENE_INFO_CFG_ADDR,SCENE_INFO_CFG_SIZE);
//		show_addr_info(SCENE_INFO_POOL_ADDR,SCENE_INFO_POOL_SIZE);

//		show_addr_info(DEV_INFO_CFG_ADDR,DEV_INFO_CFG_SIZE);
//		show_addr_info(DEV_INFO_POOL_ADDR,DEV_INFO_POOL_SIZE);
//	
//		show_addr_info(SCENE_TIMER_INFO_CFG_ADDR,SCENE_TIMER_INFO_CFG_SIZE);
//		show_addr_info(SCENE_TIMER_INFO_POOL_ADDR,SCENE_TIMER_INFO_POOL_SIZE);
//		
//		
//		show_addr_info(IMPORTANT_PARA_ADDR,IMPORTANT_PARA_SIZE);
//		show_addr_info(SUBSET_APP_ADDR,SUBSET_APP_SIZE);
////		show_addr_info(EF_START_ADDR,ENV_AREA_SIZE);
////#ifdef EF_USING_LOG
////		show_addr_info(LOG_EF_START_ADDR,LOG_AREA_SIZE);
////		
////#endif
////#ifdef USE_FATFS
////    printf("%-18s - %d K,size %d K\n","FatFs ADDR",(EX_FLASH_SECTOR_OFFSET*EX_FLASH_SECTOR_SIZE)/KB,(EX_FLASH_SECTOR_SIZE*((EX_FLASH_BLOCK_NUM*EX_FLASH_BLOCK_SIZE)-EX_FLASH_SECTOR_OFFSET))/KB);
////#endif
//    rt_kprintf("<------------------------------->\n");

//}
//INIT_DEVICE_EXPORT(show_ex_flash_desc);

int g_ota_per = 0;//no safe trans
void extend_sram(void)
{
  /* check if ram has been set to expectant size, if not, change eopb0 */
  if(((USD->eopb0) & 0xFF) != EXTEND_SRAM)
  {
    flash_unlock();
    /* erase user system data bytes */
    flash_user_system_data_erase();
    
    /* change sram size */
    flash_user_system_data_program((uint32_t)&USD->eopb0, EXTEND_SRAM);
    
    /* system reset */
    nvic_system_reset();
  }
}
void show_using_sram_size(void){

	if(((USD->eopb0) & 0xFF) == EXTEND_SRAM)
  {
  	  rt_kprintf("Extend 224KB SRAM success.\r\n");
  } else {
  	  log_e("Extend 224KB SRAM failed.\r\n");
  }		  

}
void reboot(void)
{
    rt_hw_cpu_reset();
}

//void get_random_account(void){
//		char buff[20];
//		uint8_t random=get_random(1,99);
//		sprintf(buff,"1100000000%02d",random);

//		//printf("random account %s\n",buff);
//		log_e("loss of cfg, online with the random account: %s\n",buff);
//		printf("保存的参数丢失 使用随机账号上线：%s\n",buff);
//		
//		ef_set_env("device_id",buff);
//		ef_set_env("client_id",buff);
//		ef_set_env("password",buff);
//		//ef_save_env();
//		//reboot();


//}

//#define IMPORTANT_PARA_FLAG 0x5a		//已经配置的标志位
//#define import_para_flash_read(buff,addr,len) EN25QXX_Read(buff,addr,len)					//flash读接口
//#define import_para_flash_write(buff,addr,len) EN25QXX_Write(buff,addr,len)				//flash写接口
//typedef struct __import_para{
//	uint8_t first_flag;
//	char device_id[20];
//	char remote_ip[40];
//	char remote_port[10];
//	char client_id[30];
//	char username[30];
//	char password[30];
//	char enterprise[10];
//}_import_para;

//void backup_important_para(void){
//	printf("backup_important_para\n");
//	_import_para import_para;
//	memset(&import_para,0,sizeof(_import_para));		//都置0
//	
//	import_para.first_flag=IMPORTANT_PARA_FLAG;
//	strcpy(import_para.device_id,ef_get_env("device_id"));
//	strcpy(import_para.remote_ip,ef_get_env("remote_ip"));
//	strcpy(import_para.remote_port,ef_get_env("remote_port"));
//	strcpy(import_para.client_id,ef_get_env("client_id"));
//	strcpy(import_para.username,ef_get_env("username"));
//	strcpy(import_para.password,ef_get_env("password"));
//	strcpy(import_para.enterprise,ef_get_env("enterprise"));
//	import_para_flash_write((uint8_t*)&import_para,IMPORTANT_PARA_ADDR,sizeof(_import_para));
//	
//}
//int restore_important_para(void){
//	printf("restore_important_para\n");
//	
//	_import_para import_para;
//	memset(&import_para,0,sizeof(_import_para));		//都置0
//	import_para_flash_read((uint8_t*)&import_para,IMPORTANT_PARA_ADDR,sizeof(_import_para));
//	if(IMPORTANT_PARA_FLAG==import_para.first_flag){
//		ef_set_env_int("restore_para_n",ef_get_env_int("restore_para_n")+1);		//设备池恢复次数累加记录

//		ef_set_env("device_id",import_para.device_id);	
//		printf("restore device_id: %s\n",ef_get_env("device_id"));
//		
//		ef_set_env("remote_ip",import_para.remote_ip);
//		rt_kprintf("restore remote_ip: %s\n",ef_get_env("remote_ip"));
//		
//		ef_set_env("remote_port",import_para.remote_port);
//		rt_kprintf("restore remote_port: %s\n",ef_get_env("remote_port"));
//		
//		ef_set_env("client_id",import_para.client_id);
//		printf("restore client_id: %s\n",ef_get_env("client_id"));
//		
//		ef_set_env("username",import_para.username);
//		rt_kprintf("restore username: %s\n",ef_get_env("username"));
//		
//		ef_set_env("password",import_para.password);
//		printf("restore password: %s\n",ef_get_env("password"));
//		
//		ef_set_env("enterprise",import_para.enterprise);
//		printf("restore enterprise: %s\n",ef_get_env("enterprise"));
//		ef_save_env();
//		return 0;
//	}
//	else{
//		printf("no backup important_para\n");
//		return -1;
//		
//	}
//}

//int pada_backup(uint8_t argc, char **argv) {
//		backup_important_para();
//	
//}
//CMD_EXPORT(pada_backup,test);

//int pada_restore(uint8_t argc, char **argv) {

//		restore_important_para();
//}
//CMD_EXPORT(pada_restore,test);

