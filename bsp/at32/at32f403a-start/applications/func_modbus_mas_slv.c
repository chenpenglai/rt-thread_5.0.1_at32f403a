#define FUNC_MODBUS_MASTER_MAIN
#include "func_modbus_mas_slv.h"
#include "stdio.h"
#include "modbus.h"
#include "stdio.h"
#include "string.h"
#include "drv_gpio.h"
#include <rtthread.h>
#include <rtdevice.h>
#include "func_tools.h"
//#include "net_protocol_mqtt.h" 
#include "global.h"
//#include "func_rtc.h"
//#include "at_app_network.h"
#define LOG_TAG "mas_slv"
func_rts_cfg rts_cfg;
#define RS485_RTS GET_PIN(A, 8)
unsigned short HR1[MODBUS_SLV_HR_NB];		//V2.5修改
unsigned short HR[MODBUS_SLV_HR_NB];

enum {
	FUNC_MODBUS_MASTER_STEP_CHECK_FRAME,
	FUNC_MODBUS_MASTER_STEP_SEND
};

enum{
	FUNC_RECV_DATA=0,
	FUNC_SLV_MODBUS_SEND,
	FUNC_MODBUS_MASTER_STEP_CHK_PERIOD,
	FUNC_MODBUS_MASTER_STEP_CHK_REC,
	FUNC_MODBUS_MASTER_STEP_RESEND,
	
	FUNC_SLV_CFG_SEND,		//串口配置企业码 应答
};

void func_modbus_mas_slv_init(FUNC_MODBUS_MASTER_T *p,rt_device_t device,const FUNC_MODBUS_MASTER_SLV_HR_T *tb_slv_hr,FUNC_DIRTRAN_T *p_slv,unsigned char slv)
{
	p->device=device;
	p->send_delay=MODBUS_SLV_SEND_DELAY;
	p->send_rty_nb=FUNC_MODBUS_MASTER_SEND_RTY;
	p->send_tout=FUNC_MODBUS_MASTER_SEND_TOUT;
	p->send_period=FUNC_MODBUS_MASTER_SEND_PERIOD;
	p->tb_slv_hr=tb_slv_hr;
	
		FUNC_DIRTRAN_PORT *pc;
	
	p_slv->ch1.device=device; 
	p_slv->ch1.buf_size=FUNC_DIRTRAN_BUF_SIZE;

	pc=&p_slv->ch1;
#ifdef MODBUS_COIL_OFS_EN
	pc->md.coil_ofs=MODBUS_SLV_COIL_OFS;
#endif

#ifdef MODBUS_INPUT_OFS_EN
	pc->md.input_ofs=MODBUS_SLV_INPUT_OFS;
#endif
	pc->md.slv=slv;
	pc->md.phr=HR1;
	pc->md.hr_n=countof(HR1);
	p->step=FUNC_RECV_DATA;
	
}

unsigned char func_modbus_master_add_func(FUNC_MODBUS_MASTER_T *p,FUNC_MODBUS_MASTER_FUNC_T *pf,FUNC_MODBUS_MASTER_FUNC_E type,FUNC_MODBUS_MASTER_SLV_HR_T *tb_slv_hr)
{
	unsigned char r=0;
	unsigned char ch;
	p->tb_slv_hr=tb_slv_hr;
	if(type == FUNC_MODBUS_MASTER_FUNC_CYC)
	{
		if(p->func_cyc_nb < FUNC_MODBUS_MASTER_CYC_NB)
		{
			p->func_cyc[p->func_cyc_nb]=*pf;
			p->func_cyc_nb++;
			r=1;
		}else{
			rt_kprintf("ERR add func max num: %d\n",FUNC_MODBUS_MASTER_CYC_NB);
			
		}
	}
	else
	{
		ch = (p->func_inj_t +1)%FUNC_MODBUS_MASTER_INJ_NB;
		if(ch!=p->func_inj_h)
		{
			p->func_inj[p->func_inj_t]=*pf;
			p->func_inj_t=ch;
			r=1;
		}
	}
	return(r);
}
void func_modbus_master_clr_func(FUNC_MODBUS_MASTER_T *p,FUNC_MODBUS_MASTER_FUNC_E type)
{
	if(type == FUNC_MODBUS_MASTER_FUNC_CYC)
	{
		p->func_cyc_nb=0;
		p->func_cyc_cur=0;
	}
	else
	{
		p->func_inj_t=p->func_inj_h=0;	
	}
}


__weak void func_modbus_master_after_rec(FUNC_MODBUS_MASTER_T *p)
{
	return;
}

__weak void func_modbus_master_before_send(FUNC_MODBUS_MASTER_T *p)
{
	return;
}


static unsigned char get_func(FUNC_MODBUS_MASTER_T *p)
{
	FUNC_MODBUS_MASTER_FUNC_T *pf=NULL;
	int i,r=0;
	
	if(p->func_inj_h != p->func_inj_t)
	{
		pf=&p->func_inj[p->func_inj_h];
		p->func_inj_h++;
		p->func_inj_h %= FUNC_MODBUS_MASTER_INJ_NB;
		p->cur_func_mode = FUNC_MODBUS_MASTER_FUNC_INJ;
	}
	else
	{
		if(p->func_cyc_nb !=0)
		{
			pf=&p->func_cyc[p->func_cyc_cur];
			p->func_cyc_cur++;
			p->func_cyc_cur %= p->func_cyc_nb;
		}
		p->cur_func_mode = FUNC_MODBUS_MASTER_FUNC_CYC;
	}
	
	if(pf)
	{
		
		uint16_t adr_end1=(p->md.da_adr+p->md.da_n);	//这包modbus数据的结束位置	
//		rt_kprintf("p->md.slv %d, p->md.da_adr %d, p->md.da_n %d\n",p->md.slv,p->md.da_adr,p->md.da_n);
//		rt_kprintf("get_func adr_end1 %d\n",adr_end1);
		for(i=0;;i++)
		{	
		#ifdef USE_BRK
			if(pf->slv==0)
			{//添加广播情况处理
				r=1;
				break;
			}
		#endif
			
			//利用SLV查找对应的HR与HR数量
			if(p->tb_slv_hr[i].slv==pf->slv)
			{
				r=1;
				break;
			}
//			if(p->tb_slv_hr[i].slv==0)			//去掉这里
//			{
//				break;
//			}
		}
		if(r)
		{
			p->md.phr=p->tb_slv_hr[i].phr;
			
//			rt_kprintf("get_func hr_n %d\n",p->tb_slv_hr[i].hr_n);
			
			p->md.hr_n=p->tb_slv_hr[i].hr_n;
			p->md.func=pf->func;
			p->md.slv=pf->slv;
			p->md.da_adr=pf->da_adr;
			p->md.da_n=pf->da_n;
		#ifdef MODBUS_RW_EN
			p->md.rww_adr=pf->rww_adr;
			p->md.rww_n=pf->rww_n;
		#endif
//			if(pf->mff)
//			{
//				pf->mff();
//			}
		}
	}	
	return(r);
}
void func_modbus_master_success(MODBUS_T *p)
{
	(void)p;
	switch(p->func){			//应答的功能码
		case 
			MD_RD_HR:			//轮询返回的结果
			rt_kprintf("read HR ok\n");
			//ctrl_set_event(EVENT_SVL_ASK_SUCCEED);	
			rt_event_send(global_event,EVENT_SVL_ASK_SUCCEED);
		break;
		case 
			MD_FR_MHR:		//获取写的结果
			rt_kprintf("write HR %d num ok\n",p->da_n);
			//ctrl_set_event(EVENT_SVL_RESPONSE_SUCCEED);
			rt_event_send(global_event,EVENT_SVL_RESPONSE_SUCCEED);		
		break;
			
	}
	
	//rt_kprintf("success\n");
	//rt_kprintf("hr_n: %d\n",p->hr_n);

	//g_app_networking_gw.check_ok = 1;
}

//主从一体

uint8_t g_slv_addr;
FUNC_DIRTRAN_T g_func_dirtran;			//从机

int clr_modbus_master_func(void){
		FUNC_MODBUS_MASTER_T *p=&g_func_modbus_master0;
		p->func_inj_t=p->func_inj_h=0;		//清空
}

int fill_modbus_master_send_buff(uint8_t* buff,int buff_size){
		FUNC_MODBUS_MASTER_T *p=&g_func_modbus_master0;
		//p->func_inj_t=p->func_inj_h=0;		//清空
		if(get_func(p))
		{
			int i = modbus_master_send(&p->md,buff);
			if(i>0)
			{
				return i;
				
			}
		}
		return 0;
}
int slove_modbus_master_recv_buff(uint8_t* recv,int recv_len){
		FUNC_MODBUS_MASTER_T *p=&g_func_modbus_master0;
		if(modbus_master_rec(&p->md,recv,recv_len)==MODBUS_SUCCESS)
		{				
			//一次成功的通讯
			func_modbus_master_success(&p->md);
//			func_modbus_master_ctrl_result(&p->md);
			return 1;
		}
		return 0;
}

void send_mosbud_data(rt_device_t device,uint8_t*data,int len){		//函数内带rts操作
	set_modbus_send_rts();
	rt_device_write(device,0,data,len);
	rt_int32_t nms = (rts_cfg.rts_delay_us/1000) ? (rts_cfg.rts_delay_us/1000) : 1;
	rt_thread_mdelay(nms*10);
	set_modbus_recv_rts();

}

#define MODBUS_BUFF_SIZE 1024
uint8_t modbus_buff[MODBUS_BUFF_SIZE];

void func_modbus_mas_slv_exec(FUNC_MODBUS_MASTER_T *p,FUNC_DIRTRAN_T *p_slv)
{
	int i;
	rt_device_t device;//rt_device_find
	device=p->device;
	//USART_T *pu=p->pu;	
	FUNC_DIRTRAN_PORT *pc=&p_slv->ch1;

#if 0
	switch(p->step)
	{
		   case FUNC_RECV_DATA:
				{
					//rt_kprintf("FUNC_RECV_DATA\n");
					
					int len=rt_device_read(device,0,modbus_buff,MODBUS_BUFF_SIZE);
					if(len>0){
						hexdump("<<<",modbus_buff,len);
					}
					
					if(usart_chk_recbyte(pu))
					{
						i= MyRnd() % 10;		//取0-9随机数
						i += FUNC_DIRTRAN_CHKBUS_DELAY;
						left_ms_set(&pc->tm,i);		//如果期间收到其他接收，缓10mS再操作
					}
				#if 1
					if(usart_chk_frame(pu))
					{
							//log_i("slv 接收字节 %d\n",usart_recv_bytes(pu));
							//uint16_t recv_byte=usart_recv(p->pu,dev_recv_buf,Sconx HELL_USART_RX_BUF_SIZE);
							//hexdump("<--<",pu->rx_buf,pu->r_e);
							if(pu->rx_buf[0]==p_slv->ch1.md.slv)
							{
									//rt_kprintf("slv process 253 data\n");
									if(modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e)>0)			//是本地从机地址，MODBUS解析
									{
										//253数据到达的解析 
										//ctrl_set_event(EVENT_BRK_EVT);			//收到断路器上报的告警标志，去读取告警HR
										//tmp_event_addr=1;
										
										left_ms_set(&pc->tm,FUNC_DIRTRAN_SEND_DELAY);			//解析成功		
										rt_kprintf("slv HR start addr: %d\n",pc->md.da_adr);
										if(TOUCH_HR_START_DATA_ADDR==pc->md.da_adr)
										{

												#ifdef USE_SCENE_INFO
													uint16_t t_id,s_id;			//触摸面板id 触摸面板序号
													s_id=HR1[pc->md.da_adr+2];
													t_id=HR1[pc->md.da_adr+3];
													rt_kprintf("触摸板 控制 t_id %d, s_id %d\n",t_id,s_id);

													scene_process_que_add(t_id,s_id);
													ctrl_set_event(EVENT_END_SCENE_POOL);		//结束轮询

												#endif
												p->step=FUNC_SLV_MODBUS_SEND;				//下一步：应答
										}
										else if(RELAY_ADJUST_TIME_HR_START_DATA_ADDR==pc->md.da_adr)		//为了给7寸屏校时
										{
												//填充时间 HR1
												rt_kprintf("屏幕读取时间\n");
												struct tm t;
												rtc_get_time(&t);
												//rt_kprintf("rtc_time: %d/%d/%d,%d:%d:%d\n",t.tm_year,t.tm_mon,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec);
												int pos=RELAY_ADJUST_TIME_HR_START_DATA_ADDR;		//偏移
												HR1[pos++]=OLD_ALL_SEL_EN;
												HR1[pos++]=t.tm_year;
												HR1[pos++]=t.tm_mon;
												HR1[pos++]=t.tm_mday;
												HR1[pos++]=t.tm_hour;
												HR1[pos++]=t.tm_min;
												HR1[pos++]=t.tm_sec;				
											p->step=FUNC_SLV_MODBUS_SEND;				//下一步：应答
										}
										else
										{
//										#ifdef USE_253
//											rt_kprintf("注意：253被我注释\n");
										#if 1
											g_slv_addr=HR1[pc->md.da_adr];
											rt_kprintf("slv report 253, addr: %d\n",HR1[pc->md.da_adr]);	
											if(g_app_ctrl.step==APP_CTRL_STEP_IDLE||g_app_ctrl.step==APP_CTRL_STEP_WAIT_SLV_FINISH)
											{
												ctrl_set_event(EVENT_WAIT_SLV_FINISH);
											}
											else
											{
												rt_kprintf("设备忙 忽略253\n");

											}
										#endif
//										#else
//											//rt_kprintf("function 253 has been removed\n");			//提示253功能已经移除
//										#endif
										}
//										p->step=FUNC_SLV_MODBUS_SEND;				//下一步：应答
									}
												
								
							}
							else if(pu->rx_buf[0]=='{'&&pu->rx_buf[1]=='\"')			//查看如果是json头：{"
							{
//								char tmp_str[10]={0};
//								get_json_1fs(tmp_str,sizeof(tmp_str),(char*)pu->rx_buf,"enterprise");
//								memset(pu->rx_buf,0,pu->r_e);
//								rt_kprintf("set enterprise %s\n",tmp_str);

//								if(atoi(tmp_str)!=0)
//								{
//										srt_kprintf((char*)pu->tx_buf,"SAVE_OK:%s\n",tmp_str);	
//										ef_set_env("enterprise",tmp_str);
//										ef_save_env();						
//								}
//								else
//								{
//										srt_kprintf((char*)pu->tx_buf,"SAVE_ERR\n");
//								}
//								p->step=FUNC_SLV_CFG_SEND;				//下一步：应答
								
							}
							else if(pu->rx_buf[0]==0)		//广播指令（场景面板执行指令 是广播）
							{
//									rt_kprintf("广播指令\n");
//									modbus_slv_rec(&pc->md,pu->rx_buf,pu->r_e);		//解析数据到 modbus（pc->md）结构体
//									if(SCENE_PANEL_SCENE_CTRL_HR_START_DATA_ADDR==pc->md.da_adr)
//									{
//											scene_panel_id=HR1[pc->md.da_adr];
//											scene_panel_state=HR1[pc->md.da_adr+1];
//											rt_kprintf("场景面板 执行  %d, %d\n",scene_panel_id,scene_panel_state);
//											net_set_send_event(EVENT_SEND_SCENE_PANEL_STATE_REPORT);
//											
//									}
							}
							else
							{
//							#ifndef USE_253
								if(pu->rx_buf[1]==MD_FR_MHR)		//0x10 功能码：写多个寄存器
								{
//										dev_change_state.dev_type=no_dev;
//										dev_change_state.slv_addr=pu->rx_buf[0];
//										if(dev_change_state.slv_addr<253){
//											if(no_dev!=(dev_change_state.dev_type=get_slv_addr_type_by_cfg(dev_change_state.slv_addr)))			//在设备池内存在
//											{
//													log_i("state change addr: %d, type: %d\n",dev_change_state.slv_addr,dev_change_state.dev_type);
//													uint16_t hr_num=u8_2_u16(&pu->rx_buf[4]);
//													uint16_t *hr_data=(uint16_t*)&pu->rx_buf[7];				//HR开始位置
//													//hexdump("---",&pu->rx_buf[7],len*2);
//													
//													if(hr_num>sizeof(dev_change_state.hr_data))
//													{
//														log_e("hr_num too long: %d !\n",hr_num);
//														
//													}
//													else
//													{
//														short_copy_xch(dev_change_state.hr_data,hr_data,hr_num*sizeof(uint16_t),1);
//														//hexdump_16("---",dev_change_state.hr_data,hr_num);
//														//memcpy(dev_change_state.hr_data,hr_data,hr_num*sizeof(uint16_t));
//														net_set_send_event(EVENT_SEND_STATE_CHANGE_REPORT);		
//													}
//											}
//											else
//											{
//												log_w("addr: %d, not found type\n",dev_change_state.slv_addr);
//											}
//										}
								}
//							#endif
							}
							usart_rx_rst(pu);
					}
					else	
				#endif
					{
							p->step=FUNC_MODBUS_MASTER_STEP_CHK_PERIOD;
					}
				
				}
    		break;
    	case FUNC_SLV_MODBUS_SEND:
						if(left_ms(&pc->tm)==0)
						{
							i=modbus_slv_send(&pc->md,pu->tx_buf);
							//hexdump(">-->",pu->tx_buf,i);
							if(i)
							{
								//usart_send(pu,pu->tx_buf,i);
								usart_send_start(pu,i);
							}
							
							p->step=FUNC_RECV_DATA;
						}
			
				
    		break;
				case FUNC_SLV_CFG_SEND:				//串口配置企业码 应答
						if(left_ms(&pc->tm)==0)
						{
							i=strlen((char*)pu->tx_buf);
							if(i)
							{
								usart_send_start(pu,i);
							}
							p->step=FUNC_RECV_DATA;
						}
				
    		break;
												
		case FUNC_MODBUS_MASTER_STEP_CHK_PERIOD:
			if(left_ms(&p->tm)==0)
			{
				if(usart_chk_recbyte(p->pu))
				{
					i= MyRnd() % 10;		//取0-9随机数
					i += FUNC_MODBUS_MASTER_CHKBUS_DELAY;
					left_ms_set(&p->tm,i);		//如果期间收到其他接收，缓10mS再操作
				}
				else 
				if(left_ms(&p->tm_period)==0)
				{
					left_ms_set(&p->tm_period,p->send_period);
					if(get_func(p))
					{
						
						i = modbus_master_send(&p->md,p->pu->tx_buf);				//主机发送函数
						//rt_kprintf("发送字节 %d\n",i);
						if(i>0)
						{
							p->send_rty_cnt = 0;
							if(DEV_STATE==0)
							{
								usart_hd_send_start(p->pu,i);
							}
//							else 
//							{
//								rt_kprintf("...\n");
//							}
							
							if(p->pu->tx_buf[0]==0x00){
								#ifdef DEBUG_MODE
									rt_kprintf("发送广播，不等待，不重发\n");
								#endif
									p->step=FUNC_RECV_DATA;
							}
							else{
									left_ms_set(&p->tm,p->send_tout);
									p->step=FUNC_MODBUS_MASTER_STEP_CHK_REC;
							}
						}
					}
					else{
						
						p->step=FUNC_RECV_DATA;
					
					}
					
				}
			}
			break;
		case FUNC_MODBUS_MASTER_STEP_CHK_REC:
		#if 1
			if(usart_chk_frame(p->pu))
			{
				//log_i("mas 接收字节 %d\n",usart_recv_bytes(pu));
				//rt_kprintf("数据接收\n");
				if((p->cur_func_mode == FUNC_MODBUS_MASTER_FUNC_CYC) && (p->func_inj_h != p->func_inj_t))
				{	//当前执行命令是循环命令且发现有注入命令，直接抛弃这次通讯任务
					left_ms_set(&p->tm,p->send_delay);
					p->step=FUNC_RECV_DATA;
				}
				else if(modbus_master_rec(&p->md,p->pu->rx_buf,p->pu->r_e)==MODBUS_SUCCESS)
				{
				#ifdef DEBUG_MODE
					rt_kprintf("modbus 解析成功\n");
				#endif
					left_ms_set(&p->tm,p->send_delay);
					p->step=FUNC_RECV_DATA;
					//hexdump("rx",p->pu->rx_buf,p->pu->r_e);			//输出从机的应答数据
					//一次成功的通讯
					func_modbus_master_success(&p->md);				//收到从机应答				
					
				}
				else				//重发
				{
					i= MyRnd() % 10;		//取0-9随机数
					i += FUNC_MODBUS_MASTER_CHKBUS_DELAY+p->send_tout;
					left_ms_set(&p->resend_tm,i);		//如果期间收到其他接收，缓10mS再操作
					p->step=FUNC_MODBUS_MASTER_STEP_RESEND;
				} 
				usart_rx_rst(pu);
			
			}
			else 
		#endif
			if(left_ms(&p->tm)==0)				//重发
			{
				i= MyRnd() % 10;		//取0-9随机数
				i += FUNC_MODBUS_MASTER_CHKBUS_DELAY;
				left_ms_set(&p->tm,i);		//如果期间收到其他接收，缓10mS再操作
				p->step=FUNC_MODBUS_MASTER_STEP_RESEND;	
			}
			break;
		case FUNC_MODBUS_MASTER_STEP_RESEND:			//重发
			if(left_ms(&p->tm)==0)
			{
				
				if(usart_chk_recbyte(p->pu))
				{
					i= MyRnd() % 10;		//取0-9随机数
					i += FUNC_MODBUS_MASTER_CHKBUS_DELAY;
					left_ms_set(&p->tm,i);		//如果期间收到其他接收，缓10mS再操作
				}
				else
				{
					if(p->send_rty_cnt >= p->send_rty_nb)
					{
						p->step=FUNC_RECV_DATA;
					}
					else
					{
						
						p->send_rty_cnt ++;
						p->err_cnt++;
						i = modbus_master_send(&p->md,p->pu->tx_buf);
						if(i>0)
						{
							left_ms_set(&p->tm_period,p->send_period);
							usart_hd_send_start(p->pu,i);
							left_ms_set(&p->tm,p->send_tout);
							p->step=FUNC_MODBUS_MASTER_STEP_CHK_REC;
						}
						else
						{
							p->step = FUNC_RECV_DATA;
						}
					}
				}
			}
			break;
	}
#endif

}



typedef struct{							
	uint8_t slv_addr;					//从机地址
	_dev_type dev_type;				//从机类型，根据类型 在ctrl_dev_func中 选择对应的控制函数
}__dev_info_struct;
__dev_info_struct curr_dev;

void set_slv_dev_addr(_dev_type dev_type,uint8_t slv_addr){
	curr_dev.dev_type=dev_type;
	curr_dev.slv_addr=slv_addr;
	master0_tb_slv_hr[0].slv=slv_addr;
}

_dev_type get_slv_dev_type(uint8_t slv_addr){
		if(slv_addr==curr_dev.slv_addr){
			return curr_dev.dev_type;
		
		}
		return -1;
}

uint16_t* get_slv_hr_p(uint8_t slv_addr){
	return master0_tb_slv_hr[0].phr;			//拿到 slv 地址对应的HR指针
}

uint16_t get_slv_hr_num(_dev_type slv_type){
	switch(slv_type){
		case
			relay_dev:		//必须对应 master0_tb_slv_hr 数组下标
		return RELAY_ALL_HR_NUM;
		case
			dimmer_dev:		//
		return DIMMER_ALL_HR_NUM;
		case
			srelay_dev:	
		return SRELAY_ALL_HR_NUM;
		case
			radar_dev:	
		return RADAR_ALL_HR_NUM;
		case		
			relay_16_dev:		//  
		return RELAY_16_ALL_HR_NUM;
		case		
			curtain_dev:		//  
		return CURTAIN_ALL_HR_NUM;
		case		
			scr_dev:		//  
		return SCR_ALL_HR_NUM;
		case		
			scene_panel_dev:		//
		return SCENE_PANEL_ALL_HR_NUM;	
	#ifdef USE_BRK
		case		
			brk_1p_dev:		//断路器设备
		return BRK_1P_ALL_HR_NUM;	
		case		
			brk_3p_dev:		//断路器设备
		return BRK_3P_ALL_HR_NUM;	
	#endif
	#ifdef USE_SRELAY_16
		case		
			srelay_16_dev:		//  
		return SRELAY_16_ALL_HR_NUM;
	#endif
		case		
			protect_dev:		//
		return PROTECT_ALL_HR_NUM;	
		default:
			log_e("undefined type %d\n",slv_type);
			return 0;
	}
}



//写多个寄存器 注入包：从设备地址，HR起始地址，HR数量，HR要填充的数据
void add_modbus_master_wr_hr_inj_package(uint8_t slv_addr,uint16_t HR_start_data_addr,uint16_t HR_data_num,uint16_t* HR_data){
	rt_uint32_t e=0;
	rt_event_recv(global_event,EVENT_SVL_ASK_SUCCEED,RT_EVENT_FLAG_CLEAR|RT_EVENT_FLAG_AND,RT_WAITING_NO,&e);
//	clr_event(&g_app_ctrl.event,EVENT_SVL_RESPONSE_SUCCEED);
	FUNC_MODBUS_MASTER_FUNC_T mf;
	mf.slv=slv_addr;
	mf.func=MD_FR_MHR;
	mf.da_adr=HR_start_data_addr;
	mf.da_n=HR_data_num;
	memcpy(&get_slv_hr_p(slv_addr)[HR_start_data_addr],HR_data,HR_data_num*sizeof(uint16_t));
	func_modbus_master_add_func(&g_func_modbus_master0,&mf,FUNC_MODBUS_MASTER_FUNC_INJ,master0_tb_slv_hr);
}

//写多个寄存器 循环包：从设备地址，HR起始地址，HR数量，HR要填充的数据
void add_modbus_master_wr_hr_cyc_package(uint8_t slv_addr,uint16_t HR_start_data_addr,uint16_t HR_data_num,uint16_t* HR_data){
	FUNC_MODBUS_MASTER_FUNC_T mf;
	mf.slv=slv_addr;
	mf.func=MD_FR_MHR;
	mf.da_adr=HR_start_data_addr;
	mf.da_n=HR_data_num;
	memcpy(&get_slv_hr_p(slv_addr)[HR_start_data_addr],HR_data,HR_data_num*sizeof(uint16_t));
	func_modbus_master_add_func(&g_func_modbus_master0,&mf,FUNC_MODBUS_MASTER_FUNC_CYC,master0_tb_slv_hr);
}

//读多个寄存器 注入包：从设备地址，HR起始地址，HR数量
void add_modbus_master_rd_hr_inj_package(uint8_t slv_addr,uint16_t HR_start_data_addr,uint16_t HR_data_num){
	rt_uint32_t e=0;
	rt_event_recv(global_event,EVENT_SVL_ASK_SUCCEED,RT_EVENT_FLAG_CLEAR|RT_EVENT_FLAG_AND,RT_WAITING_NO,&e);
//	clr_event(&g_app_ctrl.event,EVENT_SVL_ASK_SUCCEED);	
	FUNC_MODBUS_MASTER_FUNC_T mf;
	mf.slv=slv_addr;
	mf.func=MD_RD_HR;
	mf.da_adr=HR_start_data_addr;
	mf.da_n=HR_data_num;
	//rt_kprintf("读取 地址：%d 数量：%d\n",HR_start_data_addr,HR_data_num);
	//memcpy(&get_slv_hr_p(slv_addr)[HR_start_data_addr],HR_data,HR_data_num*sizeof(uint16_t));
	func_modbus_master_add_func(&g_func_modbus_master0,&mf,FUNC_MODBUS_MASTER_FUNC_INJ,master0_tb_slv_hr);
}

//读多个寄存器 循环包：从设备地址，HR起始地址，HR数量
void add_modbus_master_rd_hr_cyc_package(uint8_t slv_addr,uint16_t HR_start_data_addr,uint16_t HR_data_num){
	FUNC_MODBUS_MASTER_FUNC_T mf;
	mf.slv=slv_addr;
	mf.func=MD_RD_HR;
	mf.da_adr=HR_start_data_addr;
	mf.da_n=HR_data_num;
	//memcpy(&get_slv_hr_p(slv_addr)[HR_start_data_addr],HR_data,HR_data_num*sizeof(uint16_t));
	func_modbus_master_add_func(&g_func_modbus_master0,&mf,FUNC_MODBUS_MASTER_FUNC_CYC,master0_tb_slv_hr);

}
uint16_t get_master0_tb_slv_hr_size(void){
	
	return sizeof(master0_tb_slv_hr)/sizeof(FUNC_MODBUS_MASTER_SLV_HR_T);

}


static void modbus_thread(void *param)
{
		
    while (1)
    {
				func_modbus_mas_slv_exec(&g_func_modbus_master0,&g_func_dirtran);		//云平台
				rt_thread_mdelay(10);
    }

}

void cfg_modbus_rts(int rts_pin,int rts_pin_mode){
	rts_cfg.rts_pin=rts_pin;
	rts_cfg.rts_pin_mode=rts_pin_mode;
	rt_pin_mode(rts_pin,PIN_MODE_OUTPUT);
}
void cfg_modbus_rts_delay(int us)
{
   rts_cfg.rts_delay_us=us;
}
void set_modbus_send_rts(void){

	rt_pin_write(rts_cfg.rts_pin,rts_cfg.rts_pin_mode);
}

void set_modbus_recv_rts(void){

	rt_pin_write(rts_cfg.rts_pin,!rts_cfg.rts_pin_mode);
}


static int mdbus_rtu_init(void)
{
		rt_device_t device;
		struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;  /* 初始化配置参数 */
		rt_err_t open_result = RT_EOK;
		/* find and open command device */
    device = rt_device_find(MODBUS_DEV_NAME);
    if (device)
    { 
				RT_ASSERT(device->type == RT_Device_Class_Char);
				config.baud_rate = BAUD_RATE_19200;        //修改波特率为 9600
				config.data_bits = DATA_BITS_8;           //数据位 8
				config.stop_bits = STOP_BITS_1;           //停止位 1
				config.bufsz     = 1024;                   //修改缓冲区 buff size 为 1024
				config.parity    = PARITY_NONE;           //无奇偶校验位
				rt_device_control(device, RT_DEVICE_CTRL_CONFIG, &config);
       
        /* using DMA mode first */
        open_result = rt_device_open(device, RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_DMA_RX);
        /* using interrupt mode when DMA mode not supported */
        if (open_result == -RT_EIO)
        {
            open_result = rt_device_open(device, RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_INT_RX);
        }
        RT_ASSERT(open_result == RT_EOK);
        //rt_device_set_rx_indicate(device, at_client_rx_ind);		//设置接收回调函数
    }
    else
    {
        rt_kprintf("mdbus_rtu_init failed! Not find the device(%s).", MODBUS_DEV_NAME);
        return -1;
    }
		cfg_modbus_rts(RS485_RTS,1);
		cfg_modbus_rts_delay(50);
		
		func_modbus_mas_slv_init(&g_func_modbus_master0,device,master0_tb_slv_hr,&g_func_dirtran,253);
    //rt_pin_mode(RS485_RE, PIN_MODE_OUTPUT);
    rt_thread_t tid;
    tid = rt_thread_create("modbus",
                           modbus_thread, RT_NULL,
                           2048,
                           12, 10);
    if (tid != RT_NULL)
        rt_thread_startup(tid);
    return RT_EOK;
}
//INIT_APP_EXPORT(mdbus_rtu_init);

