#include "at_device_E104.h"
#include "at_device_ZLG52810.h"
#include <rtthread.h>
#include "at_device.h"
#include <at.h>
#include "blue_app.h"
#include "global_define.h"
#include "easyflash.h"
#define DBG_TAG              "app.blue"
#define DBG_LVL              LOG_LVL_DBG
#include <rtdbg.h>
//extern uint8_t protocol_ble_one_by_one(uint8_t data); //lkk delte 2020-4-10
uint16_t r = 0, step = 0;

//#define Blue_Sleep() ;
//#define Blue_Wakeup() ;
//#define Blue_cmd_mode() ;
//#define Blue_transparent_mode() ;

//void BT_RX_IT_Enable(){					//睡眠前调用
//  GPIO_InitTypeDef GPIO_InitStruct;
//	__HAL_RCC_GPIOA_CLK_ENABLE();
//	
//  GPIO_InitStruct.Pin = GPIO_PIN_3;
//  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING; 
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//  
//  HAL_NVIC_SetPriority(EXTI3_IRQn, 1, 1);
//  HAL_NVIC_EnableIRQ(EXTI3_IRQn);
//}

//void BT_STA_IT_Enable(){					//睡眠前调用
//  GPIO_InitTypeDef GPIO_InitStruct;
//	__HAL_RCC_GPIOA_CLK_ENABLE();
//	
//  GPIO_InitStruct.Pin = BT_STA_PIN;
//  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING; 
//  GPIO_InitStruct.Pull = GPIO_PULLUP;
//  HAL_GPIO_Init(BT_STA_PORT, &GPIO_InitStruct);
//  
//  HAL_NVIC_SetPriority(BT_STA_EXIT_LINE, 1, 1);
//  HAL_NVIC_EnableIRQ(BT_STA_EXIT_LINE);
//}
//void BT_RX_IT_Disable(){
//	HAL_NVIC_DisableIRQ(EXTI3_IRQn);	
//}
//void BT_STA_IT_Disable(){
//	HAL_NVIC_DisableIRQ(BT_STA_EXIT_LINE);	
//}
static int blue_data_recv(char *data, int len)
{
		//hexdump("<<<",data,len);
    //rt_kprintf("para_data %d:\n",len);//lkk recover 2020-4-10
    for (int i = 0 ; i < len; i++)
    {
       // rt_kprintf("%X",data[i]);//lkk recover 2020-4-10
        protocol_ble_one_by_one(data[i]);// lkk delete 2020-4-10
    }
    return 0;
}
uint8_t ble_has_waked=1;

struct at_device *blue_device;
int blue_app_init(void)
{
	//blue_device=(struct at_device *)0x01;
#if TEST_SWITCH
	uint8_t device_id[16]="----";			//名字
#endif
	Blue_Wakeup();	//唤醒蓝牙
  Blue_cmd_mode();
	ble_has_waked=1;
	#ifdef SYS_DEBUG_ENABLE
	if(get_sys_use_uart() == 2)
	{
		return RT_ERROR;
	}
	#endif
    blue_device = at_device_init("uart2", 1024);
    if (blue_device == NULL)
    {
				init_err.ble_init_err=2;
				LOG_E(">>> ble failed\n");
        return -1;
    }
		//rt_kprintf(">>> 蓝牙设备地址 %p\n",blue_device);		
    at_device_set_para_callback(blue_device, blue_data_recv);

    char buf[30];
    struct at_buf get_buf;
    get_buf.buf = buf;
    get_buf.len = sizeof(buf);
    at_device_control(blue_device, BLUE_GET_DEVICE_NAME, (void *)&get_buf);

    at_device_control(blue_device, BLUE_GET_RSSI, (void *)&get_buf);
    	
		//rt_thread_delay(rt_tick_from_millisecond(1000));		//死机
		char svr_uuid_buf[16] ={0x00,0x00,0xff,0xf0,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5f,0x9b,0x34,0xfb};
		char rx_uuid_buf[16] = {0x00,0x00,0xff,0xf2,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5f,0x9b,0x34,0xfb};
		char tx_uuid_buf[16] = {0x00,0x00,0xff,0xf1,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5f,0x9b,0x34,0xfb};
		
    get_buf.buf = svr_uuid_buf;
    get_buf.len = sizeof(svr_uuid_buf);
    at_device_control(blue_device, BLUE_SET_SVR_UUID, (void *)&get_buf);
			
		get_buf.buf = tx_uuid_buf;
    get_buf.len = sizeof(tx_uuid_buf);
    at_device_control(blue_device, BLUE_SET_TX_UUID, (void *)&get_buf);


    get_buf.buf = rx_uuid_buf;
    get_buf.len = sizeof(rx_uuid_buf);
    at_device_control(blue_device, BLUE_SET_RX_UUID, (void *)&get_buf);

		init_err.ble_init_err=1;
		
		char set_buf[30]={0};
#if TEST_SWITCH
    strncpy(set_buf, device_id, 16);
#else
		rt_strncpy(&set_buf[0], ef_get_env("ble_prefix"), rt_strlen(ef_get_env("ble_prefix")));
		strcpy(&set_buf[rt_strlen(ef_get_env("ble_prefix"))], ef_get_env("device_id"));
#endif
    get_buf.buf = set_buf;
    get_buf.len = rt_strlen(set_buf);
		rt_kprintf("BLE name: %s\n",get_buf.buf);
    at_device_control(blue_device, BLUE_SET_DEVICE_NAME, (void *)&get_buf);

//	#ifndef SUPP_ZLG52810				//周立功蓝牙 无连接时 引脚输出50Hz波形 将频繁触发中断
//		BT_STA_IT_Enable();			
//	#endif

		LOG_I(">>> ble succeed %p\n",blue_device);
		//at_device_control(blue_device, BLUE_GET_MAC, (void *)&get_buf);		//mac地址内含有0x00的情况 怎么处理？
		Blue_transparent_mode();

		/*
		指示已经使用的引脚，不需要自动进入低功耗
		串口不需要指示
		*/
		set_RtPin_had_lp(BLUE_TOOTH_MODE_PIN,1);
		set_RtPin_had_lp(BLUE_TOOTH_WAKEUP_PIN,1);
		set_RtPin_had_lp(BLUE_TOOTH_STA_PIN,1);
	#if SUPPORT_BLUE_TOOTH_RESET_PIN
		set_RtPin_had_lp(BLUE_TOOTH_RESET_PIN,1);
	#endif
		return 0;
}
//INIT_APP_EXPORT(blue_app_init);

#include "Tool.h"
int blue_data_send(char *data, int len)
{
		at_device_control(blue_device, CMD_EXIT_LOW_POWER,0);
		//Blue_Wakeup();	//唤醒蓝牙
		ble_has_waked=1;
		Blue_transparent_mode();
		//hexdump(data,len);
  	return  at_device_send(blue_device, 0, data, len, 0);
}


static void blue_send(uint8_t argc, char **argv)
{
    blue_data_send(argv[1], rt_strlen(argv[1]));
}
MSH_CMD_EXPORT(blue_send, send blue data to uart.);


static void test_at_device(uint8_t argc, char **argv)
{
//	static rt_device_t serial; 
//	struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT; 
//	serial = rt_device_find("uart2");
//	config.baud_rate = BAUD_RATE_19200;
//	rt_device_control(serial, RT_DEVICE_CTRL_CONFIG, &config);
//	Blue_Wakeup();

//	Blue_transparent_mode();
//    rt_console_set_device("uart2");
	set_sys_use_uart(0);
}

MSH_CMD_EXPORT(test_at_device, test_at_device.);

void ble_enter_lp(){
	//LOG_I("blue_device %p\n",blue_device);
	if(blue_device==RT_NULL){
		rt_kprintf("blue_device is NULL\n");
		return;
	}

#if 1
	Blue_transparent_mode();
//	#ifdef SUPP_ZLG52810
//		SET_DOWN_INPUT(BLUE_TOOTH_MODE_PIN);
//	#endif
//SET_UP_INPUT(BLUE_TOOTH_MODE_PIN);

	rt_kprintf("Ble sleep\n");//lkk add 2020-4-27
	at_device_control(blue_device, CMD_ENTER_LOW_POWER,0);
		//Blue_Sleep();	
//SET_UP_INPUT(BLUE_TOOTH_WAKEUP_PIN);


#else

	#ifdef SYS_DEBUG_ENABLE
		if(get_sys_use_uart() == 2)
		{
			rt_kprintf("Ble not sleep\n");
	//	#ifdef SUPP_ZLG52810
	//		BT_RX_IT_Enable();
	//	#endif
			return ;
		}
	#endif
		BT_RX_IT_Enable();
		Blue_transparent_mode();
		if(ble_has_waked==1){
			Blue_Sleep();	
			ble_has_waked=0;
		}

#endif
}

void ble_exit_lp(){	
	if(blue_device==RT_NULL){
		rt_kprintf("blue_device is NULL\n");
		return;
	}
	#ifdef SYS_DEBUG_ENABLE
	if(get_sys_use_uart() == 2)
	{
		return ;
	}
	#endif


//#ifdef SUPP_ZLG52810
//	BT_RX_IT_Disable();
//#endif
	
	at_device_control(blue_device, CMD_EXIT_LOW_POWER,0);
	//Blue_Wakeup();	//唤醒蓝牙
	ble_has_waked=1;
	Blue_transparent_mode();

}
