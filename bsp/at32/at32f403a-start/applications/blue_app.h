
#ifndef __BLUE_APP_H__
#define __BLUE_APP_H__


#include <rtthread.h>
#include <at.h>
#include <board.h>
#include "global_define.h"

#ifdef W25Q_GPIO_PIN_REMAP
 #define  BT_STA_PIN BLUE_TOOTH_STA_GPIO_PIN
 #define BT_STA_PORT BLUE_TOOTH_STA_PORT//GPIOA
 #define BT_STA_EXIT_LINE	BLUE_TOOTH_STA_EXIT_LINE//EXTI1_IRQn 
 
#else
#define BT_STA_PIN	GPIO_PIN_9//GPIO_PIN_1 
#define BT_STA_PORT GPIOC//GPIOA
#define BT_STA_EXIT_LINE	EXTI9_5_IRQn//EXTI1_IRQn 

/* lkk delete 蓝牙引脚已经在对应蓝牙的头文件定义
#define BT_STA   GET_PIN(C,9)//GET_PIN(A,1)
#define BT_WKP	GET_PIN(C,8)
#define BT_MOD 	GET_PIN(A,15)
#define BT_PW		GET_PIN(A,6)
*/
#define BLUE_TOOTH_WAKEUP_PIN   GET_PIN(C, 8)
#define BLUE_TOOTH_STA_PIN   GET_PIN(C, 9) //lkk 2020-4-17 发现该引脚同时在蓝牙头文件定义使用
#define BLUE_TOOTH_MODE_PIN   GET_PIN(A, 15)
	
#define SUPPORT_BLUE_TOOTH_RESET_PIN RT_FALSE //没使用reset 引脚
#if SUPPORT_BLUE_TOOTH_RESET_PIN
#define BLUE_TOOTH_RESET_PIN   GET_PIN(A, 6) 
#endif

#endif
int blue_app_init(void);

void BT_STA_IT_Enable(void);
void BT_RX_IT_Enable(void);
void ble_enter_lp(void);
void ble_exit_lp(void);



/******			发送接口			********/
int blue_data_send(char *data, int len);
int blue_data_recv(char *data, int len);


#endif


