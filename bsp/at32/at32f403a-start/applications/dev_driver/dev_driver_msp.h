#ifndef dev_driver_msp_h
#define dev_driver_msp_h

//#include "global.h"
#include "stdint.h"
#include "rtthread.h"
#include "stdarg.h"
#include "stdio.h"
#include "stdlib.h"

void dev_driver_msp_reset_baud_parity(rt_device_t rt_device,uint32_t baud,uint8_t UART_Parity) ;
int dev_driver_msp_usart_send(rt_device_t rt_device,unsigned char *buf,unsigned short n);
int dev_driver_msp_usart_recv(rt_device_t rt_device,unsigned char *buf,unsigned short n);
	
#endif
