#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "dev_driver.h"
#include "dev_driver_msp.h"
#include "global.h"
#define LOG_TAG "dev_driver"
DEV_DRIVER_T g_dev_driver;

#define DEBUG_PRINT

#ifdef DEBUG_PRINT
	#define driver_printf printf
#else
	#define driver_printf //
#endif

#ifndef EF_USING_LOG			//日志记录
	#define log_a driver_printf
	#define log_e driver_printf
	#define log_w driver_printf
	#define log_i driver_printf
	#define log_d driver_printf
	#define log_v driver_printf
#endif

// 设备类型数组
DeviceType device_types[MAX_DEVICE_TYPES];

// 注册设备类型
DeviceType* device_register_type(char *name) {
    int i;
    for (i = 0; i < MAX_DEVICE_TYPES; i++) {
        if (strlen(device_types[i].name) == 0) {
            strcpy(device_types[i].name, name);
            device_types[i].num_devices = 0;
            device_types[i].registered=1;
					
						device_types[i].transfer_wr = NULL;
						device_types[i].transfer_rd = NULL;
						device_types[i].read_data_wr = NULL;
						device_types[i].read_data_rd = NULL;
						device_types[i].alarm = NULL;
						device_types[i].poll_wr = NULL;
						device_types[i].poll_rd = NULL;
						device_types[i].call_wr = NULL;
						device_types[i].call_rd = NULL;
						device_types[i].chk_online_wr = NULL;
						device_types[i].chk_online_rd = NULL;
						device_types[i].control_wr = NULL;
						device_types[i].control_rd = NULL;
						device_types[i].evt_wr = NULL;
						device_types[i].evt_rd = NULL;
				
            return &device_types[i];
            break;
        }
    }
    if (i == MAX_DEVICE_TYPES) {
        log_e("Max device types exceeded\n");
        return NULL;
    }
}

// 注册设备
Device *device_register(DeviceType *type, char* dev_id) {
    // 创建设备
    if (type->num_devices >= MAX_DEVICES_PER_TYPE) {
        driver_printf("Max devices per type exceeded\n");
        return NULL;
    }
		/*
			可以添加识别相同 dev_id 来覆盖的逻辑
		*/
		int size=sizeof(Device);
		//driver_printf("malloc %d\n",size);
    Device *device = (Device *)malloc(sizeof(Device));			//动态申请内存，不释放（不允许同一个设备重复注册）
    device->type = type;
    strcpy(device->dev_id,dev_id);
    device->registered=1;
    device->is_online = false;
		
    type->devices[type->num_devices] = device;
    type->num_devices++;

    return device;
}
// 查找设备类型 通过类型名
DeviceType *device_find_type(char* type_name) {
    int i;
    for (i = 0; i < MAX_DEVICE_TYPES; i++) {
        if (device_types[i].registered != 0) {
            if(0==strcmp(device_types[i].name,type_name)) {
                return &device_types[i];

            }

        }
    }

    driver_printf("not found type: %s\n",type_name);
    return NULL;

}
// 查找设备通过顺序，从0开始
DeviceType *device_find_type_idx(uint8_t idx) {

    uint8_t curr_idx=0;
    for (int i = 0; i < MAX_DEVICE_TYPES; i++) {
        if (device_types[i].registered != 0) {
            if(curr_idx==idx) {
                return &device_types[i];
            }
            curr_idx++;
        }
    }
    //driver_printf("not found idx: %d\n",idx);
    return NULL;

}
// 查找设备
Device *device_find(DeviceType *type, char* dev_id) {
    if(type==NULL) {
        driver_printf("DeviceType NULL\n");
        return NULL;

    }
    // 查找设备
    for (int j = 0; j < type->num_devices; j++) {
        Device *device = type->devices[j];
        //driver_printf("设备名 %s\n",device->dev_id);
        if (device->registered != 0) {
            if (0==strcmp(dev_id,device->dev_id)) {
                //driver_printf("找到 : %s\n",device->dev_id);
                return device;
            }
        }
    }
    //driver_printf("找不到\n");
    return NULL;
}

// 查找设备通过顺序，从0开始
Device *device_find_idx(DeviceType *type,uint8_t idx) {
    if(type==NULL) {
        driver_printf("DeviceType NULL\n");
        return NULL;

    }
    uint8_t curr_idx=0;
    // 查找设备
    for (int j = 0; j < type->num_devices; j++) {
        Device *device = type->devices[j];
        //driver_printf("设备名 %s\n",device->dev_id);
        if (device->registered != 0) {

            if(curr_idx==idx) {
                return device;

            }
            curr_idx++;
        }
    }
    //driver_printf("找不到\n");
    return NULL;
}

// 查找设备
Device *device_find_by_type_strid(char* type_name,char* dev_id) {
	
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
		if(device==NULL)
		{
			log_e("type %s dev_id %s not found\n",type_name,dev_id);
		}
		return device;
}

// 查找设备 hex id（输入hexid ，自动转为strid处理）
Device *device_find_by_type_hexid(char* type_name,uint8_t* dev_hex_id,int id_len) {
		char str_addr[DEV_ID_SIZE];
		memset(str_addr,0,sizeof(str_addr));
		hexarr_to_strarr((uint8_t*)str_addr,dev_hex_id,id_len);		//地址 000000000001 => "000000000001"
	
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,str_addr);
		if(device==NULL)
		{
			log_e("type %s dev_id %s not found\n",type_name,str_addr);
		}
		return device;
}

void set_loop_tm(_loop_cfg* loop, unsigned long loop_tm) {			//启动is_run=1
    loop->is_run=1;
    left_ms_set(&loop->loop_tm, loop_tm);
}
void end_loop_tm(_loop_cfg* loop) {
    loop->is_run=0;


}
uint8_t check_loop_tm(_loop_cfg loop) {		//检查是否启动，并且是否到达延时时间
    if(loop.is_run==1&&left_ms(&loop.loop_tm) == 0) {
        return 1;

    }
    return 0;
}

//记录跳转是为了让状态机回到空闲状态，可以下发和接收数据
void set_ack_ok_loop_tm(_loop_cfg *loop,device_status_t step,unsigned long loop_tm){
	loop->ack_ok_step=step;
	set_loop_tm(loop,loop_tm);
	
}
void end_ack_ok_loop_tm(_loop_cfg *loop){
	loop->ack_ok_step=0;	//清空记录的步骤
	end_loop_tm(loop);
	
}

void clr_all_val_before_wr(DEV_DRIVER_T *p){
		Ring_Buf_Clr(&p->app_recv_que.data_que);		//先清除 以免之前的数据干扰
		Ring_Buf_Clr(&p->app_send_que.data_que);
		p->app_send_que.data_num=0;	
		p->app_recv_que.data_num=0;
		//end_ack_ok_loop_tm(p);
}
void clr_recv_val_before_wr(DEV_DRIVER_T *p){
		Ring_Buf_Clr(&p->app_recv_que.data_que);		//先清除 以免之前的数据干扰
//		Ring_Buf_Clr(&p->send_que.data_que);
//		p->send_que.data_num=0;	
		p->app_recv_que.data_num=0;
		//end_ack_ok_loop_tm(p);
}
void clr_send_val_before_wr(DEV_DRIVER_T *p){
		//Ring_Buf_Clr(&p->recv_que.data_que);		//先清除 以免之前的数据干扰
		Ring_Buf_Clr(&p->app_send_que.data_que);
		p->app_send_que.data_num=0;	
		//p->recv_que.data_num=0;
		//end_ack_ok_loop_tm(p);
}

void clr_inner_all_val_before_wr(DEV_DRIVER_T *p){
		Ring_Buf_Clr(&p->inner_recv_que.data_que);		//先清除 以免之前的数据干扰
		Ring_Buf_Clr(&p->inner_send_que.data_que);
		p->inner_send_que.data_num=0;	
		p->inner_recv_que.data_num=0;
		//end_ack_ok_loop_tm(p);
}
void clr_inner_recv_val_before_wr(DEV_DRIVER_T *p){
		Ring_Buf_Clr(&p->inner_recv_que.data_que);		//先清除 以免之前的数据干扰
//		Ring_Buf_Clr(&p->inner_send_que.data_que);
//		p->inner_send_que.data_num=0;	
		p->inner_recv_que.data_num=0;
		//end_ack_ok_loop_tm(p);
}
void clr_inner_send_val_before_wr(DEV_DRIVER_T *p){
		//Ring_Buf_Clr(&p->inner_recv_que.data_que);		//先清除 以免之前的数据干扰
		Ring_Buf_Clr(&p->inner_send_que.data_que);
		p->inner_send_que.data_num=0;	
		//p->inner_recv_que.data_num=0;
		//end_ack_ok_loop_tm(p);
}

// 读取设备数据 下发
dev_driver_err device_read_data_wr(Device *device) {
		DEV_DRIVER_T *p=&g_dev_driver;
		p->inner_opt_flag=0;
    if (device->is_online && device->type->read_data_wr != NULL) {
				if(p->app_send_que.data_num<=0){
					clr_all_val_before_wr(p);
					
				}
        device->type->read_data_wr(device,&p->app_send_que);		//函数会把要发送的数据填到队列内
				p->curr_wr_device=device;
				return DEV_DRIVER_OK;
    }
		log_e("%s dev_id: %s err\n",__FUNCTION__,device->dev_id);
		return DEV_DRIVER_ERR;
}

// 读取设备数据 应答数据
dev_driver_err device_read_data_rd(Device *device) {
	DEV_DRIVER_T *p=&g_dev_driver;
	if (device->is_online && device->type->read_data_rd != NULL) {
//		int len;
//		if((len= fixlen_que_Rd(&p->recv_que,p->recv_data_buff, DEV_DRIVER_DATA_BUFF_SIZE)) > 0)		//从队列 获取设备的 应答数据
		if (fixlen_que_firsize(&p->app_recv_que.data_que)>0) {
			return device->type->read_data_rd(device,&p->app_recv_que);
		}
		else{
			
			//driver_printf("dev_id: %s, no resp\n",device->dev_id);
			return DEV_DRIVER_NO_RESP;
		}
	}
	log_e("%s dev_id: %s err\n",__FUNCTION__,device->dev_id);
	return DEV_DRIVER_ERR;
}

// 透传 下发数据
dev_driver_err device_transfer_wr(Device *device,uint8_t *data,int data_len) {
		DEV_DRIVER_T *p=&g_dev_driver;
		p->inner_opt_flag=0;
    if (device->is_online && device->type->transfer_wr != NULL) {
				if(p->app_send_que.data_num<=0){
					clr_all_val_before_wr(p);
				}
        device->type->transfer_wr(device,&p->app_send_que,data,data_len);		//函数会把要发送的数据填到队列内
				p->curr_wr_device=device;
				return DEV_DRIVER_OK;
    }	
		
		log_e("%s dev_id: %s err\n",__FUNCTION__,device->dev_id);
		return DEV_DRIVER_ERR;
}

// 透传 应答数据
dev_driver_err device_transfer_rd(Device *device,uint8_t *data,int data_buff_size,int* data_len) {
	DEV_DRIVER_T *p=&g_dev_driver;
	if (device->is_online && device->type->transfer_rd != NULL) {
//		int len;
//		if((len= fixlen_que_Rd(&p->recv_que,p->recv_data_buff, DEV_DRIVER_DATA_BUFF_SIZE)) > 0)		//从队列 获取设备的 应答数据
		if (fixlen_que_firsize(&p->app_recv_que.data_que)>0) {
			return device->type->transfer_rd(device,&p->app_recv_que,data,data_buff_size,data_len);
		}
		else{
			//driver_printf("dev_id: %s, no resp\n",device->dev_id);
			*data_len=0;
			return DEV_DRIVER_NO_RESP;
		}
	}
	log_e("%s dev_id: %s err\n",__FUNCTION__,device->dev_id);
	return DEV_DRIVER_ERR;
}


// 控制 数据 下发
dev_driver_err device_control_wr(Device *device,ctrl_opt op,void* arg) {
		DEV_DRIVER_T *p=&g_dev_driver;
	p->inner_opt_flag=0;
    if (device->is_online && device->type->control_wr != NULL) {			
				if(p->app_send_que.data_num<=0){
					clr_all_val_before_wr(p);
				}
        device->type->control_wr(device,op,arg,&p->app_send_que);		//函数会把要发送的数据填到队列内
				p->curr_wr_device=device;
				return DEV_DRIVER_OK;
    }
		log_e("%s dev_id: %s err\n",__FUNCTION__,device->dev_id);
		return DEV_DRIVER_ERR;
}

// 控制 数据 应答数据
dev_driver_err device_control_rd(Device *device,ctrl_opt op,void* arg) {
	DEV_DRIVER_T *p=&g_dev_driver;
	if (device->is_online && device->type->control_rd != NULL) {
//		int len;
//		if((len= fixlen_que_Rd(&p->recv_que,p->recv_data_buff, DEV_DRIVER_DATA_BUFF_SIZE)) > 0)		//从队列 获取设备的 应答数据
		if (fixlen_que_firsize(&p->app_recv_que.data_que)>0) 
		{
			return device->type->control_rd(device,op,arg,&p->app_recv_que);
		}
		else{
			//driver_printf("dev_id: %s, no resp\n",device->dev_id);
			return DEV_DRIVER_NO_RESP;
		}
	}
	log_e("%s dev_id: %s err\n",__FUNCTION__,device->dev_id);
	return DEV_DRIVER_ERR;
}

// 开始召测
dev_driver_err device_call_start(Device *device,int time_s) {
		DEV_DRIVER_T *p=&g_dev_driver;
		left_ms_set(&p->call_cnt_down_tm,time_s*1000);
    set_event(&p->event,EVENT_CALL_START);			//发送开始召测事件
		p->curr_call_device=device;
		return DEV_DRIVER_OK;
  
}
// 结束召测
dev_driver_err device_call_end(Device *device) {
		DEV_DRIVER_T *p=&g_dev_driver;
		set_event(&p->event,EVENT_CALL_END);			//发送结束召测事件
    return DEV_DRIVER_OK;
}



// 控制设备通过句柄
//void dev_control_wr_by_handle(DeviceType *type, char* dev_id,ctrl_opt op,void* arg) {
//    Device *device = device_find(type, dev_id);
//    if (device == NULL) {
//        driver_printf("device not found\n");
//        return;
//    }
//    dev_control_wr(device,op,arg);
//}

// 报警
void device_alarm(Device *device) {
    if (device->is_online && device->type->alarm != NULL) {
        device->type->alarm(device);
    }
}

// 读取设备数据 下发 通过句柄
void device_read_data_wr_by_handle(DeviceType *type, char* dev_id) {
		if(type==NULL){
				driver_printf("device type NULL\n");
        return;
				
		}
    Device *device = device_find(type, dev_id);
    if (device == NULL) {
        driver_printf("device not found\n");
        return;
    }
    device_read_data_wr(device);
}
// 读取设备数据 应答 通过句柄
void device_read_data_rd_by_handle(DeviceType *type, char* dev_id) {
		if(type==NULL){
				driver_printf("device type NULL\n");
        return;
				
		}
    Device *device = device_find(type, dev_id);
    if (device == NULL) {
        driver_printf("device not found\n");
        return;
    }
    device_read_data_rd(device);
}

// 报警通过句柄
void device_alarm_by_handle(DeviceType *type, char* dev_id) {
    Device *device = device_find(type, dev_id);
    if (device == NULL) {
        driver_printf("device not found\n");
        return;
    }
    device_alarm(device);
}

dev_driver_err device_add_data_to_sque(que_struct *send_que,uint8_t* data,int len){
	
		//插入数据到队列
		if(-1==fixlen_que_Wr(&send_que->data_que,data,len))
		{
				log_e("send_que add failed\n");
				driver_printf("队列添加失败\n");
				return DEV_DRIVER_ERR;
		}
		send_que->data_num++;		//自增

		return DEV_DRIVER_OK;
}
//读取队列数据 ，返回剩余条数
int device_recv_rque_to_data(que_struct *recv_que,uint8_t* data,int data_size,int* len){
	
		//读取队列数据
		
		if(0==(*len=fixlen_que_Rd(&recv_que->data_que,data,data_size))){
//				log_e("recv_que no data\n");
//				driver_printf("队列数据 空\n");
				return -1;
		}
		else{
			recv_que->data_num--;
			return recv_que->data_num;
			
		}		
}

static uint8_t select_step_by_loop_tm(DEV_DRIVER_T *p) {

    if(check_loop_tm(p->loop_poll_wr)) {
        //driver_printf("轮询 WR\n");
				if(p->loop_poll_wr.ack_ok_step!=0){
						p->step=p->loop_poll_wr.ack_ok_step;
						end_ack_ok_loop_tm(&p->loop_poll_wr);
						
				}
				else{
					p->step=DEV_DRIVER_STEP_POLL_WR;
				}
        return 1;
    }
//		else if(check_loop_tm(p->loop_ack_ok)) {
//        driver_printf("未收到应答 超时，跳转记录的步骤\n");
//        p->step=p->ack_ok_step;
//				end_ack_ok_loop_tm(p);
//        return 1;
//    }
//    else if(p->loop_chk_sw.is_run==1&&left_ms(&p->loop_chk_sw.loop_tm) == 0) {
//        driver_printf("定时未做\n");
//        //p->step=APP_CTRL_STEP_CHK_SW_RD;
//        return 1;
//    }
    else if(check_loop_tm(p->loop_call_wr)) {
        //driver_printf("召测\n");
				if(p->loop_call_wr.ack_ok_step!=0){
						p->step=p->loop_call_wr.ack_ok_step;
						end_ack_ok_loop_tm(&p->loop_call_wr);
						
				}
				else{
					p->step=DEV_DRIVER_STEP_CALL_WR;
				}
        return 1;
    }    
		else if(/*(use_253==0)&&*/check_loop_tm(p->loop_evt_wr)) {
        //driver_printf("evt 事件\n");
				if(p->loop_evt_wr.ack_ok_step!=0){
						p->step=p->loop_evt_wr.ack_ok_step;
						end_ack_ok_loop_tm(&p->loop_evt_wr);
						
				}
				else{
					p->step=DEV_DRIVER_STEP_EVT_WR;
				}
        return 1;
    }
//    else if(p->loop_chk_online.is_run==1&&left_ms(&p->loop_chk_online.loop_tm) == 0) {
//        //driver_printf("检测 离线\n");
//        p->step=APP_CTRL_STEP_CHK_ONLINE_RD;
//        return 1;

//    }

    return 0;
}

void dev_driver_step_idle(DEV_DRIVER_T *p)
{
    uint32_t e;
	
//	  if(!net_is_connect()||g_app_update.start_update==1||brk_update_struct.launch_updating==1)		//网络未连接，网关升级，断路器升级 
//		{

//        return;
//		}
	
		//第一步就是检测发送队列，实现了注入优先级最高
		if(fixlen_que_firsize(&p->app_send_que.data_que)>0){			//检查是否有需要下发的数据
//				set_event(&p->event,EVENT_WR);			//通知系统跳转发送
				//driver_printf("dev_driver 跳转WR步骤\n");
			
				p->step=DEV_DRIVER_STEP_WR;
		}
		else if(fixlen_que_firsize(&p->inner_send_que.data_que)>0){			//检查是否有需要下发的数据

				p->step=DEV_DRIVER_STEP_WR;
		}
		else if(get_event(&p->event,EVENT_POLL,&e,EVENT_CLEAR))
    {
        set_loop_tm(&p->loop_poll_wr,0);		//=0为不延时 立即跳转步骤
				p->curr_poll_type=NULL;			

    }
    else if(get_event(&p->event,EVENT_CALL_START,&e,EVENT_CLEAR))		//需要应答平台，无需操作设备
    {
        set_loop_tm(&p->loop_call_wr,0);		//=0为不延时 立即跳转步骤
        driver_printf("start call\n");
    }  
		else if(get_event(&p->event,EVENT_ACK_OK,&e,EVENT_CLEAR))		//应答结束 跳转记录的步骤
		{
				driver_printf("收到应答条数到达 结束，跳转 记录的步骤\n");
				if(p->loop_poll_wr.ack_ok_step!=0){
						p->step=p->loop_poll_wr.ack_ok_step;
						end_ack_ok_loop_tm(&p->loop_poll_wr);
				
				}else if(p->loop_call_wr.ack_ok_step!=0){
						p->step=p->loop_call_wr.ack_ok_step;
						end_ack_ok_loop_tm(&p->loop_call_wr);
				
				}else if(p->loop_evt_wr.ack_ok_step!=0){
						p->step=p->loop_evt_wr.ack_ok_step;
						end_ack_ok_loop_tm(&p->loop_evt_wr);
				}
		}
    else if(left_ms(&p->poll_tm)==0) {
        set_event(&p->event,EVENT_POLL);			//发送轮询事件
        left_ms_set(&p->poll_tm, p->poll_inv_time_ms);		//设定下一次轮询时间

    } 
    else
    {
        select_step_by_loop_tm(p);			//轮询和evt优先级最低
    }
		
		//接收缓存为空 就清空接收条数
		if(fixlen_que_firsize(&p->app_recv_que.data_que)<=0){
				p->app_recv_que.data_num=0;
		}
	
		if(fixlen_que_firsize(&p->app_send_que.data_que)<=0){		//发送队列为空了 
				p->app_send_que.data_num=0;		//发送条数清零
		}
		
		//接收缓存为空 就清空接收条数
		if(fixlen_que_firsize(&p->inner_recv_que.data_que)<=0){
				p->inner_recv_que.data_num=0;
		}
	
		if(fixlen_que_firsize(&p->inner_send_que.data_que)<=0){		//发送队列为空了 
				p->inner_send_que.data_num=0;		//发送条数清零
		}
		//使用一个空闲状态统一的波特率 idle_pu
//		for(int i=0;i<MAX_DEVICE_TYPES;i++){			//导致的问题：可能别的步骤在等待数据被这里 吃掉（usart_chk_frame调用后回清标志位）
//			if(device_types[i].registered){
//				if(usart_chk_frame(device_types[i].pu)){			//方便处理设备主动上报的数据
//						driver_printf("dev type %s usart %d, has data report\n",device_types[i].name,device_types[i].pu->com);
//						
//				}
//			}
//		}
}

void dev_driver_step_wr(DEV_DRIVER_T *p)
{
		Device *device;
		int len;
		
		que_struct* tmp_send_que;
		if(fixlen_que_firsize(&p->app_send_que.data_que)>0){			//检查是否有需要下发的数据
			p->inner_opt_flag=0;
			tmp_send_que=&p->app_send_que;
		}
		else if(fixlen_que_firsize(&p->inner_send_que.data_que)>0){			//检查是否有需要下发的数据
			p->inner_opt_flag=1;
			tmp_send_que=&p->inner_send_que;
			
		}else {
			printf("no data\n");
			p->step=DEV_DRIVER_STEP_IDLE;
			return ;
		}

		if((len=fixlen_que_Rd(&tmp_send_que->data_que, p->send_data_buff, DEV_DRIVER_BUFF_SIZE)) > 0){
//			p->curr_read_data_device=buff.device;			//记录当前读取数据的设备
			device=p->curr_wr_device;			
			//设置波特率
			dev_driver_msp_reset_baud_parity(device->type->rt_device,device->type->usart_baud,device->type->usart_parity);
			//hard_delay_ms(10);
			//发送数据
			dev_driver_msp_usart_send(device->type->rt_device,p->send_data_buff,len);			
		#ifdef DEBUG_PRINT
			driver_printf("dev_driver 下发数据\n");
			hexdump(">>>",p->send_data_buff,len);
		#endif
			p->step=DEV_DRIVER_STEP_RD;
			left_ms_set(&p->tm, device->type->dev_resp_inv_ms);		//设定下一次轮询时间
		}
		else{
			 log_e("wr que no data inner_opt_flag=%d\n",p->inner_opt_flag);
			 p->step=DEV_DRIVER_STEP_IDLE;
		}		
}
void dev_driver_step_rd(DEV_DRIVER_T *p)
{
	  int chk_frame=0;
    //有一帧数据包收到 就结束等待
		rt_device_t rt_device=p->curr_wr_device->type->rt_device;
		Device * device=p->curr_wr_device;
		que_struct* tmp_send_que;
		que_struct* tmp_recv_que;
	
		
    if(left_ms(&p->tm) == 0||(1==(chk_frame=usart_chk_frame(pu))))
		{	
				if(p->inner_opt_flag==0){
					tmp_send_que=&p->app_send_que;
					tmp_recv_que=&p->app_recv_que;
				}
				else if(p->inner_opt_flag==1){			//内部操作 （优先级低一些 都是evt或者poll轮询）
					tmp_send_que=&p->inner_send_que;
					tmp_recv_que=&p->inner_recv_que;
					//printf("dev_driver_step_rd 内部操作\n");
				}
        if(chk_frame==1)
				{
            int len=dev_driver_msp_usart_recv(rt_device,p->recv_data_buff,DEV_DRIVER_BUFF_SIZE);		//底层串口接收 应答数据
					#ifdef DEBUG_PRINT
						driver_printf("dev_driver 收到数据\n");
						hexdump("<<<",p->recv_data_buff,len);
					#endif
						if(-1==fixlen_que_Wr(&tmp_recv_que->data_que,p->recv_data_buff,len))
						{
								log_e("recv_que add failed\n");
								driver_printf("队列添加失败\n");
						}
						tmp_recv_que->data_num++;
						if(tmp_recv_que->data_num>=tmp_send_que->data_num){			//应答的条数达到了 下发的条数，表示结束了（一唱一和模式）
							set_event(&p->event,EVENT_ACK_OK);
//							p->send_que.data_num=0;		//都收到应答了，发送条数清零
						}
						//set_event(&p->event,EVENT_CHK_RECV_DATA);		//收到数据 标志位
        }
        else 
				{
            driver_printf("rd dev: %s no resp\n",device->dev_id);
        }
//        set_loop_tm(&p->loop_poll_wr,200);			//这200ms 主要用来响应其他事件
        p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待200ms后调度
    }
}


void dev_driver_step_poll_wr(DEV_DRIVER_T *p)
{
		if(p->curr_poll_type==NULL){				//第一次
				p->curr_poll_type_idx=0;				//轮询 指针 从0开始
        p->curr_poll_device_idx=0;
				p->curr_poll_type=device_find_type_idx(p->curr_poll_type_idx++);
				if(p->curr_poll_type==NULL){
					//driver_printf("no device_type end poll\n");		//没注册设备
					end_loop_tm(&p->loop_poll_wr);
				}
				else
				{
					driver_printf("start poll\n");
				}

        driver_printf("curr_poll_type %s\n",p->curr_poll_type->name);
        //dev_driver_msp_reset_baud_parity(p->curr_poll_type->pu,p->curr_poll_type->usart_baud,p->curr_poll_type->usart_parity);
		}
		
    p->curr_poll_device=device_find_idx(p->curr_poll_type,p->curr_poll_device_idx++);
		Device * device=p->curr_poll_device;
    if(NULL==device) {
        p->curr_poll_type=device_find_type_idx(p->curr_poll_type_idx++);	//切换下一个类型
        if(p->curr_poll_type==NULL)			//下一个类型为空了，轮询结束
        {
            driver_printf("end poll\n");
            end_loop_tm(&p->loop_poll_wr);
						end_ack_ok_loop_tm(&p->loop_poll_wr);
            p->curr_poll_type_idx=0;
            p->curr_poll_device_idx=0;
            p->step=DEV_DRIVER_STEP_IDLE;
        }
        else {
            driver_printf("curr_poll_type %s\n",p->curr_poll_type->name);
            p->curr_poll_device_idx=0;		//新类型 设备从0开始
            //dev_driver_msp_reset_baud_parity(p->curr_poll_type->pu,p->curr_poll_type->usart_baud,p->curr_poll_type->usart_parity);
        }
    }
    else
    {
        if(device->type->poll_wr!=NULL) {
						clr_inner_all_val_before_wr(p);
						device->type->poll_wr(device,&p->inner_send_que);		//函数会把要发送的数据填到队列内
						p->curr_wr_device=device;
						set_ack_ok_loop_tm(&p->loop_poll_wr,DEV_DRIVER_STEP_POLL_RD,p->curr_poll_type->dev_resp_inv_ms*(p->inner_send_que.data_num));					
						p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待调度
        }
        else
        {
						//end_loop_tm(&p->loop_poll_wr);		//结束轮询
            driver_printf("dev: %s poll_wr is NULL\n",p->curr_poll_device->dev_id);
						p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待调度
        }
    }
}
void dev_driver_step_poll_rd(DEV_DRIVER_T *p)
{
		Device * device=p->curr_wr_device;
		int len=fixlen_que_firsize(&p->inner_recv_que.data_que);
		if (len>0) {
				if(device->type->poll_rd!=NULL)
				{
						if(DEV_DRIVER_OK==device->type->poll_rd(device,&p->inner_recv_que))
						{
								//driver_printf("dev: %s poll rd ok\n",device->dev_id);
						}
						else 
						{
								driver_printf("dev: %s poll rd ERR\n",device->dev_id);
						}
				}
				else {
						end_loop_tm(&p->loop_poll_wr);		//结束事件
						driver_printf("dev: %s poll_rd is NULL\n",device->dev_id);
				}
		}
		else 
		{
				driver_printf("poll_rd dev: %s no resp\n",device->dev_id);
		}
		
		set_loop_tm(&p->loop_poll_wr,200);			//这200ms 主要用来响应其他事件
    p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待200ms后调度
}

void dev_driver_step_evt_wr(DEV_DRIVER_T *p)
{
		if(p->curr_evt_type==NULL){				//第一次
				p->curr_evt_type_idx=0;				//轮询 指针 从0开始
        p->curr_evt_device_idx=0;
				p->curr_evt_type=device_find_type_idx(p->curr_evt_type_idx++);
				if(p->curr_evt_type==NULL){
					//driver_printf("no device_type end evt\n");		//没注册设备
					end_loop_tm(&p->loop_evt_wr);
				}
				else
				{
					driver_printf("start evt\n");
				}

        //driver_printf("curr_evt_type %s\n",p->curr_evt_type->name);
        //dev_driver_msp_reset_baud_parity(p->curr_evt_type->pu,p->curr_evt_type->usart_baud,p->curr_evt_type->usart_parity);
		}
		
    p->curr_evt_device=device_find_idx(p->curr_evt_type,p->curr_evt_device_idx++);
		Device * device=p->curr_evt_device;
    if(NULL==device) {
        p->curr_evt_type=device_find_type_idx(p->curr_evt_type_idx++);	//切换下一个类型
        if(p->curr_evt_type==NULL)			//下一个类型为空了，从头开始
        {
            driver_printf("end evt\n");
//            end_loop_tm(&p->loop_evt_wr);
//						end_ack_ok_loop_tm(p);
            p->curr_evt_type_idx=0;
            p->curr_evt_device_idx=0;
            p->step=DEV_DRIVER_STEP_IDLE;
        }
        else {
            driver_printf("curr_evt_type %s\n",p->curr_evt_type->name);
            p->curr_evt_device_idx=0;		//新类型 设备从0开始
            //dev_driver_msp_reset_baud_parity(p->curr_evt_type->pu,p->curr_evt_type->usart_baud,p->curr_evt_type->usart_parity);
        }
    }
    else
    {
        if(device->type->evt_wr!=NULL) {
						clr_inner_all_val_before_wr(p);			//注意 evt这里会清空其他操作发送的数据，但是理论上没关系，因为其他操作时候会等待拿到应答后 再evt（evt优先级最低）	
						device->type->evt_wr(device,&p->inner_send_que);		//函数会把要发送的数据填到队列内
						p->curr_wr_device=device;
						//p->inner_opt_flag=1;
						set_ack_ok_loop_tm(&p->loop_evt_wr,DEV_DRIVER_STEP_EVT_RD,p->curr_evt_type->dev_resp_inv_ms*(p->inner_send_que.data_num));		
						//end_loop_tm(&p->loop_evt_wr);			
						//set_loop_tm(&p->loop_evt_wr,p->curr_evt_type->dev_resp_inv_ms*(p->inner_send_que.data_num));			
											
						p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待调度
        }
        else
        {
						//end_loop_tm(&p->loop_evt_wr);		//结束事件
            driver_printf("dev: %s evt_wr is NULL\n",p->curr_evt_device->dev_id);
						p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待调度
        }
    }
}
void dev_driver_step_evt_rd(DEV_DRIVER_T *p)
{
		//p->inner_opt_flag=0;		//清除内部操作标志位
		Device * device=p->curr_wr_device;
		int len=fixlen_que_firsize(&p->inner_recv_que.data_que);
		if (len>0) {
				if(device->type->evt_rd!=NULL)
				{
						clr_inner_send_val_before_wr(p);
						dev_driver_err err=device->type->evt_rd(device,&p->inner_recv_que,&p->inner_send_que);
						clr_inner_recv_val_before_wr(p);
						if(DEV_DRIVER_OK==err)
						{
								driver_printf("dev: %s evt rd ok\n",device->dev_id);
						}
						else if(DEV_DRIVER_READ_MORE==err){
							driver_printf("dev: %s evt read more\n",device->dev_id);			//读取更多数据，evt_rd会填充发送队列
							//p->inner_opt_flag=1;
							set_ack_ok_loop_tm(&p->loop_evt_wr,DEV_DRIVER_STEP_EVT_RD_MORE,p->curr_evt_type->dev_resp_inv_ms*(p->inner_send_que.data_num));
							//set_loop_tm(&p->loop_evt_wr,p->curr_evt_type->dev_resp_inv_ms*(p->send_que.data_num));			//不设置时间的话 rd_more被事件抢了
//							end_loop_tm(&p->loop_evt_wr);
							p->step=DEV_DRIVER_STEP_IDLE;
							return ;
							
						}
						else 
						{
								driver_printf("dev: %s evt rd ERR\n",device->dev_id);
						}
				}
				else {
						end_loop_tm(&p->loop_evt_wr);		//结束事件
						driver_printf("dev: %s evt_rd is NULL\n",device->dev_id);
				}
		}
		else 
		{
				driver_printf("evt_rd dev: %s no resp\n",device->dev_id);
		}
		
		set_loop_tm(&p->loop_evt_wr,EVT_INTE_TM);			//这50ms 主要用来响应其他事件
    p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待50ms后调度
}
void dev_driver_step_evt_rd_more(DEV_DRIVER_T *p)
{
//		p->inner_opt_flag=0;		//清除内部操作标志位
		driver_printf("读取更多\n");
		Device * device=p->curr_wr_device;
		int len=fixlen_que_firsize(&p->inner_recv_que.data_que);
		if (len>0) {
				if(device->type->evt_rd_more!=NULL)
				{
						dev_driver_err err=device->type->evt_rd_more(device,&p->inner_recv_que);
						if(DEV_DRIVER_OK==err)
						{
								//driver_printf("dev: %s evt rd ok\n",device->dev_id);
						}
						else 
						{
								driver_printf("dev: %s evt rd ERR\n",device->dev_id);
						}
				}
				else {
						end_loop_tm(&p->loop_evt_wr);		//结束事件
						driver_printf("dev: %s evt_rd is NULL\n",device->dev_id);
				}
		}
		else 
		{
				driver_printf("dev: %s no resp\n",device->dev_id);
		}
		
		set_loop_tm(&p->loop_evt_wr,EVT_INTE_TM);			//这50ms 主要用来响应其他事件
    p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待50ms后调度
}


void dev_driver_step_call_wr(DEV_DRIVER_T *p)
{
    //p->curr_call_device=device_find_idx(p->curr_call_type,p->curr_call_device_idx++);
		Device * device=p->curr_call_device;
		if(device->type->call_wr!=NULL) {
				uint32_t e;
				if(get_event(&p->event,EVENT_CALL_END,&e,EVENT_CLEAR)) {
						log_i("end call EVENT_CALL_END\n");			//收到结束事件 停止读取，回到空闲
						p->step=DEV_DRIVER_STEP_IDLE;		
						end_loop_tm(&p->loop_call_wr);

				}
				else if(left_ms(&p->call_cnt_down_tm) == 0) {
						log_i("end call call_cnt_down_tm\n");			//时间到了 停止读取，回到空闲
						p->step=DEV_DRIVER_STEP_IDLE;					
						end_loop_tm(&p->loop_call_wr);

				}
				else
				{
					clr_inner_all_val_before_wr(p);
					device->type->call_wr(device,&p->inner_send_que);		//函数会把要发送的数据填到队列内
					p->curr_wr_device=device;
					set_ack_ok_loop_tm(&p->loop_call_wr,DEV_DRIVER_STEP_CALL_RD,device->type->dev_resp_inv_ms*(p->inner_send_que.data_num));					
					p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待调度
				}
		}
		else
		{
				driver_printf("dev: %s call_wr is NULL\n",p->curr_call_device->dev_id);
				//end_loop_tm(&p->loop_call_wr);		//结束召测
				p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待调度
		}
    
}
void dev_driver_step_call_rd(DEV_DRIVER_T *p)
{
		Device * device=p->curr_wr_device;
		int len=fixlen_que_firsize(&p->inner_recv_que.data_que);
		if (len>0) {
				if(device->type->call_rd!=NULL)
				{
						if(DEV_DRIVER_OK==device->type->call_rd(device,&p->inner_recv_que))
						{
								//driver_printf("dev: %s call rd ok\n",device->dev_id);
						}
						else 
						{
								driver_printf("dev: %s call rd ERR\n",device->dev_id);
						}
				}
				else {
						end_loop_tm(&p->loop_call_wr);		//结束召测
						driver_printf("dev: %s call_rd is NULL\n",device->dev_id);
				}
		}
		else 
		{
				driver_printf("dev: %s no resp\n",device->dev_id);
		}
		
		set_loop_tm(&p->loop_call_wr,device->type->dev_call_inv_ms);			//主要用来响应其他事件
    p->step=DEV_DRIVER_STEP_IDLE;			//回到空闲 等待200ms后调度
}



// 设备状态机表轮询函数
int dev_driver_machine(DEV_DRIVER_T *p)
{

    switch(p->step)
    {

    case 
			DEV_DRIVER_STEP_IDLE:
        dev_driver_step_idle(p);
        break;
		case 
			DEV_DRIVER_STEP_WR:
				dev_driver_step_wr(p);
        break;
    case 
			DEV_DRIVER_STEP_RD:
				dev_driver_step_rd(p);
        break;
    case 
			DEV_DRIVER_STEP_POLL_WR:
        dev_driver_step_poll_wr(p);
        break;
    case 
			DEV_DRIVER_STEP_POLL_RD:
        dev_driver_step_poll_rd(p);
        break;
		case 
			DEV_DRIVER_STEP_EVT_WR:
        dev_driver_step_evt_wr(p);
        break;
    case 
			DEV_DRIVER_STEP_EVT_RD:
        dev_driver_step_evt_rd(p);
        break;
		case 
			DEV_DRIVER_STEP_EVT_RD_MORE:
        dev_driver_step_evt_rd_more(p);
        break;
    case 
			DEV_DRIVER_STEP_CALL_WR:
				dev_driver_step_call_wr(p);
        break;
    case 
			DEV_DRIVER_STEP_CALL_RD:
				dev_driver_step_call_rd(p);
        break;
    case 
			DEV_DRIVER_STEP_CHK_ONLINE_WR:

        break;
    case 
			DEV_DRIVER_STEP_CHK_ONLINE_RD:

        break;
		default:
				driver_printf("not support step\n");
			break;
    }
#if 0
		if(left_ms(&p->step_tm) == 0)
		{	
			 left_ms_set(&p->step_tm, 1000);
				printf("step %d\n",p->step);				//输出当前的步骤号
		}
#endif
    return 0;
}


/**		 预编译 添加设备		**/
const static struct DEV_DRIVER_LIST *dev_driver_list;

int dev_driver_start()
{
    return 0;
}
DEV_DRIVER_START_EXPORT(dev_driver_start);

int dev_driver_end()
{
    return 0;
}
DEV_DRIVER_END_EXPORT(dev_driver_end);

void dev_driver_device_list_init(DEV_DRIVER_T *p) {
		driver_printf("dev_driver ver: %s\n",DEV_DRIVER_VER);
		p->chk_online_inv_time_ms=DEV_DRIVER_CHK_ONLINE_TM;
		p->poll_inv_time_ms=DEV_DRIVER_POLL_TM;			//轮询间隔
	
    p->recv_data_buff=DataPool_Get(DEV_DRIVER_BUFF_SIZE);		//申请接收缓存区空间
    p->send_data_buff=DataPool_Get(DEV_DRIVER_BUFF_SIZE);		//申请发送缓存区空间
	
		Ring_Buf_Init(&p->app_send_que.data_que, DEV_DRIVER_QUE_BUFF_SIZE);		//申请队列 空间（队列和串口参数整合为一个结构体）
		Ring_Buf_Init(&p->app_recv_que.data_que, DEV_DRIVER_QUE_BUFF_SIZE);		//申请队列 空间
	
		Ring_Buf_Init(&p->inner_send_que.data_que, DEV_DRIVER_QUE_BUFF_SIZE);		//申请队列 空间（队列和串口参数整合为一个结构体）
		Ring_Buf_Init(&p->inner_recv_que.data_que, DEV_DRIVER_QUE_BUFF_SIZE);		//申请队列 空间
	
		p->step=DEV_DRIVER_STEP_IDLE;
	  left_ms_set(&p->poll_tm, p->poll_inv_time_ms);		//设定下一次轮询时间
    left_ms_set(&p->chk_online_tm, p->chk_online_inv_time_ms);		//设定下一次离线检测时间
		if(ef_get_env_int("alr_read")==1)
			set_loop_tm(&p->loop_evt_wr,0);			//evt事件检测
		//查表 初始化 预编译时添加的设备
    const struct DEV_DRIVER_LIST *dev_driver_ptr;
    dev_driver_list = &dev_fn_dev_driver_start;
    dev_driver_list++;
    for (dev_driver_ptr = dev_driver_list; dev_driver_ptr < &dev_fn_dev_driver_end; dev_driver_ptr++)
    {
        (*dev_driver_ptr->funs)();
    }
		
}
int chk_recv_data_num(void){
	DEV_DRIVER_T *p=&g_dev_driver;
	int num=p->app_recv_que.data_num;
	return num;
	
}


/**********************			串口测试命令			***************************/

static int cmd_poll(uint8_t argc, char **argv)
{
		DEV_DRIVER_T *p=&g_dev_driver;
    set_event(&g_dev_driver.event,EVENT_POLL);			//发送轮询事件
    left_ms_set(&g_dev_driver.poll_tm, p->poll_inv_time_ms);		//设定下一次轮询时间
    return 0;

}
MSH_CMD_EXPORT(cmd_poll, j);

static int cmd_evt(uint8_t argc, char **argv)
{
		DEV_DRIVER_T *p=&g_dev_driver;
		set_loop_tm(&p->loop_evt_wr,0);
//    set_event(&g_dev_driver.event,EVENT_POLL);			//发送轮询事件
//    left_ms_set(&g_dev_driver.poll_tm, p->poll_inv_time_ms);		//设定下一次轮询时间
    return 0;

}
MSH_CMD_EXPORT(cmd_evt, j);


int cmd_dev_read_data_wr(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];

		DeviceType *device_type=device_find_type(type_name);
		device_read_data_wr_by_handle(device_type,dev_id);
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_read_data_wr,t);


int cmd_dev_read_data_rd(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2]; 

		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
		if(device!=NULL)
				device_read_data_rd(device);
		else
			driver_printf("not found dev_id\n");
			
		
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_read_data_rd,t);


int cmd_dev_transfer_wr(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];
		char* data=argv[3];		//010300000086C468 
		uint8_t buff[50];
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
	
		if(device!=NULL){
				int len=strarr_to_hexarr(data,buff,strlen(data));
				device_transfer_wr(device,buff,len);
		}
		else
			driver_printf("not found dev_id\n");
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_transfer_wr,t);


int cmd_dev_transfer_rd(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];
		uint8_t buff[500];
		
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
		if(device!=NULL)
		{	
				int len;
				if(DEV_DRIVER_ERR!=device_transfer_rd(device,buff,sizeof(buff),&len)){
					hexdump("<<<",buff,len);
				}
		}
		else
		{
			driver_printf("not found dev_id\n");
			
		}
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_transfer_rd,t);

int cmd_dev_control_wr(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];
		uint8_t op=atoi(argv[3]);		 
		uint8_t state=atoi(argv[4]);
	
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);

		if(device!=NULL){

				device_control_wr(device,op,(void*)&state);
		}
		else
			driver_printf("not found dev_id\n");
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_control_wr,t);


int cmd_dev_control_rd(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];
		uint8_t op=atoi(argv[3]);		 
		uint8_t state=atoi(argv[4]);	
	
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
		if(device!=NULL)
		{	
				device_control_rd(device,op,(void*)&state);
				
		}
		else
		{
			driver_printf("not found dev_id\n");
			
		}
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_control_rd,t);


int cmd_dev_call_start(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];
		int time_s=atoi(argv[3]);		 
	
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
		if(device!=NULL)
		{
				device_call_start(device,time_s);
		}
		else
		{
			driver_printf("not found dev_id\n");
			
		}
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_call_start,t);

int cmd_dev_call_end(uint8_t argc, char **argv) {
		char* type_name=argv[1];
		char* dev_id=argv[2];
		uint32_t time_s=atoi(argv[3]);		 
	
		DeviceType *device_type=device_find_type(type_name);
		Device *device=device_find(device_type,dev_id);
		if(device!=NULL)
		{
				device_call_end(device);
		}
		else
		{
			driver_printf("not found dev_id\n");
			
		}
		
		return 0;
}
MSH_CMD_EXPORT(cmd_dev_call_end,t);

int cmd_end_evt(uint8_t argc, char **argv) {
		DEV_DRIVER_T *p=&g_dev_driver;
		end_loop_tm(&p->loop_evt_wr);			//停止 evt事件检测
		
		return 0;
}
MSH_CMD_EXPORT(cmd_end_evt,t);



//EVENT_CALL_START		发送召测开始事件


