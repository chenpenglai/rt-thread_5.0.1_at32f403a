#ifndef dev_driver_h
#define dev_driver_h
#include "global.h"
#include "stdint.h"
#include "rtthread.h"
#include "stdarg.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "utility.h"
#include "device_env.h"
/*
	版本修订记录
		V0.0.1
			初步完成接口定义
			
		V0.0.2
			完成 透传 控制 数据下发
			轮询过程中 用户调用数据下发 将优先响应数据下发
			
		V0.0.3
			应用层和底层使用不同队列，避免数据混杂
		
		

**********		版本号		***********/
#define DEV_DRIVER_VER "V0.0.3"

/**********		事件位		***********/
#define EVENT_POLL										(1<<0)		//开始轮询
#define EVENT_CALL_START							(1<<1)		//开始召测
#define EVENT_CALL_END								(1<<2)		//结束召测
#define EVENT_ACK_OK									(1<<3)		//通用的数据 下发 应答 条数够了
//#define EVENT_CHK_RECV_DATA						(1<<4)		// 检查是否收到数据

/**********		时间间隔相关		***********/
#define EVT_INTE_TM										50						//evt事件检测间隔时间 单位：毫秒
#define DEV_DRIVER_POLL_TM 						(get_env_int("poll_time_s")*1000)
#define DEV_DRIVER_CHK_ONLINE_TM			(get_env_int("chk_online_time_s")*1000)

/**********		队列/缓存区大小		***********/
#define DEV_DRIVER_QUE_BUFF_SIZE 			(512*3)		//队列 发送/接收 大小
#define DEV_DRIVER_BUFF_SIZE 					(512*3)		//缓存区 发送/接收 大小

/**********		设备相关		***********/
#define MAX_DEVICE_TYPES 							5				//设备类型 最大数量	=5种类型的设备
#define MAX_DEVICES_PER_TYPE 					32			//一个设备类型下 最大挂接设备数量 =32个设备
#define DEV_ID_SIZE 									20			//设备id 最大长度


typedef int (*dev_driver_fun)(void);
struct DEV_DRIVER_LIST
{
    //char *cmd;		//命令字符串
    dev_driver_fun funs;						//函数地址
};

#define TO_STR(x)		#x							//转字符串
#define DEV_DRIVER_SECTION(level)  __attribute__((used,__section__(".fn_dev_driver."level)))
#define DEV_DRIVER_START_EXPORT(func) const struct DEV_DRIVER_LIST dev_fn_##func DEV_DRIVER_SECTION("0.end") = {func}
#define DEV_DRIVER_INIT(func) const struct DEV_DRIVER_LIST dev_fn_##func DEV_DRIVER_SECTION("1") = {func}		//注册设备用
#define DEV_DRIVER_END_EXPORT(func) const struct DEV_DRIVER_LIST dev_fn_##func DEV_DRIVER_SECTION("1.end") = {func}

/*******************************************************************/
typedef enum dev_err{

		DEV_DRIVER_OK=0,				//成功
		DEV_DRIVER_ERR,					//错误
		DEV_DRIVER_NO_RESP,			//没收到应答数据（可用于 应用层循环判断 等待数据的到达）
		DEV_DRIVER_READ_MORE,		//还需要继续读取（目前仅evt用到）
	
}dev_driver_err; 


typedef struct Device Device;
typedef struct DeviceType DeviceType;

//控制枚举（必须放在框架 .h 内，不能依赖设备层，因为设备层无 .h 文件 方便被随时移除）
typedef enum {
	
    CTRL_SET_STATE,		//状态（可用于brk分合闸）
    CTRL_SET_TRIP,		//脱扣（无需arg）
		CTRL_SET_LOCK,		//挂锁
		CTRL_SET_FORCE_STATE,		//强制合闸（无需arg）
		CTRL_SET_LEAKAGE_TEST,		//漏电自检（无需arg）
		
		
	
} ctrl_opt;

typedef struct{
	FIXLEN_QUE_T data_que;
	int data_num;
}que_struct;			//用户层和框架层传递数据用

// 设备类型结构体
struct DeviceType {
    char name[20];  					//设备类型名称
    int num_devices;  				//设备数量
		uint8_t registered;				//已注册的标志：1=注册 0=未注册
	
		//串口相关
		rt_device_t rt_device;
		//USART_T *pu;							//串口句柄
		uint8_t usart_parity;			//串口校验位
		uint32_t usart_baud;			//串口波特率
		
		uint32_t dev_resp_inv_ms;		//设备应答的最大间隔（等待）时间
		uint32_t dev_call_inv_ms;		//设备应答的最大间隔（等待）时间
	
		//每种类型 对应的具体实现功能
	  dev_driver_err (*transfer_wr)(Device *,que_struct* ,uint8_t *,int );  // 发送 透传数据
	  dev_driver_err (*transfer_rd)(Device *,que_struct* ,uint8_t *data,int data_size,int* recv_len);  // 接收 透传数据应答
    dev_driver_err (*read_data_wr)(Device *,que_struct* );  // 发送 读取数据（固定操作 放在函数内）
	  dev_driver_err (*read_data_rd)(Device *,que_struct* );  // 接收 读取数据应答（固定操作 放在函数内）
    dev_driver_err (*alarm)(Device *);  // 报警函数指针
    dev_driver_err (*poll_wr)(Device *,que_struct* );  // 轮询函数指针（固定操作 放在函数内）
		dev_driver_err (*poll_rd)(Device *,que_struct* );  // 轮询函数指针（固定操作 放在函数内）
		dev_driver_err (*call_wr)(Device *,que_struct* );  // 轮询函数指针（固定操作 放在函数内）
		dev_driver_err (*call_rd)(Device *,que_struct* );  // 轮询函数指针（固定操作 放在函数内）
		dev_driver_err (*chk_online_wr)(Device *,uint8_t* ,int* );  // 离线检测（固定操作 放在函数内）
		dev_driver_err (*chk_online_rd)(Device *,uint8_t* ,int );  // 离线检测（固定操作 放在函数内）
    dev_driver_err (*control_wr)(Device *, ctrl_opt,void* ,que_struct*);  // 控制函数指针（不固定操作）
		dev_driver_err (*control_rd)(Device *, ctrl_opt,void* ,que_struct*);  // 控制函数指针（不固定操作）
		dev_driver_err (*evt_wr)(Device *,que_struct* send);  // 事件函数指针（固定操作 放在函数内）
		dev_driver_err (*evt_rd)(Device *,que_struct* recv,que_struct* send);  // 事件函数指针（固定操作 放在函数内）
		dev_driver_err (*evt_rd_more)(Device *,que_struct*recv );  // 事件函数指针（固定操作 放在函数内）
		
		//指向设备的指针数组
    Device *devices[MAX_DEVICES_PER_TYPE];  
};


// 设备结构体
struct Device {
		uint8_t registered;	//已注册的标志：1=注册 0=未注册
    DeviceType *type;  // 指向设备类型的指针
    char dev_id[DEV_ID_SIZE];  // 设备编号 字符串
    bool is_online;  // 设备是否在线
		
};




// 定义设备状态枚举
typedef enum {
    DEV_DRIVER_STEP_IDLE=0,		//必须为0，因为清空 ack_ok_step 是置0
		DEV_DRIVER_STEP_WR,
		DEV_DRIVER_STEP_RD,
    DEV_DRIVER_STEP_POLL_WR,			//轮询
		DEV_DRIVER_STEP_POLL_RD,
		
		DEV_DRIVER_STEP_EVT_WR,			//事件
		DEV_DRIVER_STEP_EVT_RD,			//事件应答
		DEV_DRIVER_STEP_EVT_RD_MORE,		//读取更多的应答	
	
		DEV_DRIVER_STEP_CALL_WR,			//召测
		DEV_DRIVER_STEP_CALL_RD,
	
		DEV_DRIVER_STEP_CHK_ONLINE_WR,			//离线检测
		DEV_DRIVER_STEP_CHK_ONLINE_RD,				//离线检测应答

	
    DEV_DRIVER_STEP_MAX
} device_status_t;

typedef struct __loop_cfg{
	 unsigned long loop_tm;				//恢复读取的时间
	uint8_t is_run;
	uint8_t ack_ok_step;		//数据读取结束跳转的步骤
}_loop_cfg;
typedef struct{
	uint8_t step;

//	uint8_t ack_ok_step;		//数据读取结束跳转的步骤
//	_loop_cfg loop_ack_ok;				
	time_ms_T tm,step_tm;
	uint32_t poll_inv_time_ms,chk_online_inv_time_ms;		//间隔时间，单位：毫秒
	
	uint8_t* send_data_buff;
	uint8_t* recv_data_buff;
	que_struct app_send_que,app_recv_que;
	que_struct inner_send_que,inner_recv_que;		//内部evt call poll使用，应用层使用send_que,recv_que（分开的目的是：避免应用层下发数据后 应答包被evt call poll拿走解析）
	uint8_t inner_opt_flag;			//内部操作标志位，1：当前为内部操作，接收数据到 inner_send_que,inner_recv_que
//	int send_num,recv_num;	
//	time_ms_T check_sw_tm;

	time_ms_T poll_tm;				//定时器 轮询
	time_ms_T chk_evt_tm;
//	time_ms_T ctrl_tm;
//	time_ms_T adj_time_tm;
	time_ms_T chk_online_tm;

	_loop_cfg loop_poll_wr;				//轮询 间隔用
	_loop_cfg loop_evt_wr;				//evt 间隔用
	
	Device *curr_wr_device;		//当前下发数据的设备（通用）
	
	DeviceType *curr_poll_type;  //当前轮询的设备类型
	uint8_t curr_poll_type_idx;	//当前轮询的设备类型 idx
	Device *curr_poll_device;		//当前轮询的设备
	uint8_t curr_poll_device_idx;//当前轮询的设备 idx
	
	
	DeviceType *curr_evt_type;  //当前evt的设备类型
	uint8_t curr_evt_type_idx;	//当前evt的设备类型 idx
	Device *curr_evt_device;		//当前evt的设备
	uint8_t curr_evt_device_idx;//当前evt的设备 idx
	
	Device *curr_call_device;			//当前召测的设备
	_loop_cfg loop_call_wr;					//间隔延时用
	time_ms_T call_cnt_down_tm;				//召测结束时间
	
	_loop_cfg loop_chk_sw;				//间隔延时用
//	_loop_cfg loop_chk_evt;				//间隔延时用
	_loop_cfg loop_chk_online;		//间隔延时用
//	_loop_cfg loop_batch_state;
//	_loop_cfg loop_batch_lock;
	
	uint32_t event_ctrl,event;		//事件位
	uint8_t slv_result;						//从机应答结果
	uint8_t slv_addr;							//当前控制的从机地址
	uint32_t event_data_read;			//当前数据读取状态
	_curr_timer curr_timer;
	
	//空闲状态时 串口状态
	rt_device_t rt_device;							//串口句柄
	uint8_t idle_usart_parity;			//串口校验位
	uint32_t idle_usart_baud;			//串口波特率
		
//	uint8_t timer_opt;	
} DEV_DRIVER_T;
extern DEV_DRIVER_T g_dev_driver;


void dev_driver_device_list_init(DEV_DRIVER_T *p);

//注册
DeviceType* device_register_type(char *name);
Device *device_register(DeviceType *type, char* dev_id);

//查找
DeviceType *device_find_type(char* type_name);// 查找设备类型 通过类型名
DeviceType *device_find_type_idx(uint8_t idx) ;// 查找设备通过顺序，从0开始
Device *device_find(DeviceType *type, char* dev_id);
Device *device_find_idx(DeviceType *type,uint8_t idx);// 查找设备通过顺序，从0开始
Device *device_find_by_type_strid(char* type_name,char* dev_id) ;
Device *device_find_by_type_hexid(char* type_name,uint8_t* dev_hex_id,int id_len) ;// 查找设备 hex id（输入hexid ，自动转为strid处理）
	
void device_alarm(Device *device);
void control_device(Device *device, ctrl_opt op,void* arg);
void dev_ctrl_by_handle(DeviceType *type, char* dev_id,ctrl_opt op,void* arg);
void read_device_data_by_handle(DeviceType *type, char* dev_id);
void device_alarm_by_handle(DeviceType *type, char* dev_id);
void dev_ctrl(Device *device, ctrl_opt op,void* arg);
int dev_driver_machine(DEV_DRIVER_T *p);


//往发送队列添加数据
dev_driver_err device_add_data_to_sque(que_struct *send_que,uint8_t* data,int len);		
//读取队列数据 ，返回剩余条数
int device_recv_rque_to_data(que_struct *recv_que,uint8_t* data,int data_size,int* len);	

/********			外部使用 函数		*********/

//返回应答的数据条数，0为无应答
int chk_recv_data_num(void);

// 读取 数据 下发
dev_driver_err device_read_data_wr(Device *device);
// 读取 数据 应答数据
dev_driver_err device_read_data_rd(Device *device);


// 透传 数据 下发
dev_driver_err device_transfer_wr(Device *device,uint8_t *data,int data_len);		
// 透传 数据 应答数据
dev_driver_err device_transfer_rd(Device *device,uint8_t *data,int data_size,int* data_len) ;


// 控制 数据 下发
dev_driver_err device_control_wr(Device *device,ctrl_opt op,void* arg);
// 控制 数据 应答数据
dev_driver_err device_control_rd(Device *device,ctrl_opt op,void* arg);

// 开始召测
dev_driver_err device_call_start(Device *device,int time_s) ;
// 结束召测
dev_driver_err device_call_end(Device *device);





#endif
