#include <rtthread.h>
#include "easyflash.h"
#include "button_app.h"
#include "global_define.h"
#include "string.h"
#include "power_app.h"
#include "net_app.h"
#include "protocol_net.h"
#define DBG_TAG              "app.button"
#define DBG_LVL              LOG_LVL_DBG
#include <rtdbg.h>
#include "multi_button.h"
#include "light_sens.h"
#include "radar_app.h"
#include "cover_app.h"
#include "beep_dev.h"
#include "gas_app.h"

struct button wakeup_key,vmd_sta_key,co_sta_key,h2s_sta_key,so2_sta_key;
uint8_t read_wakeup_key(){
    if (rt_pin_read(WAKEUP_KEY_PIN))
        return 1;
    return 0;
}
uint8_t read_vmd_sta_key(){
    if (rt_pin_read(VMD_STA_KEY_PIN))
        return 1;
    return 0;
}
uint8_t read_co_sta_key(){
    if (rt_pin_read(CO_STA_KEY_PIN))
        return 1;
    return 0;
}

uint8_t read_h2s_sta_key(){
    if (rt_pin_read(H2S_STA_KEY_PIN))
        return 1;
    return 0;
}

uint8_t read_so2_sta_key(){
    if (rt_pin_read(SO2_STA_KEY_PIN))
        return 1;
    return 0;
}

/******** 激活按钮 单击**********/
void wakeup_key_single_click(void *btn)
{
		rt_kprintf("---> wakeup_key single click\n");
		beep_start(3, 1);
		gas_exit_lp();
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
		
}
/******** 激活按钮 双击 **********/
void wakeup_key_double_click(void *btn)
{
		rt_kprintf("---> wakeup_key double click\n");
		beep_start(3, 4);
		rt_thread_delay(rt_tick_from_millisecond(3000));
		reboot();
}
/******** 激活按钮 长按不松开 **********/
void wakeup_key_long_press_start(void *btn)
{
		rt_kprintf("---> wakeup_key long press start\n");
		beep_start(3, 2);
		rt_event_send(cover_event,COVER_EVENT_INIT_RADAR);
		
}


/******** 移动监测 **********/
void vmd_sta_key_press_up(void *btn)
{
		rt_kprintf("---> vmd_sta_key press up\n");
		rt_event_send(cover_event,COVER_EVENT_CHECK_SENS);
		//发送移动监测事件
		//net_send_event(SEND_MSG_DEV_HEARTBEAT);
}

/******** 气体 **********/
void co_sta_key_press_down(void *btn)				//告警
{
		rt_kprintf("---> co_sta_key press down\n");
		set_gas_state(GAS_CO,GAS_WARNING);
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
}
void co_sta_key_press_up(void *btn)					//恢复
{
		rt_kprintf("---> co_sta_key press up\n");
		set_gas_state(GAS_CO,GAS_NORMAL);
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
}

void h2s_sta_key_press_down(void *btn)			//告警
{
		rt_kprintf("---> h2s_sta_key press down\n");
		set_gas_state(GAS_H2S,GAS_WARNING);
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
}
void h2s_sta_key_press_up(void *btn)				//恢复
{
		rt_kprintf("---> h2s_sta_key press up\n");
		set_gas_state(GAS_H2S,GAS_NORMAL);
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
}


void so2_sta_key_press_down(void *btn)			//告警
{
		rt_kprintf("---> so2_sta_key press down\n");
		set_gas_state(GAS_SO2,GAS_WARNING);
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
}
void so2_sta_key_press_up(void *btn)				//恢复
{
		rt_kprintf("---> so2_sta_key press up\n");
		set_gas_state(GAS_SO2,GAS_NORMAL);
		net_send_event(SEND_MSG_DEV_HEARTBEAT);
}

void vmd_IT_Enable()
{
	rt_pin_attach_irq(VMD_STA_KEY_PIN,PIN_IRQ_MODE_RISING_FALLING,0,(void*)0);	
 	rt_pin_irq_enable(VMD_STA_KEY_PIN,RT_TRUE);
	rt_pin_mode(VMD_STA_KEY_PIN,PIN_MODE_INPUT);

}

void vmd_IT_Disable(void){
		rt_pin_irq_enable(VMD_STA_KEY_PIN,RT_FALSE);
}

void key_pin_gpio_init(){
#if 1
	rt_pin_attach_irq(WAKEUP_KEY_PIN, PIN_IRQ_MODE_FALLING, NULL, (void*)"wakeup_key_pin");
	rt_pin_irq_enable(WAKEUP_KEY_PIN, PIN_IRQ_ENABLE);
	// 上拉输入 必须在上面2个函数调用之后再调用 rt_pin_mode  因为官方提供的驱动就是坑。会把PULL重新覆盖成 NOPULL
	rt_pin_mode(WAKEUP_KEY_PIN, PIN_MODE_INPUT_PULLUP);

//	rt_pin_attach_irq(VMD_STA_KEY_PIN, PIN_IRQ_MODE_RISING, NULL, (void*)"vmd_sta_key_pin");			//
//	rt_pin_irq_enable(VMD_STA_KEY_PIN, PIN_IRQ_ENABLE);
//	// 上拉输入 必须在上面2个函数调用之后再调用 rt_pin_mode  因为官方提供的驱动就是坑。会把PULL重新覆盖成 NOPULL
//	rt_pin_mode(VMD_STA_KEY_PIN, PIN_MODE_INPUT_PULLDOWN);
	#ifdef USE_GAS_DO
		rt_pin_mode(CO_STA_KEY_PIN, PIN_MODE_INPUT_PULLUP);			//一氧化碳检测脚 上拉输入
		rt_pin_mode(H2S_STA_KEY_PIN, PIN_MODE_INPUT_PULLUP);			//一氧化碳检测脚 上拉输入
		rt_pin_mode(SO2_STA_KEY_PIN, PIN_MODE_INPUT_PULLUP);			//一氧化碳检测脚 上拉输入
	#endif
#else
    GPIO_InitTypeDef GPIO_InitStruct;
    __HAL_RCC_GPIOC_CLK_ENABLE();
    GPIO_InitStruct.Pin = GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING; //下降沿触发中断
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 1, 1);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
#endif
}
void key_event_init(){
	button_init(&wakeup_key, read_wakeup_key, 0);
	button_attach(&wakeup_key, SINGLE_CLICK, wakeup_key_single_click);			//单击
	//button_attach(&wakeup_key, DOUBLE_CLICK, wakeup_key_double_click);			//双击	无效
	button_attach(&wakeup_key, LONG_RRESS_START, wakeup_key_long_press_start);			//长按
	button_start(&wakeup_key);
	
	button_init(&vmd_sta_key, read_vmd_sta_key,0);
	button_attach(&vmd_sta_key, PRESS_UP, vmd_sta_key_press_up);			
	button_start(&vmd_sta_key);
#ifdef USE_GAS_DO
	button_init(&co_sta_key, read_co_sta_key,0);
	button_attach(&co_sta_key, PRESS_UP, co_sta_key_press_up);
	button_attach(&co_sta_key, PRESS_DOWN, co_sta_key_press_down);	
	button_start(&co_sta_key);
	
	button_init(&h2s_sta_key, read_h2s_sta_key,0);
	button_attach(&h2s_sta_key, PRESS_UP, h2s_sta_key_press_up);
	button_attach(&h2s_sta_key, PRESS_DOWN, h2s_sta_key_press_down);	
	button_start(&h2s_sta_key);
	
	button_init(&so2_sta_key, read_so2_sta_key,0);
	button_attach(&so2_sta_key, PRESS_UP, so2_sta_key_press_up);
	button_attach(&so2_sta_key, PRESS_DOWN, so2_sta_key_press_down);	
	button_start(&so2_sta_key);
#endif
}


void button_thread(void *parameter)
{
	key_pin_gpio_init();
	key_event_init();
	for(;;)
	{
		button_ticks();
		rt_thread_delay(rt_tick_from_millisecond(10));	
	}
}
int button_app_init(void)
{
    rt_thread_t pro_thread = NULL;
    pro_thread = rt_thread_create("btn_app", button_thread, NULL,256, 13, 100); //延迟启动 设备时间
    if (pro_thread != NULL)
    {
        rt_thread_startup(pro_thread);
    }
    else
    {
        rt_kprintf("create button_thread is faild .\n");
				return 0;
    }
		return 1;
}



void button_enter_lp(void)
{
#if 0		//当按钮无固定 休眠状态 时，根据 按钮状态 配置 休眠中断 唤醒方式
	if (rt_pin_read(WAKEUP_KEY_PIN)){		
		//当前为高电平 则配置为：设置下拉中断，引脚上拉输入
		rt_pin_attach_irq(WAKEUP_KEY_PIN, PIN_IRQ_MODE_FALLING, NULL, (void*)"wakeup_key_pin");
		rt_pin_irq_enable(WAKEUP_KEY_PIN, PIN_IRQ_ENABLE);
		// 上拉输入 必须在上面2个函数调用之后再调用 rt_pin_mode  因为官方提供的驱动就是坑。会把PULL重新覆盖成 NOPULL
		rt_pin_mode(WAKEUP_KEY_PIN, PIN_MODE_INPUT_PULLUP);	
	}
	else			
	{
		//当前为低电平 则配置为：设置上拉中断，引脚下拉输入
		rt_pin_attach_irq(WAKEUP_KEY_PIN, PIN_IRQ_MODE_RISING, NULL, (void*)"wakeup_key_pin");
		rt_pin_irq_enable(WAKEUP_KEY_PIN, PIN_IRQ_ENABLE);
		rt_pin_mode(WAKEUP_KEY_PIN, PIN_MODE_INPUT_PULLDOWN);	
	}
#endif
	key_pin_gpio_init();
	set_RtPin_had_lp(WAKEUP_KEY_PIN,0);		//自动休眠 忽略这个脚
	if(DEFENCE==get_defence()){
		vmd_IT_Enable();
	}
	set_RtPin_had_lp(VMD_STA_KEY_PIN,0);		//自动休眠 忽略这个脚

}
void button_exit_lp(void)
{
	vmd_IT_Disable();
	key_pin_gpio_init();

}

//void EXTI4_IRQHandler(void)				//在drv_gpio.c里已经处理了
//{
//  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
//}

