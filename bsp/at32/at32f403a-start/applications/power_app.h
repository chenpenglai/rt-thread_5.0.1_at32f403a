#ifndef __power_app_H
#define __power_app_H

#include "stdbool.h"

#include "stm32l1xx_hal.h"
#include <rtthread.h>
#include "global_define.h"

//后期可优化为插入列表的方式
#include "gjx_lock.h"

enum
{
	regular_heart_alarm_index=0,	
	period_warninig_alram_index,
	period_check_index,
	auto_lock_alarm_index,		//自动落锁闹钟
	long_time_open_lock_alarm_index,		//长时间开门闹钟
#ifdef USING_WATER_PRESSURE_SENSOR
	period_water_pressure_index,//周期采集的闹钟
#endif
/*编号结束位置*/
	alarm_max_num,
};

#define GJX_LOCK_ALRAM_INDEX gjx_lock1_alaram_index







#define ENTER_STOP (1<<0)

/*******		内部调用		********/
void exit_stop_mode(void);
bool is_flag(char* flag,char bit);
void enter_stop_mode(uint32_t _time);

/*串口1*/
#define USART1_TX_GPIO_PIN GPIO_PIN_9
#define USART1_RX_GPIO_PIN GPIO_PIN_10

/*串口2*/
#define USART2_TX_GPIO_PIN GPIO_PIN_2
#define USART2_RX_GPIO_PIN GPIO_PIN_3

/*串口3*/
#define USART3_TX_GPIO_PIN GPIO_PIN_10
#define USART3_RX_GPIO_PIN GPIO_PIN_11

#ifndef SET_OUTPUT
#define SET_OUTPUT(pin,value)						\
do{rt_pin_mode(pin,PIN_MODE_OUTPUT);		\
	rt_pin_write(pin,value);							\
}while(0)
#endif

#define set_pin_output(pin,value)				\
do{rt_pin_mode(pin,PIN_MODE_OUTPUT);		\
	rt_pin_write(pin,value);							\
}while(0)

#ifndef SET_OD_INPUT
#define SET_OD_INPUT(pin)			rt_pin_mode(pin,PIN_MODE_OUTPUT_OD)
#endif

#ifndef SET_UP_INPUT
#define SET_UP_INPUT(pin)			rt_pin_mode(pin,PIN_MODE_INPUT_PULLUP)
#endif

#ifndef SET_DOWN_INPUT
#define SET_DOWN_INPUT(pin)		rt_pin_mode(pin,PIN_MODE_INPUT_PULLDOWN)
#endif

#ifndef SET_NO_INPUT
#define SET_NO_INPUT(pin)			rt_pin_mode(pin,PIN_MODE_INPUT)
#endif

//#define KEY2_GPIO_PIN                 GPIO_PIN_1		//15号脚

//#define GJX1_PERIOD_ALRAM_EVT 			(1<<0)//光交箱警告编号不要变
//#define GJX2_PERIOD_ALRAM_EVT 			(1<<1)
//#define GJX3_PERIOD_ALRAM_EVT 			(1<<2)
//#define GJX4_PERIOD_ALRAM_EVT 			(1<<3)
//#define ADXL362_PERIOD_ALRAM_EVT   	(1<<4)
//#define THRH_PERIOD_ALRAM_EVT 			(1<<5)
//#define VOLTAGE_PERIOD_ALRAM_EVT 		(1<<6)//电压周期警报？可能不需要
//#define WATER_PERIOD_ALRAM_EVT 			(1<<7)//浸水的周期警报
//#define FIRE_PERIOD_ALRAM_EVT 			(1<<8)//浸火的周期警报

#define HIGH_TEMP_PERIOD_ALRAM_EVT					(1<<0)		//高温 周期性告警事件
#define LOW_TEMP_PERIOD_ALRAM_EVT						(1<<1)		//低温 周期性告警事件
#define HIGH_HUMIDITY_PERIOD_ALRAM_EVT			(1<<2)		//高湿 周期性告警事件
#define LOW_HUMIDITY_PERIOD_ALRAM_EVT				(1<<3)		//低湿 周期性告警事件
#define ADXL_PERIOD_ALRAM_EVT								(1<<4)		//倾斜周期性告警事件
#define LONG_TIME_OPEN_LOCK_PERIOD_ALRAM_EVT	(1<<5)		//长时间开锁 告警事件
#define WATER_OUT_PERIOD_ALRAM_EVT				(1<<6)		//水浸 周期性告警事件



void period_warninig_alarm_try_add(uint32_t warning_event);
void period_warninig_alarm_try_stop(uint32_t warning_event);


void set_life_alarm(void);
void set_period_check_alarm(void);
void cancel_period_check_alarm(void);
rt_timer_t pm_release_timer(int timeout);
void stop_release_timer(void);
rt_timer_t pm_auto_sleep(int timeout);

void alarm_add_sec(uint8_t alarm_num, int sec);//lkk add 2020-6-4
rt_err_t alarm_stop(uint8_t alarm_index);//lkk add 2020-6-4
void auto_lock_alarm_stop();
void auto_lock_alarm_add();

void long_time_open_lock_alarm_add();
void long_time_open_lock_alarm_stop();
	
void set_HalPin_had_lp(GPIO_TypeDef* gpio,uint16_t pin,uint8_t forever_flag);//lkk add 2020-6-20
void set_RtPin_had_lp(rt_base_t pin,uint8_t forever_flag);//lkk add 2020-6-20

#endif
