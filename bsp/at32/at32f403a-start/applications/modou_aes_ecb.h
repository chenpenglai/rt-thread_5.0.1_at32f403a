#ifndef __MODOU_AES_H
#define __MODOU_AES_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>

int aesEncrypt(uint8_t *data);
int aesDecrypt(uint8_t *data);
#endif
