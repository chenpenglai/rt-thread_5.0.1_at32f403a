#include "log_app.h"
#include "tool.h"
#include "global_define.h"
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <sfud.h>
#include <spi_flash.h>

/*
		可以用于记录开关锁的历史记录
		
*/
#ifdef USING_LOG
_log_para log_para;

static const sfud_flash *flash;
extern rt_spi_flash_device_t w25q80;
#define log_read_data(dat,addr,size)   sfud_read(flash,addr,size,dat)         //方便替换接口
#define log_write_data(dat,addr,size)  sfud_write(flash,addr,size,dat)
#define flash_erase(addr, size); 		 		sfud_erase(flash, addr, size);

#define get_flash_write_addr()	u8_2_u32(&log_para.log_write_addr[0])             //获取绝对写地址，注意：获取的是加了 LOG_START_ADDR 后的

void set_flash_write_addr(uint32_t w_addr)              //相对设置写地址，注意：会自动加 LOG_START_ADDR
{
    uint32_t write_addr = w_addr + LOG_START_ADDR;
    log_para.log_write_addr[0] = (uint8_t)(write_addr >> 24);
    log_para.log_write_addr[1] = (uint8_t)(write_addr >> 16);
    log_para.log_write_addr[2] = (uint8_t)(write_addr >> 8);
    log_para.log_write_addr[3] = (uint8_t)(write_addr);
    log_write_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);

}
void increase_write_addr(uint32_t w_addr)           //相对递增写地址
{
    uint32_t write_addr = w_addr+get_flash_write_addr();
//  rt_kprintf("increase write_add: %ld\n",write_addr);
    log_para.log_write_addr[0] = (uint8_t)(write_addr >> 24);
    log_para.log_write_addr[1] = (uint8_t)(write_addr >> 16);
    log_para.log_write_addr[2] = (uint8_t)(write_addr >> 8);
    log_para.log_write_addr[3] = (uint8_t)(write_addr);
    log_write_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);
}


#define get_log_size() (get_flash_write_addr()-LOG_START_ADDR)			//日志当前使用大小
//#define increase_read_num(r_num)	set_read_num(get_read_num()+r_num)			//相对递增
uint16_t get_log_num() {
    u8_2_u16(&log_para.log_num[0]);                          //获取日志总条数
}
void set_log_num(uint16_t r_num)               //设置日志条数
{
    log_para.log_num[0] = (uint8_t)(r_num >> 8);
    log_para.log_num[1] = (uint8_t)(r_num);
    flash_erase(LOG_PARA_START_ADDR, LOG_PARA_SIZE);
    log_write_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);
}

#define get_read_num_cursor()	u8_2_u16(&log_para.read_cursor[0])                                 //获取日志读取条数游标

void set_read_num_cursor(uint16_t read_cursor)               //设置日志读取条数游标
{
    log_para.read_cursor[0] = (uint8_t)(read_cursor >> 8);
    log_para.read_cursor[1] = (uint8_t)(read_cursor);
    flash_erase(LOG_PARA_START_ADDR, LOG_PARA_SIZE);
    log_write_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);
}


#define FIRST_FLAG 0xaa
void log_reset() {
    rt_kprintf("log_reset\n");
    rt_memset((uint8_t *)&log_para,0,LOG_PARA_SIZE);
    log_para.log_first_flag = FIRST_FLAG;
    set_flash_write_addr(0);

    flash_erase(LOG_PARA_START_ADDR, LOG_PARA_SIZE);
    flash_erase(LOG_START_ADDR, LOG_SIZE);
    log_write_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);


}

bool is_first_run_log()
{
    if (FIRST_FLAG == log_para.log_first_flag)         //不是第一次启动
    {
        return false;
    }
    else            //是第一次启动
    {
        rt_kprintf("first run\n");
        return true;
    }
}

void log_init()
{
    flash = (sfud_flash_t)(w25q80->user_data);
    log_read_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);
    if(is_first_run_log())
    {
        log_reset();		//初始化日志参数
    }
    //  if(0>get_log_size())
//      set_flash_write_addr(0);
    //spi_flash_chip_erase();
    //hexdump("log_para",(uint8_t*)&log_para, LOG_PARA_SIZE);
		
    //rt_kprintf("log used flash: %ld\n", get_log_size());
		rt_kprintf("log_num: %ld\n", get_log_num());
		//rt_kprintf("read_num_cursor: %ld\n",get_read_num_cursor());

}
void log_write(uint8_t *log, uint16_t log_len)
{

    if (log_len > LOG_SINGLE_SIZE)
    {
        log_len = LOG_SINGLE_SIZE;
        //rt_kprintf("log_len overstep\n");
    }
    //rt_kprintf("get_log_size: %d \n",get_log_size());
    if ((get_log_size() + LOG_SINGLE_SIZE)> LOG_SIZE)
    {
        log_reset();
        rt_kprintf("clear old log\n");
    }
    //flash_erase(get_flash_write_addr(), LOG_SINGLE_SIZE);
    log_write_data(log, get_flash_write_addr(), LOG_SINGLE_SIZE);

    //set_log_num(get_log_num()+1);
    uint16_t r_num=get_log_num()+1;
    log_para.log_num[0] = (uint8_t)(r_num >> 8);
    log_para.log_num[1] = (uint8_t)(r_num);

    //increase_write_addr(LOG_SINGLE_SIZE);
    uint32_t write_addr = LOG_SINGLE_SIZE+get_flash_write_addr();
    log_para.log_write_addr[0] = (uint8_t)(write_addr >> 24);
    log_para.log_write_addr[1] = (uint8_t)(write_addr >> 16);
    log_para.log_write_addr[2] = (uint8_t)(write_addr >> 8);
    log_para.log_write_addr[3] = (uint8_t)(write_addr);

    flash_erase(LOG_PARA_START_ADDR, LOG_PARA_SIZE);
    log_write_data((uint8_t *)&log_para, LOG_PARA_START_ADDR, LOG_PARA_SIZE);
}


/*
[#######                        ]

        [0 ##################################]
        [1 ##################################]
        [2 ##################################]
        [3 ##########  已读行  ##############]
        [4 ##################################]
        [5 ##################################]
        [6 #########  read_num  #############]      <------ read_num 位置
        [7 ----------------------------------]
        [8 ----------------------------------]
        [9 ----------------------------------]
        [10 ---------  未读行  --------------]      <------ 未读行：unread_num()
        [11 ---------------------------------]
        [12 ---------------------------------]      <------ get_log_size()/LOG_SINGLE_SIZE

        已读日志 与 所有日志的序号 从1开始；可指定序号读取
*/
uint16_t log_read(uint8_t *rd_buf, uint16_t rd_buf_len,uint16_t read_log_num)
{
    //rt_kprintf("read_log_num: %d\n",read_log_num);
    uint16_t log_num=get_log_num();
    if(log_num==0) {		//无日志，结束
        rt_kprintf("no log log_num: 0\n");
        return 0;
    }
    if (read_log_num > log_num)        //读取条数 超出 所有日志 范围
    {
        rt_kprintf("read_log_num overstep, total: %d\n", log_num);
        read_log_num=log_num;
    }
    if ((read_log_num*LOG_SINGLE_SIZE) > rd_buf_len)			//如果缓存区太小
    {
        rt_kprintf("rd_buf_len too small\n");
        read_log_num=rd_buf_len/LOG_SINGLE_SIZE;
    }
    log_read_data(rd_buf, LOG_START_ADDR+(LOG_SINGLE_SIZE*get_read_num_cursor()), read_log_num*LOG_SINGLE_SIZE);

    set_read_num_cursor(get_read_num_cursor()+read_log_num);
    set_log_num(log_num-read_log_num);

    return read_log_num*LOG_SINGLE_SIZE;
}

void w_log()
{
		uint8_t recv_buf[10];
    static int a=0;
    a++;
    rt_kprintf("a: %02X\n",a);
    rt_memset(recv_buf,a,LOG_SINGLE_SIZE);
    log_write(recv_buf,LOG_SINGLE_SIZE);
}

void r_log()
{
		uint8_t recv_buf[10];
    int pos=0,num;
    num=get_log_num();
    if(num>=1)			//日志大于等于一次最大读取条数
    {
        pos+=log_read(&recv_buf[pos], sizeof(recv_buf) - pos, 1);
    }
    else if(num==0)		//无日志
    {
        rt_kprintf("no log\n");
        return ;
    }
    rt_kprintf("log num: %d\n",num);
    hexdump("---",recv_buf,pos);
}
static void reset_log(uint8_t argc, char **argv)
{
    log_reset();
}
MSH_CMD_EXPORT(reset_log, clear);

static void write_log(uint8_t argc, char **argv)
{
    w_log();
}
MSH_CMD_EXPORT(write_log, clear);

static void read_log(uint8_t argc, char **argv)
{
    r_log();
}
MSH_CMD_EXPORT(read_log, clear);

#endif