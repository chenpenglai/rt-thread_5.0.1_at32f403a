#ifndef __GLOBAL_DEFINE_H_
#define __GLOBAL_DEFINE_H_
#include "rtthread.h"
#include "pin.h"
#include "ef_cfg.h"
#include <board.h>
#include "Tool.h"
#include "pcb_info.h"

typedef enum//全局处理的事件类型，只允许从后面添加增加，只增不减，允许中间保留一部分空着; 上报协议时可参考此枚举类型
{
	/*电量事件类型*/
	GE_BATTERY_VOLTAGE_LOW=0,//低电量告警
	GE_BATTERY_VOLTAGE_LOW_CLEAR,//解除低湿度报警
	
		/*倾斜事件*/
	GE_ADXL_WARNING=GE_BATTERY_VOLTAGE_LOW_CLEAR+3,//发生倾斜
	GE_ADXL_CLEAR,//倾斜解除
	
	/*湿度事件类型*/
	GE_HUMIDITY_HIGH=GE_ADXL_CLEAR+3,//高湿度报警
	GE_HUMIDITY_LOW,	//低湿度报警
	GE_HUMIDITY_HIGH_CLEAR,//解除高湿度报警
	GE_HUMIDITY_LOW_CLEAR,//解除低湿度报警
	
	/*温度事件类型，同湿度报警*/
	GE_TEMP_HIGH=GE_HUMIDITY_LOW_CLEAR+3,//给湿度保留(3-1)==2个枚举空间；5
	GE_TEMP_LOW,	
	GE_TEMP_HIGH_CLEAR,
	GE_TEMP_LOW_CLEAR,
	
	/*水压事件类型，同湿度报警*/
	GE_WATER_PRESSURE_HIGH=GE_TEMP_LOW_CLEAR+3,//给温度保留(3-1)==2个枚举空间；
	GE_WATER_PRESSURE_LOW,	
	GE_WATER_PRESSURE_HIGH_CLEAR,
	GE_WATER_PRESSURE_LOW_CLEAR,
	GE_WATER_PRESSURE_CHANGE,//两次采集数据波动较大的报警

	/*测距事件类型，同湿度报警*/
	GE_DISTANCE_HIGH=GE_WATER_PRESSURE_CHANGE+3,//给水压保留(3-1)==2个枚举空间
	GE_DISTANCE_LOW,	
	GE_DISTANCE_HIGH_CLEAR,
	GE_DISTANCE_LOW_CLEAR,

	/*门磁事件*/
	GE_DOOR_MAGNETIC_OPEN=GE_DISTANCE_LOW_CLEAR+3,//门磁打开；给测距事件保留(3-1)==2个枚举空间
	GE_DOOR_MAGNETIC_CLOSE,//门磁关闭

		/*浸水事件*/
	GE_WATER_IN_WARNING=GE_DOOR_MAGNETIC_CLOSE+3,//发生浸水
	GE_WATER_IN_CLEAR,//浸水解除

	/*浸火事件*/
	GE_FIRE_IN_WARNING=GE_WATER_IN_CLEAR+3,//发生浸火
	GE_FIRE_IN_CLEAR,//浸火解除
	
	
	
	
	
	
}GLOBAL_ENVENT_EN;
uint8_t global_event_pro(GLOBAL_ENVENT_EN event);//全局所有事件的处理：高低温，水浸，火浸。。。。



//lkk add start 2020-4-14  便于移植stm8的程序，移植完可进行替换处理
/*!< Signed integer types  */
#define DelayT_ms(x) rt_thread_delay(rt_tick_from_millisecond(x))

#define PrintString(x)  rt_kprintf(x)
#define printNumStr_(x) rt_kprintf("%d",x)//lkk delete  2020-4-14 
#define printNum2Str_(x)	rt_kprintf("%d",x)//"getthrt.c"使用
#define debug_send(x)	rt_kprintf(x)
#define PrintString_(x) rt_kprintf(x)

#define TIM4_Cmd(x) //"getthrt.c" 使用，推测应替换为开关全局中断,可使用临界区进出替代

typedef   signed char     int8_t;
typedef   signed short    int16_t;
//typedef   signed long     int32_t; 已被stdint.h定义

/*!< Unsigned integer types  */
typedef unsigned char     uint8_t;
typedef unsigned short    uint16_t;
//typedef unsigned long     uint32_t;int32_t; 已被stdint.h定义

/*!< STM8Lx Standard Peripheral Library old types (maintained for legacy purpose) */

typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;


//lkk add end 2020-4-14  便于移植stm8的程序，移植完可进行替换处理




extern struct rt_messagequeue send_mq;
extern struct rt_event event;
extern rt_event_t car_event;

typedef struct{
	uint8_t magnet_init_err;
	uint8_t radar_init_err;
	uint8_t ble_init_err;
}_init_err;			//初始化结果 //lkk add 2020-4-10
extern _init_err init_err;//lkk add 2020-4-10

typedef struct _time_date{
	RTC_TimeTypeDef rtctime;
	RTC_DateTypeDef rtcdate;
}time_date;

#define EVENT_LOW_POWER_EXIT                (1<<1)
#define EVENT_LOW_POWER_ENTER               (1<<2)

/*************          net event               ***************/
//#define EVENT_NET_CONNECTING                (1<<0)
//#define EVENT_NET_FAIL                      (1<<1)
//#define EVENT_NET_SUCCEED                   (1<<2)
//#define EVENT_NET_BAD                       (1<<3)
//#define EVENT_NET_SEND_OVER                 (1<<4)
//#define EVENT_CONNECT_SUCCESSED             (1<<5)


/*************          net send event               ***************/
#define NET_EVENT_CONNECT 									(1<<0)
#define NET_EVENT_RECONNECT 								(1<<1)
#define NET_EVENT_IOT_UPGRADE 							(1<<2)

#define NET_EVENT_SEND_HEARTING 						(1<<3)	
#define NET_EVENT_RESEND_HEARTING 					(1<<4)
#define NET_EVENT_HEARTING_REPLY						(1<<5)

#define NET_EVENT_SEND_PARA_SET_REPLY 			(1<<6)
#define NET_EVENT_SEND_PARA_QUERY 					(1<<7)
#define NET_SEND_DATA 											(1<<8)

#define NET_EVENT_CONN_OK              			(1 << 9)
#define NET_EVENT_CONN_FAIL            			(1 << 10)
#define NET_EVENT_RECV_DATA									(1<< 11)

/*************          htbklock send event               ***************/
#define LOCK_EVENT_CLOSE 																(1<<0)
#define LOCK_EVENT_OPEN 																(1<<1)
//#define LOCK_EVENT_GET_STATE 														(1<<2)
#define LOCK_EVENT_ADD_AUTO_LCOK_ALARM 									(1<<2)
#define LOCK_EVENT_STOP_AUTO_LCOK_ALARM 								(1<<3)
#define LOCK_EVENT_ADD_LONG_TIME_OPEN_LOCK_ALARM 				(1<<4)
#define LOCK_EVENT_STOP_LONG_TIME_OPEN_LOCK_ALARM 			(1<<5)


/*************          out cover event               ***************/
#define COVER_EVENT_INIT_RADAR 									(1<<0)				//初始雷达		
#define COVER_EVENT_CHECK_SENS									(1<<1)				//读取传感器值






//消息队列种的时间标记，使用枚举
typedef enum
{
//		SEND_MSG_NONE=0,//用于发数据
		SEND_MSG_DEV_HEARTBEAT=0,					//心跳
		SEND_MSG_PARA_SET_REPLY,					//参数配置回复
		SEND_MSG_IOT_UPGRADE,							//iot升级回复
	/**水浸**/
		SEND_MSG_WATER_LEVEL_STATE_ALARM,		//水浸状态
		SEND_MSG_WATER_WARNING,		//浸水报警
	/**温湿度**/
		SEND_MSG_HUMIDITY_STATE_REPORT,
		SEND_MSG_TEMPERATURE_STATE_REPORT,
	
		SEND_MSG_ADXL_STATE_REPORT,
		SEND_MSG_LONG_TIME_OPEN_LOCK,
//		SEND_MSG_SET_DEFENCE_SWITCH_RESP,
//		SEND_MSG_WATER_WARNING,//浸水报警
//		SEND_MSG_THRH,//温湿度信息需要上报
		SEND_MSG_PERIOD_WARNING,		//周期性警告
		
//		SEND_MSG_USR_BUF_DATA, 	 //用户缓存的数据
} SEND_MSG_EVENT_EN;
struct send_msg
{
    SEND_MSG_EVENT_EN msg_event;
	  char* data;//lkk add 2020-4-10
		int len; //lkk add 2020-4-10
};

#define SYS_DEBUG_ENABLE


#ifdef SYS_DEBUG_ENABLE
//#define MP_INFO_NEED RT_TRUE//lkk add “RT_TRUE” 2020-4-10
#define TEST_SWITCH RT_FALSE//lkk add “RT_TRUE” 2020-4-10
#else
//#define MP_INFO_NEED RT_FALSE//lkk add “RT_TRUE” 2020-4-10
#define TEST_SWITCH RT_FALSE//lkk add “RT_TRUE” 2020-4-10

#endif

#ifdef SYS_DEBUG_ENABLE
#define SYS_CONFIG_ADDR 0x803F000
struct sys_config
{
    uint8_t  use_uart;
    uint16_t check_sum;
};
#endif

#define SET_OD_OUTPUT(pin,value)                        \
do{rt_pin_mode(pin,PIN_MODE_OUTPUT_OD);     \
    rt_pin_write(pin,value);                            \
}while(0)

#define SET_OUTPUT(pin,value)                       \
do{rt_pin_mode(pin,PIN_MODE_OUTPUT);        \
    rt_pin_write(pin,value);                            \
}while(0)


#define SET_UP_INPUT(pin)           rt_pin_mode(pin,PIN_MODE_INPUT_PULLUP)
#define SET_DOWN_INPUT(pin)         rt_pin_mode(pin,PIN_MODE_INPUT_PULLDOWN)
#define SET_NO_INPUT(pin)           rt_pin_mode(pin,PIN_MODE_INPUT)

extern struct send_msg msg;
#if 0
#define net_send_event(status)\
    msg.msg_event = status;\
    rt_mq_send(&send_mq, &msg, sizeof(struct send_msg))
#else
#define net_send_event(status) do\
{msg.msg_event = status;\
rt_mq_send(&send_mq, &msg, sizeof(struct send_msg));}\
while(0)
#endif

extern rt_event_t  net_event ;
void all_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void stop_recover_old_app_upload_result(void);
#define reboot NVIC_SystemReset

/*******				boot升级参数					*********/
#define BK_APP_ADDR                     0                           
#define BK_APP_SIZE                     (220*1024)
#define OLD_APP_ADDR                    BK_APP_ADDR +BK_APP_SIZE
#define OLD_APP_SIZE          					(220*1024)          
#define UPGRADE_FLAG_ADDR               (OLD_APP_ADDR + OLD_APP_SIZE)
#define UPGRADE_FLAG_SIZE               (4*1024)                    //最小擦除为4K 所以空间不能小于4K（因为还要写入） 
#define NEED_UPGRADE_FLAG               1
#define SUCCESSED_UPGRADE_FLAG          2
#define ERR_UPGRADE_FLAG                3
/*******				升级参数					*********/

#define EF_USING_LOG_SPI_FLASH
#ifdef EF_USING_LOG_SPI_FLASH
    #define EF_LOG_SPI_FLASH_ADDR (BK_APP_SIZE + OLD_APP_SIZE +UPGRADE_FLAG_SIZE)  //520
#endif
void set_upgread_flag(uint8_t upgread_flag, uint32_t size, uint16_t check);
void reset_info_chk(void);
int inernal_flash_write(uint32_t addr, const uint32_t *buf, size_t size);
int inernal_flash_erase(uint32_t addr, size_t size);
int inernal_flash_read(uint32_t addr, uint32_t *buf, size_t size);

int set_sys_use_uart(uint8_t num);
int get_sys_use_uart(void);

extern void soft_uart_lowpower_ctr(uint8_t enter1_quit0_flag);//lkk add 2020-4-24


#define SEC_TO_TICK(s)		(s*RT_TICK_PER_SECOND) //由 秒转化为 tick

#define MS_TO_TICK(MS)	((MS+1000/RT_TICK_PER_SECOND-1)/(1000/RT_TICK_PER_SECOND))	//由 毫秒 转化为tick
//#define MS_TO_TICK(x)  ((x*RT_TICK_PER_SECOND+999)/1000)//至少1 tick
#define TICK_TO_MS(x)  (x*1000/RT_TICK_PER_SECOND)//由 tick转位 ms

#define get_sys_tick()  rt_tick_get()
#define sys_tick_value_max  RT_TICK_MAX
#ifndef sys_tick_t
typedef rt_tick_t sys_tick_t; 
#endif
sys_tick_t tick_had_gone(sys_tick_t old_tick);

#define sys_delay_ms(MS) rt_thread_delay(MS_TO_TICK(MS)) //系统延时    n毫秒
#define sys_delay_sec(SEC) rt_thread_delay(SEC_TO_TICK(SEC))//系统延时 n秒


#define SLEEP_LOCK()        rt_pm_request(PM_SLEEP_MODE_NONE)//lkk add 2020-5-6
#define SLEEP_UNLOCK()      rt_pm_release(PM_SLEEP_MODE_NONE)

//#define NB_PWK	GET_PIN(A,7)			//NB的电源脚
#define MOT_INB	GET_PIN(C,14)			//电机脚
#define MOT_INA	GET_PIN(C,13)
#define ACT_IN1	GET_PIN(C,0)		//锁体脚
#define SW_IN1	GET_PIN(C,1)		//锁体脚
#define TOU_IN1	GET_PIN(C,2)		//锁体脚


void flash_enter_lp(void);
void flash_exit_lp(void);
void peripheral_enter_quit_lp(uint8_t enter1_quit0);
void unuse_pin_exit_lp(void);
void unuse_pin_enter_lp(void);
void uart_enter_lp(void);



//void set_high_warn_enable(VALUE_WARNING_STR* value_warn,SENSOR s,uint8_t enable);
//通过此字段 选择不同的网络协议
typedef enum{
		IOT_PLAT=0,		//上iot平台
		AEP_PLAT,			//上aep平台
		ONENET_PLAT,	//onenet平台	
		TCP_PLAT,			//上TCP
		UDP_PLAT,			//上UDP
		
}PLAT_SEL;
void set_plat(PLAT_SEL sel);
int get_plat(void);
/*******************				撤/布防						*********************/
typedef enum{
	NO_DEFENCE=0,			//撤防
	DEFENCE=1,				//布防
}IS_DEFENCE;
void set_defence(IS_DEFENCE def);
int get_defence();

typedef enum{
	BLE_OPEN=0,	//0 蓝牙
	NB_OPEN,			//1 NB
	KEY_OPEN,		//2钥匙
	OTHER_OPEN,	//3 其他
}_open_lock_type;
extern _open_lock_type open_lock_type;
void set_open_lock_type(_open_lock_type type);
_open_lock_type get_open_lock_type();

typedef enum{
	NOT_ALLOW_OPEN=0,			//不允许开门
	ALLOW_OPEN,						//允许开门
}_is_allow_open_lock;
extern _is_allow_open_lock is_allow_open_lock;
void set_open_lock_allow(uint8_t flag);
uint8_t get_open_lock_allow();

typedef enum{
	GAS_O2=0,	
	GAS_CO,
	GAS_H2S,
	GAS_SO2,
	GAS_MAX_NUM,
}_gas_type;
typedef enum{
	GAS_NORMAL=0,	
	GAS_WARNING,
}_gas_state;

uint8_t get_gas_state(_gas_type gas_type);
uint8_t set_gas_state(_gas_type gas_type,uint8_t state);
#define GJX_LOCK_1 1 //上报的光交箱编号
#define GJX_LOCK_2 2 //上报的光交箱编号
#define GJX_LOCK_3 3 //上报的光交箱编号
#define GJX_LOCK_4 4 //上报的光交箱编号
void stop_recover_old_app_upload_result(void);

/******			气体 电源控制			*****/
#define OPEN_GAS_POWER()	SET_OUTPUT(PW_WP_PIN,0);
#define CLOSE_GAS_POWER()	SET_NO_INPUT(PW_WP_PIN);		//本来是输出高，考虑到后期更换电池可能会有影响，所以改成浮空

/******			雷达 电源控制			*****/
#define OPEN_RADAR()    SET_OUTPUT(POW_HKF,0)
#define CLOSE_RADAR()   SET_OUTPUT(POW_HKF,1)


/*
	V1.0.1
		解决iot升级回复发包异常问题
		解决双击初始化 成功将上报心跳
		解决双击初始化 外盖状态写反了的问题
		添加水浸上报
		配合采集平台联调网络协议 完成心跳上报 开锁 参数配置
		添加初始化 更新基角度
		完成drv_ctrl设备返回的事件上报处理

	V1.0.2
		气体电源控制引脚 反一下，拉低供电 拉高关电
		修复锁体引脚耗电，降低功耗
		
	V1.0.3
		修复蓝牙无法开锁问题
		
	V1.0.4
		改为长按不放（长按直到滴滴两声）初始化雷达和获取基角度
		修复心跳不上报长时间开锁告警规则
		单击 激活按钮，打开气体电源
		保持唤醒时间可用生产工具配置
		
	V1.0.5
		去掉所有外设功能，关闭周期性告警，成为数据模拟器
		
		
*/

//#define TEST_MODE		//测试模式将关闭看门狗
//#define CHANGE_BLE_BAUD_FROM_9600_TO_19200			//将用过的9600波特率的蓝牙模块 配置为19200波特率

#define BATTERY_VOLTAGE 3700
#define SOFTWARE_VER "V1.0.5"

#define HARDWARE_VER ef_get_env("hard_ver")						// 必须 "V%d.%d" 格式
#define VERSION_LEN 16              	//iot升级用到 版本号长度
#define BLE_VERSION_LEN 16       			//蓝牙升级用到


#endif
