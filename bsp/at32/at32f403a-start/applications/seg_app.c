#include "TM1639.h"
#include "seg_app.h"
#include "power_app.h"
#include "rtthread.h"

struct rt_messagequeue seg_mq;
static char seg_mq_pool[8];
void seg_pin_init()
{

}

void seg_show(SHOW_MODE mode, uint16_t num)
{
    seg_msg msg;
    msg.msg_event = SHOW_UPDATE;
    msg.mode=mode;
    msg.num = num;
    rt_mq_send(&seg_mq, &msg, sizeof(seg_msg));
}
void seg_end() {			//关闭数码管显示
    seg_msg msg;
    msg.msg_event = SHOW_END;
    rt_mq_send(&seg_mq, &msg, sizeof(seg_msg));

}

void seg_thread(void *parameter)
{
    rt_err_t result;
    seg_msg msg;
    seg_pin_init();
    for(;;) {

        result = rt_mq_recv(&seg_mq, &msg, sizeof( seg_msg), RT_WAITING_FOREVER);
        rt_pm_request(PM_SLEEP_MODE_NONE);
        if(msg.msg_event==SHOW_UPDATE)
        {

            if(msg.mode==MODE_TEMP)				//温度显示
            {
                rt_kprintf("seg show temp: %d\n",msg.num);
                display(msg.num);

            }
            else if(msg.mode==MODE_HUMIDITY)	//湿度显示
            {
                rt_kprintf("seg show humidity: %d\n",msg.num);
                display(msg.num);
            }

        }
        else
        {
            rt_kprintf("seg show end\n");
        }
        rt_pm_release(PM_SLEEP_MODE_NONE);
    }
}

int seg_app_init(void)
{
    rt_mq_init(&seg_mq, "seg_mq",
               &seg_mq_pool[0],           /* 内存池指向msg_pool 867725032490116 */
               sizeof(seg_msg),            /* 每个消息的大小是 128 - void* */
               sizeof(seg_mq_pool),       /* 内存池的大小是msg_pool的大小 */
               RT_IPC_FLAG_FIFO);              /* 如果有多个线程等待，按照FIFO的方法分配消息 */
    rt_thread_t pro_thread = NULL;
    pro_thread = rt_thread_create("seg_app", seg_thread, NULL,128*3, 17, 100); //延迟启动 设备时间
    if (pro_thread != NULL)
    {
        rt_thread_startup(pro_thread);
    }
    else
    {
        rt_kprintf("create seg_thread is faild .\n");
    }

    return 0;
}


void seg_enter_lp(void)
{
    seg_end();
}
void seg_exit_lp(void)
{

}

static void cmd_seg(uint8_t argc, char **argv)
{
    display(atol(argv[1]));
}

MSH_CMD_EXPORT(cmd_seg, clear blue ouput);

