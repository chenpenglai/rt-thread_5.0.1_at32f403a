#include "string.h"
#include "stdio.h"
#include "global_define.h"
#include "CO_app.h"
#include "drv_ctrl.h"
#include "Tool.h"
#include "power_app.h"
//当测量浓度大于设定浓度时，TTL IO口输出低电平
SUART_CTR_STR suart_ctr;

#define co_acq_period_second  5  //CO的采集周期
static sys_tick_t co_period_chk_cnt = 0;
int co_thre=0;
int co_state=0;
uint32_t co_warn_state;
int get_co_state() {
    return co_state;

}

int co_handler(void *para)//线程处理函数
{
    SUART_CTR_STR* p_ctr=&suart_ctr;
    uint8_t read_len;
    int res=NO_REPORT;
    if ((tick_had_gone(co_period_chk_cnt) < SEC_TO_TICK(co_acq_period_second)) && (co_period_chk_cnt)) //没到采集时间
    {
        return res;
    }
    co_period_chk_cnt = get_sys_tick();
		
		OPEN_GAS_POWER();
		
		
		
		CLOSE_GAS_POWER();
    return res;
}
int co_ctrl(co_ctrl_cmd cmd, void *arg)
{
    switch (cmd)
    {

    case CTRL_GET_CO_STATE:
    {
        int *tmp = (int *)arg;
        *tmp = get_co_state();
    }
    break;
    default:
        break;
    }
    return RT_EOK;
}
int co_init(void)
{


}

void co_enter_lp(void)
{
//    set_RtPin_had_lp(ULTRASONIC_POW_PIN, 0);    //自动休眠 忽略这个脚
//		SET_OUTPUT(ULTRASONIC_UART_TX_PIN,0);
//		set_RtPin_had_lp(ULTRASONIC_UART_TX_PIN, 0);
    SET_OUTPUT(SUART1_RX_PIN,0);
    set_RtPin_had_lp(SUART1_RX_PIN, 0);
}
void co_exit_lp(void)
{
    //OPEN_ULTRASONIC_POW();
    //key_pin_gpio_init();

}
