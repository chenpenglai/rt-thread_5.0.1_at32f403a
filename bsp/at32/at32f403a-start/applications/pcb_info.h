
#ifndef __PCB_INFO_H__
#define __PCB_INFO_H__

/*通讯模块的引脚*/
#ifdef MODULE_RESET_PIN
#undef MODULE_RESET_PIN
#endif
#define SUPPORT_MODULE_POWER_CTR 0 //是否支持控制通讯模块的供电

#define MODULE_RESET_PIN GET_PIN(A,13)		//使用了下载口 JTMS-SWDIO
#define MODULE_POWER_PIN GET_PIN(A,14)		//模组power_key引脚，使用了下载口 JTMS-SWCLK

#if SUPPORT_MODULE_POWER_CTR
#define MODULE_POWER_CTR_PIN GET_PIN(C,12)//供电控制
#endif

#define PW_WP_PIN GET_PIN(C,15)			//5V供电 拉高供电 拉低关电



//定义使用的外设功能

//#define USING_ADXL362//三轴传感器
//#define USING_DS18B20//温度传感器

//#define USING_SHT30// 温湿度传感器;暂不支持
//#define USING_SHT20 	// 温湿度传感器
//#define USING_SHT3X		// 温湿度传感器;
//#define USING_HTBKLOCK// 井盖锁
//#define USING_WATER_SENSOR//水浸传感器
//#define USING_WATER_PRESSURE_SENSOR//水压传感器；板子上二者有冲突，接的同一插口
//#define USING_FIRE_SENSOR//火浸传感器
#define USING_VOLTAGE		//电池电压采集
//#define USING_RTC_SD3078 //外部rtc芯片
//#define USING_ULTRASONIC_A01//超声波测距模块；串口数据
//#define USING_NC200		//车位传感器
//#define USING_LOG			//使用日志（非串口日志，可用于上报的）
//#define USE_IEC104		//使用iec104协议

//#define USE_GAS_AO			//气体使用模拟量 AO和下面的DO 不能共存
#define USE_GAS_DO			//气体使用开关量


#define SUPP_E104
//#define SUPP_ZLG52810
//#define USING_RADAR					//支持雷达

#define RX4_LD  GET_PIN(C,11)
#define TX4_LD  GET_PIN(C,10)
#define POW_HKF GET_PIN(B,0)        // 雷达供电     拉高：断电      拉低：供电
/****	 雷达选择	 ****/

#ifdef USING_RADAR


#define USING_HKF24BDMP			//使用全颂雷达
//#define USING_USRR196			//使用睿达雷达USRR196
//#define USING_USRR187				//使用睿达雷达USRR187
#endif

//#define	USING_LIGHT								//打开光敏
#ifdef USING_LIGHT
#define USING_LIGHT_IRQ					//使用光敏中断唤醒
#endif

//#define USING_SEG									//使用数码管
//#define USING_BUTTON							//使用按钮功能

//#define USING_LED									//使用LED功能
//#define USING_BEEP								//使用BEEP蜂鸣器
//#define USING_KEEP_LINK					//每30秒发送104测试心跳
//#define USING_O2									//使用氧气传感器

#ifdef USING_ULTRASONIC_A01

#define ULTRASONIC_A01_GPIO_PIN_REMAP

#ifdef ULTRASONIC_A01_GPIO_PIN_REMAP
#define ULTRASONIC_POW_PIN  GET_PIN(B, 0)//供电引脚
#define ULTRASONIC_USE_DEVICE_NAME "suart2"
#define ULTRASONIC_UART_TX_PIN  GET_PIN(B, 1)//通过控制引脚，来启动测量
#endif
#endif


#define adx1362_INT1_PIN GET_PIN(C, 6)//lkk add 2020-4-10
#define adx1362_INT2_PIN GET_PIN(C, 7)//lkk add 2020-4-10
#ifdef USING_ADXL362
/*使用三轴传感器*/

#define ADXL362_GPIO_PIN_REMAP
#ifdef ADXL362_GPIO_PIN_REMAP

#define adx1362_POWER_PIN GET_PIN(B, 2)//lkk add 2020-4-10
#define adx1362_CS_PIN GET_PIN(B, 12)//lkk add 2020-4-10


#define adx1362_INT1_GPIO_Pin  GPIO_PIN_6 //用于睡眠唤醒//运动到静止的中断


#define adx1362_INT2_GPIO_Pin  GPIO_PIN_7 //用于睡眠唤醒//静止到运动的中断

#define ADXL362_CLK_PIN  GET_PIN(B,13)
#define ADXL362_MOSI_PIN	GET_PIN(B,15)//spi正确接法，是主从设备同名管脚相接，
//主MOSI接从MOSI
#define ADXL362_MISO_PIN	GET_PIN(B,14)


#ifdef ADXL362_USE_MCU_SPI
#define adx1362_device_name "spi2"//lkk add 2020-4-10
#else
#define ADXL362_CLK_PIN  GET_PIN(B,13)
#define ADXL362_MOSI_PIN  GET_PIN(B,15)//spi正确接法，是主从设备同名管脚相接，
//主MOSI接从MOSI
#define ADXL362_MISO_PIN  GET_PIN(B,14)
#endif

#endif

#endif

/*定义为W25Q引脚*/

#define W25Q_GPIO_PIN_REMAP
#ifdef W25Q_GPIO_PIN_REMAP

#define W25Q_CS_PORT  GPIOB
#define W25Q_CS_GPIO_PIN  GPIO_PIN_4

#define W25Q_CS_PIN  		GET_PIN(B, 4)
#define W25Q_WP_PIN 		GET_PIN(B, 5)//lkk add 2020-4-13
#define W25Q_HOLD_PIN 	GET_PIN(B, 8)//lkk add 2020-4-13


#define W25Q_SPI_CLK_PIN  	GET_PIN(B, 3)
#define W25Q_SPI_MISO_PIN  		GET_PIN(A, 11)
#define W25Q_SPI_MOSI_PIN  		GET_PIN(A, 12)

#endif



/*定义蓝牙芯片引脚*/
#define BLUE_TOOTH_GPIO_PIN_REMAP

#ifdef BLUE_TOOTH_GPIO_PIN_REMAP
#define BLUE_TOOTH_STA_GPIO_PIN	GPIO_PIN_9//GPIO_PIN_1 
#define BLUE_TOOTH_STA_PORT GPIOC//GPIOA
#define BLUE_TOOTH_STA_EXIT_LINE	EXTI9_5_IRQn//EXTI1_IRQn 

#define BLUE_TOOTH_WAKEUP_PIN   GET_PIN(C, 8)
#define BLUE_TOOTH_STA_PIN   GET_PIN(C, 9) //lkk 2020-4-17 发现该引脚同时在蓝牙头文件定义使用
#define BLUE_TOOTH_MODE_PIN   GET_PIN(A, 15)

#define SUPPORT_BLUE_TOOTH_RESET_PIN RT_FALSE //没使用reset 引脚
#if SUPPORT_BLUE_TOOTH_RESET_PIN
#define BLUE_TOOTH_RESET_PIN   GET_PIN(A, 6)
#endif

#endif


#if defined(USING_DS18B20)||defined(USING_SHT20)||defined(USING_SHT3X)
/*定义温湿度传感器引脚*/
#define THRH_SENSOR_GPIO_PIN_REMAP

#ifdef THRH_SENSOR_GPIO_PIN_REMAP
#define THRH_POWER_ON_PIN GET_PIN(C,3)
#define THRH_POWER_ON_GPIO_PIN GPIO_PIN_3
#define THRH_POWER_ON_PORT GPIOC

#ifdef USING_DS18B20
#define DS18B20_DATA_PIN  GET_PIN(A,0)
#define DS18B20_DATA_GPIO_PIN GPIO_PIN_0
#define DS18B20_DATA_PORT GPIOA
#endif

#ifdef USING_SHT20
#define SHT20_SDA_PORT 			GPIOA
#define SHT20_SDA_GPIO_PIN 	GPIO_PIN_0
#define SHT20_SCL_PORT 			GPIOA
#define SHT20_SCL_GPIO_PIN 	GPIO_PIN_1
#endif
#ifdef USING_SHT3X

#define SHT3X_SUPPORT_RESET_PIN  0//默认不支持reset 引脚
#define SHT3X_SUPPORT_ALERT_PIN  0//默认不支持 alert 引脚

#if SHT3X_SUPPORT_RESET_PIN
#define SHT3X_RESET_PIN GET_PIN(A,2)//当前板子不支持，引脚为随便定的
#endif

#if SHT3X_SUPPORT_ALERT_PIN
#define SHT3X_ALERT_PIN GET_PIN(C,1)//当前板子不支持，引脚为随便定的
#define SHT3X_ALERT_PORT 			GPIOC
#define SHT3X_ALERT_GPIO_PIN 	GPIO_PIN_1
#endif


#define SHT3X_SDA_PORT 			GPIOA
#define SHT3X_SDA_GPIO_PIN 	GPIO_PIN_0
#define SHT3X_SCL_PORT 			GPIOA
#define SHT3X_SCL_GPIO_PIN 	GPIO_PIN_1
#endif
#endif
#endif


#ifdef USING_FIRE_SENSOR
/*定义火浸传感器引脚*/

#define FIRE_SENSOR_GPIO_PIN_REMAP
#ifdef FIRE_SENSOR_GPIO_PIN_REMAP

#define FIRE_SENSOR_PIN  GET_PIN(C,4)
#define FIRE_SENSOR_PORT  GPIOC
#define FIRE_SENSOR_GPIO_PIN  GPIO_PIN_4//使用了该中断源
#endif

#endif


#ifdef USING_WATER_SENSOR
/*定义水浸传感器引脚*/

#define WATER_SENSOR_GPIO_PIN_REMAP
#ifdef WATER_SENSOR_GPIO_PIN_REMAP
#define WATER_SENSOR_PIN  GET_PIN(A,4)
#define WATER_SENSOR_PORT  GPIOA
#define WATER_SENSOR_GPIO_PIN  GPIO_PIN_4
#endif

#endif


#ifdef USING_WATER_PRESSURE_SENSOR
/*定义水压传感器引脚*/

#define WATER_PRESSURE_GPIO_PIN_REMAP
#ifdef WATER_PRESSURE_GPIO_PIN_REMAP
#define WATER_PRESSURE_WP_PIN  GET_PIN(B,0)//水压预热供电使能管脚
#define WATER_PRESSURE_AD_PIN  GET_PIN(B,1)//水压adc采集引脚


#define WATER_PRESSURE_ADC_CHANNEL 9    //电池电压adc采样通道
#define WATER_PRESSURE_ADC_DEVICE_NAME "adc1"

//XXX_hal_msp.c 中需要对应修改外设的初始化
#define WATER_PRESSURE_ADC_Instance     ADC1
#define WATER_PRESSURE_ADC_RCC_EN() 			__HAL_RCC_ADC1_CLK_ENABLE()

#define WATER_PRESSURE_GPIO_RCC_EN() 			__HAL_RCC_GPIOB_CLK_ENABLE()
#define WATER_PRESSURE_ADC_GPIO_PIN  GPIO_PIN_1 //adc采样引脚
#define WATER_PRESSURE_ADC_PORT  GPIOB //adc采样引脚


#endif
#endif


#define AD_LIGHT GET_PIN(A,7)


#ifdef USING_VOLTAGE
#define VOLTAGE_ADC_GPIO_PIN_REMAP
#ifdef VOLTAGE_ADC_GPIO_PIN_REMAP
#define VOLTAGE_UP_ADC_PIN  GET_PIN(B,1)//adc采样引脚
//#define VOLTAGE_DO_ADC_PIN  GET_PIN(A,5)//主要是接地用于adc采样

#define VOLTAGE_UP_ADC_CHANNEL 9    //电池电压adc采样通道
#define VOLTAGE_UP_ADC_DEVICE_NAME "adc1"

//XXX_hal_msp.c 中需要对应修改外设的初始化

#define VOLTAGE_UP_ADC_Instance     ADC1
#define VOLTAGE_UP_ADC_RCC_EN() 			__HAL_RCC_ADC1_CLK_ENABLE()
#define VOLTAGE_UP_GPIO_RCC_EN() 			__HAL_RCC_GPIOB_CLK_ENABLE()
#define VOLTAGE_UP_ADC_GPIO_PIN  GPIO_PIN_1 //adc采样引脚
#define VOLTAGE_UP_ADC_PORT  GPIOB //adc采样引脚

//		#define VOLTAGE_DO_GPIO_RCC_EN() 			__HAL_RCC_GPIOA_CLK_ENABLE()
//		#define VOLTAGE_DO_ADC_GPIO_PIN  GPIO_PIN_5 //用于接地的引脚
//		#define VOLTAGE_DO_ADC_PORT  GPIOA //用于接地的引脚
#endif
#endif

#endif


