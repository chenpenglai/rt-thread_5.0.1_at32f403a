
#ifndef __BUTTON_APP_H__
#define __BUTTON_APP_H__


#include <rtthread.h>
#include <at.h>
#include <board.h>
#include "stdbool.h"
#include "global_define.h"


#define WAKEUP_KEY_PIN 			GET_PIN(A, 8)		//激活按钮

#define VMD_STA_KEY_PIN 		GET_PIN(A, 5)		//移动监测

#define CO_STA_KEY_PIN 			GET_PIN(C, 12)	//一氧化碳监测，低电平0：一氧化碳过高；高电平1：一氧化碳正常
#define H2S_STA_KEY_PIN 		GET_PIN(B, 6)		//硫化氢 		注意：该引脚和 NC200 的 STATE_PIN 引脚冲突
#define SO2_STA_KEY_PIN			GET_PIN(B, 7)		//二氧化硫  注意：该引脚和 NC200 的 RESET_PIN 引脚冲突

void button_enter_lp(void);
void button_exit_lp(void);
int button_app_init(void);

#endif


