#include "beep_dev.h"
#include "power_app.h"
#include "rtthread.h"

#ifdef USING_BEEP
struct rt_messagequeue beep_mq;
static char beep_mq_pool[8];
void beep_pin_init()
{
   BEEP_OFF();
}

beep_loop beep_loop_array[] =
{
    {4, { {ON, 500}, {OFF, 500}, {ON, 500}, {OFF, 500} }},    //slow_flash	used for button
    {4, { {ON, 100}, {OFF, 200}, {ON, 100}, {OFF, 200} }},    //fast_flash  used for upgrade
    {4, { {ON, 50}, {OFF, 100}, {ON, 150}, {OFF, 200} }},			//
		{2, { {ON, 50}, {OFF, 100}},}															//执行一次叫一声
};
 
void beep_handler(uint8_t mode, uint16_t loop)
{
    if (mode > sizeof(beep_loop_array) / sizeof(beep_loop) - 1)
    {
        rt_kprintf("beep mode error\n");
        return;
    }
		
		uint8_t count=beep_loop_array[mode].count;
    if (0 != count)
    {
				int i = 0;
        for(;;){
						beep_pin_init();
						if (i >= count)
						{
								i = 0;
								--loop;
								if (loop <= 0)
								{
										BEEP_OFF();
										break;
								}
						}
						if (ON == beep_loop_array[mode].effect[i].status)
						{
								BEEP_ON();
								//rt_kprintf("on\n");
						}
						else
						{
								BEEP_OFF();
								//rt_kprintf("off\n");
						}
						if (++i < count || loop > 0)
						{
								rt_thread_delay(rt_tick_from_millisecond(beep_loop_array[mode].effect[i - 1].interval_time));
						}
        }
    }
}

void beep_start(uint8_t mode, uint16_t loop_num)
{
		beep_msg msg;
		msg.msg_event = BEEP_START;//NET_SEND_DATA;
		msg.mode=mode;
		msg.num = loop_num;
		rt_mq_send(&beep_mq, &msg, sizeof( beep_msg));
}
void beep_end(){
		beep_msg msg;
		msg.msg_event = BEEP_END;//NET_SEND_DATA;
		rt_mq_send(&beep_mq, &msg, sizeof( beep_msg));
	
}


void beep_thread(void *parameter)
{
		rt_err_t result;
		beep_msg msg;
		beep_pin_init();
		for(;;){
				
				result = rt_mq_recv(&beep_mq, &msg, sizeof( beep_msg), RT_WAITING_FOREVER);		
				rt_pm_request(PM_SLEEP_MODE_NONE);
				if(msg.msg_event==BEEP_START)
				{
					rt_kprintf("beep start: %d, %d\n",msg.mode,msg.num);
					beep_handler(msg.mode,msg.num);
				}
				else
				{
					rt_kprintf("beep end\n");
				}
				BEEP_OFF();
				rt_pm_release(PM_SLEEP_MODE_NONE);
		}
}

int beep_app_init(void)
{
	    rt_mq_init(&beep_mq, "beep_mq",
               &beep_mq_pool[0],           /* 内存池指向msg_pool 867725032490116 */
               sizeof(beep_msg),            /* 每个消息的大小是 128 - void* */
               sizeof(beep_mq_pool),       /* 内存池的大小是msg_pool的大小 */
               RT_IPC_FLAG_FIFO);              /* 如果有多个线程等待，按照FIFO的方法分配消息 */
    rt_thread_t pro_thread = NULL;
    pro_thread = rt_thread_create("beep_app", beep_thread, NULL,128*2, 11, 100); //延迟启动 设备时间
    if (pro_thread != NULL)
    {
        rt_thread_startup(pro_thread);
    }
    else
    {
        rt_kprintf("create beep_thread is faild .\n");
			return 0;
    }

		return 1;
}

void beep_enter_lp(void)
{
	BEEP_OFF();
	set_RtPin_had_lp(BEEP_PIN,0);		//自动休眠 忽略这个脚
}
void beep_exit_lp(void)
{
	beep_pin_init();
}

static void cmd_beep_start(int argc, char **argv)
{
	beep_start(atol(argv[1]),atol(argv[2]));
}
MSH_CMD_EXPORT(cmd_beep_start,hhh);

static void cmd_beep_end(int argc, char **argv)
{
	beep_end();
}
MSH_CMD_EXPORT(cmd_beep_end,hhh);
#endif


