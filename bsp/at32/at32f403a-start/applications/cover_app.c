#include "string.h"
#include "stdio.h"
#include "global_define.h"
#include "drv_ctrl.h"
#include "Tool.h"
#include "power_app.h"
#include "cover_app.h"
#include "radar_app.h"
#include "light_sens.h"
#include "easyflash.h"
#include "beep_dev.h"
#include "net_app.h"

//处理外盖状态传感器（光敏和雷达）， 得出外盖最终状态

rt_event_t cover_event = RT_NULL;
static uint8_t cover_state=1;
static int16_t radar_val=0;
static uint16_t light_val=0;
int16_t get_radar_val() {
    return radar_val;
}

uint8_t get_cover_state() {
    rt_kprintf("cover_state: %d\n",cover_state);
    return cover_state;
}
MSH_CMD_EXPORT(get_cover_state, hello);

#define radar_valid(x) ((RADAR_ERR_VAL != x) && (RADAR_OVERLAY_VAL != x))                //雷达有效值

void cover_thread(void *parameter)
{

    uint32_t e=0;
    uint8_t tmp_cover_state=0;
    int16_t tmp_light_val=0;
    cover_state=ef_get_env_int("cover_state");
    while(1) {
        if (RT_EOK == rt_event_recv(cover_event, COVER_EVENT_INIT_RADAR|COVER_EVENT_CHECK_SENS, RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR, rt_tick_from_millisecond(100), &e))
        {
            if(!get_net_connect_flag())			//连接过程中，读取雷达将导致连接失败
                continue;
            if(e==COVER_EVENT_INIT_RADAR) {
                rt_pm_request(PM_SLEEP_MODE_NONE);
                //OPEN_GAS_POWER();
                if(radar_calibration()==0) {
                    rt_kprintf("radar init succeed\n");
                    beep_start(3, 1);		//响1声
                    cover_state=1;			//设置为开盖状态
                    ef_set_env_int("cover_state",cover_state);
                    int8_t angle;
                    adxl_get_angle_val(&angle);	//取当前角度为基角度
                    ef_set_env_int("base_angle_val",angle);
                    //rt_kprintf("set base_angle_val %d\n",angle);
                    ef_save_env();
                    net_send_event(SEND_MSG_DEV_HEARTBEAT);
                }
                else {
                    rt_kprintf("雷达初始化失败\n");
                    beep_start(3, 3);		//响3声
                }
                //net_send_event(SEND_MSG_DEV_HEARTBEAT);
                rt_pm_release(PM_SLEEP_MODE_NONE);

            }
            if(e==COVER_EVENT_CHECK_SENS) {
                tmp_light_val=light_get_val();					//获取光敏值
                if(abs(light_val-tmp_light_val)<=LIGHT_STABLE_RANGE) {	//光敏值变化很小
                    rt_kprintf("light_val change too small: %d, skip it\n",abs(light_val-tmp_light_val));
                    light_val=tmp_light_val;
                    continue;
                }

                rt_pm_request(PM_SLEEP_MODE_NONE);
                for(int i=0; i<3; i++) {
                    radar_val= radar_get_value();				//获取雷达距离3次
                    rt_kprintf("---> read radar: %d\n",radar_val);
                    if(radar_valid(radar_val)) {
                        break;
                    }
                }

                /*****		判断状态		*****/
                if((radar_valid(radar_val)))				//雷达值有效
                {
                    if((radar_val<RADAR_SPACE_VAL)&&(tmp_light_val>=LIGHT_CLOSE_COVER_VAL))			//雷达读取到关 && 光敏读取到关
                        tmp_cover_state=0;				//0：外盖关
                }
                else
                {
                    tmp_cover_state=1;				//1：外盖开 默认状态
                }
            }
            else				//雷达值无效，仅通过光敏
            {
                rt_kprintf("radar err, only light\n");
                if(tmp_light_val>=LIGHT_CLOSE_COVER_VAL)
                {
                    tmp_cover_state=0;			//0：外盖关
                }
                else
                {
                    tmp_cover_state=1;			//1：外盖开 默认状态
                }
            }
            if(tmp_cover_state!=cover_state)			//外盖状态改变了，就发送心跳
            {
                cover_state=tmp_cover_state;
                ef_set_env_int("cover_state",cover_state);
                ef_save_env();
                net_send_event(SEND_MSG_DEV_HEARTBEAT);
            }
            light_val=tmp_light_val;
            rt_kprintf(">>> radar: %d, light: %d, cover_state: %d\n",radar_val,light_val,cover_state);
            rt_pm_release(PM_SLEEP_MODE_NONE);
        }
    }
}
int cover_init(void)
{

    cover_event = rt_event_create("cover_event", RT_IPC_FLAG_FIFO);
    if (cover_event == RT_NULL)
    {
        rt_kprintf("cover_event create failed\n");
        return 0;
    }
    rt_thread_t pro_thread = NULL;
    pro_thread = rt_thread_create("cover_app", cover_thread, NULL,450, 21, 3000); //延迟启动 设备时间
    if (pro_thread != NULL)
    {
        rt_thread_startup(pro_thread);
    }
    else
    {
        rt_kprintf("create pro_deal_thread is faild .\n");
    }

}

