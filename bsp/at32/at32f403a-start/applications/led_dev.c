#include "led_dev.h"
#include "power_app.h"
#include "rtthread.h"


struct rt_messagequeue led_mq;
static char led_mq_pool[8];
void led_pin_init()
{
   LED_OFF();
}

led_loop led_loop_array[] =
{
    {4, { {ON, 500}, {OFF, 500}, {ON, 500}, {OFF, 500} }},    //slow_flash	used for button
    {4, { {ON, 100}, {OFF, 200}, {ON, 100}, {OFF, 200} }},    //fast_flash  used for upgrade
    {4, { {ON, 50}, {OFF, 100}, {ON, 150}, {OFF, 200} }},			//
};
 
void led_handler(uint8_t mode, uint16_t loop)
{
    if (mode > sizeof(led_loop_array) / sizeof(led_loop) - 1)
    {
        rt_kprintf("led mode error\n");
        return;
    }
		
		uint8_t count=led_loop_array[mode].count;
    if (0 != count)
    {
				int i = 0;
        for(;;){
						led_pin_init();
						if (i >= count)
						{
								i = 0;
								--loop;
								if (loop <= 0)
								{
										LED_OFF();
										break;
								}
						}
						if (ON == led_loop_array[mode].effect[i].status)
						{
								LED_ON();
								rt_kprintf("on\n");
						}
						else
						{
								LED_OFF();
								rt_kprintf("off\n");
						}
						if (++i < count || loop > 0)
						{
								rt_thread_delay(rt_tick_from_millisecond(led_loop_array[mode].effect[i - 1].interval_time));
						}
        }
    }
}

void led_start(uint8_t mode, uint16_t loop_num)
{
		led_msg msg;
		msg.msg_event = LED_START;//NET_SEND_DATA;
		msg.mode=mode;
		msg.num = loop_num;
		rt_mq_send(&led_mq, &msg, sizeof( led_msg));
}
void led_end(){
		led_msg msg;
		msg.msg_event = LED_END;//NET_SEND_DATA;
		rt_mq_send(&led_mq, &msg, sizeof( led_msg));
	
}


void led_thread(void *parameter)
{
		rt_err_t result;
		led_msg msg;
		led_pin_init();
		for(;;){
				
				result = rt_mq_recv(&led_mq, &msg, sizeof( led_msg), RT_WAITING_FOREVER);		
				rt_pm_request(PM_SLEEP_MODE_NONE);
				if(msg.msg_event==LED_START)
				{
					rt_kprintf("led start: %d, %d\n",msg.mode,msg.num);
					led_handler(msg.mode,msg.num);
				}
				else{
					rt_kprintf("led end\n");
				}	
				LED_OFF();
				rt_pm_release(PM_SLEEP_MODE_NONE);
		}
}

int led_app_init(void)
{
	    rt_mq_init(&led_mq, "led_mq",
               &led_mq_pool[0],           /* 内存池指向msg_pool 867725032490116 */
               sizeof(led_msg),            /* 每个消息的大小是 128 - void* */
               sizeof(led_mq_pool),       /* 内存池的大小是msg_pool的大小 */
               RT_IPC_FLAG_FIFO);              /* 如果有多个线程等待，按照FIFO的方法分配消息 */
    rt_thread_t pro_thread = NULL;
    pro_thread = rt_thread_create("led_app", led_thread, NULL,128*3, 16, 100); //延迟启动 设备时间
    if (pro_thread != NULL)
    {
        rt_thread_startup(pro_thread);
    }
    else
    {
        rt_kprintf("create led_thread is faild .\n");
    }

		return 0;
}

void led_enter_lp(void)
{

	LED_OFF();
	set_RtPin_had_lp(LED_PIN,0);		//自动休眠 忽略这个脚

}
void led_exit_lp(void)
{

	led_pin_init();

}


static void cmd_led_start(int argc, char **argv)
{
	led_start(atol(argv[1]),atol(argv[2]));
}

MSH_CMD_EXPORT(cmd_led_start,hhh);

static void cmd_led_end(int argc, char **argv)
{
	led_end();
}

MSH_CMD_EXPORT(cmd_led_end,hhh);



