#ifndef __SEG_DEV_H_
#define __SEG_DEV_H_
#include <rtthread.h>
#include <at.h>
#include <board.h>
#include "stdbool.h"
#include "stdint.h"
#include "global_define.h"

typedef enum
{
	SHOW_UPDATE=0,		//ˢ��
	SHOW_END,			//ֹͣ

}SEG_MSG_EVENT;

typedef enum
{
	MODE_TEMP=0,			//�¶�
	MODE_HUMIDITY,			//ʪ��

}SHOW_MODE;

typedef struct _seg_msg
{
    SEG_MSG_EVENT msg_event;
	  SHOW_MODE mode;
		uint16_t num;
}seg_msg;
int seg_app_init(void);
void seg_show(SHOW_MODE mode, uint16_t num);
void seg_end();
void seg_exit_lp(void);
void seg_enter_lp(void);
#endif
