#ifndef __radar_app_H
#define __radar_app_H

#include "stdbool.h"
#include "stm32l1xx_hal.h"
#include <rtthread.h>
#include "pcb_info.h"
extern struct rt_semaphore radar_lock;

typedef enum
{
    HKF24BDMP_RADAR = 0,
		USRR196_RADAR,
		USRR187_RADAR,
} radar_type;



int radar_calibration(void);
int radar_get_value(void);
void set_radar(radar_type radar);
radar_type get_radar();
void radar_enter_lp();
void radar_exit_lp();
#endif
