#ifndef __BEEP_DEV_H_
#define __BEEP_DEV_H_
#include <rtthread.h>
#include <at.h>
#include <board.h>
#include "stdbool.h"
#include "stdint.h"
#include "global_define.h"

#define ON  1
#define OFF 0

#define BEEP_PIN     GET_PIN(B,9)
#define BEEP_ON() 			SET_OUTPUT(BEEP_PIN, 1)			//����
#define BEEP_OFF()			SET_OUTPUT(BEEP_PIN, 0)			//����

typedef struct _beep_effect{
	uint8_t status;
	uint16_t interval_time;
}beep_effect;

typedef struct _beep_loop{
	uint8_t count;
	beep_effect effect[5];
}beep_loop;

typedef enum
{
		BEEP_START=0,		//��ʼ��˸
		BEEP_END,			//ֹͣ��˸

}BEEP_MSG_EVENT;
typedef struct _beep_msg
{
    BEEP_MSG_EVENT msg_event;
	  uint8_t mode;
		uint16_t num;
}beep_msg;

void beep_handler(uint8_t select,uint16_t loop_num);				
int beep_app_init();

/********   �ⲿ����   ***********/
void beep_start(uint8_t select,uint16_t loop_num);
void beep_end();
void beep_exit_lp(void);
void beep_enter_lp(void);
#endif
