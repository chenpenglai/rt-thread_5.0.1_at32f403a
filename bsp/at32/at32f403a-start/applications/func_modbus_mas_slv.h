#ifndef __FUNC_MODBUS_MASTER_H__
#define __FUNC_MODBUS_MASTER_H__
#include "rtthread.h"
#include "stdio.h"
#include "global.h"
#include "modbus.h"
#ifdef USE_BRK
	#define MODBUS_SLV_HR_NB (780)			//MODBUS HR个数（协议2.5 = 274） （协议 V2.7 新增设备 350）（协议 V2.8 400）
#else
	#define MODBUS_SLV_HR_NB (500)			//MODBUS HR个数（协议2.5 = 274） （协议 V2.7 新增设备 350）（协议 V2.8 400）

#endif


#define MODBUS_SLV_DFT 10				//默认地址
#define MODBUS_SLV_SEND_DELAY 10		//发送前延时10mS
#define MODBUS_SLV_COIL_OFS 0			//线圈偏移量
#define MODBUS_SLV_INPUT_OFS 0			//输入偏移量

extern unsigned short HR1[MODBUS_SLV_HR_NB];		//V2.5修改
extern unsigned short HR[MODBUS_SLV_HR_NB];

typedef struct{
	unsigned char slv;
	unsigned short hr_n;		//HR数量
	unsigned short *phr;		//HR指针
}FUNC_MODBUS_MASTER_SLV_HR_T;


#define __FUNC_MODBUS_MASTER_DEFINE


typedef enum __dev_type{
	no_dev=-1,
	relay_dev=0,		//必须对应 master0_tb_slv_hr 数组下标
	dimmer_dev=1,
	srelay_dev=2,
	radar_dev=3,
	relay_16_dev=4,					//协议 V2.7 新增设备
	curtain_dev=5,					//协议 V2.9.4 新增设备
	scr_dev=6,							//可控硅调光
	scene_panel_dev=7,			//场景面板
	
	brk_1p_dev=8,		//（作为单相断路器）1P断路器 必须对应 master0_tb_slv_hr 数组下标
//	brk_2p_dev=9,		//2P断路器 必须对应 master0_tb_slv_hr 数组下标
	brk_3p_dev=9,		//（作为三相断路器）3P断路器 必须对应 master0_tb_slv_hr 数组下标
//	brk_4p_dev=11,		//4P断路器 必须对应 master0_tb_slv_hr 数组下标
	srelay_16_dev=10,		//10=a
	
	protect_dev=11,			//11=b

	max_dev_type_num,				//设备类型 总数
}_dev_type;	

 //指定设备的HR数量，读设备所有HR时需要，注意：必须小于 MODBUS_SLV_HR_NB
#define RELAY_ALL_HR_NUM 					323		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define DIMMER_ALL_HR_NUM 				28		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define SRELAY_ALL_HR_NUM 				323		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define RADAR_ALL_HR_NUM 					23		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define RELAY_16_ALL_HR_NUM 			399		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define CURTAIN_ALL_HR_NUM   			334		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define SCR_ALL_HR_NUM   					57		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报
#define SCENE_PANEL_ALL_HR_NUM 		234		//场景面板

//指定设备的HR数量，读设备所有HR时需要，注意：必须小于 MODBUS_SLV_HR_NB
#define BRK_1P_ALL_HR_NUM 			158//注意：到 BRK_1P_HR_AFDD_LEARN_HOUR 为止		//必须小于 MODBUS_SLV_HR_NB
//#define BRK_2P_ALL_HR_NUM 			134//BRK_2P_HR_SIZE
#define BRK_3P_ALL_HR_NUM 			158//BRK_3P_HR_SIZE
//#define BRK_4P_ALL_HR_NUM 			134//BRK_4P_HR_SIZE

#define SRELAY_16_ALL_HR_NUM 			399		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报


#define PROTECT_ALL_HR_NUM 				274		//模块自身的场景往后的HR太多了，不算进来，不然轮询读取HR太多，太耗时，场景也用不着轮询上报


#ifdef __FUNC_MODBUS_MASTER_DEFINE
//+++++User_Code_define Begin+++++//
	#ifdef FUNC_MODBUS_MASTER_MAIN
		//从机HR对照表，一主多从的时候定义每个从机对应的HR
		
		//是否可以全部使用 HR[0-253] 的空间？
		FUNC_MODBUS_MASTER_SLV_HR_T master0_tb_slv_hr[]={
			//从机地址，				HR数量，HR指针	   
			{254,		MODBUS_SLV_HR_NB,			&HR[0]},			//253 时控（多功能）从机设备，一个HR指针将对应多台时控从机，所以地址将被实时修改，默认都是1，将在使用时填充对应从机
//			{254,		DIMMER_ALL_HR_NUM,		&HR[0]},			//27	调光从机设备
//			{254,		SRELAY_ALL_HR_NUM,		&HR[0]},			//31	继电器
//			{254,		RADAR_ALL_HR_NUM,			&HR[0]},			//22	雷达从机设备   使用通用模块预留的HR空间
//			{254,		RELAY_16_ALL_HR_NUM,	&HR[0]},			//16路时控（多功能）从机设备，
//			{254,		PROTECT_ALL_HR_NUM,		&HR[0]},			//产品保护   使用通用模块预留的HR空间
		
			//{SLV_TRAN_ADDR,			32,		&HR[253+27+22]},	//透传器
			
		};
//		const FUNC_MODBUS_MASTER_SLV_HR_T master1_tb_slv_hr[]={
//			{10,100,HR},	//从机地址，HR数量，HR指针
//			{0,0,0}
//		};
	#else
		extern const FUNC_MODBUS_MASTER_SLV_HR_T master0_tb_slv_hr[];
		//extern const FUNC_MODBUS_MASTER_SLV_HR_T master1_tb_slv_hr[];
	#endif
	#define FUNC_MODBUS_MASTER_CHKBUS_DELAY 10	//判断总线空闲延时10mS-20mS
	#define FUNC_MODBUS_MASTER_SEND_DELAY 10	//发送前延时10mS
	#define FUNC_MODBUS_MASTER_SEND_PERIOD 20	//发送周期100ms,0表示连续发送
	#define FUNC_MODBUS_MASTER_SEND_TOUT 1000	//发送超时1000mS
	#define FUNC_MODBUS_MASTER_SEND_RTY 1		//重发1次
	#define FUNC_MODBUS_MASTER_CYC_NB 8			//循环命令队列长度
	#define FUNC_MODBUS_MASTER_INJ_NB 16			//注入命令队列长度

//-----User_Code_define End-----//
//=============================================================================
#endif
//使用主模块需要对以下内容进行定义	
#ifndef __FUNC_MODBUS_MASTER_DEFINE
	#ifdef FUNC_MODBUS_MASTER_MAIN
		//从机HR对照表，一主多从的时候定义每个从机对应的HR
		const FUNC_MODBUS_MASTER_SLV_HR_T master0_tb_slv_hr[]={
			{10,100,HR},	//从机地址，HR数量，HR指针
			{0,0,0}
		};
		const FUNC_MODBUS_MASTER_SLV_HR_T master1_tb_slv_hr[]={
			{10,100,HR},	//从机地址，HR数量，HR指针
			{0,0,0}
		};
	#else
		extern const FUNC_MODBUS_MASTER_SLV_HR_T master0_tb_slv_hr[];
		extern const FUNC_MODBUS_MASTER_SLV_HR_T master1_tb_slv_hr[];
	#endif
	#define FUNC_MODBUS_MASTER_CHKBUS_DELAY 10	//判断总线空闲延时10mS-20mS
	#define FUNC_MODBUS_MASTER_SEND_DELAY 10	//发送前延时10mS
	#define FUNC_MODBUS_MASTER_SEND_PERIOD 100	//发送周期100ms,0表示连续发送
	#define FUNC_MODBUS_MASTER_SEND_TOUT 1000	//发送超时100mS
	#define FUNC_MODBUS_MASTER_SEND_RTY 10		//重发1次
	#define FUNC_MODBUS_MASTER_CYC_NB 4			//循环命令队列长度
	#define FUNC_MODBUS_MASTER_INJ_NB 2			//注入命令队列长度
#endif
//=============================================================================
	
	
	#ifdef FUNC_MODBUS_MASTER_MAIN
		#define FUNC_MODBUS_MASTER_EXT
	#else
		#define FUNC_MODBUS_MASTER_EXT extern
	#endif
	
//	typedef struct{
//		unsigned short send_tout;
//		unsigned char send_rty;
//		unsigned char send_delay;
//		unsigned char func_cyc_nb;
//		unsigned char func_inj_nb;
//	}FUNC_MODBUS_MASTER_INIT_T;	

	//typedef void (*mf_func)(void);
				
	typedef struct{
		unsigned char slv;
		unsigned char func;
		unsigned short da_adr;
		unsigned short da_n;
	#ifdef MODBUS_RW_EN
		unsigned short rww_adr;
		unsigned short rww_n;
	#endif
		//mf_func mff;
	}FUNC_MODBUS_MASTER_FUNC_T;

	typedef struct{
		unsigned char step;
		unsigned char send_delay;		//发送前延时
		unsigned char send_rty_nb;		//重发次数
		unsigned char send_rty_cnt;		//重发计数
		unsigned char func_cyc_nb;		//循环指令条数
		unsigned char func_cyc_cur;		//当前循环指令
		unsigned char func_inj_h;		//注入指令头
		unsigned char func_inj_t;		//注入指令尾
		unsigned short send_tout;		//发送超时
		unsigned short send_period;		//通讯周期
		unsigned short err_cnt;			//错误计数
		unsigned char cur_func_mode;	//当前操作的是循环指令还是注入指令
		rt_device_t device;//USART_T *pu;					//串口指针
		time_ms_T tm;					//定时器
		time_ms_T resend_tm;					//定时器
		time_ms_T tm_period;					//定时器
		const FUNC_MODBUS_MASTER_SLV_HR_T *tb_slv_hr;	//从机HR对应表
		MODBUS_T md;					//MODBUS结构体
		FUNC_MODBUS_MASTER_FUNC_T func_cyc[FUNC_MODBUS_MASTER_CYC_NB];
		FUNC_MODBUS_MASTER_FUNC_T func_inj[FUNC_MODBUS_MASTER_INJ_NB];
		//uint8_t is_reply_succeed_flag;			//从机是否应答成功标志位
	}FUNC_MODBUS_MASTER_T;
	
	typedef struct{
		int rts_pin;
		int rts_pin_mode;
		int rts_delay_us;
	}func_rts_cfg;
	void cfg_modbus_rts_delay(int us);
	void cfg_modbus_rts(int rts_pin,int rts_pin_mode);
	void set_modbus_send_rts(void);
	void set_modbus_recv_rts(void);
	FUNC_MODBUS_MASTER_EXT FUNC_MODBUS_MASTER_T g_func_modbus_master0;
//	FUNC_MODBUS_MASTER_EXT FUNC_MODBUS_MASTER_T g_func_modbus_master1;		//没用，去掉后节省450字节RAM
	
		FUNC_MODBUS_MASTER_EXT FUNC_MODBUS_MASTER_SLV_HR_T g_func_modbus_master0_slv_hr[32];
	
	//FUNC_MODBUS_MASTER_EXT unsigned short HR[MODBUS_SLV_HR_NB];
	

	
	typedef enum{
		FUNC_MODBUS_MASTER_FUNC_CYC,
		FUNC_MODBUS_MASTER_FUNC_INJ,
	}FUNC_MODBUS_MASTER_FUNC_E;
	_dev_type get_slv_dev_type(uint8_t slv_addr);
	void set_slv_dev_addr(_dev_type dev_type,uint8_t slv_addr);
//	uint16_t get_slv_hr_n(uint8_t slv_addr);
	uint16_t* get_slv_hr_p(uint8_t slv_addr);
	uint16_t get_slv_hr_num(_dev_type slv_type);
//	void set_slv_hr_n(uint8_t slv_addr,uint16_t hr_num);
	//uint16_t set_slv_hr_num(_dev_type slv_type,uint16_t hr_num);
	uint16_t get_master0_tb_slv_hr_size(void);
	unsigned char func_modbus_master_add_func(FUNC_MODBUS_MASTER_T *p,FUNC_MODBUS_MASTER_FUNC_T *pf,FUNC_MODBUS_MASTER_FUNC_E type,FUNC_MODBUS_MASTER_SLV_HR_T *tb_slv_hr);
	void func_modbus_master_clr_func(FUNC_MODBUS_MASTER_T *p,FUNC_MODBUS_MASTER_FUNC_E type);
	
	//写多个寄存器 注入包：从设备地址，功能码，HR起始地址，HR数量，HR要填充的数据
	void add_modbus_master_wr_hr_inj_package(uint8_t slv_addr,uint16_t start_data_addr,uint16_t HR_data_num,uint16_t* HR_data);
	//写多个寄存器 循环包：从设备地址，功能码，HR起始地址，HR数量，HR要填充的数据
	void add_modbus_master_wr_hr_cyc_package(uint8_t slv_addr,uint16_t start_data_addr,uint16_t HR_data_num,uint16_t* HR_data);
	
	//读多个寄存器 注入包：从设备地址，HR起始地址，HR数量
	void add_modbus_master_rd_hr_inj_package(uint8_t slv_addr,uint16_t HR_start_data_addr,uint16_t HR_data_num);
	//读多个寄存器 循环包：从设备地址，HR起始地址，HR数量
	void add_modbus_master_rd_hr_cyc_package(uint8_t slv_addr,uint16_t HR_start_data_addr,uint16_t HR_data_num);
	//extern uint8_t g_slv_addr;
	
	
	/*************			下面是从机的			************/

	
	//+++++User_Code_define Begin+++++//

//-----User_Code_define End-----//
//=============================================================================
//使用主模块需要对以下内容进行定义	
#ifndef __FUNC_DIRTRAN_DEFINE
	
	#define FUNC_DIRTRAN_CHKBUS_DELAY 200	//判断总线空闲延时10mS-20mS
	#define FUNC_DIRTRAN_SEND_DELAY 10	//发送前延时10mS
	#define FUNC_DIRTRAN_SEND_PERIOD 100	//发送周期100ms,0表示连续发送
	#define FUNC_DIRTRAN_SEND_TOUT 100	//发送超时100mS
	#define FUNC_DIRTRAN_SEND_RTY 1		//重发1次
	#define FUNC_DIRTRAN_CYC_NB 4			//循环命令队列长度
	#define FUNC_DIRTRAN_INJ_NB 2			//注入命令队列长度
#endif
//=============================================================================
	#ifdef FUNC_DIRTRAN_MAIN
		#define FUNC_DIRTRAN_EXT
	#else
		#define FUNC_DIRTRAN_EXT extern
	#endif
	
	#define FUNC_DIRTRAN_BUF_SIZE 1024
		
	typedef struct{
		unsigned char step;
		time_ms_T tm;					//定时器
		rt_device_t device;//USART_T *pu;
		unsigned char buf[FUNC_DIRTRAN_BUF_SIZE];
		unsigned short buf_n;
		unsigned short buf_size;
		MODBUS_T md;
	}FUNC_DIRTRAN_PORT;
	
	typedef struct{
		FUNC_DIRTRAN_PORT ch1;
	}FUNC_DIRTRAN_T;
	
	FUNC_DIRTRAN_EXT FUNC_DIRTRAN_T g_func_dirtran;
	
	
	
	void func_modbus_mas_slv_init(FUNC_MODBUS_MASTER_T *p,rt_device_t device,const FUNC_MODBUS_MASTER_SLV_HR_T *tb_slv_hr,FUNC_DIRTRAN_T *p_slv,unsigned char slv);
void func_modbus_mas_slv_exec(FUNC_MODBUS_MASTER_T *p,FUNC_DIRTRAN_T *p_slv);
		void func_modbus_set_step(uint8_t step);
	int clr_modbus_master_func(void);
	int fill_modbus_master_send_buff(uint8_t* buff,int buff_size);
	int slove_modbus_master_recv_buff(uint8_t* recv,int recv_len);
	
#endif
