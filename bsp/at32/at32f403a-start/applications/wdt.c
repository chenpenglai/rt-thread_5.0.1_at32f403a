#include "wdt.h"
#include "rtthread.h"
#include "watchdog.h"
#include "ulog.h"
#include "global.h"

rt_device_t wdt_dev = RT_NULL;
void wdt_free(){
#ifdef USE_WDT
	if(wdt_dev!=RT_NULL){
		rt_device_control(wdt_dev,RT_DEVICE_CTRL_WDT_KEEPALIVE,0);
		//rt_kprintf(".");
	}
#endif

}

//static void wdt_thread(void *param)
//{
//		
//    while (1)
//    {
//				wdt_free();
//				rt_thread_mdelay(10);
//    }

//}

int wdt_init(void){
#ifdef USE_WDT	
		
    wdt_dev = rt_device_find("wdt");
    if (wdt_dev){
				rt_device_open(wdt_dev, RT_DEVICE_FLAG_ACTIVATED);
				rt_device_control(wdt_dev,RT_DEVICE_CTRL_WDT_START,0);
				int time=2;
				rt_device_control(wdt_dev,RT_DEVICE_CTRL_WDT_SET_TIMEOUT,&time);	
			
//			  rt_thread_t tid;
//				tid = rt_thread_create("wdt_th",
//															 wdt_thread, RT_NULL,
//															 256,
//															 12, 25);
//				if (tid != RT_NULL)
//						rt_thread_startup(tid);
				rt_thread_idle_sethook(wdt_free);
				LOG_I("wdt open ok");
		}
    else{
			LOG_E("wdt open err");
            return -RT_ERROR;
		}

#endif
}
INIT_PREV_EXPORT(wdt_init);

