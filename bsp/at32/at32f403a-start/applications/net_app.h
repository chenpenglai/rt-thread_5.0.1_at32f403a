
#ifndef __NET_APP_H__
#define __NET_APP_H__


#include <rtthread.h>
#include <at.h>
#include "at_device.h"
//#include <board.h>
#include "stdbool.h"
#include "global.h"
//#include "AepServiceCodes.h"
//#include "device_env.h"

#define ERR_MSG    -1
#define UP_MSG   0x01
#define DOWN_MSG 0x02
#define ACK_MSG  0x03

#define SEND_KEEP_LINK_PACK (1<<0)
#define KEEP_LINK_TIME (2*60)			//单位秒

#define CHECK_LINK_TM 	(45*1000)			//45秒 检测一次连接状态

#define NET_TRANS_NUM 4		//连接数量

//连接1（业务连接）
extern struct net_trans  	*net1;
#define 			NET1_NAME		"net1"

//连接2（用于下载升级包）
extern struct net_trans  	*net2;
#define 			NET2_NAME		"net2"

//连接3（空闲 没使用）
extern struct net_trans  *net3;
#define 			NET3_NAME		"net3"

//连接4（空闲 没使用）
extern struct net_trans  *net4;
#define 			NET4_NAME		"net4"


#define MSG_CLIENT_OPEN 			(0x01 << 0)			//打开可以网卡
#define MSG_CLIENT_OPEN_SEL 	(0x01 << 1)			//打开指定名字的网卡
#define MSG_CLIENT_CLOSE 			(0x01 << 2)
#define MSG_CLIENT_CONNECT 		(0x01 << 3)
#define MSG_CLIENT_FREE_SOCK 	(0x01 << 4)			//释放当前连接的所有资源（相当于断开 但不重启网卡）
#define MSG_SEND_DATA 				(0x01 << 5)
#define MSG_NET_PRO_ACK 			(0x01 << 6)
#define MSG_WAIT_PACK_MESSAGE (0x01 << 7)
#define MSG_CHECK_LINK 				(0x01 << 8)




struct pro_msg
{
	uint32_t type;
	int msg_tag;
};


/*************          net send event               ***************/
#define NET_EVENT_CONNECT 									(1<<0)
#define NET_EVENT_RECONNECT 								(1<<1)
#define NET_EVENT_IOT_UPGRADE 							(1<<2)

#define NET_EVENT_SEND_HEARTING 						(1<<3)	
#define NET_EVENT_RESEND_HEARTING 					(1<<4)
#define NET_EVENT_HEARTING_REPLY						(1<<5)

#define NET_EVENT_SEND_PARA_SET_REPLY 			(1<<6)
#define NET_EVENT_SEND_PARA_QUERY 					(1<<7)
#define NET_SEND_DATA 											(1<<8)

#define NET_EVENT_CONN_OK              			(1 << 9)
#define NET_EVENT_CONN_FAIL            			(1 << 10)
#define NET_EVENT_RECV_DATA									(1<< 11)


//消息队列种的时间标记，使用枚举
typedef enum
{
		SEND_MSG_DEV_HEARTBEAT=0,					//心跳
	
		SEND_MSG_PARA_SET_REPLY,					//参数配置回复
		SEND_MSG_IOT_UPGRADE,							//iot升级回复


} SEND_MSG_EVENT_EN;

//extern struct send_msg msg;
//#if 0
//#define net_send_event(status)\
//    msg.msg_event = status;\
//    rt_mq_send(&send_mq, &msg, sizeof(struct send_msg))
//#else
//#define net_send_event(status) do\
//{msg.msg_event = status;\
//rt_mq_send(&send_mq, &msg, sizeof(struct send_msg));}\
//while(0)
//#endif

struct send_msg{
	uint32_t type;
	char * malloc_buf;
	uint16_t len;
	bool isAck;
	struct pro_msg (*ackCb)(uint8_t *,uint16_t);
};
#define RETRY_CHECK_LINK_CNT_MAX 3
//#define WAIT_RESP_CNT_MAX 2
struct net_trans{
	
	uint8_t net_step;
	uint8_t open_busy;		//网卡忙碌标志位
	char name[10];
	uint8_t connected;
	char ip[20];
	uint16_t port ;
//	uint8_t wait_resp_cnt;
	uint8_t retry_check_link_cnt;		//连接断开重试次数
	uint8_t auto_sel_md;				//1=自动选择网络模组。0=指定网络模组
//	char net_module_name[10];		//使用哪个网络模组（NET_4G_DEIVCE_NAME / NET_WIFI_DEIVCE_NAME / NET_ETH_DEIVCE_NAME）
	int retryNums;
	int retryTimeout;
	struct pro_msg (*urcCb)(uint8_t *,uint16_t);
	struct pro_msg (*ackCb)(uint8_t *,uint16_t);
	int (*successdCb)(int);
  int (*faildCb)(int);
	int   sock_fd;
	char *malloc_buf;
	rt_thread_t recv_thread;
	rt_thread_t send_thread;
	rt_mq_t s_mq;
	rt_event_t recv_event ;
	uint8_t* recv_data;
	
};



extern uint32_t connect_type;
int net_app_init(void);
void keep_link_timer_stop() ;
struct net_trans * net_new(char * name);


int net_open(struct net_trans * thiz);		//打开可用网卡（用于自动选择）
int net_open_sel_module(struct net_trans * thiz,char* net_module_name);//打开指定网卡
bool net_open_finish(struct net_trans * thiz);		//打开流程结束

int net_close(struct net_trans * thiz);
int net_connect(struct net_trans * thiz,char* ip,uint16_t port);  //执行连接
int net_free_sock(struct net_trans * thiz);					//正常释放连接，不重启网卡
int net_connected_state(struct net_trans * thiz);	//获取TCP的连接状态
int net_check_link(struct net_trans * thiz);
int net_set_recv_cb(struct net_trans * thiz,struct pro_msg (*urcCb)(uint8_t *,uint16_t)); 		//设置接收回调函数
int net_send_by_ackcb(struct net_trans * thiz,char*ip,uint16_t port,uint8_t *data,int len,struct pro_msg (*ackCb)(uint8_t *,uint16_t));
//int net1_send_by_ackCb(uint8_t *data,int len,struct pro_msg (*ackCb)(uint8_t *,uint16_t));
//int net2_send_by_ackCb(uint8_t *data,int len,struct pro_msg (*ackCb)(uint8_t *,uint16_t));

#endif


