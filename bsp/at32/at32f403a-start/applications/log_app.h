#ifndef __LOG_APP_H
#define __LOG_APP_H
#include "stdint.h"
#include "global_define.h"

//日志系统参数
typedef struct __log_para{
	uint8_t log_write_addr[4];
	uint8_t log_first_flag;
	uint8_t log_num[2];
	uint8_t read_cursor[2];			//日志读取游标
}_log_para;


/*
日志内容：
	时间戳			4字节
	水位				2字节
	告警标志		2字节
*/
#define LOG_SINGLE_SIZE			8					//一条日志占用（8字节）
#define LOG_MAX_NUM					15000				//日志最大保存15000条		剩余空间=1024K-848=176K  条数=(176*1024)/40=4616条 

#define LOG_PARA_START_ADDR			(EF_LOG_SPI_FLASH_ADDR+PKG_EASYFLASH_LOG_AREA_SIZE)
#define LOG_PARA_SIZE						sizeof(_log_para)

#define LOG_START_ADDR 					(LOG_PARA_START_ADDR+(4*1024)/*LOG_PARA_SIZE*/)
#define LOG_SIZE 								(LOG_SINGLE_SIZE*LOG_MAX_NUM)

void set_flash_write_addr(uint32_t w_addr);
uint32_t get_flash_write_addr();
uint32_t get_write_addr();
void set_write_addr(uint32_t w_addr);
void set_log_num(uint16_t r_num);

/******  外部调用 ******/
void log_init();
void log_reset();
uint16_t get_log_num();
void log_write(uint8_t* log,uint16_t log_len);
uint16_t log_read(uint8_t *rd_buf, uint16_t rd_buf_len,uint16_t read_log_num);

//#define unread_num()	(get_log_size()-read_num)

#endif
