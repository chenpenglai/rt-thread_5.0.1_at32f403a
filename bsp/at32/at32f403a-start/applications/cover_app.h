#ifndef __cover_app_H__
#define __cover_app_H__
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#define LOG_TAG             "cover"
#include <drv_log.h>

#define RADAR_ERR_VAL						-1			//雷达异常值
#define RADAR_OVERLAY_VAL				0				//雷达覆盖值
#define RADAR_SPACE_VAL					300			//雷达对空值

#define LIGHT_STABLE_RANGE 			100 		//光敏稳定变化区间（该区间将忽略检测 不开雷达）
#define LIGHT_CLOSE_COVER_VAL		2000		//关盖的光敏最小值（大于该值 都是关盖）

extern rt_event_t cover_event;
int cover_init(void);
int16_t get_radar_val();
uint8_t get_cover_state();


#endif 

