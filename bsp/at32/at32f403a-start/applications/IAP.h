#ifndef __IAP_H__
#define __IAP_H__
#include "stm32l1xx_hal.h"
#include "ef_cfg.h"

/*************			bootloader parameter		*************/

/*	
		innernal flash:
			0-20K 		:	bootloader
			20-256K		:	app
		
		external flash:
			0K		-	404K 		:	easyflash
			404K	-	604K 		:	new app
			604K	-	608K		:	upgrade flag parameter
			608K	-	808K		:	backup old app
*/
 
#if (defined(EF_USING_LOG)&&defined(EF_USING_ENV))
#define BK_APP_START_ADDR 					(EF_START_ADDR+ENV_AREA_SIZE+LOG_AREA_SIZE)							//外部flash存储新APP起始地址
#elif defined(EF_USING_ENV)
#define (BK_APP_START_ADDR 	EF_START_ADDR+ENV_AREA_SIZE)
#elif defined(EF_USING_LOG)
#define (BK_APP_START_ADDR 	EF_START_ADDR+LOG_AREA_SIZE)
#endif

#define BK_APP_SIZE									(200*1024)
#define UPGRADE_FLAG_ADDR 					(BK_APP_START_ADDR+BK_APP_SIZE)			//0K 放置标志位（不能放在0和216K以后，貌似flash驱动有问题）
#define UPGRADE_FLAG_SIZE						(4*1024)					//最小擦除为4K 所以空间不能小于4K（因为还要写入） 
#define BACKUP_OLD_APP_ADDR					(UPGRADE_FLAG_ADDR+UPGRADE_FLAG_SIZE)		//老APP备份地址
#define OLD_APP_SIZE								(200*1024)			//老APP大小

#define NEED_UPGRADE_FLAG 					1							//需要升级标志
#define SUCCESSED_UPGRADE_FLAG		 	2							//升级成功（必须为2 和iot升级对应）			 boot 情况 反馈
#define ERR_UPGRADE_FLAG						3							//升级失败


#endif

