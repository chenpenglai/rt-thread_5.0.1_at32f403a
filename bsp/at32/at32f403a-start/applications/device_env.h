/*
     
 */
#ifndef  __DEVICE_ENV_H__
#define  __DEVICE_ENV_H__

#include <rtthread.h>


//#include "global_define.h"


#define boolean         0x01
#define int8            0x02
#define int16           0x03
#define int32           0x04
#define uint8           0x05
#define uint16          0x06
#define uint32          0x07
#define real32          0x08
#define visible_string  0x09
#define octet_string    0x0A
#define unicode_string  0x0B
#define time_of_day     0x0C
#define time_difference 0x0D

#define N_4_2           0x0E //??????,2????
#define BCD_DIS         0x0F
#define HEX_DIS         0x10

#define N_6_1           0x10
#define N_6_2           0x11
#define N_7_3           0x12

#define N(x,n)          (x<<8 | n)


#define RW     0x00
#define WO     0x01
#define RO     0x02
#define OO     0x03  

#define RW_S   0x05  
#define WO_S   0x06  
#define RO_S   0x07  

int get_device_env_buf(char *get_name,uint8_t *buf,int len);
int get_device_env_buf_by_tag(uint16_t tag,uint8_t *buf,int len);
int get_device_env_int(char *get_name);


int set_device_env_buf_by_tag(uint16_t tag,uint8_t *buf);
int set_device_env_buf(char *get_name,uint8_t *buf);

int set_device_env_string(char *get_name,uint8_t *buf);
int set_device_env_int(char *get_name,int value);

int set_device_env(char *name,char *buf);
char * get_device_env(char *name);



char * get_env(char *name);
int get_env_int(char *name);

int set_env(char *name,char *buf);
#endif
