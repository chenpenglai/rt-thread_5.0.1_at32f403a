#include "global_define.h"
#include "stdio.h"
#include "easyflash.h"
#include <stm32l1xx.h>
#include "stdlib.h"
#include <board.h>
#define LOG_TAG             "global_define"
#include <drv_log.h>
#include <sfud.h>
#include "spi_flash.h"
#include "spi_flash_sfud.h"
#include "drv_spi.h"
#include <sfud.h>
#include <spi_flash.h>
#include "Tool.h"
struct send_msg msg;
_init_err init_err;//lkk add 2020-4-10
static const sfud_flash *flash;
extern rt_spi_flash_device_t w25q80;
extern uint8_t upload_update_result(uint8_t result);

void set_upgread_flag(uint8_t upgread_flag, uint32_t size, uint16_t check)
{
    const sfud_flash *flash1 = (sfud_flash_t)(w25q80->user_data);
    uint8_t buf[8] = {0};
    buf[0] = upgread_flag;
    buf[1] = (uint8_t)(size >> 24);     //大小
    buf[2] = (uint8_t)(size >> 16);
    buf[3] = (uint8_t)(size >> 8);
    buf[4] = (uint8_t)(size);
    buf[5] = (uint8_t)(check >> 8);
    buf[6] = (uint8_t)check;
    buf[7] = 0;
    sfud_write(flash1, UPGRADE_FLAG_ADDR, 8, buf);
}
void reset_info_chk(void)
{
		//rt_device_find
    //rt_kprintf("RCC_CSR:0X%X\r\n",RCC->CSR);
    if(__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST))
    {
        rt_kprintf("WWDG reset\r\n");
    }
    else if(__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST))
    {
        rt_kprintf("LPW reset\r\n");
    }
    else if(__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST))
    {
        rt_kprintf("POW reset\r\n");
    }
    else if(__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST))
    {
        rt_kprintf("SOFT reset\r\n");
    }
    else if(__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST))
    {
        rt_kprintf("IWDG reset\r\n");
    }
    else if(__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST))
    {
        rt_kprintf("PIN reset\r\n");
    }
    __HAL_RCC_CLEAR_RESET_FLAGS();
}
//void DEBUG_RX_IT_Enable(){                    //调用
//  GPIO_InitTypeDef GPIO_InitStruct;
//  __HAL_RCC_GPIOA_CLK_ENABLE();
//
//  GPIO_InitStruct.Pin = GPIO_PIN_9;
//  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
//  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 1, 1);
//  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
//}


rt_spi_flash_device_t w25q80;
#ifndef W25Q_GPIO_PIN_REMAP
#define WP_W   W25Q_WP_PIN
#define HOLD_W W25Q_HOLD_PIN
#else
#define WP_W 		GET_PIN(B, 5)//lkk add 2020-4-13
#define HOLD_W 	GET_PIN(B, 8)//lkk add 2020-4-13
#define W25Q_CS_PORT  GPIOB
#define W25Q_CS_GPIO_PIN  GPIO_PIN_4

#endif

int rt_hw_spi_flash_init(void)
{
    struct rt_spi_configuration cfg = {                                                \
        .mode = RT_SPI_MODE_0 | RT_SPI_MSB,          \
                .data_width = 8,                             \
                              .max_hz = 50 * 1000 * 1000,                  \
    };
    struct rt_spi_device     *	device;
    __HAL_RCC_GPIOB_CLK_ENABLE();
    rt_pin_mode(WP_W, PIN_MODE_OUTPUT);
    rt_pin_write(WP_W, PIN_HIGH);
    rt_pin_mode(HOLD_W, PIN_MODE_OUTPUT);
    rt_pin_write(HOLD_W, PIN_HIGH);

//    rt_pin_mode(POW_W, PIN_MODE_OUTPUT);
//    rt_pin_write(POW_W, PIN_LOW);

    rt_hw_spi_device_attach("spi1", "spi10", W25Q_CS_PORT, W25Q_CS_GPIO_PIN);//lkk cs 引脚在板子上修改为了PB6 2020-4-11

    device=(struct rt_spi_device *)rt_device_find("spi10");
    rt_spi_configure(device, &cfg);//配置;防止flash低功耗时，mcu 重启导致flash找不到
    flash_exit_lp();//唤醒
    SET_OUTPUT(W25Q_CS_PIN, 1);   //不 选中

    w25q80 = rt_sfud_flash_probe("W25Q80", "spi10");
    if (RT_NULL == w25q80)
    {
        return -RT_ERROR;
    }
    return RT_EOK;
}
INIT_COMPONENT_EXPORT(rt_hw_spi_flash_init);
extern uint8_t stop_flag;
extern bool is_flag(char* flag,char bit);
extern void uart_exit_lp(void);
#include "NC200_dev.h"
#include "net_app.h"
void all_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (is_flag((char*)&stop_flag, EVENT_LOW_POWER_ENTER))     //中断唤醒
    {
        SystemClock_Config();
        //rt_kprintf("GPIO_Pin %d\n",GPIO_Pin);
        //soft_uart_lowpower_ctr(0);
        uart_exit_lp();
        //rt_kprintf("#EXTI_wakeup 0X%X\n",GPIO_Pin);
        // rt_kprintf("EXTI SystemClock_Config\n");			//GPIO_PIN_11
    }
		//rt_kprintf("---> GPIO_Pin 0x%X\n",GPIO_Pin);
		
		if (GPIO_PIN_3 == GPIO_Pin)
    {
        //rt_kprintf("----> ble rx wake\n");
    }
//		if(GPIO_PIN_5 == GPIO_Pin)
//    {
//			rt_kprintf("---> vmd wake\n");	//和氧气串口脚RX-PC5 中断冲突，氧气串口数据返回将频繁触发该打印
//		}
    if (GPIO_PIN_6 == GPIO_Pin)
    {
        if(rt_pin_read(adx1362_INT1_PIN)==0)		//该引脚是下降沿触发
						rt_kprintf("---> irq1 wake\n");
//        else
//				{
//            rt_kprintf("---> car wake\n");		// NC200_STATE=PB6 , INT1=PC6
//						nc200_is_car();
//						if(get_net_connect_flag())
//							net_send_event(SEND_MSG_WELL_1_OPEN_STATE_YX);
//        }
    }

		if (GPIO_PIN_8 == GPIO_Pin)
    {
			rt_kprintf("---> button wake\n");
		}
    if (GPIO_PIN_9 == GPIO_Pin)
    {
        rt_kprintf("---> ble wake\n");
    }

//    if (adx1362_INT1_GPIO_Pin == GPIO_Pin)
//    {
//        rt_kprintf("----> adxl362 int1\n");
//    }
//		if (adx1362_INT2_GPIO_Pin == GPIO_Pin)
//    {
//        rt_kprintf("----> adxl362 int2\n");
//    }
//		if(FIRE_SENSOR_GPIO_PIN== GPIO_Pin)//复用其他引脚功能，不打印信息
//		{
//			//rt_kprintf("----> fire in\n");
//		}
    HAL_GPIO_EXTI_IRQHandler(GPIO_Pin);
}
void stop_recover_old_app_upload_result()
{
    flash = (sfud_flash_t)(w25q80->user_data);
    uint32_t isUpdata = NULL;
    uint8_t buf[8] = {0};
    sfud_read(flash, UPGRADE_FLAG_ADDR, 8, buf);
    isUpdata = buf[0];
    if (isUpdata == SUCCESSED_UPGRADE_FLAG)
    {
        buf[0] = 0;
        LOG_I("upgread successed! stop recover old app\n");
        upload_update_result(0x00);
        sfud_erase(flash, UPGRADE_FLAG_ADDR, 20);       //必须擦除再写入
        rt_thread_delay(rt_tick_from_millisecond(10));
        sfud_write(flash, UPGRADE_FLAG_ADDR, 8, buf);
    }
    else if (isUpdata == ERR_UPGRADE_FLAG)
    {
        LOG_E("Firmaware check was fail\n");
        upload_update_result(0x0A);
        buf[0] = 0;
        rt_memset(buf, 0, 7);
        sfud_erase(flash, UPGRADE_FLAG_ADDR, 20);       //必须擦除再写入
        rt_thread_delay(rt_tick_from_millisecond(10));
        sfud_write(flash, UPGRADE_FLAG_ADDR, 8, buf);
    }
}


extern RTC_HandleTypeDef RTC_Handler;

int inernal_flash_erase(uint32_t addr, size_t size)
{
    EfErrCode result = RT_EOK;
    HAL_StatusTypeDef status = HAL_OK;
    size_t erase_pages;

    /* make sure the start address is a multiple of FLASH_ERASE_MIN_SIZE */
    EF_ASSERT(addr % 4096 == 0);

    /* calculate pages */
    erase_pages = size / 4096;
    if (size % EF_ERASE_MIN_SIZE != 0)
    {
        erase_pages++;
    }
    /* start erase */
    HAL_FLASH_Unlock();
    FLASH_EraseInitTypeDef f;
    uint32_t PageError = 0;

    f.TypeErase = FLASH_TYPEERASE_PAGES;
    f.PageAddress = addr;
    f.NbPages = erase_pages;
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR);
    status = HAL_FLASHEx_Erase(&f, &PageError);
    if (status != HAL_OK)
    {
        result = RT_ERROR;
    }
    HAL_FLASH_Lock();

    return result;
}
int inernal_flash_write(uint32_t addr, const uint32_t *buf, size_t size)
{
    int result = RT_EOK;
    size_t i;
    uint32_t read_data;
    EF_ASSERT(size % 4 == 0);
    HAL_FLASH_Unlock();
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR);
    for (i = 0; i < size; i += 4, buf++, addr += 4)
    {
        /* write data */
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, *buf);
        read_data = *(uint32_t *)addr;
        /* check data */
        if (read_data != *buf)
        {
            result = 1;
            rt_kprintf("internal flash write error!\n");
            return RT_ERROR;
        }
    }
    HAL_FLASH_Lock();

    return result;
}

int inernal_flash_read(uint32_t addr, uint32_t *buf, size_t size)
{
    EfErrCode result = RT_EOK;

    EF_ASSERT(size % 4 == 0);

    /*copy from flash to ram */
    for (; size > 0; size -= 4, addr += 4, buf++)
    {
        *buf = *(uint32_t *) addr;
    }
    return result;
}
/*
uint16_t cal_check_sum(uint8_t *data, int len)
{
    uint32_t sum;
    for (int i = 0; i < len; i++)
    {
        sum = data[i];
    }
    return sum;
}
*/
int set_sys_use_uart(uint8_t num)
{
    rt_kprintf("config use %d \n", num);
    struct sys_config config;
    config.use_uart = num;
    config.check_sum = cal_check_sum((uint8_t *)&config, sizeof(config));
    inernal_flash_erase(SYS_CONFIG_ADDR, 4096);
    inernal_flash_write(SYS_CONFIG_ADDR, (uint32_t *)&config, sizeof(config));
    return 1;
}

int get_sys_use_uart(void)
{
    struct sys_config config;
    inernal_flash_read(SYS_CONFIG_ADDR, (uint32_t *)&config, sizeof(config));
    if (cal_check_sum((uint8_t *)&config, sizeof(config) == config.check_sum))
    {
        //rt_kprintf("config use %d \n", config.use_uart);
        //rt_kprintf("config use %d \n", config.check_sum);
        return config.use_uart;
    }
    return -1;
}
static void set_blue_debug(uint8_t argc, char **argv)
{
    set_sys_use_uart(2);
}

MSH_CMD_EXPORT(set_blue_debug, set blue debug);
static void clear_blue_debug(uint8_t argc, char **argv)
{
    set_sys_use_uart(0);
}

MSH_CMD_EXPORT(clear_blue_debug, clear blue ouput);



sys_tick_t tick_had_gone(sys_tick_t old_tick)
{
    sys_tick_t now_tick=get_sys_tick();

    if(now_tick>=old_tick)
    {
        return (now_tick-old_tick);
    }

    return(sys_tick_value_max-old_tick+now_tick);
}


void adc_battery(void)
{
#define ADC_DEV_NAME        "adc1"  /* ADC 设备名称 */
#define ADC_DEV_CHANNEL1     4       /* ADC 通道 */
#define ADC_DEV_CHANNEL2     5       /* ADC 通道 */

#define REFER_VOLTAGE       330         /* 参考电压 3.3V,数据精度乘以100保留2位小数*/
#define CONVERT_BITS        (1 << 12)   /* 转换位数为12位 */

    rt_adc_device_t adc_dev;            /* ADC 设备句柄 */
    rt_uint32_t value1;
    /* 查找设备 */
    adc_dev = (rt_adc_device_t)rt_device_find(ADC_DEV_NAME);
    /* 使能设备 */
    rt_adc_enable(adc_dev, ADC_DEV_CHANNEL1);
    //rt_adc_enable(adc_dev, ADC_DEV_CHANNEL2);
    /* 读取采样值 */
    value1 = rt_adc_read(adc_dev, ADC_DEV_CHANNEL1);
    //value2=rt_adc_read(adc_dev, ADC_DEV_CHANNEL2);
    rt_kprintf("adc_acq:%u\n",value1);
    /* 转换为对应电压值 */
    value1 = value1 * REFER_VOLTAGE / CONVERT_BITS;
    //value2 = value2 * REFER_VOLTAGE / CONVERT_BITS;

    //value1 = (value1>value2)?(value1-value2):0;//1通道的电压大于2
    value1=value1*3;//取样电压为1/3
    rt_kprintf("the voltage is :%d.%02d \n", value1 / 100, value1 % 100);
    rt_adc_disable(adc_dev, ADC_DEV_CHANNEL1);
//	rt_adc_disable(adc_dev, ADC_DEV_CHANNEL2);

}


extern void init_adx1326(void);
void init_thrh(void);

void all_sensor_init(void)
{

    //adxl_init();//三轴传感器初始化
    //init_thrh();//温湿度传感器初始化
}
uint8_t plat_sel=0;
void set_plat(PLAT_SEL sel) {
    plat_sel=sel;

}
int get_plat() {
    return plat_sel;
}

uint8_t setup_state=0;
void set_defence(IS_DEFENCE def) {
    setup_state=def;
}
int get_defence() {
    return setup_state;
}

//uint8_t temp_state=0,humidity_state=0;

#include "power_app.h"
//返回值：1，已经在此处理完毕；0，未知事件。没进行任何操作
uint8_t global_event_pro(GLOBAL_ENVENT_EN event)//全局所有事件的处理：高低温，水浸，火浸。。。。
{
    switch(event)
    {
    /*温度事件*/
    case GE_TEMP_HIGH:
        period_warninig_alarm_try_add(HIGH_TEMP_PERIOD_ALRAM_EVT);
        break;
    case GE_TEMP_LOW:
        period_warninig_alarm_try_add(LOW_TEMP_PERIOD_ALRAM_EVT);
        break;
    case GE_TEMP_HIGH_CLEAR:
        period_warninig_alarm_try_stop(HIGH_TEMP_PERIOD_ALRAM_EVT);
        break;
    case GE_TEMP_LOW_CLEAR:
        period_warninig_alarm_try_stop(LOW_TEMP_PERIOD_ALRAM_EVT);
        break;

    /*湿度事件*/
    case GE_HUMIDITY_HIGH:
        period_warninig_alarm_try_add(HIGH_HUMIDITY_PERIOD_ALRAM_EVT);
        break;
    case GE_HUMIDITY_LOW:
        period_warninig_alarm_try_add(LOW_HUMIDITY_PERIOD_ALRAM_EVT);
        break;
    case GE_HUMIDITY_HIGH_CLEAR:
        period_warninig_alarm_try_stop(HIGH_HUMIDITY_PERIOD_ALRAM_EVT);
        break;
    case GE_HUMIDITY_LOW_CLEAR:
        period_warninig_alarm_try_stop(LOW_HUMIDITY_PERIOD_ALRAM_EVT);
        break;


    case GE_ADXL_WARNING:
        period_warninig_alarm_try_add(ADXL_PERIOD_ALRAM_EVT);
        break;
    case GE_ADXL_CLEAR:
        period_warninig_alarm_try_stop(ADXL_PERIOD_ALRAM_EVT);
        break;


    /*浸水事件*/
		case GE_WATER_IN_WARNING:				period_warninig_alarm_try_add(WATER_OUT_PERIOD_ALRAM_EVT);			break;
		case GE_WATER_IN_CLEAR:					period_warninig_alarm_try_stop(WATER_OUT_PERIOD_ALRAM_EVT);			break;
		
		
    default:
        break;
    }
    return 0;
}


_open_lock_type open_lock_type=OTHER_OPEN;
void set_open_lock_type(_open_lock_type type){
	open_lock_type=type;
}
_open_lock_type get_open_lock_type(){
	_open_lock_type tmp=open_lock_type;
	open_lock_type=OTHER_OPEN;				//读取一次开锁方式以后，将其设置为 其他，防止下次读取还是上一次的
	return tmp;
}


_is_allow_open_lock is_allow_open_lock=NOT_ALLOW_OPEN;
void set_open_lock_allow(uint8_t flag){
	is_allow_open_lock=flag;
}
uint8_t get_open_lock_allow(){
	return is_allow_open_lock;
}

uint8_t gas_state[GAS_MAX_NUM]={GAS_NORMAL};
uint8_t get_gas_state(_gas_type gas_type){
	return gas_state[gas_type];
}
uint8_t set_gas_state(_gas_type gas_type,uint8_t state){
	gas_state[gas_type]=state;
}
