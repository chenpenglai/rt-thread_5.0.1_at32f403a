#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "dev_driver.h"
//#include "dev_rccb.h"
#include "dlt645_private.h"
#include "dlt645_sample.h"
#define LOG_TAG "dev_rccb"

#define RCCB_TYPE_NAME 			"rccb"			//类型名
#define RCCB_USART_BAUD 		DLT645_USART_BUAD	//ef_get_env_int("usart_baud_rccb") //38400
#define RCCB_USART_PARITY 	USART_PARITY_E
#define RCCB_RESP_INV   		200 	//设备应答间隔时间
#define RCCB_CALL_INV   		5000 	//设备召测间隔时间

DeviceType* device_type_rccb;
static uint8_t buff[1024];

dev_driver_err rccb_transfer_wr(Device *dev,que_struct* send_que,uint8_t *data,int data_len) {
    printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);

    device_add_data_to_sque(send_que,data,data_len);			//透传接口，直接填充数据 不处理

    return DEV_DRIVER_OK;

}
dev_driver_err rccb_transfer_rd(Device *dev,que_struct* recv_que,uint8_t *data,int data_size,int* data_len) {
    printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);

    device_recv_rque_to_data(recv_que,data,data_size,data_len);		//透传接口，直接填充数据 不处理

    return DEV_DRIVER_OK;

}

dev_driver_err rccb_read_data_wr(Device *dev,que_struct* send_que) {
    printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
    uint8_t rccb_result=0;
    uint8_t addr[DL645_ADDR_LEN];
    strarr_to_hexarr(dev->dev_id,addr,DL645_ADDR_LEN*2);
		
//    rccb_result|=dlt645_read_rated_voltage(addr,&dlt645_cfg_val);			//需要拆分掉


    return DEV_DRIVER_OK;

}

dev_driver_err rccb_read_data_rd(Device *dev,que_struct* recv_que) {
    printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);





    return DEV_DRIVER_OK;
}
dev_driver_err rccb_poll_wr(Device *dev,que_struct* send_que) {
    printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
    _gedev_info_struct info;
    //uint8_t addr=atoi(dev->dev_id);
    uint8_t addr[DL645_ADDR_LEN];
    strarr_to_hexarr(dev->dev_id,addr,DL645_ADDR_LEN*2);
		
		//device_add_data_to_sque(send_que,data,data_len);			//填充数据
	
//    if(0==read_gedev_info_by_addr(addr,DL645_ADDR_LEN,&info)) {
//			
//        //exec_read_slv_state(info.dev_type,addr);
//        //*send_len=fill_modbus_master_send_buff(send_data,DEV_DRIVER_DATA_BUFF_SIZE);			//填充 设备框架 发送缓存区

//    } else {


//    }

    return DEV_DRIVER_OK;

}

dev_driver_err rccb_poll_rd(Device *dev,que_struct* recv_que) {
    printf("dev_id %s %s\n",dev->dev_id,__FUNCTION__);


    return DEV_DRIVER_ERR;
}

dev_driver_err rccb_control_wr(Device *dev, ctrl_opt op,void* arg,que_struct* send_que)
{
    switch (op) {
    case CTRL_SET_STATE:

        break;
        //        case CONTROL_OPERATION_2:
        //
        //            break;

        // ...


    default:
        // 不支持的控制操作
        break;
    }
}


dev_driver_err rccb_control_rd(Device *dev, ctrl_opt op,void* arg,que_struct* recv_que)
{
    switch (op) {
    case CTRL_SET_STATE:

        break;
        //        case CONTROL_OPERATION_2:
        //
        //            break;

        // ...


    default:
        // 不支持的控制操作
        break;
    }
}
//轮询接口（给 dev_driver 调用）
dev_driver_err rccb_evt_wr(Device *dev,que_struct* send_que){
		printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
		uint8_t result=0;	
	  uint8_t addr[DL645_ADDR_LEN];
    strarr_to_hexarr(dev->dev_id,addr,DL645_ADDR_LEN*2);
	
		Device *device=device_find_by_type_hexid("rccb",addr,DL645_ADDR_LEN);
		if(device==NULL)
		{
			return DEV_DRIVER_ERR;
		}
		int len;
		result|=dlt645_read(addr,DIC_4000102,buff,&len);
		
		device_add_data_to_sque(send_que,buff,len);
	
		return DEV_DRIVER_OK;

}

dev_driver_err rccb_evt_rd(Device *dev,que_struct* recv_que,que_struct* send_que){
	printf("dev_id %s %s\n",dev->dev_id,__FUNCTION__);
	int len;
	if(device_recv_rque_to_data(recv_que,buff,sizeof(buff),&len)>=0){
			uint8_t result=0;	
			uint8_t addr[DL645_ADDR_LEN];
			strarr_to_hexarr(dev->dev_id,addr,DL645_ADDR_LEN*2);
			uint8_t val[30];
			result|=dlt645_read_rd(addr,DIC_4000102,val,buff,len);
			if(result==0){
				return DEV_DRIVER_OK;
			}
				
				
	}
	return DEV_DRIVER_ERR;
}


extern USART_T *dev_pu;
int dev_rccb(void) {
    // 注册设备类型
    device_type_rccb=device_register_type(RCCB_TYPE_NAME);
    if(device_type_rccb==NULL) {
        log_e("device_register_type %s failed\n",RCCB_TYPE_NAME);
        return 1;
    }
    device_type_rccb->pu=dev_pu;
    device_type_rccb->usart_baud=RCCB_USART_BAUD;							//串口波特率
    device_type_rccb->usart_parity=RCCB_USART_PARITY;			//串口 无校验位
    device_type_rccb->dev_resp_inv_ms=RCCB_RESP_INV;
    device_type_rccb->dev_call_inv_ms=RCCB_CALL_INV;

    device_type_rccb->transfer_wr = rccb_transfer_wr;
    device_type_rccb->transfer_rd = rccb_transfer_rd;
    device_type_rccb->read_data_wr = rccb_read_data_wr;
    device_type_rccb->read_data_rd = rccb_read_data_rd;
    device_type_rccb->alarm = NULL;
    device_type_rccb->poll_wr = rccb_poll_wr;
    device_type_rccb->poll_rd = rccb_poll_rd;
    device_type_rccb->call_wr = NULL;
    device_type_rccb->call_rd = NULL;
    device_type_rccb->chk_online_wr = NULL;
    device_type_rccb->chk_online_rd = NULL;
    device_type_rccb->control_wr = rccb_control_wr;
    device_type_rccb->control_rd = rccb_control_rd;
    device_type_rccb->evt_wr = rccb_evt_wr;
    device_type_rccb->evt_rd = rccb_evt_rd;

    printf("register device type: %s, baud: %d\n",device_type_rccb->name,device_type_rccb->usart_baud);

    _gedev_info_struct gedev_info;
    for(int i=0; i<get_gedev_info_pool_num(); i++)			//从flash内读取保存的rccb设备 注册
    {
        if(read_gedev_info_by_index(&gedev_info,GEDEV_INFO_SIZE,i))
        {
            // 注册设备
            char gedev_addr[20];//;		TODO
            hexarr_to_strarr(gedev_addr,gedev_info.addr,gedev_info.addr_len);
            //						sprintf(dev_id,"%s",gedev_info.addr);
            Device *device1 = device_register(device_type_rccb, gedev_addr);
            device1->is_online = true;
            printf("device_register dev_id: %s\n",device1->dev_id);
        }
    }

    return 0;
}
DEV_DRIVER_INIT(dev_rccb);			//移除此设备 只需不编译该文件 或注释这一行


