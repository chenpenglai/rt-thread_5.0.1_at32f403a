#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "dev_driver.h"
#include "modbus.h"
#include "func_modbus_mas_slv.h"
//#include "dev_brk.h"
#define LOG_TAG "dev_brk"

#define BRK_TYPE_NAME 			"brk"			//类型名
#define BRK_USART_BAUD 			MODBUS_MASTER_USART_BUAD	//ef_get_env_int("usart_baud_brk") //38400
#define BRK_USART_PARITY 		USART_PARITY_N
#define BRK_RESP_INV   			500 	//设备应答间隔时间
#define BRK_CALL_INV   			5000 	//设备召测间隔时间


DeviceType* device_type_brk;

static uint8_t buff[1024];

//透传接口
dev_driver_err brk_transfer_wr(Device *dev,que_struct* send_que,uint8_t *data,int data_len){
	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);		
	
	device_add_data_to_sque(send_que,data,data_len);			//透传接口，直接填充数据 不处理
	
	return DEV_DRIVER_OK;

}
dev_driver_err brk_transfer_rd(Device *dev,que_struct* recv_que,uint8_t *data,int data_size,int* data_len){
	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);		
	
	if(-1==device_recv_rque_to_data(recv_que,data,data_size,data_len))		//透传接口，直接填充数据 不处理
	{
		return DEV_DRIVER_ERR;
	}
	return DEV_DRIVER_OK;

}

//数据读取接口（读取设备所有数据）
dev_driver_err brk_read_data_wr(Device *dev,que_struct* send_que){
//	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
//	_dev_info_struct info;
//	uint8_t addr=atoi(dev->dev_id);
//	
//	_dev_state_struct_p dev_state_p;
//	if(NULL!=(dev_state_p=read_dev_state_by_addr_sram(addr))){

//			clr_modbus_master_func();			//清空modbus缓存的func 
//			exec_read_slv_state(dev_state_p->dev_type,addr);
//			int len=fill_modbus_master_send_buff(buff,sizeof(buff));			//modbus 填充 设备框架 发送缓存区
//			device_add_data_to_sque(send_que,buff,len);
//		
//		
//			//device_add_data_to_sque(send_que,buff,len);
//	
//	}else{
//		
//		
//	}
	return DEV_DRIVER_OK;

}

dev_driver_err brk_read_data_rd(Device *dev,que_struct* recv_que){
	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
	
	int len;
	
	while(device_recv_rque_to_data(recv_que,buff,sizeof(buff),&len)>=0){

			//hexdump("<<<",buff,len);
			if(1==slove_modbus_master_recv_buff(buff,len)){			//modbus 解析 设备框架 接收缓存区
				printf("read_data_rd ok\n");
				//return DEV_DRIVER_OK;
				
			}
			else{
				printf("slove_modbus_master ERR\n");
				//return DEV_DRIVER_ERR;
			}
	}

	
	return DEV_DRIVER_OK;

}


//轮询接口（给 dev_driver 调用）
dev_driver_err brk_poll_wr(Device *dev,que_struct* send_que){
	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
//	_dev_info_struct info;
//	uint8_t addr=atoi(dev->dev_id);

//	_dev_state_struct_p dev_state_p;
//	if(NULL!=(dev_state_p=read_dev_state_by_addr_sram(addr))){
//	//if(0==read_dev_info_by_addr(addr,&info)) {
//			clr_modbus_master_func();			//清空modbus缓存的func
//			exec_read_slv_state(dev_state_p->dev_type,addr);
////			*send_len=fill_modbus_master_send_buff(send_data,DEV_DRIVER_DATA_BUFF_SIZE);			//modbus 填充 设备框架 发送缓存区
//			int len=fill_modbus_master_send_buff(buff,sizeof(buff));			//modbus 填充 设备框架 发送缓存区
//			device_add_data_to_sque(send_que,buff,len);
////			fixlen_que_Wr(&send_que->data_que,buff,len);		//插入数据到队列
////			send_que->data_num=1;
//	}
//	else{
//		
//		
//	}
	
	return DEV_DRIVER_OK;

}

dev_driver_err brk_poll_rd(Device *dev,que_struct* recv_que){
	printf("dev_id %s %s\n",dev->dev_id,__FUNCTION__);
	int len=fixlen_que_Rd(&recv_que->data_que,buff,sizeof(buff));
	if(1==slove_modbus_master_recv_buff(buff,len)){			//modbus 解析 设备框架 接收缓存区
		printf("dev_id %s poll ok\n",dev->dev_id);
		return DEV_DRIVER_OK;
		
	
	}
	return DEV_DRIVER_ERR;
}
dev_driver_err brk_call_wr(Device *dev,que_struct* send_que){
	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
//	_dev_info_struct info;
//	uint8_t addr=atoi(dev->dev_id);

//	_dev_state_struct_p dev_state_p;
//	if(NULL!=(dev_state_p=read_dev_state_by_addr_sram(addr))){
//	//if(0==read_dev_info_by_addr(addr,&info)) {
//			clr_modbus_master_func();			//清空modbus缓存的func
//			exec_read_slv_state(dev_state_p->dev_type,addr);
////			*send_len=fill_modbus_master_send_buff(send_data,DEV_DRIVER_DATA_BUFF_SIZE);			//modbus 填充 设备框架 发送缓存区
//			int len=fill_modbus_master_send_buff(buff,sizeof(buff));			//modbus 填充 设备框架 发送缓存区
//			device_add_data_to_sque(send_que,buff,len);
////			fixlen_que_Wr(&send_que->data_que,buff,len);		//插入数据到队列
////			send_que->data_num=1;
//	}
//	else{
//		
//	}
	return DEV_DRIVER_OK;
}

dev_driver_err brk_call_rd(Device *dev,que_struct* recv_que){
	printf("dev_id %s %s\n",dev->dev_id,__FUNCTION__);
	int len=fixlen_que_Rd(&recv_que->data_que,buff,sizeof(buff));
	if(1==slove_modbus_master_recv_buff(buff,len)){			//modbus 解析 设备框架 接收缓存区
		printf("dev_id %s call ok\n",dev->dev_id);
		return DEV_DRIVER_OK;
		
	
	}
	return DEV_DRIVER_ERR;
}


//轮询接口（给 dev_driver 调用）
dev_driver_err brk_evt_wr(Device *dev,que_struct* send_que){
	printf("%s dev_id: %s\n",__FUNCTION__,dev->dev_id);
//	_dev_info_struct info;
//	uint8_t addr=atoi(dev->dev_id);

//	_dev_state_struct_p dev_state_p;
//	if(NULL!=(dev_state_p=read_dev_state_by_addr_sram(addr))){

//	//if(0==read_dev_info_by_addr(addr,&info)) {
//			clr_modbus_master_func();			//清空modbus缓存的func
//			
//			dev_read_by_hr_type(dev_state_p->addr,dev_state_p->dev_type,BRK_1P_READ_EVT);
//			
//			int len=fill_modbus_master_send_buff(buff,sizeof(buff));			//modbus 填充 设备框架 发送缓存区
//			device_add_data_to_sque(send_que,buff,len);

//	}
//	else{
//		
//		
//	}
	
	return DEV_DRIVER_OK;

}

dev_driver_err brk_evt_rd(Device *dev,que_struct* recv_que,que_struct* send_que){
	printf("dev_id %s %s\n",dev->dev_id,__FUNCTION__);
	int len;
	if(device_recv_rque_to_data(recv_que,buff,sizeof(buff),&len)>=0){
//			if(1==slove_modbus_master_recv_buff(buff,len)){			//modbus 解析 设备框架 接收缓存区
//				printf("dev_id %s evt ok\n",dev->dev_id);
//				uint8_t addr=atoi(dev->dev_id);
//				_brk_event tmp_evt;
//				_dev_state_struct_p dev_state_p;
//				dev_state_p=read_dev_state_by_addr_sram(addr);

//				_read_hr_result read_hr = get_read_hr_result(dev_state_p->addr,BRK_1P_READ_EVT);				//状态 从轮询中拿

//				tmp_evt.state=	read_hr.hr_data[0];
//				tmp_evt.protect=read_hr.hr_data[1];
//				tmp_evt.alarm=	read_hr.hr_data[2];
//				tmp_evt.error=	read_hr.hr_data[3];
//				
//				if((tmp_evt.state!=dev_state_p->event.state||\
//						tmp_evt.protect!=dev_state_p->event.protect||\
//						tmp_evt.alarm!=dev_state_p->event.alarm||\
//						tmp_evt.error!=dev_state_p->event.error))
//				{
//						log_i("happen event addr: %d, [%d,%d,%d,%d]\n",dev_state_p->addr,tmp_evt.state,tmp_evt.protect,tmp_evt.alarm,tmp_evt.error);
//						
//						//更新状态
//						dev_state_p->event.state=tmp_evt.state;
//						dev_state_p->event.protect=tmp_evt.protect;
//						dev_state_p->event.alarm=tmp_evt.alarm;
//						dev_state_p->event.error=tmp_evt.error;
//						add_dev_state_by_addr_sram(dev_state_p->addr,dev_state_p);		
//						
//						dev_read_by_hr_type(dev_state_p->addr,dev_state_p->dev_type,BRK_1P_READ_EVT_NEED_PARA);
//						int len=fill_modbus_master_send_buff(buff,sizeof(buff));			//modbus 填充 设备框架 发送缓存区
//						device_add_data_to_sque(send_que,buff,len);
//						
//						return DEV_DRIVER_READ_MORE;		//发现状态改变 需要读取更多HR
//					
//				}
//				return DEV_DRIVER_OK;
//			}
	}
	return DEV_DRIVER_ERR;
}

dev_driver_err brk_evt_rd_more(Device *dev,que_struct* recv_que){
	printf("dev_id %s %s\n",dev->dev_id,__FUNCTION__);
	int len;
	if(device_recv_rque_to_data(recv_que,buff,sizeof(buff),&len)>=0){
		if(1==slove_modbus_master_recv_buff(buff,len)){			//modbus 解析 设备框架 接收缓存区
			printf("dev_id %s evt ok\n",dev->dev_id);
			
//			net_set_send_event(EVENT_SEND_EVENT);		//上报平台 断路器事件

			return DEV_DRIVER_OK;		
		}
	}
	return DEV_DRIVER_ERR;
}

//控制接口
dev_driver_err brk_control_wr(Device *dev, ctrl_opt op,void* arg,que_struct* send_que)
{
		clr_modbus_master_func();			//清空modbus缓存的func 
		uint8_t addr=atoi(dev->dev_id);
		int len;
//    switch (op) {
//        case CTRL_SET_STATE:		//设置断路器状态
//						{
//							uint8_t* state=(uint8_t*)arg;
//							brk_set_state(addr,*state);
//						}
//            break;
//        case CTRL_SET_TRIP:			//脱扣
//            brk_set_trip(addr);
//            break;        
//				case CTRL_SET_LOCK:		//设置挂锁
//						{
//							uint8_t* lock=(uint8_t*)arg;
//							brk_set_lock(addr,*lock);
//						}
//            break;
//				case CTRL_SET_FORCE_STATE:			//强制合闸
//            brk_force_state(addr);
//            break; 
//				case CTRL_SET_LEAKAGE_TEST:			//漏电自检
//            brk_leakage_test(addr);
//            break; 
//				
//        default:
//            // 不支持的控制操作
//						log_e("unsupported control op: %d\n",op);
//						return DEV_DRIVER_ERR;
//            break;
//    }
							
		len=fill_modbus_master_send_buff(buff,sizeof(buff));			//modbus 填充 设备框架 发送缓存区
		device_add_data_to_sque(send_que,buff,len);		
		return DEV_DRIVER_OK;
}


dev_driver_err brk_control_rd(Device *dev, ctrl_opt op,void* arg,que_struct* recv_que)
{
    switch (op) {
				//解析modbus应答，写入hr的应答都是一样的，所以所有case都一个情况
        case CTRL_SET_STATE:
				case CTRL_SET_TRIP:
				case CTRL_SET_LOCK:
				case CTRL_SET_FORCE_STATE:
				case CTRL_SET_LEAKAGE_TEST:
					{
						int len;
            if(device_recv_rque_to_data(recv_que,buff,sizeof(buff),&len)>=0){
								hexdump("<<<",buff,len);
								if(1==slove_modbus_master_recv_buff(buff,len)){			//modbus 解析 设备框架 接收缓存区
									printf("dev_id %s ctrl ok\n",dev->dev_id);
									return DEV_DRIVER_OK;
								
								}
						}
					}
            break;
//        case CTRL_2:
//            
//            break;
				
        default:
            // 不支持的控制操作
            break;
    }
		return DEV_DRIVER_ERR;
}

extern USART_T *dev_pu;
int dev_brk(void) {
    // 注册设备类型
    device_type_brk=device_register_type(BRK_TYPE_NAME);
		if(device_type_brk==NULL){
			log_e("device_register_type %s failed\n",BRK_TYPE_NAME);
			return 1;
		}
		
		device_type_brk->pu=dev_pu;
		device_type_brk->usart_baud=BRK_USART_BAUD;							//串口波特率
		device_type_brk->usart_parity=BRK_USART_PARITY;			//串口 无校验位
		device_type_brk->dev_resp_inv_ms=BRK_RESP_INV;
		device_type_brk->dev_call_inv_ms=BRK_CALL_INV;
		
		
		device_type_brk->transfer_wr = brk_transfer_wr;
		device_type_brk->transfer_rd = brk_transfer_rd;
		device_type_brk->read_data_wr = brk_read_data_wr;
		device_type_brk->read_data_rd = brk_read_data_rd;
		device_type_brk->alarm = NULL;
		device_type_brk->poll_wr = brk_poll_wr;
		device_type_brk->poll_rd = brk_poll_rd;
		device_type_brk->call_wr = brk_call_wr;
		device_type_brk->call_rd = brk_call_rd;
		device_type_brk->chk_online_wr = NULL;
		device_type_brk->chk_online_rd = NULL;
		device_type_brk->control_wr = brk_control_wr;
		device_type_brk->control_rd = brk_control_rd;
		device_type_brk->evt_wr = brk_evt_wr;
		device_type_brk->evt_rd = brk_evt_rd;
		device_type_brk->evt_rd_more = brk_evt_rd_more;
		printf("register device type: %s, baud: %d\n",device_type_brk->name,device_type_brk->usart_baud);
		
		//关于该类型设备的 modbus初始化（注意串口初始化不能在这里做，以免多个设备使用同一串口 重复初始化导致死机）
		func_modbus_master_init(&g_func_modbus_master0, device_type_brk->pu, g_func_modbus_master0_slv_hr);
		
		_dev_info_struct dev_info;
		for(int i=0;i<get_dev_info_pool_num();i++)			//从flash内读取保存的brk设备 注册
		{
				if(read_dev_info_by_index(&dev_info,DEV_INFO_SIZE,i))
				{
						// 注册设备
						char dev_id[10];
						sprintf(dev_id,"%d",dev_info.addr);
						Device *device1 = device_register(device_type_brk, dev_id);
						device1->is_online = true;

						printf("device_register dev_id: %s\n",device1->dev_id);
				}
		}
		
    return 0;
}
DEV_DRIVER_INIT(dev_brk);		//移除此设备 只需不编译该文件 或注释这一行



