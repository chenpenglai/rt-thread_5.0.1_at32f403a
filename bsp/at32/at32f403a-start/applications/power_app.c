/*
 *  此为功耗管理app  请熟知各alarm接口 慎重修改。
 * Change Logs:
 * Date           Author       Notes
 * 2019-9-7       pxc         first version
 */
#include <rtthread.h>
#include <rtdevice.h>
#include "blue_app.h"
#include "net_app.h"
#include <stdlib.h>
#include "soft_I2C.h"
#include "Tool.h"
#include "power_app.h"
#include "global_define.h"
#include "easyflash.h"
#include "button_app.h"
#include "stm32l1xx_ll_exti.h"
#undef DBG_TAG
#undef DBG_LVL
#define DBG_TAG              "app.power"
#define DBG_LVL              LOG_LVL_DBG
#include <rtdbg.h>
#include "gjx_lock.h"
#include "wwdg.h"
#include "drv_ctrl.h"
#include "led_dev.h"
#include "seg_app.h"
#include "gas_app.h"
#include "drv_htbklock.h"

struct rt_alarm *ALARM[alarm_max_num];
rt_timer_t pm_release_timer(int timeout);
rt_timer_t force_sleep_timer_start(int timeout);
static void force_sleep_timer_stop(void);
static void sleep_timer(void *parameter);
extern uint8_t ble_has_waked;
uint8_t stop_flag = 0;
#define LIFE_TIME ef_get_env_int("keepwake(s)")                //激活后保持唤醒的时间 单位 s
void set_life_alarm()
{
		rt_kprintf("%s\n",__FUNCTION__);
    int second = ef_get_env_int("keepAlive(s)");
    alarm_add_sec(regular_heart_alarm_index, second);
}
void set_period_check_alarm()
{
		rt_kprintf("%s\n",__FUNCTION__);
    int second = ef_get_env_int("check_period(m)")*60;
    alarm_add_sec(period_check_index, second);
}
void cancel_period_check_alarm()
{
		rt_kprintf("%s\n",__FUNCTION__);
    alarm_stop(period_check_index);
}
void set_period_warn_alarm()
{ 
		rt_kprintf("%s\n",__FUNCTION__);
    int second = 60*ef_get_env_int("warning_period(m)");
    alarm_add_sec(period_warninig_alram_index,second);
}
void cancel_period_warn_alarm()
{
		rt_kprintf("%s\n",__FUNCTION__);
    alarm_stop(period_warninig_alram_index);
}

void auto_lock_alarm_add()
{
	int	second= 60*ef_get_env_int("autolocktime(m)");//自动落锁时间
	if(second==0){
		return;
	}
	rt_kprintf("%s add: %d (s)\n",__FUNCTION__,second);
  alarm_add_sec(auto_lock_alarm_index, second);
}


void auto_lock_alarm_stop()
{
	rt_err_t ret;
  if((ret=rt_alarm_stop(ALARM[auto_lock_alarm_index])) != RT_EOK)
  {
   	rt_kprintf("%s stop failed result: %d\n",__FUNCTION__,ret);	//因为开门超时后 再关门，闹钟已经执行完了，所以停止失败，不会有问题
  }
	else
	{
		rt_kprintf("%s: %p\n",__FUNCTION__,ALARM[auto_lock_alarm_index]);
	}
}
void long_time_open_lock_alarm_add()
{
	int	second= 60*ef_get_env_int("long_open_lock(m)");//自动落锁时间
	if(second==0){
		return;
	}
	rt_kprintf("%s add: %d (s)\n",__FUNCTION__,second);
  alarm_add_sec(long_time_open_lock_alarm_index, second);
}


void long_time_open_lock_alarm_stop()
{
	rt_err_t ret;
  if((ret=rt_alarm_stop(ALARM[long_time_open_lock_alarm_index])) != RT_EOK)
  {
		rt_kprintf("%s stop failed result: %d\n",__FUNCTION__,ret);	
  }
	else
	{
		rt_kprintf("%s: %p\n",__FUNCTION__,ALARM[long_time_open_lock_alarm_index]);
	}
}

uint8_t period_warninig_alram_flag=0;
static uint32_t warning_event_record=0;//对当前所有的警报进行记录；警报解除时需要对应位清零
void period_warninig_alarm_callback(void)
{
	if(warning_event_record==0)
	{
		return;
	}
//	net_data_send(SEND_MSG_DEV_HEARTBEAT,RT_NULL,RT_NULL);			//心跳
//	rt_thread_delay(rt_tick_from_millisecond(200));
	//周期警告，可以重新检测
	if((warning_event_record&HIGH_TEMP_PERIOD_ALRAM_EVT)||(warning_event_record&LOW_TEMP_PERIOD_ALRAM_EVT))
	{
		post_drv_ctr_thread_pro();//调度检查接口
		rt_kprintf(">>> temp alarm\n");
		net_send_event(SEND_MSG_TEMPERATURE_STATE_REPORT);
		rt_thread_delay(rt_tick_from_millisecond(200));
	}
	if((warning_event_record&HIGH_HUMIDITY_PERIOD_ALRAM_EVT)||(warning_event_record&LOW_HUMIDITY_PERIOD_ALRAM_EVT))
	{
		post_drv_ctr_thread_pro();//调度检查接口
		rt_kprintf(">>> humidity alarm\n");
		net_send_event(SEND_MSG_HUMIDITY_STATE_REPORT);
		rt_thread_delay(rt_tick_from_millisecond(200));
	
	}
	if(warning_event_record&ADXL_PERIOD_ALRAM_EVT)
	{
		post_drv_ctr_thread_pro();
		rt_kprintf(">>> adxl alarm\n");
		net_send_event(SEND_MSG_ADXL_STATE_REPORT);
		rt_thread_delay(rt_tick_from_millisecond(200));
	}
	if(warning_event_record&LONG_TIME_OPEN_LOCK_PERIOD_ALRAM_EVT){
		post_drv_ctr_thread_pro();
		rt_kprintf(">>> long time open lock alarm\n");
		net_send_event(SEND_MSG_LONG_TIME_OPEN_LOCK);
		rt_thread_delay(rt_tick_from_millisecond(200));
		
	}
	//rt_thread_delay(rt_tick_from_millisecond(100));
	period_warninig_alram_flag=0;
	if(warning_event_record)//正常情况不会为0
	{
		period_warninig_alram_flag=1;
		set_period_warn_alarm();		
	}
}
void period_warninig_alarm_try_add(uint32_t warning_event)
{
	if(NO_DEFENCE==get_defence()){
		rt_kprintf("no defence, needn't period warning");
		return ;
	}
	int second ;
	rt_enter_critical();
	warning_event_record|=warning_event;//产生的告警事件
	if(period_warninig_alram_flag==1)
	{
		rt_kprintf("period_warning_alarm had open\n");
		goto end_pro;
	}
	set_period_warn_alarm();
//	second= 60*ef_get_env_int("warning_period(m)");//开锁超时报警时间
//	if(second==0)
//	{
//		goto end_pro;
//	}
//	
//	alarm_add_sec(period_warninig_alram_index,second);
	period_warninig_alram_flag=1;
	end_pro:
	rt_exit_critical();
}

//清除报警事件时，需要对应的标记；当发现全部报警清除时才会停止alram;
void period_warninig_alarm_try_stop(uint32_t warning_event)
{
	//int second ;

	rt_enter_critical();
	
	if(warning_event_record&warning_event)
	{
		rt_kprintf("clear period_warning evnt:0x%x (0x%x) \n",warning_event,warning_event_record);
		if((warning_event&HIGH_HUMIDITY_PERIOD_ALRAM_EVT)||(warning_event&LOW_HUMIDITY_PERIOD_ALRAM_EVT))
		{
			rt_kprintf(">>> clear humidity alarm\n");
			//net_send_event(SEND_MSG_HUMIDITY_STATE_REPORT);				//可发送告警消除
			rt_thread_delay(rt_tick_from_millisecond(100));
		}
		if((warning_event&HIGH_TEMP_PERIOD_ALRAM_EVT)||(warning_event&LOW_TEMP_PERIOD_ALRAM_EVT))
		{
			rt_kprintf(">>> clear temp alarm\n");
			//net_send_event(SEND_MSG_TEMPERATURE_STATE_REPORT);
//			net_send_event(SEND_MSG_TEMPERATURE_STATE_REPORT);
			rt_thread_delay(rt_tick_from_millisecond(100));
		}
		if(warning_event&ADXL_PERIOD_ALRAM_EVT)
		{
			rt_kprintf(">>> clear adxl alarm\n");
			//net_send_event(SEND_MSG_ADXL_STATE_REPORT);
			rt_thread_delay(rt_tick_from_millisecond(100));
		}
		
	}
	else//已经清除过该警告
	{
		//rt_kprintf("had_clear_alarm_event\n");	
		goto end_pro;
	}
	
	warning_event_record&=(~warning_event);//清除告警事件
	
	if((period_warninig_alram_flag==0)||(warning_event_record))//如果还有剩余报警事件则不关闭
	{
		goto end_pro;
	}
	cancel_period_warn_alarm();
	//rt_alarm_stop(ALARM[period_warninig_alram_index]);
	period_warninig_alram_flag=0;
	
	end_pro:
	rt_exit_critical();


}

static void alarm_callback_entry(rt_alarm_t alarm, time_t timestamp)
{
		rt_kprintf("alarm_callback: %p\n",alarm);
		if(ALARM[period_check_index]==alarm)//周期检测的闹钟
		{
			rt_kprintf(">>> period check alarm\n");
			set_period_check_alarm();
			pm_release_timer(1000*LIFE_TIME);
//			gas_exit_lp();				//打开气体电源
		#ifdef USING_KEEP_LINK
      keep_link_timer_stop();		//防止周期性检测 频繁联网 发送心跳
		#endif
		}
    else if (ALARM[regular_heart_alarm_index] == alarm)
    {
			rt_kprintf(">>> regular heartbeat\n");
			set_life_alarm();
			pm_release_timer(1000*LIFE_TIME);
			net_send_event(SEND_MSG_DEV_HEARTBEAT);
			
    }
//		else if(ALARM[period_warninig_alram_index]==alarm)		//周期告警的闹钟
//		{
//			rt_kprintf(">>> period warn alarm\n");
//			pm_release_timer(1000*LIFE_TIME);
//			period_warninig_alarm_callback();
//			
//		}
	#ifdef USING_HTBKLOCK
		else if(ALARM[auto_lock_alarm_index] == alarm)
		{
			rt_kprintf(">>> auto lock alarm\n");
			rt_event_send(lock_event, LOCK_EVENT_CLOSE);
//			period_warninig_alarm_try_add(LONG_TIME_OPEN_LOCK_PERIOD_ALRAM_EVT);
		}
		else if(ALARM[long_time_open_lock_alarm_index] == alarm)
		{
			period_warninig_alarm_try_add(LONG_TIME_OPEN_LOCK_PERIOD_ALRAM_EVT);
			rt_kprintf(">>> long time open lock alarm\n");
//			period_warninig_alarm_try_add(LONG_TIME_OPEN_LOCK_PERIOD_ALRAM_EVT);
		}
	#endif
		
		
		
		
#ifdef USING_WATER_PRESSURE_SENSOR
	if(ALARM[period_water_pressure_index]==alarm)//周期检测水压的闹钟
	{	
		water_pressure_alarm_callback();
		return;
	}
#endif

}
uint8_t uart2_rx_exline_flag=0;
uint8_t uart3_rx_exline_flag=0;
void uart_enter_lp(void)
{
	#if 0
    //usart1
    SET_UP_INPUT(GET_PIN(A, 9));
   	SET_UP_INPUT(GET_PIN(A, 10));
    //usart2
    SET_UP_INPUT(GET_PIN(A, 2));
   	SET_UP_INPUT(GET_PIN(A, 3));
    //usart3
    SET_DOWN_INPUT(GET_PIN(B, 10));
    SET_DOWN_INPUT(GET_PIN(B, 11));
	#else
		GPIO_InitTypeDef GPIO_InitStruct = {0};
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_2;//发送
	 	HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);

#ifdef SUPP_ZLG52810
		GPIO_InitStruct.Pin = GPIO_PIN_10;//发送
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
		
		
		if((EXTI->IMR&LL_EXTI_LINE_3)==0)
		{
			rt_kprintf("ble_uart_rx_wakeup(PA3) set\n");				//周立功蓝牙 用串口接收中断唤醒单片机
			uart2_rx_exline_flag=1;
			GPIO_InitStruct.Pin = GPIO_PIN_3;
			GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING; 
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
			HAL_NVIC_SetPriority(EXTI3_IRQn, 1, 1);
			HAL_NVIC_EnableIRQ(EXTI3_IRQn);
//			GPIO_InitStruct.Pin = GPIO_PIN_3;
//			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//			GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
//			GPIO_InitStruct.Pull = GPIO_PULLUP;
//			HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//			//线中断配置
//			HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0); //抢占优先级为2，子优先级为3
//			HAL_NVIC_EnableIRQ(EXTI3_IRQn); 				//使能中断线
		}

		
#else
		GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_3;//接收
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
#endif
		/*获取调试口串口接收(PB11)所在线中断是否被使用，如果没有被使用，则用于外部中断线唤醒*/
		rt_kprintf("EXTI->IMR：0x%X\n",EXTI->IMR);
		if((EXTI->IMR&LL_EXTI_LINE_11)==0)
		{
			rt_kprintf("debug_uart_rx_wakeup(PB11) set\n");
			uart3_rx_exline_flag=1;
			GPIO_InitStruct.Pin = GPIO_PIN_11;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
			GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
			GPIO_InitStruct.Pull = GPIO_PULLUP;
			HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
			//线中断配置
			HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0); //抢占优先级为2，子优先级为3
			HAL_NVIC_EnableIRQ(EXTI15_10_IRQn); 				//使能中断线

			GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
			GPIO_InitStruct.Pin = GPIO_PIN_10;
			HAL_GPIO_Init(GPIOB,&GPIO_InitStruct);//调试口，常规不接收任何东西你
		}
		else
		{
			GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
			GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_10;
			HAL_GPIO_Init(GPIOB,&GPIO_InitStruct);//调试口，常规不接收任何东西你
		}
		
			
	#endif	
		
		//__HAL_RCC_GPIOC_CLK_DISABLE();
    //__HAL_RCC_GPIOA_CLK_DISABLE();
    //__HAL_RCC_GPIOB_CLK_DISABLE();
		/*
		UART_HandleTypeDef uart;

    uart.Instance = USART1;
    HAL_UART_MspDeInit(&uart);

    uart.Instance = USART2;
    HAL_UART_MspDeInit(&uart);

    uart.Instance = USART3;
    HAL_UART_MspDeInit(&uart);
		*/
}

void uart_exit_lp(void)
{
    UART_HandleTypeDef uart;

    uart.Instance = USART1;
    HAL_UART_MspInit(&uart);

    uart.Instance = USART2;
    HAL_UART_MspInit(&uart);

    uart.Instance = USART3;
    HAL_UART_MspInit(&uart);
    if(uart3_rx_exline_flag)
    {
    	uart3_rx_exline_flag=0;
    	EXTI->IMR&=~LL_EXTI_LINE_11;
    }
		if(uart2_rx_exline_flag)
    {
    	uart2_rx_exline_flag=0;
    	EXTI->IMR&=~LL_EXTI_LINE_3;
    }
}
//#define GET_PIN_STMCUBE(PORTx,PIN) (rt_base_t)((16*(((rt_base_t)PORTx -(rt_base_t)GPIOA)/(0x0400UL) ))+log2(PIN) )

uint16_t pin_low_pow_MASK_table[8];//指示不需要处理的引脚

uint16_t pin_low_pow_table[8];//指示已经处理的引脚//GPIOA,GPIOB,GPIOC,GPIOD,GPIOE,GPIOF,GPIOG,GPIOH

void clear_had_lp_pin_tabel(void)
{
	memset(pin_low_pow_table,0,sizeof(pin_low_pow_table));
}

void reuse_pin_warninig(char port_name,uint16_t pin)
{
	for(uint8_t i=0;i<16;i++)
	{
		if(pin&(1<<i))
		{
			rt_kprintf("#WARNING：PIN MAYBE REUSU (P%c%u)\n",port_name,i);
		}
	}
}

void had_lp_pin_print(char port_name,uint16_t pin,uint8_t forever_flag)
{
//	for(uint8_t i=0;i<16;i++)
//	{
//		if(pin&(1<<i))
//		{
//			rt_kprintf("#pin_lp:P%c%u\n",port_name,i);
//		}
//	}
}

/*标记已经进入低功耗的引脚; 输入参数举例；(GPIOA,GPIO_PIN_0|GPIO_PIN_2,forever_flag)

@forever_flag: 0/1; 0,本次进入低功耗，该引脚需要每次都需要设置；1指示该引脚不进行处理，初始化时设置一次即可；

如果运行中有切换引脚功能定义，建议使用：0，即每次进入都需要提示该引脚已低功耗处理*/
//#include <math.h>
void set_HalPin_had_lp(GPIO_TypeDef* gpio,uint16_t pin,uint8_t forever_flag)
{
	uint8_t num=0;
	if(pin==0)
	{
		return;
	}
	
	switch((uint32_t)(gpio))
	{
		case (uint32_t)GPIOA:
				had_lp_pin_print('A',pin,forever_flag);
				if(pin_low_pow_table[0]&pin)
				{
					//rt_kprintf("#WARNING：PIN MAYBE REUSU (PA%u)\n",log2(pin));
					reuse_pin_warninig('A',pin);
				}
				pin_low_pow_table[0]|=pin;
				num=0;
				break;
		
		case (uint32_t)GPIOB:
				had_lp_pin_print('B',pin,forever_flag);
				if(pin_low_pow_table[1]&pin)
				{
					reuse_pin_warninig('B',pin);
				}
				pin_low_pow_table[1]|=pin;
				num=1;
				break;
		case (uint32_t)GPIOC:
			had_lp_pin_print('C',pin,forever_flag);
			if(pin_low_pow_table[2]&pin)
				{
					reuse_pin_warninig('C',pin);
				}
				pin_low_pow_table[2]|=pin;
				num=2;
				break;
		
		case (uint32_t)GPIOD:
			had_lp_pin_print('D',pin,forever_flag);
			if(pin_low_pow_table[3]&pin)
				{
					reuse_pin_warninig('D',pin);
				}
				pin_low_pow_table[3]|=pin;
				num=3;
				break;
#ifdef GPIOE
		case (uint32_t)GPIOE:
			had_lp_pin_print('E',pin,forever_flag);
			if(pin_low_pow_table[4]&pin)
				{
					reuse_pin_warninig('E',pin);
				}
				pin_low_pow_table[4]|=pin;
				num=4;
				break;
#endif

#ifdef GPIOF
		case (uint32_t)GPIOF:
			had_lp_pin_print('F',pin,forever_flag);
			if(pin_low_pow_table[5]&pin)
				{
					reuse_pin_warninig('F',pin);
				}
				pin_low_pow_table[5]|=pin;
				num=5;
				break;
#endif
#ifdef GPIOG
		case (uint32_t)GPIOG:
				had_lp_pin_print('G',pin,forever_flag);
				if(pin_low_pow_table[6]&pin)
				{
					reuse_pin_warninig('G',pin);
				}
				pin_low_pow_table[6]|=pin;
				num=6;
				break;
#endif

#ifdef GPIOH
		case (uint32_t)GPIOH:
				had_lp_pin_print('G',pin,forever_flag);
				if(pin_low_pow_table[7]&pin)
				{
					reuse_pin_warninig('G',pin);
				}
				pin_low_pow_table[7]|=pin;
				num=6;
				break;
#endif



default:
	rt_kprintf("unknow port pin enter lp\n");
			num=100;
  break;
		
	}

	if(forever_flag&&(num<sizeof(pin_low_pow_table)/sizeof(pin_low_pow_table[0])))
	{
		pin_low_pow_MASK_table[num]|=pin;
	}
}

void set_RtPin_had_lp(rt_base_t pin,uint8_t forever_flag)
{
const	struct pin_index * gpio_info=get_pin(pin);
	if(gpio_info!=RT_NULL)
	{
		set_HalPin_had_lp(gpio_info->gpio,gpio_info->pin,forever_flag);
	}
}

void unuse_pin_enter_lp2(void)
{
	uint16_t pin_lp=0;
	/*未使用的引脚配置为模拟无上下拉输入*/
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;//GPIO_MODE_OUTPUT_PP 功耗一样
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate=0;
	rt_kprintf("\nAUTO LOWPOWER PIN(exti:%x)\n",EXTI->IMR);

	pin_lp=pin_low_pow_table[0]|pin_low_pow_MASK_table[0];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	had_lp_pin_print('A',GPIO_InitStruct.Pin,0);

	pin_lp=pin_low_pow_table[1]|pin_low_pow_MASK_table[1];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	had_lp_pin_print('B',GPIO_InitStruct.Pin,0);

	pin_lp=pin_low_pow_table[2]|pin_low_pow_MASK_table[2];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	had_lp_pin_print('C',GPIO_InitStruct.Pin,0);

	pin_lp=pin_low_pow_table[3]|pin_low_pow_MASK_table[3];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	had_lp_pin_print('D',GPIO_InitStruct.Pin,0);
	
#ifdef GPIOE
	pin_lp=pin_low_pow_table[4]|pin_low_pow_MASK_table[4];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
	had_lp_pin_print('E',GPIO_InitStruct.Pin,0);
#endif	

#ifdef GPIOF	
	pin_lp=pin_low_pow_table[5]|pin_low_pow_MASK_table[5];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);
	had_lp_pin_print('F',GPIO_InitStruct.Pin,0);
#endif

#ifdef GPIOG
	pin_lp=pin_low_pow_table[6]|pin_low_pow_MASK_table[6];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
	had_lp_pin_print('G',GPIO_InitStruct.Pin,0);
#endif

#ifdef GPIOH
	pin_lp=pin_low_pow_table[7]|pin_low_pow_MASK_table[7];
	GPIO_InitStruct.Pin=~pin_lp;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	had_lp_pin_print('H',GPIO_InitStruct.Pin,0);
#endif
	memset(pin_low_pow_table,0,sizeof(pin_low_pow_table));

	rt_kprintf("\nAUTO LOWPOWER PIN END(%u)\n",sizeof(pin_low_pow_table));

	
}

void unuse_pin_enter_lp(void)
{
		GPIO_InitTypeDef GPIO_InitStruct = {0};
		
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate=0;


#if		0
		//SET_OUTPUT(THRH_POWER_ON_PIN,PIN_HIGH);//关闭温湿度传感器电源
	//	SET_UP_INPUT(THRH_POWER_ON_PIN);//已经在drv_ctr 中进入退出
		SET_UP_INPUT(GET_PIN(A, 8));	//干簧锁
#else		
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate=0;
	/*

		GPIO_InitStruct.Pin=GPIO_PIN_3;//THRH_POWER_ON_PIN
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);//THRH_POWER_ON_PIN
*/
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pin=GPIO_PIN_8;//干簧锁
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);//干簧锁
		
#endif



#if 0//lkk 模拟输入
		
		SET_DOWN_INPUT(GET_PIN(A, 0));
		SET_DOWN_INPUT(GET_PIN(A, 1));
	//	SET_OUTPUT(GET_PIN(A, 0),PIN_HIGH);//湿度iic sda; 18b20 
    //SET_OUTPUT(GET_PIN(A, 1),PIN_HIGH);//湿度iic scl

		SET_DOWN_INPUT(GET_PIN(A, 4));//模拟量输入
		SET_DOWN_INPUT(GET_PIN(A, 5));//模拟量输入
	

		SET_DOWN_INPUT(GET_PIN(A, 6));//通讯控制
		SET_DOWN_INPUT(GET_PIN(A, 7));//通讯控制

		SET_DOWN_INPUT(GET_PIN(A, 13));//JTCK
		SET_DOWN_INPUT(GET_PIN(A, 14));//JTCK

#else	
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate=0;
		//GPIO_InitStruct.Pin=GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_13|GPIO_PIN_14;
		GPIO_InitStruct.Pin=GPIO_PIN_1|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_13|GPIO_PIN_14;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

/*
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pin=GPIO_PIN_4|GPIO_PIN_5;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_SET);
*/
		
#endif

#if 0

		SET_DOWN_INPUT(GET_PIN(B, 6));//时钟芯片，外部上拉//当前未焊接
		SET_DOWN_INPUT(GET_PIN(B, 7));//时钟芯片，外部上拉//当前未焊接
		
		SET_DOWN_INPUT(GET_PIN(B, 9));//蜂鸣器


		SET_DOWN_INPUT(GET_PIN(C, 13));//电机下拉、水浸上拉
		SET_DOWN_INPUT(GET_PIN(C, 14));//电机下拉、开关量2
		SET_DOWN_INPUT(GET_PIN(C, 15));//开关量1

		SET_DOWN_INPUT(GET_PIN(C, 0));//井盖外部上拉//当前未焊接
		SET_DOWN_INPUT(GET_PIN(C, 1));//井盖外部上拉//当前未焊接
		SET_DOWN_INPUT(GET_PIN(C, 2));//井盖外部上拉//当前未焊接
#else

		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;//上面暂时未焊接
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate=0;

		GPIO_InitStruct.Pin=GPIO_PIN_9;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
		
		GPIO_InitStruct.Pull = GPIO_PULLUP;//外部已上拉
		GPIO_InitStruct.Pin=GPIO_PIN_6|GPIO_PIN_7;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


		//GPIO_InitStruct.Pin=GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
			GPIO_InitStruct.Pin=GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);



		
		 //SystemClock_Config_LSI();//内部晶振，配置的16M
	#if	1// 使用外部晶振时，可以停止外部晶振的功耗10uA;也可以晶振处焊接2M电阻降低同样功耗
		 GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		 GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		 GPIO_InitStruct.Pin=GPIO_PIN_0|GPIO_PIN_1;//晶振未焊接
		 HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	#endif	
	
#endif
		//蓝牙功耗测试
		//SET_OUTPUT(BLUE_TOOTH_MODE_PIN,1);		
		//SET_OD_OUTPUT(BLUE_TOOTH_STA_PIN,PIN_HIGH);
}
void unuse_pin_exit_lp(void)
{
	
	  __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
	  __HAL_RCC_GPIOD_CLK_ENABLE();
	  __HAL_RCC_GPIOH_CLK_ENABLE();

}

void peripheral_enter_quit_lp(uint8_t enter1_quit0)
{

	if(enter1_quit0)
	{
		//__HAL_RCC_ADC1_CLK_DISABLE();
		
	//	__HAL_RCC_SPI1_CLK_DISABLE();
	//	__HAL_RCC_SPI2_CLK_DISABLE();
	//	__HAL_RCC_SPI3_CLK_DISABLE();
		
//		Stop_Dog();//先一直不打开看门狗
	}
	else
	{
		
	//	__HAL_RCC_ADC1_CLK_ENABLE();
	//	__HAL_RCC_SPI1_CLK_ENABLE();
	//	__HAL_RCC_SPI2_CLK_ENABLE();
	//	__HAL_RCC_SPI3_CLK_ENABLE();

	}
	

}

void flash_exit_lp(void)
{
		rt_device_t p_device;
		uint8_t data,res;

	
   	SET_OUTPUT(W25Q_CS_PIN, 0);   //CS_W 选中
    SET_OUTPUT(W25Q_WP_PIN, 1);
    SET_OUTPUT(W25Q_HOLD_PIN, 1);

    SPI_HandleTypeDef hspi;
    hspi.Instance = SPI1;
    HAL_SPI_MspInit(&hspi);

		data=0xAB;
		p_device=rt_device_find("spi10");
		rt_spi_transfer((struct rt_spi_device* )p_device,&data,&res,1);
		//SET_OUTPUT(W25Q_CS_PIN, 1);   //CS_W 不选中
		
}

void flash_enter_lp(void)
{
		rt_device_t p_device;
		uint8_t data,res;
		data=0xb9;
		p_device=rt_device_find("spi10");
		rt_spi_transfer((struct rt_spi_device* )p_device,&data,&res,1);

    /*
    PB3     ------> SPI1_SCK
    PA11  ------> SPI1_MISO
    PA12  ------> SPI1_MOSI
    */
    //lkk delete 2020-4-27 早前板子是断电方式进入低功耗，当前进入的是power_down模式 在此不需要拉低
#if 0
    SET_OUTPUT(W25Q_WP_PIN, 0);
    SET_OUTPUT(W25Q_HOLD_PIN, 0);
    SET_OUTPUT(GET_PIN(B, 3), 0);
    SET_OUTPUT(GET_PIN(A, 11), 0);
    SET_OUTPUT(GET_PIN(A, 12), 0);
    
  	SET_OUTPUT(W25Q_CS_PIN, 1);   //CS_W 不选中
#else
		SET_UP_INPUT(W25Q_CS_PIN); 	//CS_W 不选中
		SET_DOWN_INPUT(W25Q_WP_PIN); //以下配置测试时管脚为0电平，没有灌入电流
		SET_DOWN_INPUT(W25Q_HOLD_PIN);
		
		SET_DOWN_INPUT(W25Q_SPI_CLK_PIN);//clk
		SET_UP_INPUT(W25Q_SPI_MISO_PIN);
		SET_DOWN_INPUT(W25Q_SPI_MOSI_PIN);

		set_RtPin_had_lp(W25Q_CS_PIN,0);
		set_RtPin_had_lp(W25Q_WP_PIN,0);
		set_RtPin_had_lp(W25Q_HOLD_PIN,0);
		set_RtPin_had_lp(W25Q_SPI_CLK_PIN,0);
		set_RtPin_had_lp(W25Q_SPI_MISO_PIN,0);
		set_RtPin_had_lp(W25Q_SPI_MOSI_PIN,0);
		
#endif

}


extern void adxl362_enter_quit_lp(uint8_t enter1_quit0_flag);

#include "radar_app.h"
#include "light_sens.h"
#include "gas_app.h"

//uint8_t reconnect_flag=0;
static void notify_enter(uint8_t event, uint8_t mode, void *data)
{
    if (event == 0 && mode == PM_SLEEP_MODE_DEEP)               //进入休眠
    {
    		rt_kprintf("enter low power\n\n\n");
				{
//					SET_UP_INPUT(W25Q_CS_PIN); 	//CS_W 不选中
//					set_RtPin_had_lp(W25Q_CS_PIN,0);
					
				}
						
				/* 不起作用，adc1继续耗电
				RCC->APB2LPENR=0x00000000U;
				RCC->APB1LPENR=0U;
				RCC->AHBLPENR=0x01019000U;
				*/
//				set_life_alarm();
				//ble_enter_lp();
       	//adxl362_enter_quit_lp(1);
				net_enter_lp();
				//radar_enter_lp();
				//light_sens_enter_lp();
				soft_uart_lowpower_ctr(1);
				flash_enter_lp();
				//uart_enter_lp();                //导致蓝牙串口无法唤醒
//				gas_enter_lp();
				peripheral_enter_quit_lp(1);
				all_drv_enter_lp();
				unuse_pin_enter_lp2();//串口引脚，在XX_hal_msp中指示了不进行低功耗处理
				uart_enter_lp();                //导致蓝牙串口无法唤醒
				
				//flash_enter_lp();           //因为LOG_D 会操作flash，所以要在所有LOG_D执行后
        stop_flag |= EVENT_LOW_POWER_ENTER;
    }
    if (event == 1 && mode == PM_SLEEP_MODE_DEEP)               //退出休眠
    {
        stop_flag &= ~EVENT_LOW_POWER_ENTER;				
				unuse_pin_exit_lp();
//				net_exit_lp();
				//uart_exit_lp();
				peripheral_enter_quit_lp(0);
				flash_exit_lp();
				//radar_exit_lp();
				//light_sens_exit_lp();
				soft_uart_lowpower_ctr(0);
			 	//adxl362_enter_quit_lp(0);
				//ble_exit_lp();
				//gas_exit_lp();
			  all_drv_exit_lp();    
				pm_release_timer(1000 * LIFE_TIME);				//控制进入低功耗
				
//				if(connect_type == AT_CONNECT_TCP)
//					rt_event_send(net_event, NET_EVENT_RECONNECT);			//4G因为休眠期间关闭，唤醒必须重连
			
				if(net_event!=NULL)
					rt_event_send(net_event, NET_EVENT_CONNECT);			//如果之前进入通讯ERROR 就重连接
				rt_kprintf("exit low power\n");
				
    }
}

static uint8_t release_timer_run_flag=0;

static rt_timer_t pm_auto_timer = RT_NULL;
static void pm_auto_timer_callback(void *parameter)
{
    //rt_pm_release(PM_SLEEP_MODE_NONE);
		rt_enter_critical();
		release_timer_run_flag=0;
		rt_exit_critical();
		//rt_kprintf("apply_run_over\n");
		rt_pm_release(PM_SLEEP_MODE_NONE);
}

rt_timer_t pm_release_timer(int timeout)//申请最低的正常运行时间
{

		rt_enter_critical();
		
		if(pm_auto_timer&&release_timer_run_flag)//正在运行中，判断是否有必要重新配置；只能向更多时间进行配置
		{
			rt_tick_t now=rt_tick_get();
			
			/*计算定时器剩余的超时时间*/
			if(pm_auto_timer->timeout_tick>=now)
			{
				now=pm_auto_timer->timeout_tick-now;
			}
			else
			{
				now=RT_TICK_MAX-now+pm_auto_timer->timeout_tick;
			}
				/*确认是否需要重新配置定时器*/
			if((now>2)&&(now>timeout))
			{
				rt_exit_critical();
				return pm_auto_timer;
			}
		}		
    if(release_timer_run_flag==0)
		{
			rt_pm_request(PM_SLEEP_MODE_NONE);
			//rt_kprintf("apply_run_tick:%u",timeout);
    }
		if (pm_auto_timer != RT_NULL)
    {
				rt_timer_stop(pm_auto_timer);
				rt_enter_critical();
				release_timer_run_flag=1;
				rt_exit_critical();
        rt_timer_control(pm_auto_timer, RT_TIMER_CTRL_SET_TIME, (void *)&timeout);
				//rt_timer_control(pm_auto_timer, RT_TIMER_CTRL_SET_ONESHOT, (void *)&timeout);
        rt_timer_start(pm_auto_timer);

    }
    else
    {
				rt_enter_critical();
				release_timer_run_flag=1;
				rt_exit_critical();
        pm_auto_timer = rt_timer_create("pm_deep",
                                        pm_auto_timer_callback,
                                        RT_NULL,
                                        rt_tick_from_millisecond(timeout),
                                        RT_TIMER_FLAG_ONE_SHOT);
        rt_timer_start(pm_auto_timer);
    }				
		rt_exit_critical();
    return pm_auto_timer;
}




static void sleep_timer(void *parameter)
{
		//rt_kprintf(">>>>> car_run_time: %d\n",ef_get_env_int("car_run_time"));
		ef_set_env_int("car_run_time",ef_get_env_int("car_run_time")+1);
}

  static rt_timer_t force_sleep_timer = RT_NULL;

static void force_sleep_timer_stop(void)
{
	//ef_set_env("car_run_time","0");	
	if(force_sleep_timer!=RT_NULL){
		//rt_kprintf(">>>>> stop force sleep timer\n");	
		rt_timer_stop(force_sleep_timer);
	}
}

rt_timer_t force_sleep_timer_start(int timeout)
{
 
		if (force_sleep_timer != RT_NULL)
    {
        rt_timer_control(force_sleep_timer, RT_TIMER_FLAG_PERIODIC, (void *)&timeout);
        rt_timer_start(force_sleep_timer);
    }
    else
    {
        force_sleep_timer = rt_timer_create("force_sleep_timer",
                                        sleep_timer,
                                        RT_NULL,
                                        rt_tick_from_millisecond(timeout),
                                        RT_TIMER_FLAG_PERIODIC);
        rt_timer_start(force_sleep_timer);
    }
    return force_sleep_timer;
}

static int alarm_init(void)
{
    int dump = 0;
    rt_pm_notify_set(notify_enter, &dump);
    rt_memset(&ALARM, NULL, sizeof(ALARM));
    rt_alarm_system_init();
    return 0;
}

INIT_APP_EXPORT(alarm_init);

rt_err_t alarm_stop(uint8_t alarm_index)
{
	if(alarm_index<alarm_max_num)
	{
		return rt_alarm_stop(ALARM[alarm_index]);
	}
	return RT_EINVAL;
}

void alarm_add_sec(uint8_t alarm_num, int sec)
{

    time_t timestamp;

    struct tm stm_temp;
    timestamp = time(RT_NULL);
    //rt_kprintf("current time :%s", ctime(&timestamp));
    timestamp += sec;
    //rt_kprintf("Alarm(%d) time:%s", alarm_num, ctime(&timestamp));
    localtime_r(&timestamp, &stm_temp);
    //stm_temp->tm_year+=100;
    //stm_temp->tm_wday-=1;
    //stm_temp->tm_mon-=1;
    //rt_kprintf("\nAlarm time \nDate: %d-%d-%d Weak: %d \nTime: %d:%d:%d\n------>\n",stm_temp->tm_year,stm_temp->tm_mon,stm_temp->tm_mday,stm_temp->tm_wday,stm_temp->tm_hour,stm_temp->tm_min,stm_temp->tm_sec);

    struct rt_alarm_setup _alarm;
    _alarm.flag = RT_ALARM_ONESHOT; /* ???? */
    rt_memset(&_alarm.wktime, RT_ALARM_TM_NOW, sizeof(struct tm));

    rt_memcpy(&_alarm.wktime, &stm_temp, sizeof(struct tm));
    if (ALARM[alarm_num] == RT_NULL)
    {
        ALARM[alarm_num] = rt_alarm_create(alarm_callback_entry, &_alarm);
				if(ALARM[alarm_num]==RT_NULL)
				{
					rt_kprintf("alarm_creat fail:%u\n",alarm_num);
					return;
				}
    }
    else
    {
        if (rt_alarm_control(ALARM[alarm_num], RT_ALARM_CTRL_MODIFY, &_alarm) != RT_EOK)
        {
            LOG_E("alarm control set failed \n");
						return;
        }
    }
    if(rt_alarm_start(ALARM[alarm_num])!=RT_EOK)
    {
			rt_kprintf("---> alarm start fail:%u\n",alarm_num);
			return;
		}
		rt_kprintf("alarm start ok (%u--0x%p) sec:%u\n",alarm_num,ALARM[alarm_num],sec);
}

static void alarm_add(int argc, char **argv)
{
    struct rt_alarm_setup _alarm;
    if (argc == 3)
    {
        _alarm.flag = RT_ALARM_ONESHOT; /* ???? */
        rt_memset(&_alarm.wktime, RT_ALARM_TM_NOW, sizeof(struct tm));
        _alarm.wktime.tm_hour = atol(argv[1]);
        if (argc > 2)
        {
            _alarm.wktime.tm_min = atol(argv[2]);
            _alarm.wktime.tm_sec = 0;
        }
        if (ALARM[0] == NULL)
        {
            ALARM[0] = rt_alarm_create(alarm_callback_entry, &_alarm);
        }
        else
        {
            rt_alarm_control(ALARM[0], RT_ALARM_CTRL_MODIFY, &_alarm);
        }
        rt_alarm_start(ALARM[0]);
    }
    else if (argc == 2)
    {
        alarm_add_sec(0, atol(argv[1]));
    }
}

MSH_CMD_EXPORT(alarm_add, e.g: alarm_add 12 15);
