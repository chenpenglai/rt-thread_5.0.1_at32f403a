#ifndef __O2_app_H__
#define __O2_app_H__

#ifdef USING_O2
#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif
#include "global_define.h"
#include "drv_ctrl.h"
/******			电源控制、串口 引脚			********/
//#define OPEN_ULTRASONIC_POW() 	SET_OUTPUT(ULTRASONIC_POW_PIN,PIN_LOW)	//打开该外设电源
//#define CLOSE_ULTRASONIC_POW() 	SET_OUTPUT(ULTRASONIC_POW_PIN,PIN_HIGH)	//关闭该外设电源
#define SUART1_RX_PIN 					GET_PIN(C,5)
#define SUART1_TX_PIN 					GET_PIN(C,4)

#define RX_BUF_SIZE 20  //一帧数据20字节

typedef struct
{
    rt_device_t dev;
    uint8_t frame_buf[RX_BUF_SIZE];
    uint8_t rx_len;
    uint8_t rx_total;//为了不用接受一个字节就启动线程进行一次操作
} SUART_CTR_STR;

int o2_init(void);
int o2_handler(void *para);
int o2_ctrl(o2_ctrl_cmd cmd, void *arg);
void o2_enter_lp(void);
void o2_exit_lp(void);
int o2_get_val();
#endif
#ifdef __cplusplus
#if __cplusplus
}
#endif

#endif
#endif

