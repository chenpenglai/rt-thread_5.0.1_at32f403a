#include "string.h"
#include "stdio.h"
#include "global_define.h"
#include "O2_app.h"
#include "drv_ctrl.h"
#include "Tool.h"
#include "power_app.h"
#include "drv_soft_uart.h"

#ifdef USING_O2
#define SUART_USE_DEVICE_NAME "suart2"
SUART_CTR_STR suart_ctr;


#define read_o2_data "read O2 Data"
#define auto_o2_data "auto O2 Data"
//返回内容：o2=20.8%

static rt_err_t suart_rx_ind(rt_device_t dev, rt_size_t size)//中断中处理数据
{
    suart_ctr.rx_total+=size;
    if(suart_ctr.rx_total>RX_BUF_SIZE)
    {
        //rt_kprintf("suart_rx_ind cb\n");
        //通知处理一帧数据
        post_drv_ctr_thread_pro();
    }
    return RT_EOK;
}
#define o2_acq_period_second  5  //O2的采集周期
static sys_tick_t o2_period_chk_cnt = 0;
int o2_thre=0;
int o2_val=0;
uint32_t o2_warn_state;
int o2_get_val() {
    return o2_val;

}

int o2_handler(void *para)//线程处理函数
{
    SUART_CTR_STR* p_ctr=&suart_ctr;
    uint8_t read_len;
    int res=NO_REPORT;
    if ((tick_had_gone(o2_period_chk_cnt) < SEC_TO_TICK(o2_acq_period_second)) && (o2_period_chk_cnt)) //没到采集时间
    {
        return res;
    }
    o2_period_chk_cnt = get_sys_tick();
    //rt_kprintf("O2 run...\n");
		
		o2_thre=ef_get_env_int("o2_thre");
//		OPEN_GAS_POWER();
		//rt_thread_delay(rt_tick_from_millisecond(1000));
    do
    {
				rt_device_write(p_ctr->dev, 0, read_o2_data, strlen(read_o2_data));
        read_len=rt_device_read(p_ctr->dev,0,p_ctr->frame_buf,RX_BUF_SIZE);
        if(read_len)
        {
            //rt_kprintf("O2 read: %s\n",p_ctr->frame_buf);
            sscanf(p_ctr->frame_buf,"o2=%d\%",&o2_val);
            if(o2_val<o2_thre&&o2_warn_state!=REPORT_O2_LOW_WARNING) {
                o2_warn_state=REPORT_O2_LOW_WARNING;
								set_gas_state(GAS_O2,GAS_WARNING);
                res=REPORT_O2_LOW_WARNING;
								
                rt_kprintf("o2 warn\n");
            } else if(o2_val>=o2_thre&&o2_warn_state==REPORT_O2_LOW_WARNING) {
                o2_warn_state=REPORT_O2_LOW_CLEAR;
                res=REPORT_O2_LOW_CLEAR;
								set_gas_state(GAS_O2,GAS_NORMAL);
                rt_kprintf("o2 warn clear\n");
            }
            rt_kprintf("o2_val: %d\%\n",o2_val);
//						CLOSE_GAS_POWER();
            return res;
        }
    } while(read_len);
//		CLOSE_GAS_POWER();
    return res;
}
int o2_ctrl(o2_ctrl_cmd cmd, void *arg)
{
    switch (cmd)
    {

    case CTRL_GET_O2_VAL:
    {
        int *tmp = (int *)arg;
        *tmp = o2_get_val();
    }
    break;
		case CTRL_GET_O2_STATE:
		{
			   uint32_t *tmp = (uint32_t *)arg;
        *tmp = o2_warn_state;
		}
		break;
    case CTRL_SET_LOW_O2_THRE:
    {
        ef_set_env_int("o2_thre",*(int *)arg);
    }
    break;
    case CTRL_GET_LOW_O2_THRE:
    {
        int *tmp = (int *)arg;
        *tmp = ef_get_env_int("o2_thre");
    }
    break;

    default:
        break;
    }
    return RT_EOK;
}
int o2_init(void)
{

    rt_err_t err;
    struct serial_configure dev_cfg=RT_SERIAL_CONFIG_DEFAULT;
    dev_cfg.baud_rate=9600;
    dev_cfg.data_bits=DATA_BITS_8;
    dev_cfg.parity=PARITY_NONE;
    dev_cfg.stop_bits=STOP_BITS_1;
    dev_cfg.bufsz=RX_BUF_SIZE;//一帧数据就4个字节

    /*打开串口设备*/
    suart_ctr.dev=rt_device_find(SUART_USE_DEVICE_NAME);
    RT_ASSERT(suart_ctr.dev);

    err=rt_device_control(suart_ctr.dev, RT_DEVICE_CTRL_CONFIG, &dev_cfg);
    if(err!=RT_EOK)
    {
        rt_kprintf("uart config err\n");
    }

    /*设置接收回调*/
    rt_device_set_rx_indicate(suart_ctr.dev,suart_rx_ind);

    err=rt_device_open(suart_ctr.dev, RT_DEVICE_FLAG_INT_RX|RT_DEVICE_FLAG_INT_TX);
    if(err!=RT_EOK)
    { 
        rt_kprintf("%s open error",suart_ctr.dev->parent.name);
        return 0;
    }
    return 1;
//    rt_thread_t pro_thread = NULL;
//    pro_thread = rt_thread_create("suart_handler", suart_handler, NULL,200, 25, 3000); //延迟启动 设备时间
//    if (pro_thread != NULL)
//    {
//        rt_thread_startup(pro_thread);
//    }
//    else
//    {
//        rt_kprintf("create pro_deal_thread is faild .\n");
//    }

}



void o2_enter_lp(void)
{
//    set_RtPin_had_lp(ULTRASONIC_POW_PIN, 0);    //自动休眠 忽略这个脚
//		SET_OUTPUT(ULTRASONIC_UART_TX_PIN,0);
//		set_RtPin_had_lp(ULTRASONIC_UART_TX_PIN, 0);
    SET_OUTPUT(SUART1_RX_PIN,0);
		set_RtPin_had_lp(SUART1_RX_PIN, 0);		//串口会被自动恢复
	
//		CLOSE_GAS_POWER();
//		set_RtPin_had_lp(PW_WP_PIN, 0);
}
void o2_exit_lp(void)
{
    //OPEN_ULTRASONIC_POW();
    //key_pin_gpio_init();

}
#endif
