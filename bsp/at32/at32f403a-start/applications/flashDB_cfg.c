#include <stdio.h>
#include <board.h>
#include <flashdb.h>
#include <at32f403a_407.h>
#include <rtthread.h>
#define FDB_LOG_TAG "[main]"

static uint32_t boot_count = 0;
/* default KV nodes */
static struct fdb_default_kv_node default_kv_table[] = {
				/*********** 微断网关 系统参数 ***********/
				{"device_id", "666666666666"},				//网关编号=网关地址
				{"soft_ver", SOFTWARE_VER},
				{"hard_ver", HARDWARE_VER},
				{"sn",				"DC2291140000"},				//默认sn 加西亚需要 
				/********** 网络通讯参数 **********/
		//    {"enable_dhcp", "0"},										//DHCP功能，0：关闭（默认），1：打开
				{"heart_time_s", "60"},				//心跳时间，单位：秒
		//    {"net_mode", "TCP_CLIENT"},			//TCP_SERVER：TCP服务端， TCP_CLIENT：TCP客户端
				
		//		{"net_mode", "WIFI"},					//已废弃
				{"net_module", "AUTO"},			//网络模组选择 (AUTO WIFI 4G ETH)
				{"net_type", "MQTT"},			//用于建立何种连接 MQTT：连接MQTT服务器 ，TCP_SERVER：TCP服务端， TCP_CLIENT：TCP客户端  
				
				{"state","0"},
		//    {"remote_ip", "192.168.0.100"},			//
		//    {"remote_port", "10000"},
				
				{"remote_ip", "mqtt.gacia.cn"},					//加西亚 微断mqtt平台
				{"remote_port", "1883"},
				{"IMEI", "0"},
				{"ICCID", "0"},
				{"CSQ", "0"},

				 //{"adj_time_tm", "3600"},									//设备校时间隔，单位：秒	
				/************			MQTT 参数		************/
				
				{"client_id","666666666666"},			//想上线就改成：666666666666
				{"username","gateway"},
				{"password","666666666666"},				//想上线就改成：666666666666
				
				 
				/*
					升级文件下载地址
					http://182.254.214.242:83/download/update.bin
				*/
				{"is_up","0"},
				{"update_ip", "182.254.214.242"},										//升级主机地址
				{"update_port", "83"},															//升级端口
				{"file_path","/iot-light-update/4g/firmware.bin"},								//升级文件URL路径，通过http下载
				{"file_chk","0"},		//文件的CRC校验

				{"usart_baud","38400"},			//串口波特率
				{"poll_time_s","600"},		//轮询时间 单位：秒
				{"chk_online_time_s","180"},		//轮询时间 单位：秒
		//		{"enterprise","201"},		
		//		{"protect_slv_addr","1"},
				//{"is_decode","0"},				//是否为解码器，是的话 将订阅解码主题
		//		{"remote_decode","0"},		//1：远程解码设备，0：本地直接解码设备
				//{"ctrl_e_code","0"},			//场景控制，使用的企业代码
				
		//		{"keepAlive(s)","10"},
		//		{"period_warninig(s)","15"},
				{"boot_times","0"},				//设备的重启次数记录
				{"longitude", "0"},		//经度
				{"latitude", "0"},		//维度
				{"get_gps_num", "0"},		//经纬度获取次数，超过10次直接上线，不然将导致离线
//				{"restore_para_n","0"},
				{"wifi_ssid","hello"},
				{"wifi_pwd","13587575423"},
				
				{"enable_dhcp", "0"},										//DHCP功能，0：关闭（默认），1：打开
		//    {"net_mode", "TCP_CLIENT"},			//TCP_SERVER：TCP服务端， TCP_CLIENT：TCP客户端
		//		{"net_type", "LAN"},
				{"local_ip", "192.168.0.9"},					//未开启DHCP时，设备自身配置的ip。开启了DHCP，获取ip后自动填充
				{"local_port", "8802"},								//TCP服务端 监听端口
				
				{"subnet_mask","255.255.255.0"},
				{"gateway_addr", "192.168.0.1"},				//IP网关地址
				
				{"dns_ip", "8.8.8.8"},					//未开启DHCP时，设备自身配置的ip。开启了DHCP，获取ip后自动填充
				{"dns_port", "53"},								//TCP服务端 监听端口
				
				//{"usart_cfg_brk","18n1fpu1a8p+"},			//断路器串口配置
				{"usart_baud_brk","38400"},						//断路器串口波特率
				
				//{"usart_cfg_645","18e1fpu1a8p+"},			//645协议串口配置
				{"usart_baud_645","19200"},						//645协议 串口波特率
				
		//	{"listen_ip","192.168.1.200"},					//开启了DHCP，获取ip后自动填充
		//	{"listen_port","8802"},
	

				
				{"boot_count",&boot_count,4},
				/*********** 多合一网关 网络参数 ***********/
				
				//{"file_update", "1"},
			#ifdef USE_NEW_UPDATE
				 /*****			断路器 升级包下载参数			*****/
				{"brk_ip", "115.159.144.115"},												//断路器升级主机地址
				{"brk_port", "83"},																		//断路器升级端口
				{"brk_file_path","/iot-light-update/4g/GACIA_BRK_2P_4G.bin"},		//断路器升级文件URL路径，通过http下载
				{"brk_md_tm","0"},		//断路器升级包修改时间
				
				//升级包参数
				{"brk_file_size","0"},				//升级包大小，单位：字节
				{"brk_have_file","0"},				//是否存在 断路器升级包 0：不存在， 1：存在
			#endif
				{"rccb_opt_coed","00000000"},			//上位机设置的 操作者代码（用于添加在 ctrl write 命令前部）
				{"rccb_addr","000000000001"},	//rccb 地址
				{"alr_read","1"},	// 
};

/* KVDB object */
struct fdb_kvdb _global_kvdb = { 0 };
/* TSDB object */
struct fdb_tsdb _global_tsdb = { 0 };
/* counts for simulated timestamp */
static int counts = 0;

extern void kvdb_basic_sample(fdb_kvdb_t kvdb);
extern void kvdb_type_string_sample(fdb_kvdb_t kvdb);
extern void kvdb_type_blob_sample(fdb_kvdb_t kvdb);
extern void tsdb_sample(fdb_tsdb_t tsdb);
void bootCount(fdb_kvdb_t kvdb);
static void lock(fdb_db_t db)
{
		//__disable_irq();
   // rt_enter_critical();
}

static void unlock(fdb_db_t db)
{
	//__enable_irq();
   // rt_exit_critical();
}

static fdb_time_t get_time(void)
{
    /* Using the counts instead of timestamp.
     * Please change this function to return RTC time.
     */
	  time_t timestamp;
    timestamp = time(RT_NULL);
    return timestamp ;
}
//
int flashDB_cfg(void)
{
    fdb_err_t result;

#ifdef FDB_USING_KVDB
    { /* KVDB Sample */
        struct fdb_default_kv default_kv;
  			
        default_kv.kvs = default_kv_table;
        default_kv.num = sizeof(default_kv_table) / sizeof(default_kv_table[0]);
        /* set the lock and unlock function if you want */
         fdb_kvdb_control(&_global_kvdb, FDB_KVDB_CTRL_SET_LOCK, lock);
         fdb_kvdb_control(&_global_kvdb, FDB_KVDB_CTRL_SET_UNLOCK, unlock);
        /* Key-Value database initialization
         *
         *       &kvdb: database object
         *       "env": database name
         * "fdb_kvdb1": The flash partition name base on FAL. Please make sure it's in FAL partition table.
         *              Please change to YOUR partition name.
         * &default_kv: The default KV nodes. It will auto add to KVDB when first initialize successfully.
         *        NULL: The user data if you need, now is empty.
         */
				//_global_kvdb.ver_num = 1;
        result = fdb_kvdb_init(&_global_kvdb, "env", "kv1", &default_kv, NULL);
				
        if (result != FDB_NO_ERR) {
            return -1;
        }
 
        bootCount(&_global_kvdb);
				
//        /* run string KV samples */
//        kvdb_type_string_sample(&_global_kvdb);
//        /* run blob KV samples */
//        kvdb_type_blob_sample(&_global_kvdb);
    }
#endif /* FDB_USING_KVDB */

#ifdef FDB_USING_TSDB
    { /* TSDB Sample */
        /* set the lock and unlock function if you want */
        fdb_tsdb_control(&_global_tsdb, FDB_TSDB_CTRL_SET_LOCK, lock);
        fdb_tsdb_control(&_global_tsdb, FDB_TSDB_CTRL_SET_UNLOCK, unlock);
			
        /* Time series database initialization
         *
         *       &tsdb: database object
         *       "log": database name
         * "fdb_tsdb1": The flash partition name base on FAL. Please make sure it's in FAL partition table.
         *              Please change to YOUR partition name.
         *    get_time: The get current timestamp function.
         *         128: maximum length of each log
         *        NULL: The user data if you need, now is empty.
         */
			
        result = fdb_tsdb_init(&_global_tsdb, "record", "ts1", get_time, 128, NULL);
        /* read last saved time for simulated timestamp */
        fdb_tsdb_control(&_global_tsdb, FDB_TSDB_CTRL_GET_LAST_TIME, &counts);
				
        if (result != FDB_NO_ERR) {
            return -1;
        }
			
        /* run TSDB sample */
        //tsdb_sample(&_global_tsdb);
    }
#endif /* FDB_USING_TSDB */
	// fdb_kv_print(&_global_kvdb);
    return 0;
}

void bootCount(fdb_kvdb_t kvdb)
{
	  struct fdb_blob blob;
    int boot_count = 0;

    { /* GET the KV value */
        /* get the "boot_count" KV value */
        fdb_kv_get_blob(&_global_kvdb, "boot_count", fdb_blob_make(&blob, &boot_count, sizeof(boot_count)));
        /* the blob.saved.len is more than 0 when get the value successful */
        if (blob.saved.len > 0) {
						  FDB_INFO("boot count. %d\n", boot_count);
           
        } else {
            FDB_INFO("get the 'boot_count' failed\n");
				//	 while(1);
        }
    }
    { /* CHANGE the KV value */
        /* increase the boot count */
        boot_count ++;
        /* change the "boot_count" KV's value */
        if(fdb_kv_set_blob(kvdb, "boot_count", fdb_blob_make(&blob, &boot_count, sizeof(boot_count))) == FDB_NO_ERR)
				{
					
				}
				else
				{
					 FDB_INFO("set the 'boot_count' failed\n");
				}
				
    }
}



