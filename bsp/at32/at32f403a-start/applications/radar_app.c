/*

 * Change Logs:
 * Date           Author       Notes

 */
#include <rtthread.h>
#include <rtdevice.h>
#include <stdlib.h>
#include "global_define.h"
#include "drv_soft_uart.h"
#include "power_app.h"
#include "radar_app.h"
#define DBG_TAG              "app.radar"
#define DBG_LVL              LOG_LVL_DBG
#include <rtdbg.h>

#define SAMPLE_UART_NAME       "suart1"
uint16_t cur_radar_value = 0;
static struct rt_semaphore rx_sem;
struct rt_semaphore radar_sem;
rt_device_t soft_serial;
uint8_t g_radar_recv_start_flag = 0;
static uint8_t radar_data_recv_start_flag(uint8_t data);
struct rt_semaphore radar_lock;
#ifdef USING_RADAR

#include "radar_HKF24BDMP.h"            //全颂雷达驱动
#include "radar_usrr187.h"              //usrr187雷达驱动
#include "pcb_info.h"              //usrr196雷达驱动

static uint8_t radar_data_recv_start_flag(uint8_t data)
{
    static uint8_t step = 0;
    const uint8_t compare_str[] = "reset";
    if (compare_str[step] == data)
    {
        if (step == sizeof(compare_str) - 2)
        {
            g_radar_recv_start_flag = 1;
            step = 0;
            return 0;
        }
        else
        {
            step++;
            return 1;
        }
    }
    else
    {
        step = 0;
    }

    return 1;
}

static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    rt_sem_release(&rx_sem);
    return RT_EOK;
}
static void serial_thread_entry(void *parameter)
{
    char ch;
//		rt_kprintf("radar task run\n");
    while (1)
    {
        while (rt_device_read(soft_serial, -1, &ch, 1) != 1)
        {
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
        }
        //rt_kprintf("%02X ", ch);
#if defined(USING_HKF24BDMP)
        radar_data_recv_start_flag(ch);
#endif
        radar_data_recv_onebyone(ch);
    }
}


static int radar_init(int argc, char *argv[])
{
    rt_err_t ret = RT_EOK;
    char uart_name[RT_NAME_MAX];
		rt_sem_init(&radar_lock, "radar_lock", 1, RT_IPC_FLAG_PRIO);		
    //char str[] = "RSTBG\n";//init
    CLOSE_RADAR();
    if (argc == 2)
    {
        rt_strncpy(uart_name, argv[1], RT_NAME_MAX);
    }
    else
    {
        rt_strncpy(uart_name, SAMPLE_UART_NAME, RT_NAME_MAX);
    }

    soft_serial = rt_device_find(uart_name);
    if (!soft_serial)
    {
        rt_kprintf("find %s failed!\n", uart_name);
        return RT_ERROR;
    }

    ret=rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
		//rt_kprintf(">>> ret %d\n",ret);
    ret=rt_sem_init(&radar_sem, "radar_sem", 0, RT_IPC_FLAG_PRIO);
		//rt_kprintf(">>> ret %d\n",ret);
		
		/*设置为9600波特率*/
		struct serial_configure dev_cfg=RT_SERIAL_CONFIG_DEFAULT;
    dev_cfg.baud_rate=9600;
    dev_cfg.data_bits=DATA_BITS_8;
    dev_cfg.parity=PARITY_NONE;
    dev_cfg.stop_bits=STOP_BITS_1;
    dev_cfg.bufsz=20;//一帧数据就20个字节
    rt_device_control(soft_serial,RT_DEVICE_CTRL_CONFIG, &dev_cfg);
		/*设置为9600波特率*/
		
    rt_device_open(soft_serial, RT_DEVICE_FLAG_INT_RX);
    rt_device_set_rx_indicate(soft_serial, uart_input);

    //  rt_device_write(serial, 0, str, (sizeof(str) - 1));

    rt_thread_t thread = rt_thread_create("radar_app", serial_thread_entry, RT_NULL, 350, 22, 10);

    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
    }
    else
    {
        ret = RT_ERROR;
    }

    return ret;
}

#ifdef USING_RADAR   
	INIT_APP_EXPORT(radar_init);
#endif

void radar_send(uint8_t argc, char **argv)
{
    OPEN_RADAR();                                           //雷达将保持开启
    rt_device_write(soft_serial, 0, argv[1], (rt_strlen(argv[1])));
    rt_device_write(soft_serial, 0, "\r\n", (rt_strlen("\r\n")));
}
MSH_CMD_EXPORT(radar_send, clear blue ouput);

void set_radar(radar_type radar){
	ef_set_env_int("radar",radar);
}
radar_type get_radar(){
	return ef_get_env_int("radar");
}

#endif


void radar_enter_lp()
{
#ifdef USING_RADAR                               //支持雷达
	  //HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);         //中断线13
   CLOSE_RADAR();
#else
    SET_OUTPUT(POW_HKF, 0);
#endif
	
		set_RtPin_had_lp(POW_HKF,0);

    SET_DOWN_INPUT(RX4_LD);
    SET_DOWN_INPUT(TX4_LD);
		set_RtPin_had_lp(RX4_LD,0);	
		set_RtPin_had_lp(TX4_LD,0);	
}

void radar_exit_lp()
{
#ifdef USING_RADAR
	 CLOSE_RADAR();
    //softUart_TimerInit(9600);
#endif
}




