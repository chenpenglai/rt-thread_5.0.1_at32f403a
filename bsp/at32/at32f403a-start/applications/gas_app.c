#include "string.h"
#include "stdio.h"
#include "global_define.h"
#include "O2_app.h"
#include "gas_app.h"
#include "drv_ctrl.h"
#include "Tool.h"
#include "power_app.h"

//用于控制气体总开关

int gas_init(void)
{
	OPEN_GAS_POWER();

}

void gas_enter_lp(void)
{
		CLOSE_GAS_POWER();
		set_RtPin_had_lp(PW_WP_PIN, 0);
	
}

void gas_exit_lp(void)
{
		OPEN_GAS_POWER();

}
