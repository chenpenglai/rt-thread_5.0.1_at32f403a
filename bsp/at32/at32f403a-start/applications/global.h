#ifndef __GLOBAL_H
#define __GLOBAL_H
#include "stdio.h"
#include "string.h"
#include "stdint.h"
#include "stdbool.h"
#include "func_tools.h"
#include "utility.h"
#include "global.h"
#include "rtthread.h"
//#include <at32f4xx.h>
#include "fixlen_que.h"
/*******************		版本更新/修改说明 		*********************			

		本工程是以 rt-thread 为基础开发的一套多网络驱动的 网关框架系统，作为后期灵活开发更多不同功能网关的基本模板
				已实现：参数存储、日志存储，文件系统、U盘驱动、强分离灵活移植的网络模组AT驱动、POSIX网络应用层
				待实现：boot+远程升级
				
				
		V0.0.1
			完成air724连接网络
			#define AIR720_SAMPLE_STATUS_PIN -1				//状态脚被我注释，因为板子上没接
			
			flash驱动的关系                                                                                                                                                                                       
				dfs（文件系统抽象层 device file system，
								dfs_mount 通过查找 dfs_register 注册的文件系统 和传入的fal的分区名来将把文件系统挂载到指定的分区路径上
								注意：通过 fal_mtd_nor_device_create(FS_PARTITION_NAME) 来创建mtd设备（依赖 mtd_nor.c），让dfs挂载到mtd设备上）
				lfs（文件系统 little_fs，使用 dfs_register 注册到dfs）
				fal（Flash抽象层，提供统一的存储器上层操作接口，可以为Flash设备分区，挂接多种存储设备：spi的w25qxx或者sdio的SD卡
								通过 FAL_FLASH_DEV_TABLE ）
				sfud（万能spi Flash组件，用来挂接spi Flash，自动识别Flash型号 w25qxx）
				FlashDB （FlashDB 依赖 fal）
			
			加入 http_ota	目前boot还未做兼容
			
		V0.0.2
			加入libmodbus 目前还有问题（_modbus_rtu_send内rts延时的漏洞修改）
			使用陈老师的modbus.c 因为照明项目HR超出标准长度
			动态内存大小修改 AT32_SRAM_SIZE 96K -> 224K
			
			at32_putc 内加入喂狗 避免串口大批输出 导致重启
			netdev_check_link_status内添加删除线程，解决多次创建导致老的线程没释放而内存不足的漏洞(还存在直接删除任务 导致任务内的resp未释放的问题)
			修改left_ms 和 left_ms_set 1ms中断改成 rt_tick_get()
			
		V0.0.3
			加入ulog功能（依赖easyflash和fal）
			TCP连接过程中 获取网络时间写入RTC
			整理外部Flash地址分区
			
			//加入虚拟串口功能 还未验证
			修改下载偏移地址 需要打开魔术棒的linker edit 然后改成0x08008000，VECT_TAB_OFFSET改成0x8000
			idwg驱动需要更换为wdt
			使用新的库文件 USD->eopb0 扩展内存至224K
			没有RCC驱动 查看重启原因改为使用 crm_flag_get
			
		注意：
			close_cur_netdev 内添加 删除接收线程（解决关闭网卡后 接收线程还在等待数据，导致死机）
			
			解决net_app网络断开 未释放内存的漏洞
			
			目前无法使用最新的RTThread工程（调用flashDB_cfg和 lfs_init 都会死机，等待lfs官方更新dfs_lfs.c兼容DFS_V1后再使用 ）
			尝试了uffs和yaffs这两种MTD NAND文件系统 也都会死机
			文件系统lfs已经可用 魔术棒内添加宏 LFS_CONFIG=lfs_config.h，修改dfs_lfs.c解决，参考：https://club.rt-thread.org/ask/question/0fefb68b5b66cace.html
			
			放弃lfs 使用elm fatfs，因为lfs创建的文件系统不能被电脑USB读取。
			elm fatfs文件系统成功初始化，解决方法:
			rtconfig.h文件
				#define RT_DFS_ELM_MAX_SECTOR_SIZE 4096   之前默认是512,改为4096
			dfs.h文件
				#define SECTOR_SIZE              		4096  之前默认是512,改为4096
			
			移植命令行的 vi 文本编辑命令到设备中（依赖optparse mem_sandbox）
			
			elm文件系统的块设备互斥问题，不能同时电脑USB和网关都mount（识别到USB插入后要把文件系统从单片机上卸载。elm umount掉，USB拔出再重新elm mount）
				https://blog.csdn.net/lzs940320/article/details/112261856
		
		V0.0.4	
			把多合一网关的 net_app 搬到该工程中
			加入ec20 AT驱动，居然可以直接驱动ec800模组
			
		V0.0.5
			EC20_SAMPLE_RECV_BUFF_LEN 、ESP8266_SAMPLE_RECV_BUFF_LEN 接收缓冲区改大 512 -> (1024*2)
			
			TODO 使用这条命令触发的死机还未解决：wget http://115.159.144.115:83/iot-light-update/4g/V0.2.bin V0.1.bin
			
			解决closesocket死机问题，加入调试口485功能
			解决卡哇伊mqtt内存泄漏问题
			
			
*/

/*******************		固定参数 		*********************/
#define SOFTWARE_VER    	"V0.0.5"		//软件版本号
#define HARDWARE_VER			"V0.2"		//新版 硬件版本号

#define DEVICE_ID					get_env("device_id")		//设备 ID

#define CONFIG_FILE 			"config.txt"			//参数配置文件,
#define UPDATE_FILE 			"update.bin"			//升级包文件名

//#define NO_NET_TEST_MODE						//无网络的测试模式
//#define VIRTUAL_NET_MODULE				//可以使用串口工具 模拟网络模块


/**************		 功能选择		***************/

#define MCU_AT32F403x		//为了兼容从陈老师的工程搬来的文件，所以定义此宏


#define USE_AT_NETWORK			//使用AT模块
#define AT_USING_TIMER_1		//使用定时器中断 处理接收数据。否则在主循环内处理
//#define AT_RAW_OUTPUT		//调试串口 输出 AT指令交互过程

//#define DEBUG_MODE			//调试模式
//#define USE_DECODER		//作为解码器用（shell串口改到485主机串口）
//#define DEV_CTRL_NOT_READ			//下发控制设备 不读取最新状态（不可打开，因为下发控制上报的状态将没有之前的状态）
#define USE_FATFS				//使用FATFS
#define USE_USB					//使用USB
#define USE_HEART				//开启定时心跳包
#define USE_WDT				//使用独立看门狗
#define USE_SCENE_INFO	//使用场景功能
//#define USE_253					//解析使用253报文（目前已经废除2022-12-03，使用 EVENT_SEND_STATE_CHANGE_REPORT 取代）
//#define USE_ALARM				//使用RTC 多闹钟功能
#define USE_REMOTE_CMD		//使用远程命令行功能
//#define EF_USING_LOG		//使用 EasyLogger 日志功能 占用4.5K空间
//#define LOG_USING_FILE		//日志输出到file文件
#define USE_RELAY_16		//使用16路灯控 占用6K空间
#define USE_CURTAIN		//使用窗帘模块
#define USE_SCR				//使用可控硅模块
//#define USE_CMD_STOP_RECOVER	//使用下发命令 使设备升级后重启停止回滚
//#define USE_SFUD		//使用SFUD（串行 Flash 通用驱动库）https://gitee.com/chenpenglai/SFUD
//#define printf 	//				//打开可减少 12K 的code空间
//#ifdef WATERFALL_TEST		//流水灯测试功能

//#ifdef NEW_BOOT_PFS	//可以使用更多功能，因为bin文件去掉了100KB限制
	#define USE_SRELAY_16		//使用16路继电器设备
	#define USE_SENSOR			//使用3合1传感器 设备
	#define USE_BRK									//使用断路器（需要定时任务支持）
	#define EF_ENV_USING_PFS_MODE 	//easyflash掉电保护模式
	#define USE_SCENE_TIMER		//定时器执行场景广播功能（非_P的版本不要加这个功能）
	
	#if defined(USE_SCENE_TIMER)||defined(USE_BRK)
		#define USE_TIMER_TASK					//定时任务
	#endif
	
//#endif
#define USE_RS485_CMD
#define USE_FILE_UPDATE	//使用文件升级 update.bin
#define USE_HTTP_UPDATE				//使用http升级

#ifdef USE_HTTP_UPDATE
	//#define USE_HTTP_PARSER				//使用开源库解析HTTP（Code占用=11K，实现完整的HTTP解析功能）
	#define USE_USER_PARSER				//使用自定程序解析HTTP（Code占用可忽略，但只能解析响应报文的body数据段）
#endif
#include "ulog.h"
#ifdef RT_USING_ULOG
	#define log_a LOG_A
	#define log_e LOG_E
	#define log_w LOG_W
	#define log_i LOG_I
	#define log_d LOG_D
	#define log_v LOG_V
#else
	#define log_a rt_kprintf
	#define log_e rt_kprintf
	#define log_w rt_kprintf
	#define log_i rt_kprintf
	#define log_d rt_kprintf
	#define log_v rt_kprintf
	
#endif



/************* 企业代码 *************/
#define E_CODE 						ef_get_env_int("enterprise")

/************    串口配置		***************/
#if defined(AT_START_F403A_V1_0_gateway_new)||defined(AT_START_F403A_V1_0_gateway_new_y_p)
	//	ALL_BUF_SIZE*(6+10)
	#define ALL_BUF_SIZE	(512*4)			//因为资源充足 所有内存统一大小 无需担心资源不够
	
	#define NET_USART_TX_BUF_SIZE 	ALL_BUF_SIZE
	#define NET_USART_RX_BUF_SIZE 	ALL_BUF_SIZE

	#define SHELL_USART_TX_BUF_SIZE ALL_BUF_SIZE
	#define SHELL_USART_RX_BUF_SIZE ALL_BUF_SIZE

	#define SLV_USART_TX_BUF_SIZE 	ALL_BUF_SIZE
	#define SLV_USART_RX_BUF_SIZE 	ALL_BUF_SIZE
	
	#define ETH_NET_USART_TX_BUF_SIZE 	ALL_BUF_SIZE
	#define ETH_NET_USART_RX_BUF_SIZE 	ALL_BUF_SIZE
	
#else
	#define ALL_BUF_SIZE	(300)			//资源有限
	#ifdef EXTEND_SRAM_64K
		#define NET_USART_TX_BUF_SIZE 1400		//实际需要1200，由于使用http下载固件包at_client_init必需900字节缓存区，所有1200->1000
		#define NET_USART_RX_BUF_SIZE 550			//实际需要550
	#else
		#define NET_USART_TX_BUF_SIZE 550			//实际需要1200，由于使用http下载固件包at_client_init必需900字节缓存区，所有1200->1000
		#define NET_USART_RX_BUF_SIZE 550			//实际需要550
	#endif
	#define SHELL_USART_TX_BUF_SIZE 550
	#define SHELL_USART_RX_BUF_SIZE 580
	#define SLV_USART_TX_BUF_SIZE 	550
	#define SLV_USART_RX_BUF_SIZE 	580
#endif


#if defined(AT_START_F403A_V1_0_gateway_new)||defined(AT_START_F403A_V1_0_gateway_new_y_p)	//4G网关板（章老师画的 第2版）
	#define NET_USART_CFG 		"28n1fpu200p+"		//网络口
	#define NET_USART_BUAD		115200
	#define NET_AT_USART_NAME "usart2"	
	
	//（主机串口目前无用）
	#define MAS_USART_CFG 		"48n1hpu1afp+"		//"18n1hpu1a8p+"	//		//主机 设备串口		e偶校验 n无校验 
	#define MAS_USART_BUAD		115200//ef_get_env_int("usart_baud")
	
#ifdef USE_DECODER
	#define SHELL_USART_CFG 	MAS_USART_CFG	
	#define SHELL_USART_BUAD	MAS_USART_BUAD		
#else
	//串口1中间，串口4最右边
	#define SHELL_USART_CFG 	"38n1hpu100p+"	//使用串口3是测试用的，实际需要使用1 "18n1fpu1a8p+"		//调试口 主机串口
	#define SHELL_USART_BUAD	115200		
#endif
	#define SLV_USART_CFG 		"18n1fpu1a8p+"	//"48n1hpu1afp+"	//		//从机 设备串口		e偶校验 n无校验 
	#define SLV_USART_BUAD		ef_get_env_int("usart_baud")


#define NET_CFG_BUAD 	9600			//初始化模块配置的波特率
#define NET_TRAN_BUAD 115200		//模块传输数据时的波特率

#define ETH_NET_USART_CFG 			"58n1fpu200p+"		//串口
#define ETH_NET_USART_BUAD			NET_CFG_BUAD 			//默认波特率

#define ETH_NET_BUF_SIZE				ETH_NET_USART_RX_BUF_SIZE			//网络缓存区大小

	
#elif defined(AT_START_F413_V1_0)				//开发板
	#define NET_USART_CFG 		"28n1fpu200p+"
	#define NET_USART_BUAD		115200
	#define NET_AT_USART_NAME "usart2"
	
	#define SHELL_USART_CFG 	"18n1fpu1b8p+"		//调试口 主机串口
	#define SHELL_USART_BUAD	115200						//初始化波特率，
	
	#define SLV_USART_CFG 		"38n1fpu1b9p+"		//从机串口
	#define SLV_USART_BUAD		ef_get_env_int("usart_baud")
	
#elif defined(AT_START_F413_V1_0_gateway_new)	//4G网关板（章老师画的 第2版）
	#define NET_USART_CFG 		"28n1fpu200p+"		//网络口
	#define NET_USART_BUAD		115200
	#define NET_AT_USART_NAME "usart2"
	
	#define SHELL_USART_CFG 	"48n1fpu1afp+"		//调试口 主机串口
	#define SHELL_USART_BUAD	115200		
	
	#define SLV_USART_CFG 		"18n1fpu1a8p+"		//从机 设备串口		e偶校验 n无校验 
	#define SLV_USART_BUAD		ef_get_env_int("usart_baud")
	
	#define MAS_USART_CFG 		"38n1fpu1b2p+"		//主机 设备串口		e偶校验 n无校验 
	#define MAS_USART_BUAD		ef_get_env_int("usart_baud")
	
#endif
#define DEV_STATE 				ef_get_env_int("state")



/**************  外部 flash 空间分布图 ****************** 
			
			BK_APP_ADDR        -   8 K, size 100 K
			-----------------------------------------
			OLD_APP_ADDR       - 108 K, size 100 K
			-----------------------------------------
			UPGRADE_FLAG_ADDR  - 208 K, size 4 K
			-----------------------------------------
			ADDR_TYPE_CFG_ADDR - 212 K, size 4 K
			-----------------------------------------
			SCENE_INFO_CFG_ADDR - 216 K, size 4 K
			-----------------------------------------
			SCENE_INFO_POOL_ADDR - 220 K, size 32 K
			-----------------------------------------
			SUBSET_APP_ADDR    - 252 K, size 100 K
			-----------------------------------------
			EF_START_ADDR      - 352 K, size 1 K
			-----------------------------------------
			LOG_EF_START_ADDR  - 356 K, size 1024 K
			-----------------------------------------
			//-FatFs & USB ADDR   - addr 1024 K,size 15360 K
			-----------------------------------------
*/
#define KB (1024)
#define MB (1024*1024)
#if defined(USE_FATFS)||defined(USE_USB)
	/*****				FatFs 和 USB 使用的flash配置参数						****/
	#define USB_FatFs_USE_EXTERN_FLASH			//使用 W25Q128 作为 USB 存储器，否则使用芯片内部flash
	#ifdef USB_FatFs_USE_EXTERN_FLASH
	//外部flash
		#if 0
			//这是W25Q128真实参数
			#define EX_FLASH_SECTOR_OFFSET 		256			//存储器向后偏移的扇区数 256*4096字节=1048576字节=1M
			#define	EX_FLASH_SECTOR_SIZE			4096		//扇区大小
			#define	EX_FLASH_BLOCK_NUM				256			//块数量
			#define EX_FLASH_BLOCK_SIZE				16			//扇区为单位的块大小（1个块=16扇区）
			#define EX_FLASH_SECTOR_ALL_NUM 	(EX_FLASH_BLOCK_NUM*EX_FLASH_BLOCK_SIZE)	//外部flash扇区总数
		#else
			//PC USB为了兼容FatFs，所以需要用虚拟参数（扇区大小从 4096 => 512，因为fatfs用4096会死机）
			#define EX_FLASH_SECTOR_OFFSET 		(512*8)			//=2048 存储器向后偏移的扇区数 256*4096字节=1048576字节=1M
			#define	EX_FLASH_SECTOR_SIZE			(4096/8)		//=512 	扇区大小
			#define	EX_FLASH_BLOCK_NUM				(256*8)			//=2048 块数量
			#define EX_FLASH_BLOCK_SIZE				12					//=16 	扇区为单位的块大小（1个块=16扇区）
			#define EX_FLASH_SECTOR_ALL_NUM 	(EX_FLASH_BLOCK_NUM*EX_FLASH_BLOCK_SIZE)	//外部flash扇区总数
		#endif
	#else
		//内部flash
		#define IN_FLASH_SECTOR_OFFSET 		(0x8005000+(100*1024))		//片内flash向后偏移的字节数
		#define IN_FLASH_SECTOR_SIZE			2048		//扇区大小，（flash >= 256K的是2K，16K-128K的是1K）
		#define IN_FLASH_SECTOR_ALL_NUM 	40			//片内可用flash扇区
	#endif
#endif
/****************			外部flash 地址/空间 分配参数（注意：必须和 BootLoader 程序内保持一致）			****************/

//升级标志位
#define UPGRADE_FLAG_ADDR               (0)         //0K 放置标志位（不能放在0和216K以后，貌似flash驱动有问题）
#define UPGRADE_FLAG_SIZE               (16*KB)                    				//空间不能小于4K（因为还要写入）
//新APP下载保存的地址
#define DOWN_APP_ADDR                   (UPGRADE_FLAG_ADDR+UPGRADE_FLAG_SIZE)       //外部flash存储新APP起始地址 (最前面4K为app参数配置的easyflash区域)
#define DOWN_APP_SIZE                   (504*KB)
//备份原来的旧APP的地址
#define BK_OLD_APP_ADDR                 (DOWN_APP_ADDR+DOWN_APP_SIZE)
#define BK_OLD_APP_SIZE                 (504*KB)    
//FlashDB KV数据库
#define FLASH_DB_KV1_ADDR								(BK_OLD_APP_ADDR+BK_OLD_APP_SIZE)
#define FLASH_DB_KV1_SIZE               (40*KB) 
//FlashDB TS数据库
#define FLASH_DB_TS1_ADDR								(FLASH_DB_KV1_ADDR+FLASH_DB_KV1_SIZE)
#define FLASH_DB_TS1_SIZE               (512*KB) 
//dfs文件系统
#define FS_ADDR													(FLASH_DB_TS1_ADDR+FLASH_DB_TS1_SIZE)
#define FS_SIZE               					(2*MB)
//easyflash
#define EF_ADDR													(FS_ADDR+FS_SIZE)
#define EF_SIZE               					(1*MB)

#define ADDR_TYPE_CFG_ADDR              (EF_ADDR+EF_SIZE)			//
#define ADDR_TYPE_CFG_SIZE              (4*KB)

#define SCENE_INFO_CFG_ADDR             (ADDR_TYPE_CFG_ADDR+ADDR_TYPE_CFG_SIZE)			//场景池 参数
#define SCENE_INFO_CFG_SIZE             (4*KB)

#define SCENE_INFO_POOL_ADDR            (SCENE_INFO_CFG_ADDR+SCENE_INFO_CFG_SIZE)			//场景池
#define SCENE_INFO_POOL_SIZE            (32*KB)

//断路器 单独设置一个设备池，方便移植
#define DEV_INFO_CFG_ADDR              	(SCENE_INFO_POOL_ADDR+SCENE_INFO_POOL_SIZE)			//
#define DEV_INFO_CFG_SIZE              	(4*KB)

#define DEV_INFO_POOL_ADDR              (DEV_INFO_CFG_ADDR+DEV_INFO_CFG_SIZE)			//
#define DEV_INFO_POOL_SIZE              (32*KB)

#define SCENE_TIMER_INFO_CFG_ADDR       (DEV_INFO_POOL_ADDR+DEV_INFO_POOL_SIZE)			//定时执行场景广播的
#define SCENE_TIMER_INFO_CFG_SIZE       (4*KB)

#define SCENE_TIMER_INFO_POOL_ADDR      (SCENE_TIMER_INFO_CFG_ADDR+SCENE_TIMER_INFO_CFG_SIZE)			//
#define SCENE_TIMER_INFO_POOL_SIZE      (32*KB)

#define IMPORTANT_PARA_ADDR							(SCENE_TIMER_INFO_POOL_ADDR+SCENE_TIMER_INFO_POOL_SIZE)
#define IMPORTANT_PARA_SIZE      				(4*KB)



#define NEED_UPGRADE_FLAG               1                           
#define SUCCESSED_UPGRADE_FLAG          2                                    
#define ERR_UPGRADE_FLAG                3 


typedef struct gps_data{
	float longitude;
	float	latitude;

}gps_data;

typedef enum __dev_phase{
		brk_err_phase=0,	//错误
		brk_1_phase=1,		//单相设备
		brk_3_phase=3,		//三相设备

}_dev_phase; 

typedef struct __curr_timer{							//当前日 时 分 
	uint16_t year;
	uint8_t mon;
	uint8_t week;		// 1 to 7 
	uint8_t day;
	uint8_t hous;
	uint8_t min;
}_curr_timer;


#define FLASH_WR GET_PIN(C,6)		//是不是 W25QXX_WP_Pin
#define FLASH_CS_PORT GPIOB
#define FLASH_CS_PIN  GPIO_PINS_12

#define FS_PARTITION_NAME "fs"		//文件系统分区名
#define FAL_EF_PART_NAME 	"ef"						//easyflash 分区名
extern rt_event_t global_event;
/******			全局状态标志位	state_flag		******/
#define EVENT_NEED_REPLY 										(1<<0)	//需要应答平台事件
#define EVENT_NEED_REPLY_NO_CTRL 						(1<<1)	//不需要操作设备，直接应答平台事件
#define EVENT_SVL_RESPONSE_SUCCEED 					(1<<2)	//下发modbus到从机 响应成功事件（写数据指令）
#define EVENT_SVL_ASK_SUCCEED 							(1<<3)	//轮询 响应成功事件						（读数据指令）

//下面两条属于253操作 已经废除
#define EVENT_READ_SLV_253									(1<<4)	//读取253的事件
#define EVENT_WAIT_SLV_FINISH								(1<<5)	//开始等待从机上报结束

#define EVENT_EXEC_PROTECT									(1<<6)	//执行：企业码 和 加密ID 写入操作
#define EVENT_EXEC_SCENE										(1<<7)	//执行场景
//#define EVENT_EXEC_SCENE_FINISH							(1<<8)	//执行完场景后 轮询上报状态
#define EVENT_END_SCENE_POOL								(1<<8)	//打断轮询
#define EVENT_WATERFALL											(1<<9)	//流水灯
#define EVENT_ENCODE_OK											(1<<10)	//解码成功
#define EVENT_SCENE_TIMER										(1<<11)	//定时执行场景广播

#define EVENT_TIMED_CHK_SW									(1<<12)	//断路器定时器执行任务
#define EVENT_BRK_EVT												(1<<13)	//断路器事件
#define EVENT_READ_NEW_BRK_EVT							(1<<14)	//读取新增断路器

#define DEV_EOK 	  0
#define DEV_ERROR 	1

void reboot(void);
int show_version(void);
int reset_info_chk(void);
int show_ex_flash_desc(void) ;
void extend_sram(void);
void show_using_sram_size(void);
#endif

