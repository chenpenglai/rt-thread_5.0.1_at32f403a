#ifndef __LED_DEV_H_
#define __LED_DEV_H_
#include <rtthread.h>
#include <at.h>
#include <board.h>
#include "stdbool.h"
#include "stdint.h"
#include "global_define.h"

#define ON  1
#define OFF 0

#define LED_PIN     GET_PIN(C,9)
#define LED_ON() 			SET_OUTPUT(LED_PIN, 1)			//����
#define LED_OFF()			SET_OUTPUT(LED_PIN, 0)			//����

typedef struct _led_effect{
	uint8_t status;
	uint16_t interval_time;
}led_effect;

typedef struct _led_loop{
	uint8_t count;
	led_effect effect[5];
}led_loop;

typedef enum
{
		LED_START=0,		//��ʼ��˸
		LED_END,			//ֹͣ��˸

}LED_MSG_EVENT;
typedef struct _led_msg
{
    LED_MSG_EVENT msg_event;
	  uint8_t mode;
		uint16_t num;
}led_msg;

void led_handler(uint8_t select,uint16_t loop_num);				
int led_app_init();

/********   �ⲿ����   ***********/
void led_start(uint8_t select,uint16_t loop_num);
void led_end();
void led_exit_lp(void);
void led_enter_lp(void);
#endif
