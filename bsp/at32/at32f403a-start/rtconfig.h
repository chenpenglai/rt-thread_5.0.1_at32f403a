#ifndef RT_CONFIG_H__
#define RT_CONFIG_H__

/* Automatically generated file; DO NOT EDIT. */
/* RT-Thread Configuration */

/* RT-Thread Kernel */

#define RT_NAME_MAX 8
#define RT_ALIGN_SIZE 8
#define RT_THREAD_PRIORITY_32
#define RT_THREAD_PRIORITY_MAX 32
#define RT_TICK_PER_SECOND 1000
#define RT_USING_OVERFLOW_CHECK
#define RT_USING_HOOK
#define RT_HOOK_USING_FUNC_PTR
#define RT_USING_IDLE_HOOK
#define RT_IDLE_HOOK_LIST_SIZE 4
#define IDLE_THREAD_STACK_SIZE 256
#define RT_USING_TIMER_SOFT
#define RT_TIMER_THREAD_PRIO 4
#define RT_TIMER_THREAD_STACK_SIZE 512

/* kservice optimization */

#define RT_DEBUG
#define RT_DEBUG_COLOR

/* Inter-Thread communication */

#define RT_USING_SEMAPHORE
#define RT_USING_MUTEX
#define RT_USING_EVENT
#define RT_USING_MAILBOX
#define RT_USING_MESSAGEQUEUE

/* Memory Management */

#define RT_USING_MEMPOOL
#define RT_USING_SMALL_MEM
#define RT_USING_MEMHEAP
#define RT_MEMHEAP_FAST_MODE
#define RT_USING_SMALL_MEM_AS_HEAP
#define RT_USING_HEAP

//#define RT_USING_MEMTRACE		//内存调试功能
/* Kernel Device Object */

#define RT_USING_DEVICE
#define RT_USING_CONSOLE
#define RT_CONSOLEBUF_SIZE 128
#if defined(AT_START_F403A_V1_0_gateway_new_y_p)
	#define RT_CONSOLE_DEVICE_NAME "uart4"
#else
	#define RT_CONSOLE_DEVICE_NAME "uart3"
#endif
#define RT_VER_NUM 0x50001
#define RT_USING_HW_ATOMIC
#define RT_USING_CPU_FFS
#define ARCH_ARM
#define ARCH_ARM_CORTEX_M
#define ARCH_ARM_CORTEX_M4

/* RT-Thread Components */

#define RT_USING_COMPONENTS_INIT
#define RT_USING_USER_MAIN
#define RT_MAIN_THREAD_STACK_SIZE 2048
#define RT_MAIN_THREAD_PRIORITY 10

#define RT_USING_MSH
#define RT_USING_FINSH
#define FINSH_USING_MSH
#define FINSH_THREAD_NAME "tshell"
#define FINSH_THREAD_PRIORITY 20
#define FINSH_THREAD_STACK_SIZE 4096
#define FINSH_USING_HISTORY
#define FINSH_HISTORY_LINES 5
#define FINSH_USING_SYMTAB
#define FINSH_CMD_SIZE 80
#define MSH_USING_BUILT_IN_COMMANDS
#define FINSH_USING_DESCRIPTION
#define FINSH_ARG_MAX 10


/* Device Drivers */

#define RT_USING_DEVICE_IPC
#define RT_UNAMED_PIPE_NUMBER 64
#define RT_USING_SYSTEM_WORKQUEUE
#define RT_SYSTEM_WORKQUEUE_STACKSIZE 2048
#define RT_SYSTEM_WORKQUEUE_PRIORITY 23
#define RT_USING_SERIAL
#define RT_USING_SERIAL_V1
#define RT_SERIAL_RB_BUFSZ 256
#define RT_USING_PIN
#define RT_USING_ADC
#define RT_USING_RTC
#define RT_USING_ALARM
#define RT_USING_SPI
#define RT_USING_MTD_NOR
//#define RT_USING_MTD_NAND
#define RT_USING_FAL		//新版本中，FAL已经成为RTThread内部组件

/* Using USB */

#define RT_USING_USB
#define RT_USING_USB_DEVICE
#define RT_USBD_THREAD_STACK_SZ 4096
#define USB_VENDOR_ID 0x0FFE
#define USB_PRODUCT_ID 0x0001
//#define RT_USB_DEVICE_COMPOSITE
//#define RT_USB_DEVICE_CDC
#define RT_USB_DEVICE_MSTORAGE
//#define RT_VCOM_TASK_STK_SIZE 512
//#define RT_CDC_RX_BUFSIZE 128
//#define RT_VCOM_SERNO "32021919830108"
//#define RT_VCOM_SER_LEN 14
//#define RT_VCOM_TX_TIMEOUT 1000
#define RT_USB_MSTORAGE_DISK_NAME "fs"//"norfla0"//

/* C/C++ and POSIX layer */

#define RT_LIBC_DEFAULT_TIMEZONE 0

/* POSIX (Portable Operating System Interface) layer */

//#define RT_USING_POSIX_FS
#define RT_USING_POSIX_DEVIO
//#define RT_USING_LIBC		//魔术棒内已经定义
#define RT_USING_POSIX
#define RT_USING_POSIX_STDIO
#define RT_USING_POSIX_TERMIOS

/* Interprocess Communication (IPC) */


/* Socket is in the 'Network' category */


/* Network */

#define RT_USING_SAL
#define SAL_INTERNET_CHECK

/* Docking with protocol stacks */

#define SAL_USING_AT
#define SAL_USING_POSIX
#define RT_USING_NETDEV
#define NETDEV_USING_IFCONFIG
#define NETDEV_USING_PING
#define NETDEV_USING_NETSTAT
#define NETDEV_USING_AUTO_DEFAULT
#define NETDEV_IPV4 1
#define NETDEV_IPV6 0
#define RT_USING_AT
#define AT_USING_CLIENT
#define AT_CLIENT_NUM_MAX 1
#define AT_USING_SOCKET
#define AT_USING_CLI
#define AT_CMD_MAX_LEN 128
#define AT_SW_VERSION_NUM 0x10301

/* Utilities */

#define RT_USING_RYM
#define YMODEM_USING_FILE_TRANSFER
#define RT_USING_ULOG
#define ULOG_OUTPUT_LVL_I
#define ULOG_OUTPUT_LVL 7
#define ULOG_ASSERT_ENABLE
#define ULOG_LINE_BUF_SIZE 128

/* log format */

//#define ULOG_USING_COLOR
#define ULOG_OUTPUT_TIME
#define ULOG_TIME_USING_TIMESTAMP
#define ULOG_OUTPUT_LEVEL
#define ULOG_OUTPUT_TAG
#define ULOG_BACKEND_USING_CONSOLE

/* RT-Thread Utestcases */


/* RT-Thread online packages */

/* IoT - internet of things */

#define PKG_USING_WEBCLIENT
#define WEBCLIENT_USING_SAMPLES
#define WEBCLIENT_USING_FILE_DOWMLOAD
#define WEBCLIENT_NOT_USE_TLS
#define PKG_USING_WEBCLIENT_V212
#define PKG_WEBCLIENT_VER_NUM 0x20102

/* Wi-Fi */

/* Marvell WiFi */


/* Wiced WiFi */

#define PKG_USING_AT_DEVICE
//#define AT_PRINT_RAW_CMD			//打印完整的AT交互数据
#define AT_DEVICE_USING_ESP8266
#define AT_DEVICE_ESP8266_INIT_ASYN
#define AT_DEVICE_ESP8266_SAMPLE
#define ESP8266_SAMPLE_WIFI_SSID "rtthread"
#define ESP8266_SAMPLE_WIFI_PASSWORD "12345678"
#define ESP8266_SAMPLE_CLIENT_NAME "uart5"
#define ESP8266_SAMPLE_RECV_BUFF_LEN (1024*2)

#define AT_DEVICE_USING_AIR720
#define AT_DEVICE_AIR720_INIT_ASYN
#define AT_DEVICE_AIR720_SAMPLE
#define AIR720_SAMPLE_POWER_PIN GET_PIN(C,5)
#define AIR720_SAMPLE_STATUS_PIN -1
#define AIR720_SAMPLE_CLIENT_NAME "uart2"
#define AIR720_SAMPLE_RECV_BUFF_LEN (1024*2)

#define AT_DEVICE_USING_EC20
#define AT_DEVICE_EC20_INIT_ASYN
#define AT_DEVICE_EC20_SAMPLE
#define EC20_SAMPLE_POWER_PIN GET_PIN(C,5)
#define EC20_SAMPLE_STATUS_PIN -1
#define EC20_SAMPLE_CLIENT_NAME "uart2"
#define EC20_SAMPLE_RECV_BUFF_LEN (1024*2)

#define PKG_USING_AT_DEVICE_LATEST_VERSION
#define PKG_AT_DEVICE_VER_NUM 0x99999

/* IoT Cloud */

#define PKG_USING_OTA_DOWNLOADER
#define OTA_DOWNLOADER_DEBUG
#define PKG_USING_HTTP_OTA
#define PKG_HTTP_OTA_URL "http://115.159.144.115:83/iot-light-update/4g/V0.2.bin"
//#define PKG_USING_YMODEM_OTA
#define PKG_USING_OTA_DOWNLOADER_LATEST_VERSION

/* security packages */

#define MODBUS_DEV_NAME "uart1"					//modbus使用的设备
/* language packages */


/* multimedia packages */

/* LVGL: powerful and easy-to-use embedded GUI library */


/* u8g2: a monochrome graphic library */


/* tools packages */
#define PKG_USING_OPTPARSE
#define PKG_USING_OPTPARSE_LATEST_VERSION
#define PKG_USING_VI
#define VI_SANDBOX_SIZE_KB 20
#define VI_MAX_LEN 4096
#define VI_ENABLE_COLON
#define VI_ENABLE_COLON_EXPAND
#define VI_ENABLE_YANKMARK
#define VI_ENABLE_SEARCH
#define VI_ENABLE_DOT_CMD
#define VI_ENABLE_READONLY
#define VI_ENABLE_SETOPTS
#define VI_ENABLE_SET
#define VI_ENABLE_VI_ASK_TERMINAL
#define VI_ENABLE_UNDO
#define VI_ENABLE_UNDO_QUEUE
#define VI_UNDO_QUEUE_MAX 256
#define VI_ENABLE_VERBOSE_STATUS
#define PKG_USING_VI_LATEST_VERSION

#define PKG_USING_MEM_SANDBOX
#define PKG_USING_MEM_SANDBOX_LATEST_VERSION

//串口命令行 二维码生成
#define PKG_USING_QRCODE
#define PKG_QRCODE_SAMPLE
#define PKG_USING_QRCODE_LATEST_VERSION

//串口绘图 VT100
#define PKG_USING_VT100
#define VT100_USING_MONITOR
#define PKG_USING_VT100_LATEST_VERSION
#define PKG_VT100_VER_NUM 0x99999

/* Device virtual file system */
//DFS
//#define RT_USING_DFS
//#define DFS_USING_WORKDIR
//#define DFS_FILESYSTEMS_MAX 2
//#define DFS_FILESYSTEM_TYPES_MAX 2
//#define DFS_FD_MAX 16
//#define RT_USING_DFS_DEVFS
//#define RT_USING_MEMTRACE

#define RT_USING_DFS
#define DFS_USING_POSIX
#define DFS_USING_WORKDIR
#define DFS_FD_MAX 16
#define RT_USING_DFS_V1
#define DFS_FILESYSTEMS_MAX 4
#define DFS_FILESYSTEM_TYPES_MAX 4
#define RT_USING_DFS_ELMFAT

/* elm-chan's FatFs, Generic FAT Filesystem Module */

#define RT_DFS_ELM_CODE_PAGE 437
#define RT_DFS_ELM_WORD_ACCESS
#define RT_DFS_ELM_USE_LFN_3
#define RT_DFS_ELM_USE_LFN 3
#define RT_DFS_ELM_LFN_UNICODE_0
#define RT_DFS_ELM_LFN_UNICODE 0
#define RT_DFS_ELM_MAX_LFN 255
#define RT_DFS_ELM_DRIVES 2
#define RT_DFS_ELM_MAX_SECTOR_SIZE 4096
#define RT_DFS_ELM_REENTRANT
#define RT_DFS_ELM_MUTEX_TIMEOUT 3000
#define RT_USING_DFS_DEVFS
//#define RT_USING_DFS
//#define DFS_USING_POSIX
//#define DFS_USING_WORKDIR
//#define DFS_FD_MAX 16
//#define RT_USING_DFS_V2
//#define RT_USING_DFS_DEVFS




//little fs
//#define PKG_USING_LITTLEFS
//#define PKG_USING_LITTLEFS_LATEST_VERSION
//#define LFS_READ_SIZE 256
//#define LFS_PROG_SIZE 256
//#define LFS_BLOCK_SIZE 4096
//#define LFS_CACHE_SIZE 256
//#define LFS_BLOCK_CYCLES -1		//擦写平衡 -1=disable
//#define LFS_THREADSAFE
//#define LFS_LOOKAHEAD_MAX 128

////yaffs
//#define PKG_USING_DFS_YAFFS
//#define PKG_USING_YAFFS_LATEST_VERSION
//#define PKG_USING_DFS_JFFS2
//#define RT_USING_DFS_JFFS2
//#define PKG_USING_DFS_JFFS2_LATEST_VERSION

////uffs
//#define PKG_USING_DFS_UFFS
//#define RT_USING_DFS_UFFS
//#define RT_UFFS_ECC_MODE_1
//#define RT_UFFS_ECC_MODE 1
//#define PKG_USING_DFS_UFFS_LATEST_VERSION


//flashDB
#define PKG_USING_FLASHDB
#define FDB_USING_KVDB
#define FDB_KV_AUTO_UPDATE
#define FDB_USING_TSDB
#define FDB_USING_FAL_MODE
#define FDB_WRITE_GRAN_1BIT
#define FDB_WRITE_GRAN 1
#define FDB_DEBUG_ENABLE
#define PKG_USING_FLASHDB_V10000
#define PKG_FLASHDB_VER_NUM 0x10000

//easyflash
#define PKG_USING_EASYFLASH
//#define PKG_EASYFLASH_ENV			//不使用ENV功能 使用FlashDB代替了，使用easyflash是为了用ulog
#define PKG_EASYFLASH_LOG
#define PKG_EASYFLASH_LOG_AREA_SIZE (1024*(128*7))		//=896K
#define PKG_EASYFLASH_IAP
#define PKG_EASYFLASH_ERASE_GRAN 4096
#define PKG_EASYFLASH_WRITE_GRAN_1BIT
#define PKG_EASYFLASH_WRITE_GRAN 1
#define PKG_EASYFLASH_START_ADDR 0
#define PKG_EASYFLASH_DEBUG

//ulog flash 输出到flash上
#define PKG_USING_EASYFLASH_V410
#define PKG_EASYFLASH_VER_NUM 0x40100
#define PKG_USING_ULOG_EASYFLASH
#define ULOG_EASYFLASH_BACKEND_ENABLE
#define PKG_USING_ULOG_EASYFLASH_V00200

////ulog file 输出到文件ulog.log（不方便查看，cat ulog.log 查看内容会卡死 可能因为文件一直被ulog占用着）
//#define PKG_USING_ULOG_FILE
//#define ULOG_FILE_ROOT_PATH "/logs"
//#define ULOG_FILE_NAME_BASE "ulog.log"
//#define ULOG_FILE_MAX_NUM 5
//#define ULOG_FILE_MAX_SIZE 524288
//#define PKG_USING_ULOG_FILE_V100
//#define ULOG_BACKEND_USING_FILE

//FAL
#define PKG_USING_FAL
#define FAL_DEBUG_CONFIG
#define FAL_DEBUG 1
#define FAL_PART_HAS_TABLE_CFG
#define FAL_USING_SFUD_PORT
#define FAL_USING_NOR_FLASH_DEV_NAME "norfla0"
//#define FAL_USING_SD_FLASH_DEV_NAME "sdflash0"		//目前没用到
#define PKG_USING_FAL_V00500
#define PKG_FAL_VER_NUM 0x00500

//SFUD 万能Flash驱动
#define RT_USING_SFUD
#define RT_SFUD_USING_SFDP
#define RT_SFUD_USING_FLASH_INFO_TABLE
#define RT_SFUD_SPI_MAX_HZ 50000000

//kawaii mqtt（不用了 存在内存泄露问题）
#define PKG_USING_KAWAII_MQTT
#define KAWAII_MQTT_NETSOCKET_USE_SAL
//#define KAWAII_MQTT_LOG_IS_SALOF
//#define SALOF_USING_LOG
#define SALOF_USING_SALOF
#define SALOF_BUFF_SIZE 512
#define SALOF_FIFO_SIZE 2048
#define SALOF_TASK_STACK_SIZE 2048
#define SALOF_TASK_TICK 50
#define SALOF_LOG_LEVEL 4
#define KAWAII_MQTT_LOG_LEVEL 4
#define KAWAII_MQTT_VERSION 4
#define KAWAII_MQTT_KEEP_ALIVE_INTERVAL 100
#define KAWAII_MQTT_THREAD_STACK_SIZE 4096
#define KAWAII_MQTT_THREAD_PRIO 5
#define KAWAII_MQTT_THREAD_TICK 50
#define KAWAII_MQTT_MAX_PACKET_ID 0xFFFE
#define KAWAII_MQTT_TOPIC_LEN_MAX 64
#define KAWAII_MQTT_ACK_HANDLER_NUM_MAX 64
#define KAWAII_MQTT_DEFAULT_BUF_SIZE 1024
#define KAWAII_MQTT_DEFAULT_CMD_TIMEOUT 4000
#define KAWAII_MQTT_MAX_CMD_TIMEOUT 20000
#define KAWAII_MQTT_MIN_CMD_TIMEOUT 1000
#define KAWAII_MQTT_RECONNECT_DEFAULT_DURATION 1000
#define PKG_USING_KAWAII_MQTT_TEST
#define PKG_USING_KAWAII_MQTT_LATEST_VERSION
#define KAWAII_MQTT_NETSOCKET_USE_SAL

//vconsole 串口控制台切换（和rtt 5.0不兼容）
//#define PKG_USING_VCONSOLE
//#define PKG_USING_VCONSOLE_LATEST_VERSION
/* system packages */

/* acceleration: Assembly language or algorithmic acceleration packages */


/* Micrium: Micrium software products porting for RT-Thread */


/* peripheral libraries and drivers */


/* AI packages */


/* miscellaneous packages */

/* samples: kernel and components samples */


/* entertainment: terminal games and other interesting software packages */

#define SOC_FAMILY_AT32
#define SOC_SERIES_AT32F403A

/* Hardware Drivers Config */

#define SOC_AT32F403AVGT7

/* Onboard Peripheral Drivers */

#define BSP_USING_SERIAL

/* On-chip Peripheral Drivers */
#define BSP_USING_ON_CHIP_FLASH

#define BSP_USING_GPIO
#define BSP_USING_USBD
#define BSP_USING_RTC
#define BSP_RTC_USING_LEXT
#define BSP_USING_UART
#define BSP_USING_UART1
#define BSP_USING_UART2
#define BSP_USING_UART3
#define BSP_USING_UART4
#define BSP_USING_UART5
#define BSP_USING_SPI
#define BSP_USING_SPI1
#define BSP_USING_SPI2
#define BSP_USING_ADC
#define RT_USING_WDT

#endif
