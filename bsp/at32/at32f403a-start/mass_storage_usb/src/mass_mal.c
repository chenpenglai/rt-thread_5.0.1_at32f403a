/**
  ******************************************************************************
  * File   : USB_Device/MassStorage/src/mass_mal.c
  * Version: V1.2.5
  * Date   : 2020-10-16
  * Brief  : Medium Access Layer interface
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "at32f4xx.h"
#include "mass_mal.h"
//#include "at32_sdio_sd.h"
//#include "flash.h"
#include "global.h"
//#include "w25qxx.h"
#include "stdio.h"

/** @addtogroup AT32F413_StdPeriph_Examples
  * @{
  */

/** @addtogroup USB_MassStorage
  * @{
  */ 
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t Mass_Memory_Size[2];
uint32_t Mass_Block_Size[2];
uint32_t Mass_Block_Count[2];
__IO uint32_t Status = 0;


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Initializes the Media on the AT32
  * @param  lun: 0 is SD Card
  * @retval status of media init(MAL_OK/MAL_FAIL).
  */
uint16_t MAL_Init(uint8_t lun)
{
  uint16_t status = MAL_OK;

  switch (lun)
  {
    case 0:
#ifdef USE_USB		
		#ifdef USB_FatFs_USE_EXTERN_FLASH
	
		#else
			FLASH_Unlock();
      //status = SD_Init();  //FLASH
		#endif
#endif
      break;
    case 1:
      break;
    default:
      return MAL_FAIL;
  }
  return status;
}

/**
  * @brief  Write data to Storage
  * @param  lun: 0 is SD Card, others not support
  * @param  Memory_Offset: Write address offset
  * @param  Writebuff: Write data buffer pointer
  * @param  Transfer_Length: Write data length
  * @retval status of Write data(MAL_OK/MAL_FAIL).
  */

uint16_t MAL_Write(uint8_t lun, uint32_t Memory_Offset, uint32_t *Writebuff, uint16_t Transfer_Length)
{
  switch (lun)
  {
    case 0:
#ifdef USE_USB
			//printf("USB Write %d %d\n",Memory_Offset,Transfer_Length);
		#ifdef USB_FatFs_USE_EXTERN_FLASH
			// Writebuff 为uint32_t可能需要转成uint8_t再处理
//			Memory_Offset+=(EX_FLASH_SECTOR_OFFSET*EX_FLASH_SECTOR_SIZE);
//			EN25QXX_Write((uint8_t*)Writebuff,(uint32_t)(Memory_Offset),(uint16_t)(Transfer_Length));
		
		#else
			FLASH_Unlock();
			Memory_Offset+=IN_FLASH_SECTOR_OFFSET;
			FLASH_ErasePage( Memory_Offset); 			
			Flash_Write(Memory_Offset,Writebuff,Transfer_Length);
		#endif
#endif
      
      break;
    default:
      return MAL_FAIL;
  }
  return MAL_OK;
}

/**
  * @brief  Read data from Storage
  * @param  lun: 0 is SD Card, others not support
  * @param  Memory_Offset: Write address offset
  * @param  Readbuff: read data buffer pointer
  * @param  Transfer_Length: read data length
  * @retval status of read data(MAL_OK/MAL_FAIL).
  */
uint16_t MAL_Read(uint8_t lun, uint32_t Memory_Offset, uint32_t *Readbuff, uint16_t Transfer_Length)
{

  switch (lun)
  {
    case 0:
#ifdef USE_USB
			//printf("USB Read %d %d\n",Memory_Offset,Transfer_Length);
		#ifdef USB_FatFs_USE_EXTERN_FLASH
			//Readbuff为uint32_t可能需要转成uint8_t再处理
//			Memory_Offset+=(EX_FLASH_SECTOR_OFFSET*EX_FLASH_SECTOR_SIZE);		//扇区偏移从1M开始，最前面1M给ef和升级用 (一个扇区=4K)
//			EN25QXX_Read((uint8_t*)Readbuff,(uint32_t)(Memory_Offset),(uint16_t)(Transfer_Length));  // <<9 相当于 *512
		
		#else
			FLASH_Unlock();
			//printf("主机USB 读操作 %d %d\n",Memory_Offset,Transfer_Length);
			Memory_Offset+=IN_FLASH_SECTOR_OFFSET;
			Flash_Read(Memory_Offset,Readbuff,Transfer_Length);
		
		#endif
#endif
     break;
    default:
      return MAL_FAIL;
  }
  return MAL_OK;
}

/**
  * @brief  get mal status
  * @param  lun: 0 is SD Card, others not support
  * @retval status (MAL_OK/MAL_FAIL).
  */
uint16_t MAL_GetStatus (uint8_t lun)
{

  uint32_t DeviceSizeMul = 0, NumberOfBlocks = 0;

  if (lun == 0)
  {
#ifdef USE_USB
			//printf("主机USB 获取状态\n");
			//return MAL_FAIL;
 		#ifdef USB_FatFs_USE_EXTERN_FLASH		
//      Mass_Block_Size[0] =EX_FLASH_SECTOR_SIZE;		//受限于 BULK_MAX_PACKET_SIZE 大小，最大为 8*BULK_MAX_PACKET_SIZE。 
//			Mass_Block_Count[0]=(EX_FLASH_SECTOR_ALL_NUM-EX_FLASH_SECTOR_OFFSET);
//      Mass_Memory_Size[0] = Mass_Block_Count[0] * Mass_Block_Size[0];
		#else
			//片内flash：只有2K才能格式化
      Mass_Block_Size[0] =IN_FLASH_SECTOR_SIZE;		//受限于 BULK_MAX_PACKET_SIZE 大小，最大为 8*BULK_MAX_PACKET_SIZE。 
			Mass_Block_Count[0]=IN_FLASH_SECTOR_ALL_NUM;
      Mass_Memory_Size[0] =Mass_Block_Count[0] * Mass_Block_Size[0];
      
		#endif
#endif
			return MAL_OK;
  }
  else
  {

  }

  return MAL_FAIL;
}


/**
  * @}
  */

/**
  * @}
  */
  
