/**
  **************************************************************************
  * File   : at32_board.h
  * Version: V1.2.5
  * Date   : 2020-10-16
  * Brief  : Header file for AT-START board
  *          1. Set of firmware functions to manage Leds, push-button and COM ports.
  *          2. initialize Delay Function and USB
  */

#ifndef __AT32_BOARD_H
#define __AT32_BOARD_H	 
#include <at32f4xx.h>
/*
* This header include define support list:
* 	1. AT-START-F403 V1.2 Board
* 	2. AT-START-F413 V1.0 Board
* 	3. AT-START-F415 V1.0 Board
* 	4. AT-START-F403A V1.0 Board
* 	5. AT-START-F407 V1.0 Board
* 	6. AT-START-F421 V1.0 Board
* if define AT_START_F403_V1_2, the header file support AT-START-F403 V1.2 Board
* if define AT_START_F413_V1_0, the header file support AT-START-F413 V1.0 Board
* if define AT_START_F415_V1_0, the header file support AT-START-F415 V1.0 Board
* if define AT_START_F403A_V1_0, the header file support AT-START-F403A V1.0 Board
* if define AT_START_F407_V1_0, the header file support AT-START-F407 V1.0 Board
* if define AT_START_F421_V1_0, the header file support AT-START-F421 V1.0 Board
*/
#if !defined (AT_START_F403_V1_2) && !defined (AT_START_F413_V1_0) && !defined (AT_START_F403_V1_0) && !defined (AT_START_F415_V1_0) && \
    !defined (AT_START_F403A_V1_0)&& !defined (AT_START_F407_V1_0) && !defined (AT_START_F421_V1_0)&& !defined (AT_START_F403A_V1_0_gateway_new)&&\
		!defined (AT_START_F403A_V1_0_gateway_new_y_p)
#error "Please select first the board AT-START device used in your application (in at32_board.h file)"
#endif

/*define usb pin*/
#define USB_DP_PIN          GPIO_Pins_12
#define USB_DM_PIN          GPIO_Pins_11

#define USB_GPIO            GPIOA
#if defined (AT32F421xx)
#define USB_GPIO_RCC_CLK    RCC_AHBPERIPH_GPIOA
#else
#define USB_GPIO_RCC_CLK    RCC_APB2PERIPH_GPIOA
#endif

void AT32_USB_GPIO_init(void);

#endif

/****************** (C) COPYRIGHT 2018 ArteryTek *********END OF FILE*********/
