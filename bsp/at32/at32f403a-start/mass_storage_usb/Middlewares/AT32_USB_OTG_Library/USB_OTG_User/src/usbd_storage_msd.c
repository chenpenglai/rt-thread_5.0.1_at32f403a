/**
  ******************************************************************************
  * File   : usbd_storage_msd.c
  * Version: V1.2.5
  * Date   : 2020-10-16
  * Brief  : This file provides the disk operations functions.
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "usbd_msc_mem.h"
//#include "at32_sdio_sd.h"
#include "w25qxx.h"
#include "flash.h"
#include "global.h"

/** @addtogroup AT32_USB_OTG_DEVICE_LIBRARY
  * @{
  */


/** @defgroup STORAGE 
  * @brief media storage application module
  * @{
  */ 

/** @defgroup STORAGE_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup STORAGE_Private_Defines
  * @{
  */ 

#define STORAGE_LUN_NBR                  1 
/**
  * @}
  */ 


/** @defgroup STORAGE_Private_Macros
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup STORAGE_Private_Variables
  * @{
  */ 
/* USB Mass storage Standard Inquiry Data */
const int8_t  STORAGE_Inquirydata[] = {//36
  
  /* LUN 0 */
  0x00,		
  0x80,		
  0x02,		
  0x02,
  (USBD_STD_INQUIRY_LENGTH - 5),
  0x00,
  0x00,
  0x00,
  'G', 'a', 't', 'e', 'w', 'a', 'y', ' ', /* Manufacturer : 8 bytes */
  'F', 'l', 'a', 's', 'h', ' ', ' ', ' ', ' ',/* Product      : 16 Bytes */
  'W', '2', '5', 'Q',  '1', '2', '8',
  '1', '.', '0' ,'0',                     /* Version      : 4 Bytes */
}; 

/**
  * @}
  */ 


/** @defgroup STORAGE_Private_FunctionPrototypes
  * @{
  */ 
int8_t STORAGE_Init (uint8_t lun);

int8_t STORAGE_GetCapacity (uint8_t lun, 
                           uint32_t *block_num, 
                           uint32_t *block_size);

int8_t  STORAGE_IsReady (uint8_t lun);

int8_t  STORAGE_IsWriteProtected (uint8_t lun);

int8_t STORAGE_Read (uint8_t lun, 
                        uint8_t *buf, 
                        uint32_t blk_addr,
                        uint16_t blk_len);

int8_t STORAGE_Write (uint8_t lun, 
                        uint8_t *buf, 
                        uint32_t blk_addr,
                        uint16_t blk_len);

int8_t STORAGE_GetMaxLun (void);


USBD_STORAGE_cb_TypeDef USBD_MICRO_W25Q128_fops =
{
  STORAGE_Init,
  STORAGE_GetCapacity,
  STORAGE_IsReady,
  STORAGE_IsWriteProtected,
  STORAGE_Read,
  STORAGE_Write,
  STORAGE_GetMaxLun,
  (int8_t *)STORAGE_Inquirydata,
};

USBD_STORAGE_cb_TypeDef  *USBD_STORAGE_fops = &USBD_MICRO_W25Q128_fops; 
//extern SD_CardInfo SDCardInfo;
__IO uint32_t count = 0;
/**
  * @}
  */ 


/** @defgroup STORAGE_Private_Functions
  * @{
  */ 


/**
  * @brief  Initialize the storage medium
  * @param  lun : logical unit number
  * @retval Status
  */

int8_t STORAGE_Init (uint8_t lun)
{
//  NVIC_InitType NVIC_InitStructure;
//  NVIC_InitStructure.NVIC_IRQChannel = SDIO_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority =0;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);

//  if( SD_Init() != 0)
//  {
//    return (-1); 
//  } 
	#ifdef USB_FatFs_USE_EXTERN_FLASH
		
	#else
		FLASH_Unlock();
		//status = SD_Init();  //FLASH
	#endif
  return (0);
  
}

/**
  * @brief  return medium capacity and block size
  * @param  lun : logical unit number
  * @param  block_num :  number of physical block
  * @param  block_size : size of a physical block
  * @retval Status
  */

int8_t STORAGE_GetCapacity (uint8_t lun, uint32_t *block_num, uint32_t *block_size)
{
 		#ifdef USB_FatFs_USE_EXTERN_FLASH
      *block_size =EX_FLASH_SECTOR_SIZE;		//受限于 BULK_MAX_PACKET_SIZE 大小，最大为 8*BULK_MAX_PACKET_SIZE。 
			*block_num =(EX_FLASH_SECTOR_ALL_NUM-EX_FLASH_SECTOR_OFFSET);
     
		#else
			//片内flash：只有2K才能格式化
      *block_size =IN_FLASH_SECTOR_SIZE;		//受限于 BULK_MAX_PACKET_SIZE 大小，最大为 8*BULK_MAX_PACKET_SIZE。 
			*block_num=IN_FLASH_SECTOR_ALL_NUM;
      
		#endif

	
  return (0);
  
}

/**
  * @brief  check whether the medium is ready
  * @param  lun : logical unit number
  * @retval Status
  */
int8_t  STORAGE_IsReady (uint8_t lun)
{

  static int8_t last_status = 0;

  if(last_status  < 0)
  {
    //SD_Init();
    last_status = 0;
  }
  
//  if(SD_GetStatus() != 0)
//  {
//    last_status = -1;
//    return (-1); 
//  }  

  return (0);
}

/**
  * @brief  check whether the medium is write-protected
  * @param  lun : logical unit number
  * @retval Status
  */
int8_t  STORAGE_IsWriteProtected (uint8_t lun)
{
  return  0;
}

/**
  * @brief  Read data from the medium
  * @param  lun : logical unit number
  * @param  buf : Pointer to the buffer to save data
  * @param  blk_addr :  address of 1st block to be read
  * @param  blk_len : nmber of blocks to be read
  * @retval Status
  */

int8_t STORAGE_Read (uint8_t lun, 
                 uint8_t *buf, 
                 uint32_t blk_addr,                       
                 uint16_t blk_len)
{
  
	#ifdef USB_FatFs_USE_EXTERN_FLASH
			//缓存区为4K：MSC_BOT_Data[MSC_MEDIA_PACKET]
	
			//printf("USB read %d %d\n",blk_addr,blk_len);
			blk_addr+=(EX_FLASH_SECTOR_OFFSET);
			EN25QXX_Read((uint8_t*)buf,(uint32_t)(blk_addr*EX_FLASH_SECTOR_SIZE),(uint16_t)(blk_len*EX_FLASH_SECTOR_SIZE));  // <<9 相当于 *512
		
		#else
			FLASH_Unlock();
			Memory_Offset+=IN_FLASH_SECTOR_OFFSET;
			FLASH_ErasePage( Memory_Offset); 			
			Flash_Write(Memory_Offset,Writebuff,Transfer_Length);
		#endif
    
  return 0;
}
/**
  * @brief  Write data to the medium
  * @param  lun : logical unit number
  * @param  buf : Pointer to the buffer to write from
  * @param  blk_addr :  address of 1st block to be written
  * @param  blk_len : nmber of blocks to be read
  * @retval Status
  */
int8_t STORAGE_Write (uint8_t lun, 
                  uint8_t *buf, 
                  uint32_t blk_addr,
                  uint16_t blk_len)
{
		#ifdef USB_FatFs_USE_EXTERN_FLASH
			//缓存区为4K：MSC_BOT_Data[MSC_MEDIA_PACKET]
	
			//printf("USB write %d %d\n",blk_addr,blk_len);
			blk_addr+=(EX_FLASH_SECTOR_OFFSET);
			EN25QXX_Write((uint8_t*)buf,(uint32_t)(blk_addr*EX_FLASH_SECTOR_SIZE),(uint16_t)(blk_len*EX_FLASH_SECTOR_SIZE));  // <<9 相当于 *512 
		#else
			FLASH_Unlock();
			Memory_Offset+=IN_FLASH_SECTOR_OFFSET;
			FLASH_ErasePage( Memory_Offset); 			
			Flash_Read(Memory_Offset,Writebuff,Transfer_Length);
		#endif
  return (0);
}

/**
  * @brief  Return number of supported logical unit
  * @param  None
  * @retval number of logical unit
  */

int8_t STORAGE_GetMaxLun (void)
{
  return (STORAGE_LUN_NBR - 1);
//	return (STORAGE_LUN_NBR);
}
/**
  * @}
  */ 


/**
  * @}
  */ 


/**
  * @}
  */ 

/************************ (C) COPYRIGHT Artery Technology *****END OF FILE****/

