#include "at32_board.h"
#include "stdio.h"

/**
  * @brief  USB GPIO initialize
  *         USB use DP->PA12, DM->PA11    
  * @param  None
  * @retval None
  */
void AT32_USB_GPIO_init()
{
  GPIO_InitType GPIO_InitStructure;
  /* Enable the USB Clock*/
  RCC_APB2PeriphClockCmd(USB_GPIO_RCC_CLK, ENABLE);

  /*Configure DP, DM pin as GPIO_Mode_OUT_PP*/
  GPIO_StructInit(&GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pins  = USB_DP_PIN | USB_DM_PIN;
#if !defined (AT32F421xx)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT_PP;
#else
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OutType = GPIO_OutType_PP;
  GPIO_InitStructure.GPIO_Pull = GPIO_Pull_NOPULL;
#endif
  GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
  GPIO_Init(USB_GPIO, &GPIO_InitStructure);
  GPIO_ResetBits(USB_GPIO, USB_DP_PIN);
}
