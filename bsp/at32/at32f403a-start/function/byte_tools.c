#include "byte_tools.h"


#define tool_BIG_ENDIAN 'b'
#define tool_LITTLE_ENDIAN 'l'

static union { char c[4]; unsigned long mylong; } endian_test = {{ 'l', '?', '?', 'b' } };
#define tool_ENDIANNESS ((char)endian_test.mylong)

//计算位数
int numPlaces (int n) {

    int r = 1;
    if (n < 0) n = (n == -2147483647) ? 2147483647: -n;
    while (n > 9) {
        n /= 10;
        r++;
    }
    return r;
}

unsigned char BcdToHex(unsigned char data)
{
    unsigned char temp;
 
    temp = ((data>>4)*10 + (data&0x0f));
    return temp;
}
unsigned char HexToBcd(unsigned char data)
{
    unsigned char temp;
    temp = (((data/10)<<4) + (data%10));
    return temp;
}





#include <stdio.h>
uint8_t StrToBCD(char *dst, char *src, int srcLen)
{
    char byteHigh = 0x00;
    char byteLow = 0x00;
    int dstLen = srcLen/2; //??????,3/2=1
    int srcIndex = 0;
    for (int bytesIndex = 0; bytesIndex < dstLen; ++bytesIndex)
    {
        srcIndex = bytesIndex*2;
        byteHigh = src[srcIndex] - '0';
        byteLow = src[srcIndex+1] - '0';
        dst[bytesIndex] = (byteHigh<<4) | byteLow;
    }
 
    if (srcLen > dstLen*2) // ???
    {
        dst[dstLen] = (src[srcLen-1]-'0') << 4;
    }
    return 0;
}

unsigned long DecToBcd(long long Dec, char *Bcd,int length)
{
   int i ;
	 long long temp = 0;
	 for( i=length-1;i>=0;i-- )
	{
		temp = Dec%100;
		Bcd[i] = (unsigned char)(((temp/10)<<4)+((temp %10)& 0x0f));
		Dec/=100;
	}
	return 0;
}


#include "math.h"
unsigned long BcdToDec(const char * bcd,int length)
{
	int i ,tmp;
	unsigned long dec = 0;
	 for(i=length-1;i>=0;i--)
	{
		tmp=((bcd[i] >> 4)&0x0f)*10 + (bcd[i] & 0x0f);
		dec +=tmp*pow(100,length -1 -i);
	}
	return dec;
}
//倒叙排列
void flashback_2byte(uint16_t *array,int j)
{
	int i = 0;
	while (i<j) {     

			uint16_t temp = array[j];

			array[j] = array[i];

			array[i] = temp;

			i++;

			j--;

	}
}

void HexToStr(char *pbDest, char *pbSrc, int nLen)
{
	unsigned char ddl,ddh;
	int i;

	for (i=0; i<nLen; i++)
	{
		ddh = 48 + (unsigned char)pbSrc[i] / 16;
		ddl = 48 + (unsigned char)pbSrc[i] % 16;
		if (ddh > 57) ddh = ddh + 7;
		if (ddl > 57) ddl = ddl + 7;
		pbDest[i*2] = ddh;
		pbDest[i*2+1] = ddl;
	}

	//pbDest[nLen*2] = '\0';
}

//转十六进制数值
void StrToHex(char *pbDest, char *pbSrc, int nLen)
{
	unsigned char h1,h2;
	unsigned char s1,s2;
	int i;

	for (i=0; i<nLen; i++)
	{
		h1 = pbSrc[2*i];
		h2 = pbSrc[2*i+1];

		s1 = toupper(h1) - 0x30;
		if (s1 > 9) 
			s1 -= 7;

		s2 = toupper(h2) - 0x30;
		if (s2 > 9) 
			s2 -= 7;

		pbDest[i] = s1*16 + s2;
	}
}



//?????16?  
uint16_t tool_htons(uint16_t source)  
{  

	if(tool_ENDIANNESS == tool_BIG_ENDIAN)
		return source;
	else
		return (uint16_t)( 0
		| ((source & 0x00ff) << 8)
		| ((source & 0xff00) >> 8) );  
}  

//?????32?
uint32_t tool_htoni(uint32_t source)
{  
	if(tool_ENDIANNESS == tool_BIG_ENDIAN)
		return source;
	else
		return 0
		| ((source & 0x000000ff) << 24)
		| ((source & 0x0000ff00) << 8)
		| ((source & 0x00ff0000) >> 8)
		| ((source & 0xff000000) >> 24);  
}

//?????64?
uint64_t tool_htonl(uint64_t source)  
{  
	if(tool_ENDIANNESS == tool_BIG_ENDIAN)
		return source;
	else
		return 0
		| ((source & (uint64_t)(0x00000000000000ff)) << 56)
		| ((source & (uint64_t)(0x000000000000ff00)) << 40)
		| ((source & (uint64_t)(0x0000000000ff0000)) << 24)
		| ((source & (uint64_t)(0x00000000ff000000)) << 8)
		| ((source & (uint64_t)(0x000000ff00000000)) >> 8)
		| ((source & (uint64_t)(0x0000ff0000000000)) >> 24)
		| ((source & (uint64_t)(0x00ff000000000000)) >> 40)
		| ((source & (uint64_t)(0xff00000000000000)) >> 56);
}

//float
float tool_htonf(float source)  
{  
	if(tool_ENDIANNESS == tool_BIG_ENDIAN)
		return source;
	else
	{
		uint32_t t= 0
			| ((*(uint32_t*)&source & 0x000000ff) << 24)
			| ((*(uint32_t*)&source & 0x0000ff00) << 8)
			| ((*(uint32_t*)&source & 0x00ff0000) >> 8)
			| ((*(uint32_t*)&source & 0xff000000) >> 24);
		return *(float*)&t;
	} 
}

//double
double tool_htond(double source)  
{  
	if(tool_ENDIANNESS == tool_BIG_ENDIAN)
		return source;
	else
	{
		uint64_t t= 0
			| ((*(uint64_t*)&source & (uint64_t)(0x00000000000000ff)) << 56)
			| ((*(uint64_t*)&source & (uint64_t)(0x000000000000ff00)) << 40)
			| ((*(uint64_t*)&source & (uint64_t)(0x0000000000ff0000)) << 24)
			| ((*(uint64_t*)&source & (uint64_t)(0x00000000ff000000)) << 8)
			| ((*(uint64_t*)&source & (uint64_t)(0x000000ff00000000)) >> 8)
			| ((*(uint64_t*)&source & (uint64_t)(0x0000ff0000000000)) >> 24)
			| ((*(uint64_t*)&source & (uint64_t)(0x00ff000000000000)) >> 40)
			| ((*(uint64_t*)&source & (uint64_t)(0xff00000000000000)) >> 56);
		return *(double*)&t;
	}
}


const uint16_t  crc_table_2[ 256 ] =
{
   0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
		0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
		0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
		0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
		0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
		0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
		0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
		0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
		0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
		0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
		0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
		0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
		0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
		0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
		0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
		0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
		0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
		0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
		0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
		0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
		0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
		0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
		0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
		0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
		0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
		0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
		0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
		0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
		0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
		0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
		0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
		0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040,
} ;

static uint16_t do_crc(uint16_t reg_init, uint8_t  *message,  uint16_t  length)
{
    uint16_t crc_reg = reg_init;
    uint16_t  i;

    for (i = 0 ; i < length; i ++)
    {
        crc_reg = (crc_reg >> 8) ^ crc_table_2[(crc_reg ^ message[i]) & 0xff];

    }

    return crc_reg;
}
uint16_t get_crc16(uint8_t  *d,  uint16_t  len)
{
    uint16_t crc_reg = 0xffff;
    return do_crc(crc_reg, d, len);
}