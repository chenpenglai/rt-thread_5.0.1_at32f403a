/*
     
 */
#ifndef __BYTE_TOOLS_H__
#define __BYTE_TOOLS_H__


#include <rtthread.h>
#include <stdlib.h>
#include <string.h>


uint16_t tool_htons(uint16_t source);
uint16_t get_crc16(uint8_t  *d,  uint16_t  len);
void StrToHex(char *pbDest, char *pbSrc, int nLen);
unsigned char BcdToHex(unsigned char data);
unsigned char HexToBcd(unsigned char data);
void HexToStr(char *pbDest, char *pbSrc, int nLen);
unsigned long DecToBcd(long long  Dec, char *Bcd,int length);
int numPlaces (int n);
unsigned long BcdToDec(const char * bcd,int length);
uint32_t tool_htoni(uint32_t source)  ;
void flashback_2byte(uint16_t *array,int j);
uint8_t StrToBCD(char *dst, char *src, int srcLen);
#endif
