#include "func_tools.h"
#include <stdlib.h>    //用到了srand函数, 用来取伪随机数, 所以要有这个头文件
#include <stdio.h>
#include <stdint.h>
#include <string.h>
//#include "easyflash.h"
#include "stdlib.h"

uint16_t u8_2_u16(uint8_t *dat)             //拼接2个byte为word
{
    return (uint16_t)dat[0] << 8 | (uint16_t)dat[1];
}
uint32_t u8_2_u32(uint8_t *dat)             //拼接4个byte为word
{
    return ((uint32_t)dat[0] << 24) | ((uint32_t)dat[1] << 16) | ((uint32_t)dat[2] << 8) | ((uint32_t)dat[3]);
}
uint32_t u16_2_u32(uint16_t *dat)             //拼接2个word
{
   return (uint32_t)dat[0] <<16 | (uint32_t)dat[1];
}
uint64_t u16_2_u64(uint16_t *dat)             //拼接4个word
{
	return ((uint64_t)dat[0]<<(16*3))|((uint64_t)dat[1]<<(16*2))|((uint64_t)dat[2]<<(16*1))|((uint64_t)dat[3]<<(16*0));
}
uint8_t half_2_u8(uint8_t *dat)                 //拼接2个半字为byte
{
    return (uint8_t)dat[0] << 4 | (uint8_t)dat[1];
}
uint16_t get_crc(uint8_t *pack, uint16_t length)
{
    uint16_t crc_reg = 0xFFFF;
    uint16_t multi = 0xa001;
    //  uint16_t tmp = 0;
    int i, t;
    //rt_kprintf("len=%d\n", len);
    for (i = 0; i < length; i++)
    {
        crc_reg ^= ((uint16_t)pack[i]);
        for (t = 0; t < 8; t++)                         //重复右移8次
        {
            ((crc_reg & 0x01) == 0x01) ? (crc_reg = (crc_reg >> 1) ^ multi) : (crc_reg >>= 1);
        }
    }
    //  tmp = (crc_reg >> 8) & 0x00ff;
    //  return (crc_reg << 8) | tmp;        //结果 颠倒高8低8
    return crc_reg;
}

/*
例：
	68656c6c6f => hello
	616263646566676869676b6c6d6e6f707172737475767778797a30313233343536373839 => abcdefghigklmnopqrstuvwxyz0123456789
*/
void hex_to_ascii(uint8_t *in, uint16_t in_len, uint8_t *out)
{
//  rt_kprintf("转换前数据：%s\n", in);
    for (int i = 0; i < in_len * 2; i += 2)
    {
        out[i / 2] = ((((in[i] - 0x30) << 4) & 0xf0) | (in[i + 1] >= 0x41 ? ((in[i + 1] - 0x37) & 0x0f) : ((in[i + 1] - 0x30) & 0x0f)));
    }
}


int str_search(const char *src, char *dst, int num)         //在 src 查找 dst 第n次 出现的 首位置
{
    char *tmpsrc = (char *)src;
// int pos=0,len=strlen(src);
    while (num--)
    {
        tmpsrc = strstr(tmpsrc, dst);
        if (!tmpsrc)
            return -1;
        ++tmpsrc;
    }
    return (int)(tmpsrc - src) - 1;
}

int mem_search(uint8_t *array, int array_len, char dst, int num)        //在内存中查找查找 char 第n 次出现的 首位置
{
    uint8_t *tmp_mem = (uint8_t *)array;
    int offset = 0;
    while (num--)
    {
        tmp_mem = memchr(tmp_mem, dst, array_len);
        if (offset > array_len)     //找到末尾，直接退出
            return -1;
        ++tmp_mem;      //指针+1
        ++offset;
    }
    return (int)(tmp_mem - array) - 1;
}


int my_hex_to_str(uint8_t *sdata, uint8_t hdata)               //用于替代：srt_kprintf(sdata,"%02X",hdata)
{
    sdata[0] = ((hdata >> 4) <= 0x09) ? '0' + (hdata >> 4) : 'A' + (hdata >> 4) - 0x0a;
    sdata[1] = ((hdata & 0x0f) <= 0x09) ? '0' + (hdata & 0x0f) : 'A' + (hdata & 0x0f) - 0x0a;
    return 2;
}

int hexarr_to_strarr(uint8_t *sdata, uint8_t *hdata, uint16_t hlength)
{
    int i, offset = 0;
    for (i = 0; i < hlength; i++)
    {
        offset += my_hex_to_str(sdata + offset, hdata[i]);
    }
    return hlength * 2;
}

void hexdump(const char* note,uint8_t *buf, int len)
{
	rt_kprintf("\n%s\n",note);
    for (int i = 0; i < len; i++)
        rt_kprintf("%02X", buf[i]);
//		rt_kprintf("\nhex to ascii:\n");
//	   for (int i = 0; i < len; i++)
//       rt_kprintf("%c", buf[i]);
    rt_kprintf("\n%s\n\n",note);
    
}
void hexdump_16(const char* note,uint16_t *buf, int len)
{
	rt_kprintf("\n%s\n",note);
    for (int i = 0; i < len; i++)
        rt_kprintf("%04X ", buf[i]);
//		rt_kprintf("\nhex to ascii:\n");
//	   for (int i = 0; i < len; i++)
//       rt_kprintf("%c", buf[i]);
    rt_kprintf("\n%s\n\n",note);
    
}
void chardump(const char* note,uint8_t *buf, int len)
{
	rt_kprintf("\n%s\n",note);
    for (int i = 0; i < len; i++)
        rt_kprintf("%c", buf[i]);
//		rt_kprintf("\nhex to ascii:\n");
//	   for (int i = 0; i < len; i++)
//       rt_kprintf("%c", buf[i]);
    rt_kprintf("\n%s\n\n",note);
    
}
//  "3a" => 0x3a
void my_str_to_u8(uint8_t *str,uint8_t *hex){
    if(str[0]<='9')
        *hex=(str[0]-'0')<<4;
    else if(str[0]>='a'&&str[0]<='f')
        *hex=((str[0]-'a')+0x0a)<<4;
    else          //(str[0]>='A'&&str[0]<='F')
        *hex=((str[0]-'A')+0x0a)<<4;
    
    if(str[1]<='9')
        *hex|=str[1]-'0';
    else if(str[1]>='a'&&str[1]<='f')
        *hex|=((str[1]-'a')+0x0a);
    else          //(str[1]>='A'&&str[1]<='F')
        *hex|=((str[1]-'A')+0x0a);
    
}

int strarr_to_hexarr(uint8_t *sdata, uint8_t *hdata, uint16_t str_len)		//hdata 长度是 sdata的1/2 ，转换hexarr后长度缩短位原来一半
{
    for (int i = 0; i < str_len/2; i++)
        my_str_to_u8(&sdata[i*2],&hdata[i]);
			return str_len/2;
		
}
//  "3a22" => 0x3a22
void my_str_to_u16(uint8_t *str,uint16_t *hex){
	uint8_t h=0,l=0;
	my_str_to_u8(&str[0],&h);
	my_str_to_u8(&str[2],&l);
	*hex=(uint16_t)((h<<8)|l);
}
int strarr_to_u16(uint8_t *sdata, uint16_t *hdata, uint16_t str_len)		
{
    for (int i = 0; i < str_len/4; i++)
        my_str_to_u16(&sdata[i*4],&hdata[i]);
			
		return str_len/4;
}


void set_event(uint32_t * flag,uint32_t bit){
	(*flag)|=bit;
	
}
bool get_event(uint32_t * flag,uint32_t bit,uint32_t* e,EVENT_CTRL clear){
	*e=*flag;
	if(bit&(*flag)){
		if(clear==EVENT_CLEAR)
			*flag&=~bit;			//清除事件位
		
		return true;	
	}
	return false;
}
void clr_event(uint32_t * flag,uint32_t bit){
		*flag&=~bit;			//清除事件位
}

//static char env_buff[11]={0};

//void ef_set_env_int(char* key,int val){
//	srt_kprintf(env_buff,"%d",val);
//	ef_set_env(key,env_buff);
//}
//int ef_get_env_int(char* key){
//	return atoi(ef_get_env(key));
//}
//void ef_set_env_float(char* key,float val){
//	srt_kprintf(env_buff,"%.6f",val);
//	ef_set_env(key,env_buff);
//}
//float ef_get_env_float(char* key){
//	return atof(ef_get_env(key));
//}

//void ef_set_env_u32(char* key,uint32_t val){
//	
//	srt_kprintf(env_buff,"%d",val);
//	ef_set_env(key,env_buff);
//}
//uint32_t ef_get_env_u32(char* key){
//	return atol(ef_get_env(key));
//}
// 0x33 => "33"
//注意：要先HEX转成ASC再求校验和
//uint16_t hex_check_sum(uint8_t *data, uint16_t len)                 //hex直接生成校验
//{
//    uint16_t sum = 0;
//    uint8_t str_temp[2];
//    for (int i = 0; i < len; i++)
//    {
//        hex_to_str(str_temp, data[i]);
//        sum += str_temp[0];
//        sum += str_temp[1];
//    }
//    sum = ~sum + 1;
//    return sum;
//}

uint16_t str_check_sum(uint8_t *data, uint16_t len)                 //str生成校验
{
    uint16_t sum = 0;
    for (int i = 0; i < len; i++)
        sum += data[i];
    sum = ~sum + 1;
    return sum;
}
uint16_t cal_check_sum(uint8_t *data, int len)
{
    uint32_t sum=0;
    for (int i = 0; i < len; i++)
    {
        sum+=data[i];
    }
    return sum;
}
unsigned short crctab[256] = {
    0x0000,  0x1021,  0x2042,  0x3063,  0x4084,  0x50a5,  0x60c6,  0x70e7,
    0x8108,  0x9129,  0xa14a,  0xb16b,  0xc18c,  0xd1ad,  0xe1ce,  0xf1ef,
    0x1231,  0x0210,  0x3273,  0x2252,  0x52b5,  0x4294,  0x72f7,  0x62d6,
    0x9339,  0x8318,  0xb37b,  0xa35a,  0xd3bd,  0xc39c,  0xf3ff,  0xe3de,
    0x2462,  0x3443,  0x0420,  0x1401,  0x64e6,  0x74c7,  0x44a4,  0x5485,
    0xa56a,  0xb54b,  0x8528,  0x9509,  0xe5ee,  0xf5cf,  0xc5ac,  0xd58d,
    0x3653,  0x2672,  0x1611,  0x0630,  0x76d7,  0x66f6,  0x5695,  0x46b4,
    0xb75b,  0xa77a,  0x9719,  0x8738,  0xf7df,  0xe7fe,  0xd79d,  0xc7bc,
    0x48c4,  0x58e5,  0x6886,  0x78a7,  0x0840,  0x1861,  0x2802,  0x3823,
    0xc9cc,  0xd9ed,  0xe98e,  0xf9af,  0x8948,  0x9969,  0xa90a,  0xb92b,
    0x5af5,  0x4ad4,  0x7ab7,  0x6a96,  0x1a71,  0x0a50,  0x3a33,  0x2a12,
    0xdbfd,  0xcbdc,  0xfbbf,  0xeb9e,  0x9b79,  0x8b58,  0xbb3b,  0xab1a,
    0x6ca6,  0x7c87,  0x4ce4,  0x5cc5,  0x2c22,  0x3c03,  0x0c60,  0x1c41,
    0xedae,  0xfd8f,  0xcdec,  0xddcd,  0xad2a,  0xbd0b,  0x8d68,  0x9d49,
    0x7e97,  0x6eb6,  0x5ed5,  0x4ef4,  0x3e13,  0x2e32,  0x1e51,  0x0e70,
    0xff9f,  0xefbe,  0xdfdd,  0xcffc,  0xbf1b,  0xaf3a,  0x9f59,  0x8f78,
    0x9188,  0x81a9,  0xb1ca,  0xa1eb,  0xd10c,  0xc12d,  0xf14e,  0xe16f,
    0x1080,  0x00a1,  0x30c2,  0x20e3,  0x5004,  0x4025,  0x7046,  0x6067,
    0x83b9,  0x9398,  0xa3fb,  0xb3da,  0xc33d,  0xd31c,  0xe37f,  0xf35e,
    0x02b1,  0x1290,  0x22f3,  0x32d2,  0x4235,  0x5214,  0x6277,  0x7256,
    0xb5ea,  0xa5cb,  0x95a8,  0x8589,  0xf56e,  0xe54f,  0xd52c,  0xc50d,
    0x34e2,  0x24c3,  0x14a0,  0x0481,  0x7466,  0x6447,  0x5424,  0x4405,
    0xa7db,  0xb7fa,  0x8799,  0x97b8,  0xe75f,  0xf77e,  0xc71d,  0xd73c,
    0x26d3,  0x36f2,  0x0691,  0x16b0,  0x6657,  0x7676,  0x4615,  0x5634,
    0xd94c,  0xc96d,  0xf90e,  0xe92f,  0x99c8,  0x89e9,  0xb98a,  0xa9ab,
    0x5844,  0x4865,  0x7806,  0x6827,  0x18c0,  0x08e1,  0x3882,  0x28a3,
    0xcb7d,  0xdb5c,  0xeb3f,  0xfb1e,  0x8bf9,  0x9bd8,  0xabbb,  0xbb9a,
    0x4a75,  0x5a54,  0x6a37,  0x7a16,  0x0af1,  0x1ad0,  0x2ab3,  0x3a92,
    0xfd2e,  0xed0f,  0xdd6c,  0xcd4d,  0xbdaa,  0xad8b,  0x9de8,  0x8dc9,
    0x7c26,  0x6c07,  0x5c64,  0x4c45,  0x3ca2,  0x2c83,  0x1ce0,  0x0cc1,
    0xef1f,  0xff3e,  0xcf5d,  0xdf7c,  0xaf9b,  0xbfba,  0x8fd9,  0x9ff8,
    0x6e17,  0x7e36,  0x4e55,  0x5e74,  0x2e93,  0x3eb2,  0x0ed1,  0x1ef0
};

unsigned short updcrc_ccitt(char cp, unsigned short crc)
{
    return (unsigned short)((crc << 8) ^ crctab[((crc >> 8) ^ cp++) & 255]);
}
//==========================================================
//	函数名称：	CRC16//	函数功能：	计算CRC16校验码//	入口参数：	cp，数据首地址；leng，数据长度//	返回参数：	crc，计算结果
//	说明：		
//==========================================================
unsigned short CRC16_1(char* cp, unsigned short leng)
{
   // hexdump((uint8_t*)cp,leng);
    uint32_t i;
    uint16_t crc = 0;
    for (i = 0; i < leng; i++) {
        crc = updcrc_ccitt(cp[i], (unsigned short)crc);
    }
    return crc;
}


typedef struct CRC_CONFIG{
    uint32_t poly;//多项式 
    uint32_t init;//初值 
    uint32_t xorout;//结果异或值
    uint8_t refin;//输入翻转 
    uint8_t refout; //输出翻转 
    uint8_t wide; //位宽 
}CRC_CONFIG;
const CRC_CONFIG crc_configs[CRC_TYPE_MAX]=
{
    {0x31,0,0,1,1,8},
    {0x8005,0xFFFF,0,1,1,16},
    {0x04C11DB7,0xFFFFFFFF,0xFFFFFFFF,1,1,32},
};

uint32_t crc_invert(uint32_t in,uint8_t bn)
{
    int i;
    uint32_t tmp=0;
    for(i=0;i<bn;i++)
    {
        tmp<<=1;
        if(in&1)
            tmp|=1;
        in>>=1;
    }
    return tmp;
}
static uint32_t do_crc(CRC_TYPE crc_type,const uint8_t* pDataIn, uint16_t iLenIn,uint32_t init)  
{
    int i,j;
    uint8_t tmp;
    for (i = 0; i < iLenIn; i++)
    {
        tmp=*pDataIn++;  
        if(crc_configs[crc_type].refin)
        {
            tmp=crc_invert(tmp,8);
        }
        init = init ^ ((uint32_t)tmp<<(crc_configs[crc_type].wide-8)); 
        for( j = 0; j < 8; j++)
        {
            if(init & ((uint32_t)1<<(crc_configs[crc_type].wide-1))){
                init = (init << 1) ^ crc_configs[crc_type].poly;
            }else{
                init <<= 1;
            }
        } 
        
    }  
    if(crc_configs[crc_type].refout)
        init=crc_invert(init,crc_configs[crc_type].wide);
   
    
    return crc_configs[crc_type].xorout?crc_configs[crc_type].xorout^init:init;  
}

uint32_t get_crc_8(const uint8_t* pDataIn, uint16_t iLenIn)
{  
    return do_crc(CRC8_MAXIM,pDataIn,iLenIn,crc_configs[CRC8_MAXIM].init);  
}

//uint16_t get_random(uint16_t min,uint16_t max){
//	if(max <= min){
//		return 0;
//	}

//	uint16_t random = min+(MyRnd()%(max-min));
//	return random;
//}



uint32_t float_to_hex(float in) {
    uint32_t* out = (uint32_t*)&in;
    return *out;
}

float hex_to_float(uint32_t in) {
    float f;
    memcpy(&f, &in, 4);
    return f;
}
/*
	char buff[30] = "hello/aaa";
	strrpl(buff, "aaa", "bbb");
*/
char *strrpl(char *s, const char *old_str, const char *new_str)
{
	char *ptr;
	while (ptr = strstr(s, old_str)) /* 如果在s中找到s1 */
	{
		memmove(ptr + strlen(new_str) , ptr + strlen(old_str), strlen(ptr) - strlen(old_str) + 1);
		memcpy(ptr, &new_str[0], strlen(new_str));
	}
	return s;
}


void show_process(uint8_t process) {
		char process_buf[64];
		//int limit=50;
    process/=2;
    memset(process_buf, 0x00, sizeof(process_buf));
    process_buf[0] = '=';
    process_buf[1] = '>';        
     
		for(int i=1; i<=process; i++){
        process_buf[i] = '=';
		}
    process_buf[process+1] = '>';
		rt_kprintf("\rprocess:[%-50s]%d%% \n", process_buf, process*2);

    rt_kprintf("\n");  
		
}

//返回加密结果
uint32_t en(uint32_t *dat,uint16_t key){
	uint32_t result;
	result= dat[0] + dat[1] - (dat[2]/key);
	return result;
}
#define DELTA 0x9e3779b9  
#define MX (((z>>5^y<<2) + (y>>3^z<<4)) ^ ((sum^y) + (key[(p&3)^e] ^ z)))  
  
void btea(uint32_t *v, int n, uint32_t const key[4])  
{
    uint32_t y, z, sum;  
    unsigned p, rounds, e;  
    if (n > 1)            /* Coding Part */  
    {
        rounds = 6 + 52/n;  
        sum = 0;  
        z = v[n-1];  
        do  
        {  
            sum += DELTA;  
            e = (sum >> 2) & 3;  
            for (p=0; p<n-1; p++)  
            {  
                y = v[p+1];  
                z = v[p] += MX;  
            }  
            y = v[0];  
            z = v[n-1] += MX;  
        }  
        while (--rounds);  
    }  
    else if (n < -1)      /* Decoding Part */  
    {  
        n = -n;  
        rounds = 6 + 52/n;  
        sum = rounds*DELTA;  
        y = v[0];  
        do  
        {  
            e = (sum >> 2) & 3;  
            for (p=n-1; p>0; p--)  
            {  
                z = v[p-1];  
                y = v[p] -= MX;  
            }  
            z = v[n-1];  
            y = v[0] -= MX;  
            sum -= DELTA;  
        }  
        while (--rounds);  
    }  
}  
/*******************/

uint16_t other_crc(uint16_t C,uint8_t* bytes,int len){
	int POLYNOMIAL = 0x0000a001;

	int i, j;
	for (i = 0; i < len; i++) {
			C ^= (int) bytes[i];
			for (j = 0; j < 8; j++) {
					if ((C & 0x00000001) == 1) {
							C >>= 1;
							C ^= POLYNOMIAL;
					} else {
							C >>= 1;
					}
			}
	}

	//高低位转换，看情况使用（譬如本人这次对led彩屏的通讯开发就规定校验码高位在前低位在后，也就不需要转换高低位)
	//C = ( (C & 0x0000FF00) >> 8) | ( (C & 0x000000FF ) << 8);

	return C;
}

uint8_t get_checksum(uint8_t checksum, uint8_t* bytes, int len) {

	int i, j;
	for (i = 0; i < len; i++) {
		checksum += bytes[i];
		
	}

	return checksum;
}
