
/*
 * @Author: jiejie
 * @Github: https://github.com/jiejieTop
 * @LastEditTime: 2020-06-17 14:35:29
 * @Description: the code belongs to jiejie, please keep the author information and source code according to the license.
 */
#include <stdio.h>
#include <stdint.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "mqttclient.h"
#include "device_env.h"
//#include "net_protocol_mqtt.h"
#include <vconsole.h>

#define DBG_TAG              "ka_mqtt"
#define DBG_LVL              LOG_LVL_DBG
#include <rtdbg.h>

#include "net_app.h"
static struct netdev * cur_net_dev = RT_NULL;
static void netdev_find_high_prio(void)
{
	int reboot_count=0;
	int reboot_net = 0;
	cur_net_dev = netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP);
	if(RT_NULL == cur_net_dev)
	{
	//	netdev_close_all();
		rt_kprintf("\n打开所有网卡\n");
		netdev_open_all();
	}
	else
	{
		rt_kprintf("启动网卡 %s\n",cur_net_dev->name);
		if(netdev_is_up(cur_net_dev) == RT_FALSE)
		{
			netdev_set_up(cur_net_dev);
		}
		
	}
	while(RT_NULL == netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP))
	{
		reboot_count++;
		reboot_net++;
		if(reboot_net > 3600/3)
		{
			reboot_net = 0;
			netdev_close_all();
			rt_thread_delay(3000);
			netdev_open_all();
		}
		if(reboot_count > 3600*4/3)//4小时连不上网重启
		{
			
			rt_kprintf("<<<<< reconnect timeout 2 hour ,will reboot >>>>>\n");
			 rt_hw_cpu_reset();
		}
		rt_kprintf("\n没有可用网络 次数：%d\n",reboot_count);
		rt_thread_delay(3000);

	}
		reboot_count=0;
	//	if(ex_result!=0){
		cur_net_dev = netdev_get_first_by_flags(NETDEV_FLAG_LINK_UP);
//	}
	if(RT_NULL == cur_net_dev)
	{
	//	netdev_close_all();
		rt_kprintf("\n打开所有网卡\n");
		netdev_open_all();
	}
	else
	{
		rt_kprintf("启动网卡 %s\n",cur_net_dev->name);
		if(netdev_is_up(cur_net_dev) == RT_FALSE)
		{
			netdev_set_up(cur_net_dev);
		}
	} //196kcta
	
	rt_kprintf("网卡名字：%s\n",cur_net_dev->name);
	reboot_count=0;

	netdev_set_default(cur_net_dev);

}

static void close_cur_netdev(void)
{

	rt_kprintf("关闭当前网卡\n");

	if(cur_net_dev != RT_NULL)
	{
			netdev_set_down(cur_net_dev);
			netdev_low_level_set_link_status(cur_net_dev,RT_FALSE);
	}
}
void split_topic_by_id(char* out_buff,int out_buff_size,char*topic_prefix, char* topic,char* device_id) {
    memset(out_buff,0,out_buff_size);
    sprintf(out_buff,topic_prefix,device_id);
    strcat(out_buff,topic);
}
#define SUB_TOPIC_PREFIX 													"gateway/%s/"					//订阅的主题 接收数据
#define PUB_TOPIC_PREFIX 													"server/%s/"					//发布的主题 发送数据 
#define WILDCARD_TOPIC_SUFFIX 										"#"										//通配符订阅
#define REMOTE_CMD_TOPIC_SIFFIX 									"cmd"								//执行远程命令行
extern int msh_exec(char *cmd, rt_size_t length);
//mqtt_client_t *g_client;
mqtt_client_t *client = NULL;
static rt_device_t new_dev=RT_NULL;
static rt_device_t old_dev=RT_NULL;

char cmd_resp_buff[1024+512];


/* 数据输出回调函数 */ 
static rt_size_t cmd_outfunction(rt_device_t device, rt_uint8_t *buff, rt_size_t size)
{
		strcat(cmd_resp_buff,(char*)buff);
		
}
static rt_timer_t cmd_resp_timer = RT_NULL;

void cmd_resp_timer_cb(void *parameter) {
     /* 将 buff 输出到通信设备（例如以太网、CAN总线、串口） */
	  mqtt_message_t msg;
    memset(&msg, 0, sizeof(msg));

    msg.qos = QOS0;
    msg.payload = (void *)cmd_resp_buff;
		msg.payloadlen=strlen(cmd_resp_buff);
		char topic_buff[50];
		split_topic_by_id(topic_buff,sizeof(topic_buff),PUB_TOPIC_PREFIX,REMOTE_CMD_TOPIC_SIFFIX,DEVICE_ID);		//通配符订阅
		
    mqtt_publish(client, topic_buff, &msg);
		memset(cmd_resp_buff,0,sizeof(cmd_resp_buff));
		//rt_thread_mdelay(500);
		
			/* 切换至旧控制台 */
		vconsole_switch(old_dev);
			/* 删除设备对象 */
		vconsole_delete(new_dev);
		new_dev=RT_NULL;
		old_dev=RT_NULL;
		//rt_kprintf("cmd resp finish\n");
}
void cmd_resp_timer_start(void) {

    //rt_timer_control(keep_link_timer,RT_TIMER_CTRL_SET_TIME,&keeplink);
    rt_timer_start(cmd_resp_timer);
}
static void sub_topic_handle1(void* client, message_data_t* msg)
{
    (void) client;
//		g_client=client;
    LOG_I("<<<< topic: %s",msg->topic_name);
    rt_kprintf("message: %s\n\n",(char*)msg->message->payload);
		//protocol_transfer_check(msg->topic_name,msg->message->payload,msg->message->payloadlen);
		if(RT_NULL!=rt_strstr(msg->topic_name,REMOTE_CMD_TOPIC_SIFFIX)){
			if(new_dev==RT_NULL){
		//		/* 创建一个设备对象，传入数据输出函数指针 */
				new_dev = vconsole_create("mqtt_cmd", cmd_outfunction);
		//		/* 切换控制台，并保存旧设备指针 */
				//old_dev = vconsole_switch(new_dev);
				msh_exec(msg->message->payload,strlen(msg->message->payload));
				//vconsole_input(new_dev,"cmd_resp",strlen("cmd_resp"));
		
			
			}else {
				msh_exec(msg->message->payload,strlen(msg->message->payload));
				//vconsole_input(new_dev,"cmd_resp",strlen("cmd_resp"));
			}
			cmd_resp_timer_start();
			rt_kprintf("cmd resp timer start\n");
		}
}


static int mqtt_publish_handle1(mqtt_client_t *client)
{
    mqtt_message_t msg;
    memset(&msg, 0, sizeof(msg));

    msg.qos = QOS0;
    msg.payload = (void *)"this is a kawaii mqtt test ...";

    return mqtt_publish(client, "rtt-pub", &msg);
		
	
	
}

static void kawaii_mqtt(void *parameter)
{
        int timeout_ms=500;
    if(cmd_resp_timer==NULL) {
        cmd_resp_timer = rt_timer_create("cmd_resp",
                                          cmd_resp_timer_cb,
                                          RT_NULL,
                                          rt_tick_from_millisecond(timeout_ms),
                                          RT_TIMER_FLAG_ONE_SHOT);
    }
		char mqtt_host[40];
		char mqtt_port[10];
		char user_name[40];
		char password[40];
		char client_id[30];
		netdev_find_high_prio();
	
//    rt_thread_delay(6000);
    mqtt_log_init();

    client = mqtt_lease();

    //rt_snprintf(cid, sizeof(cid), "rtthread%d", rt_tick_get());
		//client->mqtt_password=get_env("password");
		strcpy(mqtt_host,get_env("remote_ip"));
    mqtt_set_host(client, mqtt_host);
	
		strcpy(mqtt_port,get_env("remote_port"));
    mqtt_set_port(client, mqtt_port);
	
		strcpy(user_name,get_env("username"));
    mqtt_set_user_name(client, user_name);
	
		strcpy(password,get_env("password"));
    mqtt_set_password(client,password);
	
		strcpy(client_id,get_env("client_id"));
    mqtt_set_client_id(client,  client_id);
		
    mqtt_set_clean_session(client, 1);

//    KAWAII_MQTT_LOG_I("The ID of the Kawaii client is: %s ",cid);

    mqtt_connect(client);
		
		char topic_buff[50];
		/************* 主题前缀 *************/			

		split_topic_by_id(topic_buff,sizeof(topic_buff),SUB_TOPIC_PREFIX,WILDCARD_TOPIC_SUFFIX,DEVICE_ID);		//通配符订阅

		//split_topic_by_id(buff,sizeof(buff),PUB_TOPIC_PREFIX,WILL_TOPIC_SUFFIX,DEVICE_ID);
							
    mqtt_subscribe(client, topic_buff, QOS0, sub_topic_handle1);
    //mqtt_subscribe(client, "gateway/110000000001/#", QOS0, sub_topic_handle1);
		//rt_kprintf("mqtt socket %d\n",client->mqtt_network->socket);
    while (1) {

			
			
        if(KAWAII_MQTT_SUCCESS_ERROR!=mqtt_publish_handle1(client)){
					
					log_e("mqtt publish err, end kawaii_demo\n");

					break;
				}
                               
        mqtt_sleep_ms(1000); 
    }
		//network_release(client->mqtt_network);
		mqtt_release(client);				//释放句柄空间
		
		close_cur_netdev();
		
}

int ka_mqtt(void)
{
    rt_thread_t tid_mqtt;

    tid_mqtt = rt_thread_create("kawaii", kawaii_mqtt, RT_NULL, 2048, 17, 10);
    if (tid_mqtt == RT_NULL) {
        return -RT_ERROR;
    }

    rt_thread_startup(tid_mqtt);

    return RT_EOK;
}
MSH_CMD_EXPORT(ka_mqtt, Kawaii MQTT client test program);


