/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-03-22     Murphy       the first version
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <rtthread.h>
#include <finsh.h>

#include "webclient.h"
#include <fal.h>

#define DBG_ENABLE
#define DBG_SECTION_NAME          "http_ota"
#ifdef OTA_DOWNLOADER_DEBUG
#define DBG_LEVEL                 DBG_LOG
#else
#define DBG_LEVEL                 DBG_INFO
#endif 
#define DBG_COLOR
#include <rtdbg.h>

#ifdef PKG_USING_HTTP_OTA

//#define HTTP_OTA_BUFF_LEN         4096*2
//#define GET_HEADER_BUFSZ          1024*2
//#define GET_RESP_BUFSZ            1024*2
#define HTTP_OTA_BUFF_LEN         1024
#define GET_HEADER_BUFSZ          512
#define GET_RESP_BUFSZ            512
#define HTTP_OTA_DL_DELAY         (120 * RT_TICK_PER_SECOND)

#define HTTP_OTA_URL              PKG_HTTP_OTA_URL
extern int g_ota_per;

static void print_progress(size_t cur_size, size_t total_size)
{
    static unsigned char progress_sign[100 + 1];
    uint8_t i, per = cur_size * 100 / total_size;

    if (per > 100)
    {
        per = 100;
    }

    for (i = 0; i < 100; i++)
    {
        if (i < per)
        {
            progress_sign[i] = '=';
        }
        else if (per == i)
        {
            progress_sign[i] = '>';
        }
        else
        {
            progress_sign[i] = ' ';
        }
    }

    progress_sign[sizeof(progress_sign) - 1] = '\0';
		g_ota_per = per;
    LOG_I("\033[2A");
    LOG_I("Download: [%s] %d%%", progress_sign, per);
}
uint8_t updating_flag=0;
static int http_ota_fw_download(const char* uri)
{
    int ret = 0, resp_status;
    int file_size = 0, length, total_length = 0;
    rt_uint8_t *buffer_read = RT_NULL;
    struct webclient_session* session = RT_NULL;
    const struct fal_partition * dl_part = RT_NULL;

    /* create webclient session and set header response size */
    session = webclient_session_create(GET_HEADER_BUFSZ);
    if (!session)
    {
        LOG_E("open uri failed.");
        ret = -RT_ERROR;
        goto __exit;
    }
		
    file_size = 400*1024;
    /* Get download partition information and erase download partition data */
    if ((dl_part = fal_partition_find("download")) == RT_NULL)
    {
        LOG_E("Firmware download failed! Partition (%s) find error!", "download");
        ret = -RT_ERROR;
        goto __exit;
    }

    LOG_I("Start erase flash (%s) partition!", dl_part->name);

    if (fal_partition_erase(dl_part, 0, file_size) < 0)
    {
        LOG_E("Firmware download failed! Partition (%s) erase error!", dl_part->name);
        ret = -RT_ERROR;
        goto __exit;
    }
    LOG_I("Erase flash (%s) partition success!", dl_part->name);

    buffer_read = web_malloc(HTTP_OTA_BUFF_LEN);
    if (buffer_read == RT_NULL)
    {
        LOG_E("No memory for http ota!");
        ret = -RT_ERROR;
        goto __exit;
    }
    memset(buffer_read, 0x00, HTTP_OTA_BUFF_LEN);

    LOG_I("OTA file size is (%d)", file_size);
    /* send GET request by default header */
    if ((resp_status = webclient_get(session, uri)) != 200)
    {
        LOG_E("webclient GET request failed, response(%d) error.", resp_status);
        ret = -RT_ERROR;
        goto __exit;
    }
		

		
    file_size = webclient_content_length_get(session);
    rt_kprintf("http file_size:%d\n",file_size);

    if (file_size == 0)
    {
        LOG_E("Request file size is 0!");
        ret = -RT_ERROR;
        goto __exit;
    }
    else if (file_size < 0)
    {
        LOG_E("webclient GET request type is chunked.");
        ret = -RT_ERROR;
        goto __exit;
    }


    do
    {
				updating_flag=1;
        length = webclient_read(session, buffer_read, file_size - total_length > HTTP_OTA_BUFF_LEN ?
                            HTTP_OTA_BUFF_LEN : file_size - total_length);   
        if (length > 0)
        {
            /* Write the data to the corresponding partition address */
            if (fal_partition_write(dl_part, total_length, buffer_read, length) < 0)
            {
                LOG_E("Firmware download failed! Partition (%s) write data error!", dl_part->name);
                ret = -RT_ERROR;
                goto __exit;
            }
            total_length += length;

            print_progress(total_length, file_size);
        }
        else
        {
            LOG_E("Exit: server return err (%d)!", length);
            ret = -RT_ERROR;
            goto __exit;
        }
    } while(total_length != file_size);

    ret = RT_EOK;

    if (total_length == file_size)
    {
        if (session != RT_NULL)
            webclient_close(session);
        if (buffer_read != RT_NULL)
            web_free(buffer_read);

        LOG_I("Download firmware to flash success.");
        LOG_I("System now will restart...");

				//TODO 给boot写标志位
					
        rt_thread_delay(rt_tick_from_millisecond(5));
				
        /* Reset the device, Start new firmware */
        extern void rt_hw_cpu_reset(void);
        rt_hw_cpu_reset();
    }

__exit:
    if (session != RT_NULL)
        webclient_close(session);
    if (buffer_read != RT_NULL)
        web_free(buffer_read);

    return ret;
}

#include "device_env.h"

#include <arpa/inet.h>
#include <netdev.h> 
static void ota_init(void *parameter)
{
	  rt_thread_t th1; 
		th1 = rt_thread_find("solarrtu");
	  if(th1 != NULL)
			rt_thread_delete(th1);
	  
		th1 = rt_thread_find("data_app");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("mdrtu");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("dht_tem");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("air720_net_init");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("air720_");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("bc26");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("motion_d");
		if(th1 != NULL)
			rt_thread_delete(th1);
		
		th1 = rt_thread_find("e0");
		if(th1 != NULL)
			rt_thread_delete(th1);
		//stop_data_app();

		rt_thread_mdelay(2000);
		struct netdev * curNetdev ;
		curNetdev	= netdev_get_by_name("W5500");
		if(netdev_is_link_up(curNetdev))
		{
				netdev_set_default(curNetdev);
				http_ota_fw_download(get_device_env("ota_url"));
				extern void rt_hw_cpu_reset(void);
				rt_hw_cpu_reset();
		}else
		{
			
		}
	  curNetdev = netdev_get_by_name("air720");
		if(netdev_is_link_up(curNetdev))
		{
				netdev_set_default(curNetdev);
		}
		else
		{
			  rt_kprintf("尝试重启网络升级");
			  netdev_close_all();
			  rt_thread_mdelay(3000);
			  netdev_open_all();
			  rt_thread_mdelay(180000);
				if(netdev_is_link_up(curNetdev))
				{
						netdev_set_default(curNetdev);
				}
				else
				{
						netdev_open_all();
				}
		}
		http_ota_fw_download(get_device_env("ota_url"));//http://115.159.144.115:83/iot-light-update/4g/V0.1_MRS.bin
	  extern void rt_hw_cpu_reset(void);
    rt_hw_cpu_reset(); 
}
rt_thread_t ota_thread = RT_NULL;
int http_ota(uint8_t argc, char **argv)
{
	
	  if(ota_thread!=RT_NULL){
			rt_kprintf(">>>> It's already in the process of upgrading...\n");
			return -1;
		
		}
		
    ota_thread = rt_thread_create("ota",
                                     ota_init,
                                     RT_NULL,
                                     4096,
                                     RT_THREAD_PRIORITY_MAX / 2,
                                     20);
    if (ota_thread != RT_NULL)
    {
			rt_kprintf(">>>> startup ota_thread\n");
        rt_thread_startup(ota_thread);
    }
return 0;

}
/**
 * msh />http_ota [url]
*/
MSH_CMD_EXPORT(http_ota, Use HTTP to download the firmware);
void start_http_ota()
{
	
	  rt_thread_t ota_thread;
    ota_thread = rt_thread_create("ota",
                                     ota_init,
                                     RT_NULL,
                                     4096,
                                     RT_THREAD_PRIORITY_MAX / 2,
                                     20);
    if (ota_thread != RT_NULL)
    {
        rt_thread_startup(ota_thread);
    }
}
#endif /* PKG_USING_HTTP_OTA */
