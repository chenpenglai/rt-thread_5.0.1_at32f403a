#ifndef __func_mqtt_h
#define __func_mqtt_h

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "MQTTPacket.h"

typedef int (*send_tcp_data_func)(int sock,unsigned char* data, int len);
typedef int (*recv_tcp_data_func)(int sock,unsigned char* data, int len);

uint8_t mqtt_login(int socket_id,char* client_id,char*username,char* password,char*will_topic,char*will_msg,send_tcp_data_func send_func);
uint8_t mqtt_login_read(int socket_id,recv_tcp_data_func recv_func);

uint8_t mqtt_set_topic(int socket_id,char* topic,send_tcp_data_func send_func);
uint8_t mqtt_set_topic_read(int socket_id,recv_tcp_data_func recv_func);

int mqtt_ping(uint8_t *buf, size_t size);
	
	
uint8_t mqtt_publish_package_slove(uint8_t *buf, size_t size,MQTTString *receivedTopic,unsigned char** payload_in,int *payloadlen_in);
uint8_t mqtt_pingresp_package_slove(uint8_t *buf, size_t size);
int mqtt_publish_package(char* topic,char* payload,int len,uint8_t* out_buff,int* out_len,int out_buff_size);

//ʹ�û���������
uint8_t mqtt_login_buff(char* client_id,char*username,char* password,char*will_topic,char*will_msg,uint8_t *out_buff,int buff_size,int*out_len);
uint8_t mqtt_login_read_buff(uint8_t *in_buff,int buff_size);
uint8_t mqtt_set_topic_buff(char* topic,uint8_t *out_buff,int buff_size,int*out_len);
uint8_t mqtt_set_topic_read_buff(uint8_t *in_buff,int buff_size);


#endif
