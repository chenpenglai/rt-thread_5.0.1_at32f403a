#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
//#include "user_ctrl.h"
#include "func_mqtt.h"
#include "global.h"
#define LOG_TAG "func_mqtt"
static int rc = 0;
//static int mysock = 0;
static int msgid = 1;
static int req_qos = 0;//QOS 无需响应

uint8_t mqtt_login(int socket_id,char* client_id,char*username,char* password,char*will_topic,char*will_msg,send_tcp_data_func send_func){
		rt_kprintf("mqtt_login\n");
		MQTTPacket_connectData connect_data = MQTTPacket_connectData_initializer;
		connect_data.clientID.cstring = (char*)client_id;//客户端ID
		connect_data.keepAliveInterval = 300;//设置心跳包间隔时间
		connect_data.cleansession = 1;//清除会话
		connect_data.username.cstring = (char*)username;//用户名
		connect_data.password.cstring = (char*)password;//密码
	
		/********			遗嘱设置		********/
		connect_data.will.topicName.cstring=will_topic;
		connect_data.will.message.cstring=will_msg;		
		connect_data.willFlag=1;	
		/******************************/
		uint8_t buff[100];
		int len = MQTTSerialize_connect(buff, sizeof(buff), &connect_data);		//现在将连接字符串格式化一下，现在还没有发送
		//rt_kprintf("%s",buf);//现在是数据的发送
		//rc = transport_sendPacketBuffer(mysock, esp_send_buff, len);
		//esp8266_send_tcp_data(socket_id, esp_send_buff, len);
		send_func(socket_id, buff, len);

}
uint8_t mqtt_login_buff(char* client_id,char*username,char* password,char*will_topic,char*will_msg,uint8_t *out_buff,int buff_size,int*out_len){
		rt_kprintf("mqtt_login_buff\n");
		MQTTPacket_connectData connect_data = MQTTPacket_connectData_initializer;
		connect_data.clientID.cstring = (char*)client_id;//客户端ID
		connect_data.keepAliveInterval = 300;//设置心跳包间隔时间
		connect_data.cleansession = 1;//清除会话
		connect_data.username.cstring = (char*)username;//用户名
		connect_data.password.cstring = (char*)password;//密码
	
		/********			遗嘱设置		********/
		connect_data.will.topicName.cstring=will_topic;
		connect_data.will.message.cstring=will_msg;		
		connect_data.willFlag=1;	
		/******************************/
		//uint8_t buff[100];
		int len = MQTTSerialize_connect(out_buff, buff_size, &connect_data);		//现在将连接字符串格式化一下，现在还没有发送
		*out_len=len;
		//rt_kprintf("%s",buf);//现在是数据的发送
		//rc = transport_sendPacketBuffer(mysock, esp_send_buff, len);
		//esp8266_send_tcp_data(socket_id, esp_send_buff, len);
		//send_func(socket_id, buff, len);

}

uint8_t mqtt_login_read(int socket_id,recv_tcp_data_func recv_func){
//		delay_ms(300);
	rt_kprintf("mqtt_login_read\n");
	uint8_t buff[100];
	recv_func(socket_id,buff,sizeof(buff));
//	if(recv_func(socket_id,buff,sizeof(buff)<=0)){
//		rt_kprintf("recv_func no data\n");
//		return DEV_ERROR;
//	
//	}
	
	MQTTHeader header = {0};
	header.byte = buff[0];					//放置头字节
	unsigned int packet_type;
	packet_type = header.bits.type;				//设包类型
	if(packet_type==CONNACK){
		unsigned char sessionPresent, connack_rc;
		if (MQTTDeserialize_connack(&sessionPresent, &connack_rc,buff, sizeof(buff)) != 1 || connack_rc != 0)
		{
				log_e("unable to connect login, return code %d\n", connack_rc);
//				is_connect=0;
				//p->step=APP_NETWORK_STEP_ERROR;
				return DEV_ERROR;
		}
		else
		{
				rt_kprintf("login ok\n");
//				is_connect=1;
				return DEV_EOK;
				//p->step=APP_NETWORK_STEP_MQTT_TOPIC;
			
		}
	}
	return DEV_ERROR;
}
uint8_t mqtt_login_read_buff(uint8_t *in_buff,int buff_size){
//		delay_ms(300);
	rt_kprintf("mqtt_login_read_buff\n");
//	recv_func(socket_id,buff,sizeof(buff));
//	if(recv_func(socket_id,buff,sizeof(buff)<=0)){
//		rt_kprintf("recv_func no data\n");
//		return DEV_ERROR;
//	
//	}
	
	MQTTHeader header = {0};
	header.byte = in_buff[0];					//放置头字节
	unsigned int packet_type;
	packet_type = header.bits.type;				//设包类型
	if(packet_type==CONNACK){
		unsigned char sessionPresent, connack_rc;
		if (MQTTDeserialize_connack(&sessionPresent, &connack_rc,in_buff,buff_size) != 1 || connack_rc != 0)
		{
				log_e("unable to connect login, return code %d\n", connack_rc);
//				is_connect=0;
				//p->step=APP_NETWORK_STEP_ERROR;
				return DEV_ERROR;
		}
		else
		{
				rt_kprintf("login ok\n");
//				is_connect=1;
				return DEV_EOK;
				//p->step=APP_NETWORK_STEP_MQTT_TOPIC;
			
		}
	}
	return DEV_ERROR;
}
uint8_t mqtt_set_topic(int socket_id,char* topic,send_tcp_data_func send_func){
		uint8_t buff[100];
		MQTTString topicString = MQTTString_initializer;
		topicString.cstring = topic;
		int len = MQTTSerialize_subscribe(buff, sizeof(buff), 0, msgid, 1, &topicString, &req_qos);
		
		rc = send_func(socket_id, buff, len);
		
	
}
uint8_t mqtt_set_topic_buff(char* topic,uint8_t *out_buff,int buff_size,int*out_len){
		//uint8_t buff[100];
		MQTTString topicString = MQTTString_initializer;
		topicString.cstring = topic;
		int len = MQTTSerialize_subscribe(out_buff, buff_size, 0, msgid, 1, &topicString, &req_qos);
		*out_len=len;
		//rc = send_func(socket_id, buff, len);
		
	
}
uint8_t mqtt_set_topic_read(int socket_id,recv_tcp_data_func recv_func){
//		delay_ms(300);
		//rt_kprintf("mqtt_topic_read\n");
//		if (MQTTPacket_read(esp_send_buff, ESP_SEND_BUFF_SIZE, esp8266_recv_tcp_data,socket_id) == SUBACK) 	/* 等待 suback */
//		{
		uint8_t buff[100];
		recv_func(socket_id,buff,sizeof(buff));
		
		MQTTHeader header = {0};
		header.byte = buff[0];					//放置头字节
		unsigned int packet_type;
		packet_type = header.bits.type;				//设包类型
		if(packet_type==SUBACK){
				
			unsigned short submsgid;
			int subcount;
			int granted_qos;

			rc = MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos, buff, sizeof(buff));
			if (granted_qos != 0)
			{
				rt_kprintf("granted qos != 0, %d\n", granted_qos);
				log_e("topic ERR\n");
				return DEV_ERROR;
				
			}
			else
			{
				rt_kprintf("topic ok\n");
				return DEV_EOK;
				//p->step=APP_NETWORK_STEP_RUN;
			}
		}
		else{
			log_e("packet_type not SUBACK\n");
			return DEV_ERROR;
		}
}
uint8_t mqtt_set_topic_read_buff(uint8_t *in_buff,int buff_size){
//		delay_ms(300);
		//rt_kprintf("mqtt_topic_read\n");
//		if (MQTTPacket_read(esp_send_buff, ESP_SEND_BUFF_SIZE, esp8266_recv_tcp_data,socket_id) == SUBACK) 	/* 等待 suback */
//		{
//		uint8_t buff[100];
//		recv_func(socket_id,buff,sizeof(buff));
		
		MQTTHeader header = {0};
		header.byte = in_buff[0];					//放置头字节
		unsigned int packet_type;
		packet_type = header.bits.type;				//设包类型
		if(packet_type==SUBACK){
				
			unsigned short submsgid;
			int subcount;
			int granted_qos;

			rc = MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos, in_buff, buff_size);
			if (granted_qos != 0)
			{
				rt_kprintf("granted qos != 0, %d\n", granted_qos);
				log_e("topic ERR\n");
				return DEV_ERROR;
				
			}
			else
			{
				rt_kprintf("topic ok\n");
				return DEV_EOK;
				//p->step=APP_NETWORK_STEP_RUN;
			}
		}
		else{
			log_e("packet_type not SUBACK\n");
			return DEV_ERROR;
		}
}

int mqtt_ping(uint8_t *buf, size_t size){

	return MQTTSerialize_pingreq(buf,size);
	
}


uint8_t mqtt_publish_package_slove(uint8_t *buf, size_t size,MQTTString *receivedTopic,unsigned char** payload_in,int *payloadlen_in){
	
	MQTTHeader header = {0};
	header.byte = buf[0];					//放置头字节
	unsigned int packet_type;
	packet_type = header.bits.type;				//设包类型
	if(packet_type==PUBLISH){
			unsigned char dup;
			int qos;
			unsigned char retained;
			unsigned short msgid;
//			int payloadlen_in;
//			unsigned char* payload_in;
			int rc;
//			MQTTString receivedTopic;

			if(1==(rc= MQTTDeserialize_publish(&dup, &qos, &retained, &msgid, receivedTopic,	payload_in, payloadlen_in, buf, size))){
				return DEV_EOK;
			}
	}
	else 
	{
		return DEV_ERROR;
	}
}

//接收数据解包
uint8_t mqtt_pingresp_package_slove(uint8_t *buf, size_t size){
	
	MQTTHeader header = {0};
	header.byte = buf[0];					//放置头字节
	unsigned int packet_type;
	packet_type = header.bits.type;				//设包类型
	if(packet_type==PINGRESP){

			return DEV_EOK;		//是心跳包应答
	}
	else 
	{
			return DEV_ERROR;
	}
}


int mqtt_publish_package(char* topic,char* payload,int len,uint8_t* out_buff,int* out_len,int out_buff_size)
{
	//chardump("---",topic,strlen(topic));
//	rt_kprintf(">>> topic: %.*s\n",strlen(topic),topic);
//	chardump(">>>",(uint8_t*)data,len);
	MQTTString topic_str= MQTTString_initializer;
	topic_str.cstring=topic;
	int n = MQTTSerialize_publish(out_buff, out_buff_size, 0, 0, 0, 0, topic_str, (unsigned char*)payload, len);
	if(n!=MQTTPACKET_BUFFER_TOO_SHORT){
		*out_len=n;
		return 0;
		//rc = tcp_send(g_eth_app_network.sock, g_eth_app_network.net_buff, n);
	}
	else{
		return -1;
		
	}
#if 0
	//qos 响应报文，当qos>0时才有响应
	if (MQTTPacket_read(g_eth_app_network.net_buff, NET_BUFF_SIZE, transport_getdata) == PUBLISH)
	{
		unsigned char dup;
		int qos;
		unsigned char retained;
		unsigned short msgid;
		int payloadlen_in;
		unsigned char* payload_in;
		int rc;
		MQTTString receivedTopic;
		
		rc = MQTTDeserialize_publish(&dup, &qos, &retained, &msgid, &receivedTopic,
				&payload_in, &payloadlen_in, g_eth_app_network.net_buff, NET_BUFF_SIZE);
		rt_kprintf("message arrived %.*s\n", payloadlen_in, payload_in); //消息到达
	}
#endif
	
	//is_resend 重发功能未添加
	
	return 0;
}








