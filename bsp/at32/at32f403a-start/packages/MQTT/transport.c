/*******************************************************************************
 * Copyright (c) 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Ian Craggs - initial API and implementation and/or initial documentation
 *    Sergio R. Caprile - "commonalization" from prior samples and/or documentation extension
 *******************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
//#include "user_ctrl.h"
#include "transport.h"
#include "global.h"
/**
This simple low-level implementation assumes a single connection for a single thread. Thus, a static
variable is used for that connection.
On other scenarios, the user must solve this by taking into account that the current implementation of
MQTTPacket_read() has a function pointer for a function call to get the data to a buffer, but no provisions
to know the caller or other indicator (the socket id): int (*getfn)(unsigned char*, int)
*/

//发送数据
int transport_sendPacketBuffer(int sock, unsigned char* buf, int buflen)
{
//	tcp_send_sock(sock,buf,buflen);
	//hexdump(">>>",buf,buflen);
	return 0;
}

/*
*   作用: 替代原来的transport_getdata函数，因为没有用到socket
*   说明：使用方式与原函数一致=recv();
*   参数说明：
*       USART2_RX_BUF[]是串口接收的缓存区
*       read_buf_len 串口是读到的实际字节数
*   返回值：非常重要！自行看源码吧
*/
//接收数据,recv函数仅仅是copy数据，真正的接收数据是协议来完成的）， recv函数返回其实际copy的字节数。
//uint8_t recv_buff[2][1024];				//用到2个sock 所以定义2个缓存区
//int recv_len[2];
//int already_read_len[2];
int transport_getdata(int sock,unsigned char* buf, int count)
{
//		/*
//			注意：因为 tcp_rd_data_sock 将读取接收的一包所有数据，
//						而transport_getdata需要指定读取的字节数count（而不是缓存区大小），
//						所以下面需要特殊处理
//		*/
//		int i=tcp_chkrec_sock(sock);
//		if(i>0){
//			recv_len[sock]=tcp_rd_data_sock(sock,recv_buff[sock],i);
//			already_read_len[sock]=0;
//			//hexdump("<<< all data",recv_buff,recv_len);
//		}
//		if(already_read_len[sock]>=recv_len[sock]){		//读完了 or 读超了
//				already_read_len[sock]=0;
//				recv_len[sock]=0;
//				return 0;
//		}
//		memcpy(buf,&recv_buff[sock][already_read_len[sock]],count);
//		already_read_len[sock]+=count;

//		//hexdump("<<<",buf,count);

//		return count;	
}

//打开网络并连接服务器
int transport_open(int sock,char* addr, uint16_t port)
{
//		//rt_kprintf("%s\n",__FUNCTION__);
//		char buff[50];
//		srt_kprintf(buff,"\"%s\",%d",addr,port);
//		tcp_connect_sock(sock,buff);
//		return 0;
}

//关闭网络
int transport_close(int sock)
{
//		//rt_kprintf("%s\n",__FUNCTION__);
//		tcp_disconnect_sock(sock);
//		return 0;
}
